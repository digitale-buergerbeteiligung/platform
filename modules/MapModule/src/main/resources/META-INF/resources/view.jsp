<%@ include file="/init.jsp" %>
<head>
<script>
    define._amd = define.amd;
    define.amd = false;
</script>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css" />
<script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"></script>

<!-- Load Esri Leaflet from CDN -->
<script src="https://unpkg.com/esri-leaflet@2.2.3/dist/esri-leaflet.js"></script> 							
<!-- Load Esri Leaflet Geocoder from CDN -->
<link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.2.13/dist/esri-leaflet-geocoder.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/vex-js/4.0.0/js/vex.combined.min.js"></script>
<script>vex.defaultOptions.className = 'vex-theme-default'</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/vex-js/4.0.0/css/vex.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/vex-js/4.0.0/css/vex-theme-default.css" />
<script>
    define.amd = define._amd;
    define.amd = false;
</script> 
</head>
<script src="<%=request.getContextPath()%>/esri-leaflet-geocoder.js" ></script>

<div id="map"></div>
<style>
<!--
#map {height: 600px;z-index:1;}
-->
</style>

<script type="text/javascript">
$(document).ready(function(){
	//init the map

    // Hessen configuration
    var map = L.map('map').setView(
        [ 50.7, 9.1 ], 8);
    map.options.minZoom = 8;
    map.options.maxZoom = 16;
    //----------------------

    /* // Kassel configuration
    /*var map = L.map('map').setView([51.312066, 9.492901],12);
	var southWest = L.latLng(51.18994825699286, 9.743328094482424),
	northEast = L.latLng(51.455778558471586, 9.244995117187502);
	var bounds = L.latLngBounds(southWest, northEast);
	map.setMaxBounds(bounds);
	map.on('drag', function() {
		map.panInsideBounds(bounds, { animate: false });
	});
	//----------------------*/
			
	var redIcon = L.icon({
			//iconUrl: 'https://www.weckdenherkulesindir.de/documents/20142/32730/marker-icon.png/f9a304c8-716b-9684-b7d4-a31318ba5e50?t=1544108384492',
			//shadowUrl: 'https://www.weckdenherkulesindir.de/documents/20142/32730/marker-shadow.png/20ac54bb-8339-1fea-659a-e473c5a18bb5?t=1544108384613',

            // Local instance
            iconUrl: '/documents/20142/31473/marker-icon.png',
            shadowUrl: '/documents/20142/31473/marker-shadow.png',
            //----------------------*/

            /*// Server instance
            iconUrl: '/documents/20142/32730/marker-icon.png',
            shadowUrl: '/documents/20142/32730/marker-shadow.png',
            //----------------------*/

			iconSize:      [25, 41], // size of the icon
			iconAnchor:    [12, 41], // point of the icon which will correspond to marker's location
			popupAnchor:   [0, -34], // point from which the popup should open relative to the iconAnchor
			tooltipAnchor: [16, -20],
			
			shadowSize:    [41, 41], // size of the shadow
			shadowAnchor:  [14, 41]  // the same for the shadow

		});
	
	var greyIcon = L.icon({
		//iconUrl: 'https://www.weckdenherkulesindir.de/documents/20142/32730/marker-gray.png/c4732086-fc0d-2c95-d581-e8c8d8635d08?t=1544544826763',
		//shadowUrl: 'https://www.weckdenherkulesindir.de/documents/20142/32730/marker-shadow.png/20ac54bb-8339-1fea-659a-e473c5a18bb5?t=1544108384613',

        // Local instance
        iconUrl: '/documents/20142/31473/marker-gray.png',
        shadowUrl: '/documents/20142/31473/marker-shadow.png',
        //----------------------*/

        /*// Server instance
        iconUrl: '/documents/20142/32730/marker-gray.png',
        shadowUrl: '/documents/20142/32730/marker-shadow.png',
        //----------------------*/

		iconSize:      [25, 41], // size of the icon
		iconAnchor:    [12, 41], // point of the icon which will correspond to marker's location
		popupAnchor:   [0, -34], // point from which the popup should open relative to the iconAnchor
		tooltipAnchor: [16, -20],
		
		shadowSize:    [41, 41], // size of the shadow
		shadowAnchor:  [14, 41]  // the same for the shadow

	});

    // Hessen configuration
    map.options.minZoom = 8;
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Copyright OpenStreetMap'
    }).addTo(map);
    //----------------------

	/* // Kassel configuration
	map.options.minZoom = 12;
	var wmsUrl = 'https://geoportal.kassel.de/arcgis/services/Service_Raster/Geoportal_WebMaps_WGS84/MapServer/WMSServer?';
	L.tileLayer.wms(wmsUrl,{
		layers:'1:9.028,1:18.056,1:36.112,1:72.224,1:144.448'
	}).addTo(map);
    //----------------------*/
	
	var searchControl = L.esri.Geocoding.geosearch().addTo(map);
	var adressMarker;
    var results = L.layerGroup().addTo(map);
    searchControl.on('results', function(data) {
        results.clearLayers();
        for (var i = data.results.length - 1; i >= 0; i--) {
            results.addLayer(adressMarker = L.circleMarker(data.results[i].latlng));
			adressMarker.on('click', function(){results.clearLayers();})
        }
    });
	//load markers from server
	var ideasJsonArrayOLD = JSON.parse('${oldIdeas}');
	var ideasJsonArrayCURRENT = JSON.parse('${currentIdeas}');
	
	console.log("OLD IDEAS =", ideasJsonArrayOLD.length);
	console.log("CURRENT IDEAS =", ideasJsonArrayCURRENT.length);
	
	if(ideasJsonArrayCURRENT.length > 0 || ideasJsonArrayOLD.length > 0){
		var markerGroup = new L.featureGroup();
		
		$.each(ideasJsonArrayCURRENT, function (index, value) {
						if(value.isVisibleOnMap){
						var latlng = L.latLng(value.latitude,value.longitude);
				        var loadedMarker = new L.Marker(latlng, {icon: redIcon, draggable:false});
				        markerGroup.addLayer(loadedMarker);
				        loadedMarker.dbId = value.id;
				        loadedMarker.bindTooltip(value.title).openTooltip();
				        loadedMarker.bindPopup(getMarkerPopUp(value.id,value.title,value.userName,value.pageUrl,value.rating));
				        map.addLayer(loadedMarker);
						}
				    });
		$.each(ideasJsonArrayOLD, function (index, value) {
			if(value.isVisibleOnMap){
				var latlng = L.latLng(value.latitude,value.longitude);
		        var loadedMarker = new L.Marker(latlng, {icon: greyIcon, draggable:false});
		        markerGroup.addLayer(loadedMarker);
		        loadedMarker.dbId = value.id;
		        loadedMarker.bindTooltip(value.title).openTooltip();
		        loadedMarker.bindPopup(getMarkerPopUp(value.id,value.title,value.userName,value.pageUrl,value.rating));
		        map.addLayer(loadedMarker);
			}
	    });
		map.options.maxZoom = 16;
		map.fitBounds(markerGroup.getBounds().pad(0.5));
	}else{
		map.setView([51.312066, 9.492901],12)
	}
	function getMarkerPopUp(id, title, username,link,votes){
		var votescount;
		if(votes == '') {
			votescount = [];
		} else {
			votescount = votes.split(",");			
		}		
		return "<h4>" + title + "</h4>" + "<p> Eingereicht von " + username + "</p>" + "<p> Stimmen: " + votescount.length + "</p><br>" + "<a href=" + link + ">Details</a>";
	}
});
</script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/cookie-bar/cookiebar-latest.min.js?"></script>