package JSPOverrides.portlet;

import JSPOverrides.constants.JSPOverridesPortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

/**
 * @author englmeier
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.een",
		"com.liferay.portlet.instanceable=false",
		"javax.portlet.display-name=JSPOverrides Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + JSPOverridesPortletKeys.JSPOverrides,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class JSPOverridesPortlet extends MVCPortlet {
}