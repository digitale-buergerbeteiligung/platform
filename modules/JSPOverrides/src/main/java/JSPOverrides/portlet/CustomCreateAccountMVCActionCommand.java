package JSPOverrides.portlet;


import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;


@Component(
	    property = {
	    	"javax.portlet.name = com_liferay_login_web_portlet_FastLoginPortlet",
	    	"javax.portlet.name = com_liferay_login_web_portlet_LoginPortlet",
	    	"mvc.command.name=/login/create_account",
	        "service.ranking:Integer=100"
	    },
	    service = MVCActionCommand.class)
public class CustomCreateAccountMVCActionCommand extends BaseMVCActionCommand{

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>> OVERRIDE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");

		mvcActionCommand.processAction(actionRequest, actionResponse);
	}


    @Reference(
            target = "(component.name=com.liferay.login.web.internal.portlet.action.CreateAccountMVCActionCommand)")

        protected MVCActionCommand mvcActionCommand;


}
