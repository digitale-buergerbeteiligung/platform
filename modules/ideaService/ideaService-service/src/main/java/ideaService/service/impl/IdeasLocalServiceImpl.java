/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetLinkConstants;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.portal.kernel.backgroundtask.BackgroundTaskManagerUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutFriendlyURL;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchEngineHelperUtil;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.generic.BooleanQueryImpl;
import com.liferay.portal.kernel.service.LayoutFriendlyURLLocalServiceUtil;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.PortalUtil;

import aQute.bnd.annotation.ProviderType;
import analysisService.service.TagDataLocalServiceUtil;
import ideaService.exception.NoSuchIdeasException;
import ideaService.exception.NoSuchPicturesException;
import ideaService.model.Ideas;
import ideaService.model.Pictures;
import ideaService.service.IdeasLocalServiceUtil;
import ideaService.service.PicturesLocalServiceUtil;
import ideaService.service.VideosLocalServiceUtil;
import ideaService.service.avconversion.VideoConverter;
import ideaService.service.base.IdeasLocalServiceBaseImpl;
import ideaService.service.persistence.IdeasUtil;
import ideaService.service.persistence.PicturesUtil;
import ideasService.service.enums.ReviewStatus;
import ideasService.service.enums.VideoExtensions;

/**
 * The implementation of the ideas local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link ideaService.service.IdeasLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IdeasLocalServiceBaseImpl
 * @see ideaService.service.IdeasLocalServiceUtil
 */
@ProviderType
public class IdeasLocalServiceImpl extends IdeasLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link ideaService.service.IdeasLocalServiceUtil} to access the ideas local service.
	 */

	private final String ENTRY_CLASS_NAME = "entryClassName";
	private final String ENTRY_CLASS_PK = "entryClassPK";

	public List<Ideas>getIdeasByUserRoleProjectId(String role, long userId, long projectId){
		ArrayList<Ideas> result = new ArrayList<Ideas>();
		for(Ideas i : IdeasUtil.findAll()){
			if(projectId ==0L || i.getProjectRef() == projectId){
				if((role.equals(RoleConstants.ADMINISTRATOR) || role.equals(RoleConstants.PORTAL_CONTENT_REVIEWER))
						&& !i.getReviewStatus().equals(ReviewStatus.SAVED.getReviewStatusDescription())){
					result.add(i);
				}
			else if(i.getUserId() == userId){
				result.add(i);
			}
		}
	}
		return result;
	}


	public List<String> getTagsForIdea(long ideasId){
		ArrayList<String> result = new ArrayList<String> ();
		try {
			Ideas idea = IdeasUtil.findByPrimaryKey(ideasId);
			String [] tagPrimaryKeys = idea.getTags().split(",");

			for(int i=0; i<tagPrimaryKeys.length; i++){
				long tagKey = Long.parseLong(tagPrimaryKeys[i].replaceAll("\\s+",""));
				if(tagKey == -1){
					return new ArrayList<String>();
				}
				result.add(TagDataLocalServiceUtil.getTagData(tagKey).getTag());
			}

		} catch (NumberFormatException | PortalException e) {
			//CASE no tags found
		}
		return result;
	}

	public void setIdeaReviewStatus(long ideasId, ReviewStatus status){
		try {
			Ideas i = IdeasUtil.findByPrimaryKey(ideasId);
			i.setReviewStatus(status.getReviewStatusDescription());
			if(status.equals(ReviewStatus.ACCEPTED)){
				//Makes the layout visible to everyone.
				Layout layout = LayoutLocalServiceUtil.getLayout(i.getLayoutRef());
				//Change this to true if the drop down is supposed to show the idea
				//layout.setHidden(false);
				layout.persist();
			}

			i.persist();
		} catch (NoSuchIdeasException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addPictureToExistingIdea(long ideasId, long pictureId, String pictureRef,int position){
		PicturesLocalServiceUtil.createNewPictureEntry(ideasId, pictureId, pictureRef,position);
	}

	public String getPictureUrlByIdeasRefAndPosition(long ideasRef, int position){
		try {
			return  PicturesUtil.findByPositionIdeasRef(ideasRef, position).getPictureUrl();
		} catch (NoSuchPicturesException e) {
			return "/";
		}
	}
	
	public long getPictureIdByIdeasRefAndPosition(long ideasRef, int position){
		try {
			return  PicturesUtil.findByPositionIdeasRef(ideasRef, position).getPictureId();
		} catch (NoSuchPicturesException e) {
			return 0;
		}
	}


	public void addVideoToExistingIdea(long ideasId, long videoId, String videoRef, ThemeDisplay themedisplay){
		try {
			Ideas i = IdeasUtil.findByPrimaryKey(ideasId);
			VideosLocalServiceUtil.createNewVideoEntry(ideasId, videoId, videoRef, VideoExtensions.ORIGINAL.getVideoExtensionDescription());

			startBackgroundVideoConversion(i,VideoExtensions.WEBM.getVideoExtensionDescription(), themedisplay);
			startBackgroundVideoConversion(i,VideoExtensions.OGV.getVideoExtensionDescription(), themedisplay);
			startBackgroundVideoConversion(i,VideoExtensions.MP4.getVideoExtensionDescription(), themedisplay);
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * This is to be replaced or removed as soon as the elevator pitch functionality (Video recording in the browser) has been implemented.
	 * @param ideasId
	 * @param pitch
	 */
	public void addElevatorPitchToExisitingIdea(long ideasId, String pitch){
		try {
			Ideas i = IdeasUtil.findByPrimaryKey(ideasId);
			i.setPitch(pitch);
			i.persist();
		} catch (NoSuchIdeasException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void startBackgroundVideoConversion(Ideas i, String convertTo, ThemeDisplay themeDisplay ) throws PortalException{
		Map<String, Serializable> taskContextMap = new HashMap<String, Serializable>();
		taskContextMap.put("ideasId",i.getPrimaryKey());
		taskContextMap.put("videoId",VideosLocalServiceUtil.getVideoByIdeaRefAndExtension(i.getPrimaryKey(), VideoExtensions.ORIGINAL).getFileRef());
		taskContextMap.put("videoRef",VideosLocalServiceUtil.getVideoUrlByIdeaRefAndExtension(i.getPrimaryKey(), VideoExtensions.ORIGINAL));
		taskContextMap.put("convertTo",convertTo);
		taskContextMap.put("portalUrl", themeDisplay.getPortalURL());
		taskContextMap.put("contextPath", themeDisplay.getPathContext());
		taskContextMap.put("gId", themeDisplay.getScopeGroupId());
		BackgroundTaskManagerUtil.addBackgroundTask(i.getUserId(),i.getGroupId(),"ConvertVideo",VideoConverter.class.getName(),
				 taskContextMap, new ServiceContext());
	}


	/**
	 * Gets all Ideas that reference layoutRef
	 */
	public Ideas getIdeasByLayoutIdRef(long layoutRef){
		return IdeasUtil.fetchByLayoutRef(layoutRef);
	}

	/**
	 * @param projectId the project id
	 * @return all Ideas with projectRef == projectId
	 */
	public List<Ideas> getAllIdeasForProject(long projectId){
		List<Ideas> allIdeas = IdeasUtil.findAll();
		ArrayList<Ideas> result = new ArrayList<Ideas>();
		for(Ideas i : allIdeas){
			if(i.getProjectRef() == projectId){
				result.add(i);
			}
		}
		return result;
	}


	public List<Ideas> getAllAccpetedIdeas(){
		List<Ideas> allIdeas = IdeasUtil.findAll();
		ArrayList<Ideas> result = new ArrayList<Ideas>();
		for(Ideas i : allIdeas){
			if(i.getReviewStatus().equals(ReviewStatus.ACCEPTED.getReviewStatusDescription())){
				result.add(i);
			}
		}
		return result;
	}

	@Indexable(type = IndexableType.DELETE)
	public void deletePictureById(long pictureId) {
		try {
			 PicturesLocalServiceUtil.deletePictures(pictureId);			
			 resourceLocalService.deleteResource(pictureId,
		                Pictures.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, pictureId);
			
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Indexable(type = IndexableType.DELETE)
	public Ideas deleteIdeasAndLayoutOnCascade(long id){
		try {
			Ideas idea = deleteIdeas(id);
			LayoutLocalServiceUtil.deleteLayout(idea.getLayoutRef());
			//Delete friendly url
			List<LayoutFriendlyURL> urls = LayoutFriendlyURLLocalServiceUtil
					.getLayoutFriendlyURLs(0, LayoutFriendlyURLLocalServiceUtil.getLayoutFriendlyURLsCount());
			for(LayoutFriendlyURL l : urls){
				if(idea.getPageUrl().equals(l.getFriendlyURL())){
					 LayoutFriendlyURLLocalServiceUtil.deleteLayoutFriendlyURL(l);
				}
			}
			//delete video file
			if(idea.getVideoFileRef() > 0){
				DLAppLocalServiceUtil.deleteFileEntry(idea.getVideoFileRef());
			}
			PicturesLocalServiceUtil.deleteAllPicturesForIdeaRef(id);
			VideosLocalServiceUtil.deleteAllVideosForIdeaRef(id);

			resourceLocalService.deleteResource(idea.getCompanyId(),
	                Ideas.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
	               idea.getPrimaryKey());

			return idea;
		} catch (PortalException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Creates a new Idea using the supplied parameters.
	 */
	public Ideas createIdeasWithAutomatedDbId(String title,long userId,long groupId,long category,String shortDescription,
			String description,double latitude, double longitude, boolean published, boolean showOnMap,
			int rating,long projectIdRef,String pageUrl,long layoutRef, long videoFileRef, String videoUrl, ReviewStatus reviewStatus ){
		int nextDbId = (int)CounterLocalServiceUtil.increment(Ideas.class.getName());
		Ideas nextIdea = IdeasLocalServiceUtil.createIdeas(nextDbId);
		nextIdea.setGroupId(groupId);
		nextIdea.setTitle(title);
		nextIdea.setCategory(category);
		nextIdea.setProjectRef(projectIdRef);
		nextIdea.setDescription(description);
		nextIdea.setShortdescription(shortDescription);
		nextIdea.setUserId(userId);
		nextIdea.setLatitude(latitude);
		nextIdea.setLongitude(longitude);
		nextIdea.setPublished(published);
		nextIdea.setIsVisibleOnMap(showOnMap);
		nextIdea.setLayoutRef(layoutRef);
		nextIdea.setPageUrl(pageUrl);
		nextIdea.setVideoFileRef(videoFileRef);
		nextIdea.setVideoUrl(videoUrl);
		nextIdea.setReviewStatus(reviewStatus.getReviewStatusDescription());

		try {
			User user = UserLocalServiceUtil.getUser(userId);
			nextIdea.setUserName(user.getScreenName());
			resourceLocalService.addResources(user.getCompanyId(), groupId, userId, Ideas.class.getName(), nextDbId, false, true, true);
		} catch (PortalException e) {
			e.printStackTrace();
		}



		return nextIdea;
	}

	/**
	 * This method is used to create a intermediate Idea with less information in the first step of the input process.
	 * @param userId
	 * @param groupId
	 * @param title
	 * @return
	 */
	public Ideas createIdeasWithAutomatedDbId(long userId,long projectRef, long groupId,long layoutRef, String title,String solution,  String description, String importance,
			String targetAudience, String tags, String goal,String pageUrl, double latitude, double longitude, ReviewStatus reviewStatus, boolean isVisibleOnMap){

		int nextDbId = (int)CounterLocalServiceUtil.increment(Ideas.class.getName());
		Ideas nextIdea = IdeasLocalServiceUtil.createIdeas(nextDbId);
		nextIdea.setUserId(userId);
		nextIdea.setProjectRef(projectRef);
		nextIdea.setGroupId(groupId);
		nextIdea.setPageUrl(pageUrl);
		nextIdea.setLayoutRef(layoutRef);
		nextIdea.setTitle(title);
		nextIdea.setDescription(description);
		nextIdea.setImportance(importance);
		nextIdea.setTargetAudience(targetAudience);
		nextIdea.setSolution(solution);
		nextIdea.setTags(tags);
		nextIdea.setGoal(goal);
		nextIdea.setPageUrl(pageUrl);
		nextIdea.setLatitude(latitude);
		nextIdea.setLongitude(longitude);
		nextIdea.setReviewStatus(reviewStatus.getReviewStatusDescription());
		nextIdea.setIsVisibleOnMap(isVisibleOnMap);

		try {
			User user = UserLocalServiceUtil.getUser(userId);
			nextIdea.setUserName(user.getScreenName());
			resourceLocalService.addResources(user.getCompanyId(), groupId, userId, Ideas.class.getName(), nextDbId, false, true, true);
		} catch (PortalException e) {
			e.printStackTrace();
		}
		return nextIdea;
	}



	/**
	 * persists the new Idea and performs some checks e.g. if the entry is a duplicate it won't be inserted.
	 * @param idea
	 */
	@Indexable(type = IndexableType.REINDEX)
	public void persistIdeasAndPerformTypeChecks(Ideas idea){
		idea.persist();
		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setScopeGroupId(idea.getGroupId());
		try {
				AssetEntry assetEntry= 	assetEntryLocalService.updateEntry(idea.getUserId(), idea.getGroupId(), idea.getCreateDate(), idea.getModifiedDate(),
					Ideas.class.getName(), idea.getIdeasId(),idea.getUuid(),0,serviceContext.getAssetCategoryIds(), serviceContext.getAssetTagNames(), true, true,
					null, null, null, null,ContentTypes.TEXT_HTML, idea.getTitle(), null, null,null,null, 0,0, null);

					assetLinkLocalService.updateLinks(idea.getUserId(), assetEntry.getEntryId(),
		                      serviceContext.getAssetLinkEntryIds(),
		                      AssetLinkConstants.TYPE_RELATED);

			} catch (PortalException e) {
					e.printStackTrace();
		}

	}

	/**
	 * Searches the DB forthe given idea with a field that contains the query.
	 * Only supports search within text fields.
	 * @param fieldName
	 * @param query
	 * @param groupIds
	 * @return a list of ideas with each containing the query.
	 */
	public List<Ideas> searchIdeasByField(String fieldName, String query, long [] groupIds){
		SearchContext searchContext  = new SearchContext();
		searchContext.setCompanyId(PortalUtil.getDefaultCompanyId());
		searchContext.setGroupIds(groupIds);
		BooleanQueryImpl booleanQueryImpl = new BooleanQueryImpl();
		booleanQueryImpl.addRequiredTerm(fieldName, query);
		return searchIdeas(searchContext ,booleanQueryImpl);
	}

	/**
	 * Searches the DB for the given idea with a field that contains the query.
	 * Only returns a result if the query is found in all given fields.
	 * Only supports search within text fields.
	 * @param fieldNames
	 * @param query
	 * @param groupIds
	 * @return a list of ideas with each containing the query.
	 */
	public List<Ideas> seachIdeasByFieldArray(String [] fieldNames, String query, long [] groupIds){
		SearchContext searchContext  = new SearchContext();
		searchContext.setCompanyId(PortalUtil.getDefaultCompanyId());
		searchContext.setGroupIds(groupIds);
		BooleanQueryImpl booleanQueryImpl = new BooleanQueryImpl();
		for(int i=0; i<fieldNames.length; i++){
		booleanQueryImpl.addRequiredTerm(fieldNames[i], query);
		}
		return searchIdeas(searchContext ,booleanQueryImpl);
	}

	/**
	 * Searches the DB for any field that contains the query.
	 * Only supports search within text fields.
	 * @param fieldNames
	 * @param query
	 * @param groupIds
	 * @return a list of ideas with each containing the query.
	 */
	public List<Ideas> searchIdeasByFieldArrayQueryArray(String [] fieldNames, String [] queries, long [] groupIds){
		SearchContext searchContext = new SearchContext();
		searchContext.setCompanyId(PortalUtil.getDefaultCompanyId());
		searchContext.setGroupIds(groupIds);
		BooleanQueryImpl booleanQueryImpl = new BooleanQueryImpl();
		for(int i=0; i<fieldNames.length; i++){
		booleanQueryImpl.addRequiredTerm(fieldNames[i], queries[i]);
		}
		return searchIdeas(searchContext ,booleanQueryImpl);
	}

	/**
	 *
	 * @param s searchContext
	 * @param booleanQueryImpl bql
	 * @return List with Ideas according to s
	 */
	private List<Ideas> searchIdeas(SearchContext s, BooleanQueryImpl booleanQueryImpl){
		ArrayList<Ideas> result = new ArrayList<Ideas>();
		Hits searchResults = null;
		try {
			searchResults=SearchEngineHelperUtil.getSearchEngine(SearchEngineHelperUtil.getDefaultSearchEngineId()).getIndexSearcher().search(s, booleanQueryImpl);
		} catch (SearchException e) {
			e.printStackTrace();
		}
		Document[] docs = searchResults.getDocs();
		for(int i=0; i<docs.length; i++){
			if(docs[i].get(ENTRY_CLASS_NAME).equals(Ideas.class.getName())){
				try {
					result.add(IdeasUtil.findByPrimaryKey(Long.parseLong(docs[i].get(ENTRY_CLASS_PK ))));
				} catch (NoSuchIdeasException | NumberFormatException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	/**
	 * Finds all ideas with Category cat.
	 * @param cat the desired cat
	 * @return a list with all ideas that have type equal to input param
	 */
	public List<Ideas> getIdeasByCategory(long catId){
		ArrayList<Ideas> result = new ArrayList<Ideas>();
		for(Ideas i: IdeasUtil.findAll()){
			if(i.getCategory()  ==catId){
				result.add(i);
			}
		}
		return result;
	}

	public int getIdeasRatingCount(long ideasId){
		try {
			Ideas i = IdeasLocalServiceUtil.getIdeas(ideasId);
			return ratingStringToList(i.getRating()).size();
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}

	public boolean removeUserFromRating(User u, long ideasId){
		try {
			Ideas i = IdeasLocalServiceUtil.getIdeas(ideasId);
			List<String> ratingNames = ratingStringToList(i.getRating());
			ratingNames.remove(u.getScreenName());
			if(ratingNames.isEmpty()){
				i.setRating(null);
			}
			else{
			i.setRating(ratingListToString(ratingNames));
			}
			i.persist();
			return true;
		} catch (PortalException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean addUserToRating(User u, long ideasId){
		try {
			Ideas i = IdeasLocalServiceUtil.getIdeas(ideasId);
			List<String> ratingNames = ratingStringToList(i.getRating());

			if(i.getRating().equals(null) || i.getRating().equals("") || i.getRating().equals(" ")){
				i.setRating(u.getScreenName());
				i.persist();
				return true;
			}
			else if(!ratingNames.contains(u.getScreenName())){
				ratingNames.add(u.getScreenName());
				i.setRating(ratingListToString(ratingNames));
				i.persist();
				return true;
			}

			return false;
		} catch (PortalException e) {
			e.printStackTrace();
			return false;
		}
	}

	public String ratingListToString(List<String> list){
		StringBuilder sb = new StringBuilder();
		for(String s : list){
			sb.append(s);
			sb.append(",");
		}
		return sb.toString().substring(0 , sb.toString().length() -1);
	}

	public List<String> ratingStringToList(String s){
		String [] splitArray = s.split(",");
		if(splitArray[0] == ""){
			return new ArrayList<String>();
		}
		ArrayList<String> result = new ArrayList<String> ();
		if(splitArray.length > 0)
		for(String i : splitArray){
			result.add(i);
		}
		return result;
	}

	/**
	 * gets all ideas with published true
	 */
	@Deprecated
	//TODO remove published from service xml
	public List<Ideas> getIdeasByIsPublished(boolean published){
		ArrayList<Ideas> result = new ArrayList<Ideas>();
		for(Ideas i: IdeasUtil.findAll()){
			if(i.getPublished() == published){
				result.add(i);
			}
		}
		return result;
	}
	/**
	 * gets all ideas that are visible on the map
	 */
	public List<Ideas> getIdeasByIsVisibleOnMap(boolean visible){
		ArrayList<Ideas> result = new ArrayList<Ideas>();
		for(Ideas i: IdeasUtil.findAll()){
			if(i.getPublished() ==  visible){
				result.add(i);
			}
		}
		return result;
	}

}



