/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import ideaService.exception.NoSuchVideosException;

import ideaService.model.Videos;

import ideaService.model.impl.VideosImpl;
import ideaService.model.impl.VideosModelImpl;

import ideaService.service.persistence.VideosPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the videos service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VideosPersistence
 * @see ideaService.service.persistence.VideosUtil
 * @generated
 */
@ProviderType
public class VideosPersistenceImpl extends BasePersistenceImpl<Videos>
	implements VideosPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link VideosUtil} to access the videos persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = VideosImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VideosModelImpl.ENTITY_CACHE_ENABLED,
			VideosModelImpl.FINDER_CACHE_ENABLED, VideosImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VideosModelImpl.ENTITY_CACHE_ENABLED,
			VideosModelImpl.FINDER_CACHE_ENABLED, VideosImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VideosModelImpl.ENTITY_CACHE_ENABLED,
			VideosModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEASREF = new FinderPath(VideosModelImpl.ENTITY_CACHE_ENABLED,
			VideosModelImpl.FINDER_CACHE_ENABLED, VideosImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByIdeasRef",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEASREF =
		new FinderPath(VideosModelImpl.ENTITY_CACHE_ENABLED,
			VideosModelImpl.FINDER_CACHE_ENABLED, VideosImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByIdeasRef",
			new String[] { Long.class.getName() },
			VideosModelImpl.IDEASREF_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEASREF = new FinderPath(VideosModelImpl.ENTITY_CACHE_ENABLED,
			VideosModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdeasRef",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the videoses where IdeasRef = &#63;.
	 *
	 * @param IdeasRef the ideas ref
	 * @return the matching videoses
	 */
	@Override
	public List<Videos> findByIdeasRef(long IdeasRef) {
		return findByIdeasRef(IdeasRef, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the videoses where IdeasRef = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param IdeasRef the ideas ref
	 * @param start the lower bound of the range of videoses
	 * @param end the upper bound of the range of videoses (not inclusive)
	 * @return the range of matching videoses
	 */
	@Override
	public List<Videos> findByIdeasRef(long IdeasRef, int start, int end) {
		return findByIdeasRef(IdeasRef, start, end, null);
	}

	/**
	 * Returns an ordered range of all the videoses where IdeasRef = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param IdeasRef the ideas ref
	 * @param start the lower bound of the range of videoses
	 * @param end the upper bound of the range of videoses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching videoses
	 */
	@Override
	public List<Videos> findByIdeasRef(long IdeasRef, int start, int end,
		OrderByComparator<Videos> orderByComparator) {
		return findByIdeasRef(IdeasRef, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the videoses where IdeasRef = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param IdeasRef the ideas ref
	 * @param start the lower bound of the range of videoses
	 * @param end the upper bound of the range of videoses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching videoses
	 */
	@Override
	public List<Videos> findByIdeasRef(long IdeasRef, int start, int end,
		OrderByComparator<Videos> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEASREF;
			finderArgs = new Object[] { IdeasRef };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEASREF;
			finderArgs = new Object[] { IdeasRef, start, end, orderByComparator };
		}

		List<Videos> list = null;

		if (retrieveFromCache) {
			list = (List<Videos>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Videos videos : list) {
					if ((IdeasRef != videos.getIdeasRef())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_VIDEOS_WHERE);

			query.append(_FINDER_COLUMN_IDEASREF_IDEASREF_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(VideosModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(IdeasRef);

				if (!pagination) {
					list = (List<Videos>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Videos>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first videos in the ordered set where IdeasRef = &#63;.
	 *
	 * @param IdeasRef the ideas ref
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching videos
	 * @throws NoSuchVideosException if a matching videos could not be found
	 */
	@Override
	public Videos findByIdeasRef_First(long IdeasRef,
		OrderByComparator<Videos> orderByComparator)
		throws NoSuchVideosException {
		Videos videos = fetchByIdeasRef_First(IdeasRef, orderByComparator);

		if (videos != null) {
			return videos;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("IdeasRef=");
		msg.append(IdeasRef);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchVideosException(msg.toString());
	}

	/**
	 * Returns the first videos in the ordered set where IdeasRef = &#63;.
	 *
	 * @param IdeasRef the ideas ref
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching videos, or <code>null</code> if a matching videos could not be found
	 */
	@Override
	public Videos fetchByIdeasRef_First(long IdeasRef,
		OrderByComparator<Videos> orderByComparator) {
		List<Videos> list = findByIdeasRef(IdeasRef, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last videos in the ordered set where IdeasRef = &#63;.
	 *
	 * @param IdeasRef the ideas ref
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching videos
	 * @throws NoSuchVideosException if a matching videos could not be found
	 */
	@Override
	public Videos findByIdeasRef_Last(long IdeasRef,
		OrderByComparator<Videos> orderByComparator)
		throws NoSuchVideosException {
		Videos videos = fetchByIdeasRef_Last(IdeasRef, orderByComparator);

		if (videos != null) {
			return videos;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("IdeasRef=");
		msg.append(IdeasRef);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchVideosException(msg.toString());
	}

	/**
	 * Returns the last videos in the ordered set where IdeasRef = &#63;.
	 *
	 * @param IdeasRef the ideas ref
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching videos, or <code>null</code> if a matching videos could not be found
	 */
	@Override
	public Videos fetchByIdeasRef_Last(long IdeasRef,
		OrderByComparator<Videos> orderByComparator) {
		int count = countByIdeasRef(IdeasRef);

		if (count == 0) {
			return null;
		}

		List<Videos> list = findByIdeasRef(IdeasRef, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the videoses before and after the current videos in the ordered set where IdeasRef = &#63;.
	 *
	 * @param VideoId the primary key of the current videos
	 * @param IdeasRef the ideas ref
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next videos
	 * @throws NoSuchVideosException if a videos with the primary key could not be found
	 */
	@Override
	public Videos[] findByIdeasRef_PrevAndNext(long VideoId, long IdeasRef,
		OrderByComparator<Videos> orderByComparator)
		throws NoSuchVideosException {
		Videos videos = findByPrimaryKey(VideoId);

		Session session = null;

		try {
			session = openSession();

			Videos[] array = new VideosImpl[3];

			array[0] = getByIdeasRef_PrevAndNext(session, videos, IdeasRef,
					orderByComparator, true);

			array[1] = videos;

			array[2] = getByIdeasRef_PrevAndNext(session, videos, IdeasRef,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Videos getByIdeasRef_PrevAndNext(Session session, Videos videos,
		long IdeasRef, OrderByComparator<Videos> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_VIDEOS_WHERE);

		query.append(_FINDER_COLUMN_IDEASREF_IDEASREF_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(VideosModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(IdeasRef);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(videos);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Videos> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the videoses where IdeasRef = &#63; from the database.
	 *
	 * @param IdeasRef the ideas ref
	 */
	@Override
	public void removeByIdeasRef(long IdeasRef) {
		for (Videos videos : findByIdeasRef(IdeasRef, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(videos);
		}
	}

	/**
	 * Returns the number of videoses where IdeasRef = &#63;.
	 *
	 * @param IdeasRef the ideas ref
	 * @return the number of matching videoses
	 */
	@Override
	public int countByIdeasRef(long IdeasRef) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEASREF;

		Object[] finderArgs = new Object[] { IdeasRef };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_VIDEOS_WHERE);

			query.append(_FINDER_COLUMN_IDEASREF_IDEASREF_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(IdeasRef);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEASREF_IDEASREF_2 = "videos.IdeasRef = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_EXTENSION = new FinderPath(VideosModelImpl.ENTITY_CACHE_ENABLED,
			VideosModelImpl.FINDER_CACHE_ENABLED, VideosImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByExtension",
			new String[] { String.class.getName() },
			VideosModelImpl.EXTENSION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_EXTENSION = new FinderPath(VideosModelImpl.ENTITY_CACHE_ENABLED,
			VideosModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByExtension",
			new String[] { String.class.getName() });

	/**
	 * Returns the videos where Extension = &#63; or throws a {@link NoSuchVideosException} if it could not be found.
	 *
	 * @param Extension the extension
	 * @return the matching videos
	 * @throws NoSuchVideosException if a matching videos could not be found
	 */
	@Override
	public Videos findByExtension(String Extension)
		throws NoSuchVideosException {
		Videos videos = fetchByExtension(Extension);

		if (videos == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("Extension=");
			msg.append(Extension);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchVideosException(msg.toString());
		}

		return videos;
	}

	/**
	 * Returns the videos where Extension = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param Extension the extension
	 * @return the matching videos, or <code>null</code> if a matching videos could not be found
	 */
	@Override
	public Videos fetchByExtension(String Extension) {
		return fetchByExtension(Extension, true);
	}

	/**
	 * Returns the videos where Extension = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param Extension the extension
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching videos, or <code>null</code> if a matching videos could not be found
	 */
	@Override
	public Videos fetchByExtension(String Extension, boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { Extension };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_EXTENSION,
					finderArgs, this);
		}

		if (result instanceof Videos) {
			Videos videos = (Videos)result;

			if (!Objects.equals(Extension, videos.getExtension())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_VIDEOS_WHERE);

			boolean bindExtension = false;

			if (Extension == null) {
				query.append(_FINDER_COLUMN_EXTENSION_EXTENSION_1);
			}
			else if (Extension.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_EXTENSION_EXTENSION_3);
			}
			else {
				bindExtension = true;

				query.append(_FINDER_COLUMN_EXTENSION_EXTENSION_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindExtension) {
					qPos.add(Extension);
				}

				List<Videos> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_EXTENSION,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"VideosPersistenceImpl.fetchByExtension(String, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					Videos videos = list.get(0);

					result = videos;

					cacheResult(videos);

					if ((videos.getExtension() == null) ||
							!videos.getExtension().equals(Extension)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_EXTENSION,
							finderArgs, videos);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_EXTENSION,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Videos)result;
		}
	}

	/**
	 * Removes the videos where Extension = &#63; from the database.
	 *
	 * @param Extension the extension
	 * @return the videos that was removed
	 */
	@Override
	public Videos removeByExtension(String Extension)
		throws NoSuchVideosException {
		Videos videos = findByExtension(Extension);

		return remove(videos);
	}

	/**
	 * Returns the number of videoses where Extension = &#63;.
	 *
	 * @param Extension the extension
	 * @return the number of matching videoses
	 */
	@Override
	public int countByExtension(String Extension) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_EXTENSION;

		Object[] finderArgs = new Object[] { Extension };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_VIDEOS_WHERE);

			boolean bindExtension = false;

			if (Extension == null) {
				query.append(_FINDER_COLUMN_EXTENSION_EXTENSION_1);
			}
			else if (Extension.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_EXTENSION_EXTENSION_3);
			}
			else {
				bindExtension = true;

				query.append(_FINDER_COLUMN_EXTENSION_EXTENSION_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindExtension) {
					qPos.add(Extension);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_EXTENSION_EXTENSION_1 = "videos.Extension IS NULL";
	private static final String _FINDER_COLUMN_EXTENSION_EXTENSION_2 = "videos.Extension = ?";
	private static final String _FINDER_COLUMN_EXTENSION_EXTENSION_3 = "(videos.Extension IS NULL OR videos.Extension = '')";

	public VideosPersistenceImpl() {
		setModelClass(Videos.class);
	}

	/**
	 * Caches the videos in the entity cache if it is enabled.
	 *
	 * @param videos the videos
	 */
	@Override
	public void cacheResult(Videos videos) {
		entityCache.putResult(VideosModelImpl.ENTITY_CACHE_ENABLED,
			VideosImpl.class, videos.getPrimaryKey(), videos);

		finderCache.putResult(FINDER_PATH_FETCH_BY_EXTENSION,
			new Object[] { videos.getExtension() }, videos);

		videos.resetOriginalValues();
	}

	/**
	 * Caches the videoses in the entity cache if it is enabled.
	 *
	 * @param videoses the videoses
	 */
	@Override
	public void cacheResult(List<Videos> videoses) {
		for (Videos videos : videoses) {
			if (entityCache.getResult(VideosModelImpl.ENTITY_CACHE_ENABLED,
						VideosImpl.class, videos.getPrimaryKey()) == null) {
				cacheResult(videos);
			}
			else {
				videos.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all videoses.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(VideosImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the videos.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Videos videos) {
		entityCache.removeResult(VideosModelImpl.ENTITY_CACHE_ENABLED,
			VideosImpl.class, videos.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((VideosModelImpl)videos, true);
	}

	@Override
	public void clearCache(List<Videos> videoses) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Videos videos : videoses) {
			entityCache.removeResult(VideosModelImpl.ENTITY_CACHE_ENABLED,
				VideosImpl.class, videos.getPrimaryKey());

			clearUniqueFindersCache((VideosModelImpl)videos, true);
		}
	}

	protected void cacheUniqueFindersCache(VideosModelImpl videosModelImpl) {
		Object[] args = new Object[] { videosModelImpl.getExtension() };

		finderCache.putResult(FINDER_PATH_COUNT_BY_EXTENSION, args,
			Long.valueOf(1), false);
		finderCache.putResult(FINDER_PATH_FETCH_BY_EXTENSION, args,
			videosModelImpl, false);
	}

	protected void clearUniqueFindersCache(VideosModelImpl videosModelImpl,
		boolean clearCurrent) {
		if (clearCurrent) {
			Object[] args = new Object[] { videosModelImpl.getExtension() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_EXTENSION, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_EXTENSION, args);
		}

		if ((videosModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_EXTENSION.getColumnBitmask()) != 0) {
			Object[] args = new Object[] { videosModelImpl.getOriginalExtension() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_EXTENSION, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_EXTENSION, args);
		}
	}

	/**
	 * Creates a new videos with the primary key. Does not add the videos to the database.
	 *
	 * @param VideoId the primary key for the new videos
	 * @return the new videos
	 */
	@Override
	public Videos create(long VideoId) {
		Videos videos = new VideosImpl();

		videos.setNew(true);
		videos.setPrimaryKey(VideoId);

		return videos;
	}

	/**
	 * Removes the videos with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param VideoId the primary key of the videos
	 * @return the videos that was removed
	 * @throws NoSuchVideosException if a videos with the primary key could not be found
	 */
	@Override
	public Videos remove(long VideoId) throws NoSuchVideosException {
		return remove((Serializable)VideoId);
	}

	/**
	 * Removes the videos with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the videos
	 * @return the videos that was removed
	 * @throws NoSuchVideosException if a videos with the primary key could not be found
	 */
	@Override
	public Videos remove(Serializable primaryKey) throws NoSuchVideosException {
		Session session = null;

		try {
			session = openSession();

			Videos videos = (Videos)session.get(VideosImpl.class, primaryKey);

			if (videos == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchVideosException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(videos);
		}
		catch (NoSuchVideosException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Videos removeImpl(Videos videos) {
		videos = toUnwrappedModel(videos);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(videos)) {
				videos = (Videos)session.get(VideosImpl.class,
						videos.getPrimaryKeyObj());
			}

			if (videos != null) {
				session.delete(videos);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (videos != null) {
			clearCache(videos);
		}

		return videos;
	}

	@Override
	public Videos updateImpl(Videos videos) {
		videos = toUnwrappedModel(videos);

		boolean isNew = videos.isNew();

		VideosModelImpl videosModelImpl = (VideosModelImpl)videos;

		Session session = null;

		try {
			session = openSession();

			if (videos.isNew()) {
				session.save(videos);

				videos.setNew(false);
			}
			else {
				videos = (Videos)session.merge(videos);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!VideosModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { videosModelImpl.getIdeasRef() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_IDEASREF, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEASREF,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((videosModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEASREF.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						videosModelImpl.getOriginalIdeasRef()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_IDEASREF, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEASREF,
					args);

				args = new Object[] { videosModelImpl.getIdeasRef() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_IDEASREF, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEASREF,
					args);
			}
		}

		entityCache.putResult(VideosModelImpl.ENTITY_CACHE_ENABLED,
			VideosImpl.class, videos.getPrimaryKey(), videos, false);

		clearUniqueFindersCache(videosModelImpl, false);
		cacheUniqueFindersCache(videosModelImpl);

		videos.resetOriginalValues();

		return videos;
	}

	protected Videos toUnwrappedModel(Videos videos) {
		if (videos instanceof VideosImpl) {
			return videos;
		}

		VideosImpl videosImpl = new VideosImpl();

		videosImpl.setNew(videos.isNew());
		videosImpl.setPrimaryKey(videos.getPrimaryKey());

		videosImpl.setVideoId(videos.getVideoId());
		videosImpl.setIdeasRef(videos.getIdeasRef());
		videosImpl.setFileRef(videos.getFileRef());
		videosImpl.setVideoUrl(videos.getVideoUrl());
		videosImpl.setExtension(videos.getExtension());

		return videosImpl;
	}

	/**
	 * Returns the videos with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the videos
	 * @return the videos
	 * @throws NoSuchVideosException if a videos with the primary key could not be found
	 */
	@Override
	public Videos findByPrimaryKey(Serializable primaryKey)
		throws NoSuchVideosException {
		Videos videos = fetchByPrimaryKey(primaryKey);

		if (videos == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchVideosException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return videos;
	}

	/**
	 * Returns the videos with the primary key or throws a {@link NoSuchVideosException} if it could not be found.
	 *
	 * @param VideoId the primary key of the videos
	 * @return the videos
	 * @throws NoSuchVideosException if a videos with the primary key could not be found
	 */
	@Override
	public Videos findByPrimaryKey(long VideoId) throws NoSuchVideosException {
		return findByPrimaryKey((Serializable)VideoId);
	}

	/**
	 * Returns the videos with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the videos
	 * @return the videos, or <code>null</code> if a videos with the primary key could not be found
	 */
	@Override
	public Videos fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(VideosModelImpl.ENTITY_CACHE_ENABLED,
				VideosImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Videos videos = (Videos)serializable;

		if (videos == null) {
			Session session = null;

			try {
				session = openSession();

				videos = (Videos)session.get(VideosImpl.class, primaryKey);

				if (videos != null) {
					cacheResult(videos);
				}
				else {
					entityCache.putResult(VideosModelImpl.ENTITY_CACHE_ENABLED,
						VideosImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(VideosModelImpl.ENTITY_CACHE_ENABLED,
					VideosImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return videos;
	}

	/**
	 * Returns the videos with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param VideoId the primary key of the videos
	 * @return the videos, or <code>null</code> if a videos with the primary key could not be found
	 */
	@Override
	public Videos fetchByPrimaryKey(long VideoId) {
		return fetchByPrimaryKey((Serializable)VideoId);
	}

	@Override
	public Map<Serializable, Videos> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Videos> map = new HashMap<Serializable, Videos>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Videos videos = fetchByPrimaryKey(primaryKey);

			if (videos != null) {
				map.put(primaryKey, videos);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(VideosModelImpl.ENTITY_CACHE_ENABLED,
					VideosImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Videos)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_VIDEOS_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Videos videos : (List<Videos>)q.list()) {
				map.put(videos.getPrimaryKeyObj(), videos);

				cacheResult(videos);

				uncachedPrimaryKeys.remove(videos.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(VideosModelImpl.ENTITY_CACHE_ENABLED,
					VideosImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the videoses.
	 *
	 * @return the videoses
	 */
	@Override
	public List<Videos> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the videoses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of videoses
	 * @param end the upper bound of the range of videoses (not inclusive)
	 * @return the range of videoses
	 */
	@Override
	public List<Videos> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the videoses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of videoses
	 * @param end the upper bound of the range of videoses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of videoses
	 */
	@Override
	public List<Videos> findAll(int start, int end,
		OrderByComparator<Videos> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the videoses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of videoses
	 * @param end the upper bound of the range of videoses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of videoses
	 */
	@Override
	public List<Videos> findAll(int start, int end,
		OrderByComparator<Videos> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Videos> list = null;

		if (retrieveFromCache) {
			list = (List<Videos>)finderCache.getResult(finderPath, finderArgs,
					this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_VIDEOS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_VIDEOS;

				if (pagination) {
					sql = sql.concat(VideosModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Videos>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Videos>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the videoses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Videos videos : findAll()) {
			remove(videos);
		}
	}

	/**
	 * Returns the number of videoses.
	 *
	 * @return the number of videoses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_VIDEOS);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return VideosModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the videos persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(VideosImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_VIDEOS = "SELECT videos FROM Videos videos";
	private static final String _SQL_SELECT_VIDEOS_WHERE_PKS_IN = "SELECT videos FROM Videos videos WHERE VideoId IN (";
	private static final String _SQL_SELECT_VIDEOS_WHERE = "SELECT videos FROM Videos videos WHERE ";
	private static final String _SQL_COUNT_VIDEOS = "SELECT COUNT(videos) FROM Videos videos";
	private static final String _SQL_COUNT_VIDEOS_WHERE = "SELECT COUNT(videos) FROM Videos videos WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "videos.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Videos exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Videos exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(VideosPersistenceImpl.class);
}