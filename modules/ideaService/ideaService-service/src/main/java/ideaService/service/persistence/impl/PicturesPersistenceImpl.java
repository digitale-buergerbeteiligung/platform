/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import ideaService.exception.NoSuchPicturesException;

import ideaService.model.Pictures;

import ideaService.model.impl.PicturesImpl;
import ideaService.model.impl.PicturesModelImpl;

import ideaService.service.persistence.PicturesPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the pictures service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PicturesPersistence
 * @see ideaService.service.persistence.PicturesUtil
 * @generated
 */
@ProviderType
public class PicturesPersistenceImpl extends BasePersistenceImpl<Pictures>
	implements PicturesPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link PicturesUtil} to access the pictures persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = PicturesImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PicturesModelImpl.ENTITY_CACHE_ENABLED,
			PicturesModelImpl.FINDER_CACHE_ENABLED, PicturesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PicturesModelImpl.ENTITY_CACHE_ENABLED,
			PicturesModelImpl.FINDER_CACHE_ENABLED, PicturesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PicturesModelImpl.ENTITY_CACHE_ENABLED,
			PicturesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEASREF = new FinderPath(PicturesModelImpl.ENTITY_CACHE_ENABLED,
			PicturesModelImpl.FINDER_CACHE_ENABLED, PicturesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByIdeasRef",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEASREF =
		new FinderPath(PicturesModelImpl.ENTITY_CACHE_ENABLED,
			PicturesModelImpl.FINDER_CACHE_ENABLED, PicturesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByIdeasRef",
			new String[] { Long.class.getName() },
			PicturesModelImpl.IDEASREF_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEASREF = new FinderPath(PicturesModelImpl.ENTITY_CACHE_ENABLED,
			PicturesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdeasRef",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the pictureses where IdeasRef = &#63;.
	 *
	 * @param IdeasRef the ideas ref
	 * @return the matching pictureses
	 */
	@Override
	public List<Pictures> findByIdeasRef(long IdeasRef) {
		return findByIdeasRef(IdeasRef, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the pictureses where IdeasRef = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param IdeasRef the ideas ref
	 * @param start the lower bound of the range of pictureses
	 * @param end the upper bound of the range of pictureses (not inclusive)
	 * @return the range of matching pictureses
	 */
	@Override
	public List<Pictures> findByIdeasRef(long IdeasRef, int start, int end) {
		return findByIdeasRef(IdeasRef, start, end, null);
	}

	/**
	 * Returns an ordered range of all the pictureses where IdeasRef = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param IdeasRef the ideas ref
	 * @param start the lower bound of the range of pictureses
	 * @param end the upper bound of the range of pictureses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching pictureses
	 */
	@Override
	public List<Pictures> findByIdeasRef(long IdeasRef, int start, int end,
		OrderByComparator<Pictures> orderByComparator) {
		return findByIdeasRef(IdeasRef, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the pictureses where IdeasRef = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param IdeasRef the ideas ref
	 * @param start the lower bound of the range of pictureses
	 * @param end the upper bound of the range of pictureses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching pictureses
	 */
	@Override
	public List<Pictures> findByIdeasRef(long IdeasRef, int start, int end,
		OrderByComparator<Pictures> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEASREF;
			finderArgs = new Object[] { IdeasRef };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEASREF;
			finderArgs = new Object[] { IdeasRef, start, end, orderByComparator };
		}

		List<Pictures> list = null;

		if (retrieveFromCache) {
			list = (List<Pictures>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Pictures pictures : list) {
					if ((IdeasRef != pictures.getIdeasRef())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PICTURES_WHERE);

			query.append(_FINDER_COLUMN_IDEASREF_IDEASREF_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PicturesModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(IdeasRef);

				if (!pagination) {
					list = (List<Pictures>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Pictures>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first pictures in the ordered set where IdeasRef = &#63;.
	 *
	 * @param IdeasRef the ideas ref
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching pictures
	 * @throws NoSuchPicturesException if a matching pictures could not be found
	 */
	@Override
	public Pictures findByIdeasRef_First(long IdeasRef,
		OrderByComparator<Pictures> orderByComparator)
		throws NoSuchPicturesException {
		Pictures pictures = fetchByIdeasRef_First(IdeasRef, orderByComparator);

		if (pictures != null) {
			return pictures;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("IdeasRef=");
		msg.append(IdeasRef);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPicturesException(msg.toString());
	}

	/**
	 * Returns the first pictures in the ordered set where IdeasRef = &#63;.
	 *
	 * @param IdeasRef the ideas ref
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching pictures, or <code>null</code> if a matching pictures could not be found
	 */
	@Override
	public Pictures fetchByIdeasRef_First(long IdeasRef,
		OrderByComparator<Pictures> orderByComparator) {
		List<Pictures> list = findByIdeasRef(IdeasRef, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last pictures in the ordered set where IdeasRef = &#63;.
	 *
	 * @param IdeasRef the ideas ref
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching pictures
	 * @throws NoSuchPicturesException if a matching pictures could not be found
	 */
	@Override
	public Pictures findByIdeasRef_Last(long IdeasRef,
		OrderByComparator<Pictures> orderByComparator)
		throws NoSuchPicturesException {
		Pictures pictures = fetchByIdeasRef_Last(IdeasRef, orderByComparator);

		if (pictures != null) {
			return pictures;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("IdeasRef=");
		msg.append(IdeasRef);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPicturesException(msg.toString());
	}

	/**
	 * Returns the last pictures in the ordered set where IdeasRef = &#63;.
	 *
	 * @param IdeasRef the ideas ref
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching pictures, or <code>null</code> if a matching pictures could not be found
	 */
	@Override
	public Pictures fetchByIdeasRef_Last(long IdeasRef,
		OrderByComparator<Pictures> orderByComparator) {
		int count = countByIdeasRef(IdeasRef);

		if (count == 0) {
			return null;
		}

		List<Pictures> list = findByIdeasRef(IdeasRef, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the pictureses before and after the current pictures in the ordered set where IdeasRef = &#63;.
	 *
	 * @param PictureId the primary key of the current pictures
	 * @param IdeasRef the ideas ref
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next pictures
	 * @throws NoSuchPicturesException if a pictures with the primary key could not be found
	 */
	@Override
	public Pictures[] findByIdeasRef_PrevAndNext(long PictureId, long IdeasRef,
		OrderByComparator<Pictures> orderByComparator)
		throws NoSuchPicturesException {
		Pictures pictures = findByPrimaryKey(PictureId);

		Session session = null;

		try {
			session = openSession();

			Pictures[] array = new PicturesImpl[3];

			array[0] = getByIdeasRef_PrevAndNext(session, pictures, IdeasRef,
					orderByComparator, true);

			array[1] = pictures;

			array[2] = getByIdeasRef_PrevAndNext(session, pictures, IdeasRef,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Pictures getByIdeasRef_PrevAndNext(Session session,
		Pictures pictures, long IdeasRef,
		OrderByComparator<Pictures> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PICTURES_WHERE);

		query.append(_FINDER_COLUMN_IDEASREF_IDEASREF_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(PicturesModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(IdeasRef);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(pictures);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Pictures> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the pictureses where IdeasRef = &#63; from the database.
	 *
	 * @param IdeasRef the ideas ref
	 */
	@Override
	public void removeByIdeasRef(long IdeasRef) {
		for (Pictures pictures : findByIdeasRef(IdeasRef, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(pictures);
		}
	}

	/**
	 * Returns the number of pictureses where IdeasRef = &#63;.
	 *
	 * @param IdeasRef the ideas ref
	 * @return the number of matching pictureses
	 */
	@Override
	public int countByIdeasRef(long IdeasRef) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEASREF;

		Object[] finderArgs = new Object[] { IdeasRef };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PICTURES_WHERE);

			query.append(_FINDER_COLUMN_IDEASREF_IDEASREF_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(IdeasRef);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEASREF_IDEASREF_2 = "pictures.IdeasRef = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_POSITIONIDEASREF = new FinderPath(PicturesModelImpl.ENTITY_CACHE_ENABLED,
			PicturesModelImpl.FINDER_CACHE_ENABLED, PicturesImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByPositionIdeasRef",
			new String[] { Long.class.getName(), Integer.class.getName() },
			PicturesModelImpl.IDEASREF_COLUMN_BITMASK |
			PicturesModelImpl.POSITION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_POSITIONIDEASREF = new FinderPath(PicturesModelImpl.ENTITY_CACHE_ENABLED,
			PicturesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByPositionIdeasRef",
			new String[] { Long.class.getName(), Integer.class.getName() });

	/**
	 * Returns the pictures where IdeasRef = &#63; and Position = &#63; or throws a {@link NoSuchPicturesException} if it could not be found.
	 *
	 * @param IdeasRef the ideas ref
	 * @param Position the position
	 * @return the matching pictures
	 * @throws NoSuchPicturesException if a matching pictures could not be found
	 */
	@Override
	public Pictures findByPositionIdeasRef(long IdeasRef, int Position)
		throws NoSuchPicturesException {
		Pictures pictures = fetchByPositionIdeasRef(IdeasRef, Position);

		if (pictures == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("IdeasRef=");
			msg.append(IdeasRef);

			msg.append(", Position=");
			msg.append(Position);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchPicturesException(msg.toString());
		}

		return pictures;
	}

	/**
	 * Returns the pictures where IdeasRef = &#63; and Position = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param IdeasRef the ideas ref
	 * @param Position the position
	 * @return the matching pictures, or <code>null</code> if a matching pictures could not be found
	 */
	@Override
	public Pictures fetchByPositionIdeasRef(long IdeasRef, int Position) {
		return fetchByPositionIdeasRef(IdeasRef, Position, true);
	}

	/**
	 * Returns the pictures where IdeasRef = &#63; and Position = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param IdeasRef the ideas ref
	 * @param Position the position
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching pictures, or <code>null</code> if a matching pictures could not be found
	 */
	@Override
	public Pictures fetchByPositionIdeasRef(long IdeasRef, int Position,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { IdeasRef, Position };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_POSITIONIDEASREF,
					finderArgs, this);
		}

		if (result instanceof Pictures) {
			Pictures pictures = (Pictures)result;

			if ((IdeasRef != pictures.getIdeasRef()) ||
					(Position != pictures.getPosition())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_PICTURES_WHERE);

			query.append(_FINDER_COLUMN_POSITIONIDEASREF_IDEASREF_2);

			query.append(_FINDER_COLUMN_POSITIONIDEASREF_POSITION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(IdeasRef);

				qPos.add(Position);

				List<Pictures> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_POSITIONIDEASREF,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"PicturesPersistenceImpl.fetchByPositionIdeasRef(long, int, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					Pictures pictures = list.get(0);

					result = pictures;

					cacheResult(pictures);

					if ((pictures.getIdeasRef() != IdeasRef) ||
							(pictures.getPosition() != Position)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_POSITIONIDEASREF,
							finderArgs, pictures);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_POSITIONIDEASREF,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Pictures)result;
		}
	}

	/**
	 * Removes the pictures where IdeasRef = &#63; and Position = &#63; from the database.
	 *
	 * @param IdeasRef the ideas ref
	 * @param Position the position
	 * @return the pictures that was removed
	 */
	@Override
	public Pictures removeByPositionIdeasRef(long IdeasRef, int Position)
		throws NoSuchPicturesException {
		Pictures pictures = findByPositionIdeasRef(IdeasRef, Position);

		return remove(pictures);
	}

	/**
	 * Returns the number of pictureses where IdeasRef = &#63; and Position = &#63;.
	 *
	 * @param IdeasRef the ideas ref
	 * @param Position the position
	 * @return the number of matching pictureses
	 */
	@Override
	public int countByPositionIdeasRef(long IdeasRef, int Position) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_POSITIONIDEASREF;

		Object[] finderArgs = new Object[] { IdeasRef, Position };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PICTURES_WHERE);

			query.append(_FINDER_COLUMN_POSITIONIDEASREF_IDEASREF_2);

			query.append(_FINDER_COLUMN_POSITIONIDEASREF_POSITION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(IdeasRef);

				qPos.add(Position);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_POSITIONIDEASREF_IDEASREF_2 = "pictures.IdeasRef = ? AND ";
	private static final String _FINDER_COLUMN_POSITIONIDEASREF_POSITION_2 = "pictures.Position = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PICTURESID =
		new FinderPath(PicturesModelImpl.ENTITY_CACHE_ENABLED,
			PicturesModelImpl.FINDER_CACHE_ENABLED, PicturesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByPicturesId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PICTURESID =
		new FinderPath(PicturesModelImpl.ENTITY_CACHE_ENABLED,
			PicturesModelImpl.FINDER_CACHE_ENABLED, PicturesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByPicturesId",
			new String[] { Long.class.getName() },
			PicturesModelImpl.PICTUREID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PICTURESID = new FinderPath(PicturesModelImpl.ENTITY_CACHE_ENABLED,
			PicturesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPicturesId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the pictureses where PictureId = &#63;.
	 *
	 * @param PictureId the picture ID
	 * @return the matching pictureses
	 */
	@Override
	public List<Pictures> findByPicturesId(long PictureId) {
		return findByPicturesId(PictureId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the pictureses where PictureId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param PictureId the picture ID
	 * @param start the lower bound of the range of pictureses
	 * @param end the upper bound of the range of pictureses (not inclusive)
	 * @return the range of matching pictureses
	 */
	@Override
	public List<Pictures> findByPicturesId(long PictureId, int start, int end) {
		return findByPicturesId(PictureId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the pictureses where PictureId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param PictureId the picture ID
	 * @param start the lower bound of the range of pictureses
	 * @param end the upper bound of the range of pictureses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching pictureses
	 */
	@Override
	public List<Pictures> findByPicturesId(long PictureId, int start, int end,
		OrderByComparator<Pictures> orderByComparator) {
		return findByPicturesId(PictureId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the pictureses where PictureId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param PictureId the picture ID
	 * @param start the lower bound of the range of pictureses
	 * @param end the upper bound of the range of pictureses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching pictureses
	 */
	@Override
	public List<Pictures> findByPicturesId(long PictureId, int start, int end,
		OrderByComparator<Pictures> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PICTURESID;
			finderArgs = new Object[] { PictureId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PICTURESID;
			finderArgs = new Object[] { PictureId, start, end, orderByComparator };
		}

		List<Pictures> list = null;

		if (retrieveFromCache) {
			list = (List<Pictures>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Pictures pictures : list) {
					if ((PictureId != pictures.getPictureId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PICTURES_WHERE);

			query.append(_FINDER_COLUMN_PICTURESID_PICTUREID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PicturesModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(PictureId);

				if (!pagination) {
					list = (List<Pictures>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Pictures>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first pictures in the ordered set where PictureId = &#63;.
	 *
	 * @param PictureId the picture ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching pictures
	 * @throws NoSuchPicturesException if a matching pictures could not be found
	 */
	@Override
	public Pictures findByPicturesId_First(long PictureId,
		OrderByComparator<Pictures> orderByComparator)
		throws NoSuchPicturesException {
		Pictures pictures = fetchByPicturesId_First(PictureId, orderByComparator);

		if (pictures != null) {
			return pictures;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("PictureId=");
		msg.append(PictureId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPicturesException(msg.toString());
	}

	/**
	 * Returns the first pictures in the ordered set where PictureId = &#63;.
	 *
	 * @param PictureId the picture ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching pictures, or <code>null</code> if a matching pictures could not be found
	 */
	@Override
	public Pictures fetchByPicturesId_First(long PictureId,
		OrderByComparator<Pictures> orderByComparator) {
		List<Pictures> list = findByPicturesId(PictureId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last pictures in the ordered set where PictureId = &#63;.
	 *
	 * @param PictureId the picture ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching pictures
	 * @throws NoSuchPicturesException if a matching pictures could not be found
	 */
	@Override
	public Pictures findByPicturesId_Last(long PictureId,
		OrderByComparator<Pictures> orderByComparator)
		throws NoSuchPicturesException {
		Pictures pictures = fetchByPicturesId_Last(PictureId, orderByComparator);

		if (pictures != null) {
			return pictures;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("PictureId=");
		msg.append(PictureId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPicturesException(msg.toString());
	}

	/**
	 * Returns the last pictures in the ordered set where PictureId = &#63;.
	 *
	 * @param PictureId the picture ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching pictures, or <code>null</code> if a matching pictures could not be found
	 */
	@Override
	public Pictures fetchByPicturesId_Last(long PictureId,
		OrderByComparator<Pictures> orderByComparator) {
		int count = countByPicturesId(PictureId);

		if (count == 0) {
			return null;
		}

		List<Pictures> list = findByPicturesId(PictureId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Removes all the pictureses where PictureId = &#63; from the database.
	 *
	 * @param PictureId the picture ID
	 */
	@Override
	public void removeByPicturesId(long PictureId) {
		for (Pictures pictures : findByPicturesId(PictureId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(pictures);
		}
	}

	/**
	 * Returns the number of pictureses where PictureId = &#63;.
	 *
	 * @param PictureId the picture ID
	 * @return the number of matching pictureses
	 */
	@Override
	public int countByPicturesId(long PictureId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PICTURESID;

		Object[] finderArgs = new Object[] { PictureId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PICTURES_WHERE);

			query.append(_FINDER_COLUMN_PICTURESID_PICTUREID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(PictureId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PICTURESID_PICTUREID_2 = "pictures.PictureId = ?";

	public PicturesPersistenceImpl() {
		setModelClass(Pictures.class);
	}

	/**
	 * Caches the pictures in the entity cache if it is enabled.
	 *
	 * @param pictures the pictures
	 */
	@Override
	public void cacheResult(Pictures pictures) {
		entityCache.putResult(PicturesModelImpl.ENTITY_CACHE_ENABLED,
			PicturesImpl.class, pictures.getPrimaryKey(), pictures);

		finderCache.putResult(FINDER_PATH_FETCH_BY_POSITIONIDEASREF,
			new Object[] { pictures.getIdeasRef(), pictures.getPosition() },
			pictures);

		pictures.resetOriginalValues();
	}

	/**
	 * Caches the pictureses in the entity cache if it is enabled.
	 *
	 * @param pictureses the pictureses
	 */
	@Override
	public void cacheResult(List<Pictures> pictureses) {
		for (Pictures pictures : pictureses) {
			if (entityCache.getResult(PicturesModelImpl.ENTITY_CACHE_ENABLED,
						PicturesImpl.class, pictures.getPrimaryKey()) == null) {
				cacheResult(pictures);
			}
			else {
				pictures.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all pictureses.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(PicturesImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the pictures.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Pictures pictures) {
		entityCache.removeResult(PicturesModelImpl.ENTITY_CACHE_ENABLED,
			PicturesImpl.class, pictures.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((PicturesModelImpl)pictures, true);
	}

	@Override
	public void clearCache(List<Pictures> pictureses) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Pictures pictures : pictureses) {
			entityCache.removeResult(PicturesModelImpl.ENTITY_CACHE_ENABLED,
				PicturesImpl.class, pictures.getPrimaryKey());

			clearUniqueFindersCache((PicturesModelImpl)pictures, true);
		}
	}

	protected void cacheUniqueFindersCache(PicturesModelImpl picturesModelImpl) {
		Object[] args = new Object[] {
				picturesModelImpl.getIdeasRef(), picturesModelImpl.getPosition()
			};

		finderCache.putResult(FINDER_PATH_COUNT_BY_POSITIONIDEASREF, args,
			Long.valueOf(1), false);
		finderCache.putResult(FINDER_PATH_FETCH_BY_POSITIONIDEASREF, args,
			picturesModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		PicturesModelImpl picturesModelImpl, boolean clearCurrent) {
		if (clearCurrent) {
			Object[] args = new Object[] {
					picturesModelImpl.getIdeasRef(),
					picturesModelImpl.getPosition()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_POSITIONIDEASREF, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_POSITIONIDEASREF, args);
		}

		if ((picturesModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_POSITIONIDEASREF.getColumnBitmask()) != 0) {
			Object[] args = new Object[] {
					picturesModelImpl.getOriginalIdeasRef(),
					picturesModelImpl.getOriginalPosition()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_POSITIONIDEASREF, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_POSITIONIDEASREF, args);
		}
	}

	/**
	 * Creates a new pictures with the primary key. Does not add the pictures to the database.
	 *
	 * @param PictureId the primary key for the new pictures
	 * @return the new pictures
	 */
	@Override
	public Pictures create(long PictureId) {
		Pictures pictures = new PicturesImpl();

		pictures.setNew(true);
		pictures.setPrimaryKey(PictureId);

		return pictures;
	}

	/**
	 * Removes the pictures with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param PictureId the primary key of the pictures
	 * @return the pictures that was removed
	 * @throws NoSuchPicturesException if a pictures with the primary key could not be found
	 */
	@Override
	public Pictures remove(long PictureId) throws NoSuchPicturesException {
		return remove((Serializable)PictureId);
	}

	/**
	 * Removes the pictures with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the pictures
	 * @return the pictures that was removed
	 * @throws NoSuchPicturesException if a pictures with the primary key could not be found
	 */
	@Override
	public Pictures remove(Serializable primaryKey)
		throws NoSuchPicturesException {
		Session session = null;

		try {
			session = openSession();

			Pictures pictures = (Pictures)session.get(PicturesImpl.class,
					primaryKey);

			if (pictures == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchPicturesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(pictures);
		}
		catch (NoSuchPicturesException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Pictures removeImpl(Pictures pictures) {
		pictures = toUnwrappedModel(pictures);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(pictures)) {
				pictures = (Pictures)session.get(PicturesImpl.class,
						pictures.getPrimaryKeyObj());
			}

			if (pictures != null) {
				session.delete(pictures);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (pictures != null) {
			clearCache(pictures);
		}

		return pictures;
	}

	@Override
	public Pictures updateImpl(Pictures pictures) {
		pictures = toUnwrappedModel(pictures);

		boolean isNew = pictures.isNew();

		PicturesModelImpl picturesModelImpl = (PicturesModelImpl)pictures;

		Session session = null;

		try {
			session = openSession();

			if (pictures.isNew()) {
				session.save(pictures);

				pictures.setNew(false);
			}
			else {
				pictures = (Pictures)session.merge(pictures);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!PicturesModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { picturesModelImpl.getIdeasRef() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_IDEASREF, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEASREF,
				args);

			args = new Object[] { picturesModelImpl.getPictureId() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_PICTURESID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PICTURESID,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((picturesModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEASREF.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						picturesModelImpl.getOriginalIdeasRef()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_IDEASREF, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEASREF,
					args);

				args = new Object[] { picturesModelImpl.getIdeasRef() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_IDEASREF, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEASREF,
					args);
			}

			if ((picturesModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PICTURESID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						picturesModelImpl.getOriginalPictureId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PICTURESID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PICTURESID,
					args);

				args = new Object[] { picturesModelImpl.getPictureId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PICTURESID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PICTURESID,
					args);
			}
		}

		entityCache.putResult(PicturesModelImpl.ENTITY_CACHE_ENABLED,
			PicturesImpl.class, pictures.getPrimaryKey(), pictures, false);

		clearUniqueFindersCache(picturesModelImpl, false);
		cacheUniqueFindersCache(picturesModelImpl);

		pictures.resetOriginalValues();

		return pictures;
	}

	protected Pictures toUnwrappedModel(Pictures pictures) {
		if (pictures instanceof PicturesImpl) {
			return pictures;
		}

		PicturesImpl picturesImpl = new PicturesImpl();

		picturesImpl.setNew(pictures.isNew());
		picturesImpl.setPrimaryKey(pictures.getPrimaryKey());

		picturesImpl.setPictureId(pictures.getPictureId());
		picturesImpl.setIdeasRef(pictures.getIdeasRef());
		picturesImpl.setFileRef(pictures.getFileRef());
		picturesImpl.setPictureUrl(pictures.getPictureUrl());
		picturesImpl.setPosition(pictures.getPosition());

		return picturesImpl;
	}

	/**
	 * Returns the pictures with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the pictures
	 * @return the pictures
	 * @throws NoSuchPicturesException if a pictures with the primary key could not be found
	 */
	@Override
	public Pictures findByPrimaryKey(Serializable primaryKey)
		throws NoSuchPicturesException {
		Pictures pictures = fetchByPrimaryKey(primaryKey);

		if (pictures == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchPicturesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return pictures;
	}

	/**
	 * Returns the pictures with the primary key or throws a {@link NoSuchPicturesException} if it could not be found.
	 *
	 * @param PictureId the primary key of the pictures
	 * @return the pictures
	 * @throws NoSuchPicturesException if a pictures with the primary key could not be found
	 */
	@Override
	public Pictures findByPrimaryKey(long PictureId)
		throws NoSuchPicturesException {
		return findByPrimaryKey((Serializable)PictureId);
	}

	/**
	 * Returns the pictures with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the pictures
	 * @return the pictures, or <code>null</code> if a pictures with the primary key could not be found
	 */
	@Override
	public Pictures fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(PicturesModelImpl.ENTITY_CACHE_ENABLED,
				PicturesImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Pictures pictures = (Pictures)serializable;

		if (pictures == null) {
			Session session = null;

			try {
				session = openSession();

				pictures = (Pictures)session.get(PicturesImpl.class, primaryKey);

				if (pictures != null) {
					cacheResult(pictures);
				}
				else {
					entityCache.putResult(PicturesModelImpl.ENTITY_CACHE_ENABLED,
						PicturesImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(PicturesModelImpl.ENTITY_CACHE_ENABLED,
					PicturesImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return pictures;
	}

	/**
	 * Returns the pictures with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param PictureId the primary key of the pictures
	 * @return the pictures, or <code>null</code> if a pictures with the primary key could not be found
	 */
	@Override
	public Pictures fetchByPrimaryKey(long PictureId) {
		return fetchByPrimaryKey((Serializable)PictureId);
	}

	@Override
	public Map<Serializable, Pictures> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Pictures> map = new HashMap<Serializable, Pictures>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Pictures pictures = fetchByPrimaryKey(primaryKey);

			if (pictures != null) {
				map.put(primaryKey, pictures);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(PicturesModelImpl.ENTITY_CACHE_ENABLED,
					PicturesImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Pictures)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_PICTURES_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Pictures pictures : (List<Pictures>)q.list()) {
				map.put(pictures.getPrimaryKeyObj(), pictures);

				cacheResult(pictures);

				uncachedPrimaryKeys.remove(pictures.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(PicturesModelImpl.ENTITY_CACHE_ENABLED,
					PicturesImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the pictureses.
	 *
	 * @return the pictureses
	 */
	@Override
	public List<Pictures> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the pictureses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of pictureses
	 * @param end the upper bound of the range of pictureses (not inclusive)
	 * @return the range of pictureses
	 */
	@Override
	public List<Pictures> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the pictureses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of pictureses
	 * @param end the upper bound of the range of pictureses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of pictureses
	 */
	@Override
	public List<Pictures> findAll(int start, int end,
		OrderByComparator<Pictures> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the pictureses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of pictureses
	 * @param end the upper bound of the range of pictureses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of pictureses
	 */
	@Override
	public List<Pictures> findAll(int start, int end,
		OrderByComparator<Pictures> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Pictures> list = null;

		if (retrieveFromCache) {
			list = (List<Pictures>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_PICTURES);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PICTURES;

				if (pagination) {
					sql = sql.concat(PicturesModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Pictures>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Pictures>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the pictureses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Pictures pictures : findAll()) {
			remove(pictures);
		}
	}

	/**
	 * Returns the number of pictureses.
	 *
	 * @return the number of pictureses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PICTURES);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return PicturesModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the pictures persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(PicturesImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_PICTURES = "SELECT pictures FROM Pictures pictures";
	private static final String _SQL_SELECT_PICTURES_WHERE_PKS_IN = "SELECT pictures FROM Pictures pictures WHERE PictureId IN (";
	private static final String _SQL_SELECT_PICTURES_WHERE = "SELECT pictures FROM Pictures pictures WHERE ";
	private static final String _SQL_COUNT_PICTURES = "SELECT COUNT(pictures) FROM Pictures pictures";
	private static final String _SQL_COUNT_PICTURES_WHERE = "SELECT COUNT(pictures) FROM Pictures pictures WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "pictures.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Pictures exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Pictures exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(PicturesPersistenceImpl.class);
}