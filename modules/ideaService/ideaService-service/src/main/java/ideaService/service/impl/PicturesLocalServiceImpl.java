/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.impl;

import java.util.List;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;

import ideaService.exception.NoSuchPicturesException;
import ideaService.model.Pictures;
import ideaService.service.PicturesLocalServiceUtil;
import ideaService.service.base.PicturesLocalServiceBaseImpl;
import ideaService.service.persistence.PicturesUtil;

/**
 * The implementation of the pictures local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link ideaService.service.PicturesLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PicturesLocalServiceBaseImpl
 * @see ideaService.service.PicturesLocalServiceUtil
 */
public class PicturesLocalServiceImpl extends PicturesLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link ideaService.service.PicturesLocalServiceUtil} to access the pictures local service.
	 */

	public long createNewPictureEntry(long ideasRef, long fileRef, String picUrl, int position){
		try {
				Pictures p = PicturesUtil.findByPositionIdeasRef(ideasRef, position);
				DLAppLocalServiceUtil.deleteFileEntry(p.getFileRef());
				PicturesUtil.remove(p.getPrimaryKey());
		} catch (PortalException e) {
		}

		int nextDbId = (int)CounterLocalServiceUtil.increment(Pictures.class.getName());
		Pictures nextPic = PicturesLocalServiceUtil.createPictures(nextDbId);
		nextPic.setIdeasRef(ideasRef);
		nextPic.setPosition(position);
		nextPic.setFileRef(fileRef);
		nextPic.setPictureUrl(picUrl);
		nextPic.persist();
		return nextPic.getPrimaryKey();
	}

	public List<Pictures> getAllPicturesForIdeasRef(long ideasRef) throws NoSuchPicturesException{
		return PicturesUtil.findByIdeasRef(ideasRef);
	}

	public void deleteAllPicturesForIdeaRef(long ideasRef){
		List<Pictures> pics = PicturesUtil.findAll();
		for(Pictures p : pics){
			if(p.getIdeasRef() == ideasRef){
				try {
					PicturesUtil.remove(p.getPrimaryKey());
				} catch (NoSuchPicturesException e) {
					e.printStackTrace();
				}
			}
		}
	}

}