/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.impl;

import java.util.List;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;

import ideaService.exception.NoSuchVideosException;
import ideaService.model.Videos;
import ideaService.service.VideosLocalServiceUtil;
import ideaService.service.base.VideosLocalServiceBaseImpl;
import ideaService.service.persistence.VideosUtil;
import ideasService.service.enums.VideoExtensions;

/**
 * The implementation of the videos local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link ideaService.service.VideosLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VideosLocalServiceBaseImpl
 * @see ideaService.service.VideosLocalServiceUtil
 */
public class VideosLocalServiceImpl extends VideosLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link ideaService.service.VideosLocalServiceUtil} to access the videos local service.
	 */
	public long createNewVideoEntry(long ideasRef, long fileRef, String videoUrl, String extension){

		try {
			Videos v = this.getVideoUrlByIdeaRefAndExtension(ideasRef,extension);
			if(v != null){
				DLAppLocalServiceUtil.deleteFileEntry(v.getFileRef());
				VideosUtil.remove(v.getPrimaryKey());
			}
	} catch (PortalException e) {
		e.printStackTrace();
	}
		int nextDbId = (int)CounterLocalServiceUtil.increment(Videos.class.getName());
		Videos nextVideo = VideosLocalServiceUtil.createVideos(nextDbId);
		nextVideo.setIdeasRef(ideasRef);
		nextVideo.setExtension(extension);
		nextVideo.setFileRef(fileRef);
		nextVideo.setVideoUrl(videoUrl);
		nextVideo.persist();
		return nextVideo.getPrimaryKey();
	}

	public List<Videos> getAllVideosForIdeasRef(long ideasRef) throws NoSuchVideosException{
		return VideosUtil.findByIdeasRef(ideasRef);
	}

	public String getVideoUrlByIdeaRefAndExtension(long ideasRef, VideoExtensions ex){
		List<Videos> vids = VideosUtil.findAll();
		for(Videos v : vids){
			if(v.getIdeasRef() == ideasRef && v.getExtension().equals(ex.getVideoExtensionDescription())){
				return v.getVideoUrl();
			}
		}
		return "/";
	}

	public Videos getVideoUrlByIdeaRefAndExtension(long ideasRef, String ex){
		List<Videos> vids = VideosUtil.findAll();
		for(Videos v : vids){
			if(v.getIdeasRef() == ideasRef && v.getExtension().equals(ex)){
				return v;
			}
		}
		return null;
	}

	public Videos getVideoByIdeaRefAndExtension(long ideasRef, VideoExtensions ex){
		List<Videos> vids = VideosUtil.findAll();
		for(Videos v : vids){
			if(v.getIdeasRef() == ideasRef && v.getExtension().equals(ex.getVideoExtensionDescription())){
				return v;
			}
		}
		return null;
	}

	public void deleteAllVideosForIdeaRef(long ideasRef){
		List<Videos> vids = VideosUtil.findAll();
		for(Videos v : vids){
			if(v.getIdeasRef() == ideasRef){
				try {
					VideosUtil.remove(v.getPrimaryKey());
				} catch (NoSuchVideosException e) {
					e.printStackTrace();
				}
			}
		}
	}

}