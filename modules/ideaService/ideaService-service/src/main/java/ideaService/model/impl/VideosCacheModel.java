/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import ideaService.model.Videos;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Videos in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Videos
 * @generated
 */
@ProviderType
public class VideosCacheModel implements CacheModel<Videos>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VideosCacheModel)) {
			return false;
		}

		VideosCacheModel videosCacheModel = (VideosCacheModel)obj;

		if (VideoId == videosCacheModel.VideoId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, VideoId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{VideoId=");
		sb.append(VideoId);
		sb.append(", IdeasRef=");
		sb.append(IdeasRef);
		sb.append(", FileRef=");
		sb.append(FileRef);
		sb.append(", VideoUrl=");
		sb.append(VideoUrl);
		sb.append(", Extension=");
		sb.append(Extension);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Videos toEntityModel() {
		VideosImpl videosImpl = new VideosImpl();

		videosImpl.setVideoId(VideoId);
		videosImpl.setIdeasRef(IdeasRef);
		videosImpl.setFileRef(FileRef);

		if (VideoUrl == null) {
			videosImpl.setVideoUrl(StringPool.BLANK);
		}
		else {
			videosImpl.setVideoUrl(VideoUrl);
		}

		if (Extension == null) {
			videosImpl.setExtension(StringPool.BLANK);
		}
		else {
			videosImpl.setExtension(Extension);
		}

		videosImpl.resetOriginalValues();

		return videosImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		VideoId = objectInput.readLong();

		IdeasRef = objectInput.readLong();

		FileRef = objectInput.readLong();
		VideoUrl = objectInput.readUTF();
		Extension = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(VideoId);

		objectOutput.writeLong(IdeasRef);

		objectOutput.writeLong(FileRef);

		if (VideoUrl == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(VideoUrl);
		}

		if (Extension == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(Extension);
		}
	}

	public long VideoId;
	public long IdeasRef;
	public long FileRef;
	public String VideoUrl;
	public String Extension;
}