/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.expando.kernel.util.ExpandoBridgeFactoryUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.impl.BaseModelImpl;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import ideaService.model.Videos;
import ideaService.model.VideosModel;
import ideaService.model.VideosSoap;

import java.io.Serializable;

import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The base model implementation for the Videos service. Represents a row in the &quot;IDEA_Videos&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link VideosModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link VideosImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VideosImpl
 * @see Videos
 * @see VideosModel
 * @generated
 */
@JSON(strict = true)
@ProviderType
public class VideosModelImpl extends BaseModelImpl<Videos>
	implements VideosModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a videos model instance should use the {@link Videos} interface instead.
	 */
	public static final String TABLE_NAME = "IDEA_Videos";
	public static final Object[][] TABLE_COLUMNS = {
			{ "VideoId", Types.BIGINT },
			{ "IdeasRef", Types.BIGINT },
			{ "FileRef", Types.BIGINT },
			{ "VideoUrl", Types.VARCHAR },
			{ "Extension", Types.VARCHAR }
		};
	public static final Map<String, Integer> TABLE_COLUMNS_MAP = new HashMap<String, Integer>();

	static {
		TABLE_COLUMNS_MAP.put("VideoId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("IdeasRef", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("FileRef", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("VideoUrl", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("Extension", Types.VARCHAR);
	}

	public static final String TABLE_SQL_CREATE = "create table IDEA_Videos (VideoId LONG not null primary key,IdeasRef LONG,FileRef LONG,VideoUrl STRING null,Extension VARCHAR(75) null)";
	public static final String TABLE_SQL_DROP = "drop table IDEA_Videos";
	public static final String ORDER_BY_JPQL = " ORDER BY videos.VideoId ASC";
	public static final String ORDER_BY_SQL = " ORDER BY IDEA_Videos.VideoId ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(ideaService.service.util.ServiceProps.get(
				"value.object.entity.cache.enabled.ideaService.model.Videos"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(ideaService.service.util.ServiceProps.get(
				"value.object.finder.cache.enabled.ideaService.model.Videos"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(ideaService.service.util.ServiceProps.get(
				"value.object.column.bitmask.enabled.ideaService.model.Videos"),
			true);
	public static final long EXTENSION_COLUMN_BITMASK = 1L;
	public static final long IDEASREF_COLUMN_BITMASK = 2L;
	public static final long VIDEOID_COLUMN_BITMASK = 4L;

	/**
	 * Converts the soap model instance into a normal model instance.
	 *
	 * @param soapModel the soap model instance to convert
	 * @return the normal model instance
	 */
	public static Videos toModel(VideosSoap soapModel) {
		if (soapModel == null) {
			return null;
		}

		Videos model = new VideosImpl();

		model.setVideoId(soapModel.getVideoId());
		model.setIdeasRef(soapModel.getIdeasRef());
		model.setFileRef(soapModel.getFileRef());
		model.setVideoUrl(soapModel.getVideoUrl());
		model.setExtension(soapModel.getExtension());

		return model;
	}

	/**
	 * Converts the soap model instances into normal model instances.
	 *
	 * @param soapModels the soap model instances to convert
	 * @return the normal model instances
	 */
	public static List<Videos> toModels(VideosSoap[] soapModels) {
		if (soapModels == null) {
			return null;
		}

		List<Videos> models = new ArrayList<Videos>(soapModels.length);

		for (VideosSoap soapModel : soapModels) {
			models.add(toModel(soapModel));
		}

		return models;
	}

	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(ideaService.service.util.ServiceProps.get(
				"lock.expiration.time.ideaService.model.Videos"));

	public VideosModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _VideoId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setVideoId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _VideoId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return Videos.class;
	}

	@Override
	public String getModelClassName() {
		return Videos.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("VideoId", getVideoId());
		attributes.put("IdeasRef", getIdeasRef());
		attributes.put("FileRef", getFileRef());
		attributes.put("VideoUrl", getVideoUrl());
		attributes.put("Extension", getExtension());

		attributes.put("entityCacheEnabled", isEntityCacheEnabled());
		attributes.put("finderCacheEnabled", isFinderCacheEnabled());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long VideoId = (Long)attributes.get("VideoId");

		if (VideoId != null) {
			setVideoId(VideoId);
		}

		Long IdeasRef = (Long)attributes.get("IdeasRef");

		if (IdeasRef != null) {
			setIdeasRef(IdeasRef);
		}

		Long FileRef = (Long)attributes.get("FileRef");

		if (FileRef != null) {
			setFileRef(FileRef);
		}

		String VideoUrl = (String)attributes.get("VideoUrl");

		if (VideoUrl != null) {
			setVideoUrl(VideoUrl);
		}

		String Extension = (String)attributes.get("Extension");

		if (Extension != null) {
			setExtension(Extension);
		}
	}

	@JSON
	@Override
	public long getVideoId() {
		return _VideoId;
	}

	@Override
	public void setVideoId(long VideoId) {
		_VideoId = VideoId;
	}

	@JSON
	@Override
	public long getIdeasRef() {
		return _IdeasRef;
	}

	@Override
	public void setIdeasRef(long IdeasRef) {
		_columnBitmask |= IDEASREF_COLUMN_BITMASK;

		if (!_setOriginalIdeasRef) {
			_setOriginalIdeasRef = true;

			_originalIdeasRef = _IdeasRef;
		}

		_IdeasRef = IdeasRef;
	}

	public long getOriginalIdeasRef() {
		return _originalIdeasRef;
	}

	@JSON
	@Override
	public long getFileRef() {
		return _FileRef;
	}

	@Override
	public void setFileRef(long FileRef) {
		_FileRef = FileRef;
	}

	@JSON
	@Override
	public String getVideoUrl() {
		if (_VideoUrl == null) {
			return StringPool.BLANK;
		}
		else {
			return _VideoUrl;
		}
	}

	@Override
	public void setVideoUrl(String VideoUrl) {
		_VideoUrl = VideoUrl;
	}

	@JSON
	@Override
	public String getExtension() {
		if (_Extension == null) {
			return StringPool.BLANK;
		}
		else {
			return _Extension;
		}
	}

	@Override
	public void setExtension(String Extension) {
		_columnBitmask |= EXTENSION_COLUMN_BITMASK;

		if (_originalExtension == null) {
			_originalExtension = _Extension;
		}

		_Extension = Extension;
	}

	public String getOriginalExtension() {
		return GetterUtil.getString(_originalExtension);
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(0,
			Videos.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public Videos toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (Videos)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		VideosImpl videosImpl = new VideosImpl();

		videosImpl.setVideoId(getVideoId());
		videosImpl.setIdeasRef(getIdeasRef());
		videosImpl.setFileRef(getFileRef());
		videosImpl.setVideoUrl(getVideoUrl());
		videosImpl.setExtension(getExtension());

		videosImpl.resetOriginalValues();

		return videosImpl;
	}

	@Override
	public int compareTo(Videos videos) {
		long primaryKey = videos.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof Videos)) {
			return false;
		}

		Videos videos = (Videos)obj;

		long primaryKey = videos.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return ENTITY_CACHE_ENABLED;
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return FINDER_CACHE_ENABLED;
	}

	@Override
	public void resetOriginalValues() {
		VideosModelImpl videosModelImpl = this;

		videosModelImpl._originalIdeasRef = videosModelImpl._IdeasRef;

		videosModelImpl._setOriginalIdeasRef = false;

		videosModelImpl._originalExtension = videosModelImpl._Extension;

		videosModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<Videos> toCacheModel() {
		VideosCacheModel videosCacheModel = new VideosCacheModel();

		videosCacheModel.VideoId = getVideoId();

		videosCacheModel.IdeasRef = getIdeasRef();

		videosCacheModel.FileRef = getFileRef();

		videosCacheModel.VideoUrl = getVideoUrl();

		String VideoUrl = videosCacheModel.VideoUrl;

		if ((VideoUrl != null) && (VideoUrl.length() == 0)) {
			videosCacheModel.VideoUrl = null;
		}

		videosCacheModel.Extension = getExtension();

		String Extension = videosCacheModel.Extension;

		if ((Extension != null) && (Extension.length() == 0)) {
			videosCacheModel.Extension = null;
		}

		return videosCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{VideoId=");
		sb.append(getVideoId());
		sb.append(", IdeasRef=");
		sb.append(getIdeasRef());
		sb.append(", FileRef=");
		sb.append(getFileRef());
		sb.append(", VideoUrl=");
		sb.append(getVideoUrl());
		sb.append(", Extension=");
		sb.append(getExtension());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append("ideaService.model.Videos");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>VideoId</column-name><column-value><![CDATA[");
		sb.append(getVideoId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>IdeasRef</column-name><column-value><![CDATA[");
		sb.append(getIdeasRef());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>FileRef</column-name><column-value><![CDATA[");
		sb.append(getFileRef());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>VideoUrl</column-name><column-value><![CDATA[");
		sb.append(getVideoUrl());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>Extension</column-name><column-value><![CDATA[");
		sb.append(getExtension());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static final ClassLoader _classLoader = Videos.class.getClassLoader();
	private static final Class<?>[] _escapedModelInterfaces = new Class[] {
			Videos.class
		};
	private long _VideoId;
	private long _IdeasRef;
	private long _originalIdeasRef;
	private boolean _setOriginalIdeasRef;
	private long _FileRef;
	private String _VideoUrl;
	private String _Extension;
	private String _originalExtension;
	private long _columnBitmask;
	private Videos _escapedModel;
}