/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import ideaService.model.Review;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Review in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Review
 * @generated
 */
@ProviderType
public class ReviewCacheModel implements CacheModel<Review>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ReviewCacheModel)) {
			return false;
		}

		ReviewCacheModel reviewCacheModel = (ReviewCacheModel)obj;

		if (reviewId == reviewCacheModel.reviewId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, reviewId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", reviewId=");
		sb.append(reviewId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", accepted=");
		sb.append(accepted);
		sb.append(", feedback=");
		sb.append(feedback);
		sb.append(", ideasIdRef=");
		sb.append(ideasIdRef);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Review toEntityModel() {
		ReviewImpl reviewImpl = new ReviewImpl();

		if (uuid == null) {
			reviewImpl.setUuid(StringPool.BLANK);
		}
		else {
			reviewImpl.setUuid(uuid);
		}

		reviewImpl.setReviewId(reviewId);
		reviewImpl.setGroupId(groupId);
		reviewImpl.setCompanyId(companyId);
		reviewImpl.setUserId(userId);

		if (userName == null) {
			reviewImpl.setUserName(StringPool.BLANK);
		}
		else {
			reviewImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			reviewImpl.setCreateDate(null);
		}
		else {
			reviewImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			reviewImpl.setModifiedDate(null);
		}
		else {
			reviewImpl.setModifiedDate(new Date(modifiedDate));
		}

		reviewImpl.setAccepted(accepted);

		if (feedback == null) {
			reviewImpl.setFeedback(StringPool.BLANK);
		}
		else {
			reviewImpl.setFeedback(feedback);
		}

		reviewImpl.setIdeasIdRef(ideasIdRef);

		reviewImpl.resetOriginalValues();

		return reviewImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		reviewId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		accepted = objectInput.readBoolean();
		feedback = objectInput.readUTF();

		ideasIdRef = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(reviewId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeBoolean(accepted);

		if (feedback == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(feedback);
		}

		objectOutput.writeLong(ideasIdRef);
	}

	public String uuid;
	public long reviewId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public boolean accepted;
	public String feedback;
	public long ideasIdRef;
}