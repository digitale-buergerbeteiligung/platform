package ideaService.ideas.asset;

import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.PortletURL;
import javax.portlet.WindowState;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.asset.kernel.model.BaseJSPAssetRenderer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.StringUtil;

import ideaService.model.Ideas;

public class IdeasAssetRenderer extends BaseJSPAssetRenderer<Ideas> {

	private Ideas _idea;

	private final String ideas_key = "IDEAS";

	public IdeasAssetRenderer(Ideas idea){
		_idea = idea;
	}

	@Override
	public boolean hasEditPermission(PermissionChecker permissionChecker)
	throws PortalException {
	  return true;
	}

	@Override
	public boolean hasViewPermission(PermissionChecker permissionChecker)
	throws PortalException {
	  return true;
	}

	@Override
	public Ideas getAssetObject() {
	  return _idea;
	}

	@Override
	public long getGroupId() {
	  return _idea.getGroupId();
	}

	@Override
	public long getUserId() {

	  return _idea.getUserId();
	}

	@Override
	public String getUserName() {
	  return _idea.getUserName();
	}

	@Override
	public String getUuid() {
	  return _idea.getUuid();
	}

	@Override
	public String getClassName() {
	  return Ideas.class.getName();
	}

	@Override
	public long getClassPK() {
	  return _idea.getIdeasId();
	}

	@Override
	public String getSummary(PortletRequest portletRequest, PortletResponse
	  portletResponse) {
	    return "Short description: " + _idea.getShortdescription();
	}

	@Override
	public String getTitle(Locale locale) {
	  return _idea.getTitle();
	}

	@Override
	public boolean include(HttpServletRequest request, HttpServletResponse
	  response, String template) throws Exception {
	    request.setAttribute("IDEAS", _idea);
	    request.setAttribute("HtmlUtil", HtmlUtil.getHtml());
	    request.setAttribute("StringUtil", new StringUtil());
	    return super.include(request, response, template);
	}


	  @Override
	  public String getJspPath(HttpServletRequest request, String template) {
	    if (template.equals(TEMPLATE_FULL_CONTENT)) {
	      request.setAttribute("IDEA_ideas", _idea);
	      return "/asset/idea/" + template + ".jsp";
	    } else {
	      return null;
	    }
	  }

	  @Override
	  public PortletURL getURLEdit(LiferayPortletRequest liferayPortletRequest,
			  //TODO replace with actual url or find way to block editing.
	      LiferayPortletResponse liferayPortletResponse) throws Exception {
	    PortletURL portletURL = liferayPortletResponse.createLiferayPortletURL(
	        getControlPanelPlid(liferayPortletRequest), ideas_key,
	        PortletRequest.RENDER_PHASE);
	   // portletURL.setParameter("mvcRenderCommandName", "/guestbookwebportlet/edit_guestbook");
	    portletURL.setParameter("IdeasId", String.valueOf(_idea.getIdeasId()));
	    portletURL.setParameter("showback", Boolean.FALSE.toString());

	    return portletURL;
	  }

	  @Override
	  public String getURLViewInContext(LiferayPortletRequest liferayPortletRequest,
	      LiferayPortletResponse liferayPortletResponse, String noSuchEntryRedirect) throws Exception {
	    try {
//	      long plid = PortalUtil.getPlidFromPortletId(_idea.getGroupId(),
//	    		  ideas_key);
//
//	      PortletURL portletURL;
//	      if (plid == LayoutConstants.DEFAULT_PLID) {
//	        portletURL = liferayPortletResponse.createLiferayPortletURL(getControlPanelPlid(liferayPortletRequest),
//	        		ideas_key, PortletRequest.RENDER_PHASE);
//	      } else {
//	        portletURL = PortletURLFactoryUtil.create(liferayPortletRequest,
//	        		ideas_key, plid, PortletRequest.RENDER_PHASE);
//	      }
//
//	      portletURL.setParameter("mvcRenderCommandName", "/guestbookwebportlet/view");
//	      portletURL.setParameter("guestbookId", String.valueOf(_idea.getIdeasId()));
//
//	      String currentUrl = PortalUtil.getCurrentURL(liferayPortletRequest);
//
//	      portletURL.setParameter("redirect", currentUrl);

	      return _idea.getPageUrl();

//	    } catch (PortalException e) {
//
	    }
	      catch (Exception e) {
	    }

	    return noSuchEntryRedirect;
	  }

	  @Override
	  public String getURLView(LiferayPortletResponse liferayPortletResponse,
	  WindowState windowState) throws Exception {

	    return super.getURLView(liferayPortletResponse, windowState);
	  }
}