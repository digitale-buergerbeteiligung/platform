package ideaService.ideas.search;

import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BaseIndexer;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.IndexWriterHelper;
import com.liferay.portal.kernel.search.IndexWriterHelperUtil;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Summary;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import ideaService.model.Ideas;
import ideaService.service.IdeasLocalService;

@Component(immediate = true, service = Indexer.class)
public class IdeasIndexer extends BaseIndexer<Ideas> {

	public static final String CLASS_NAME = Ideas.class.getName();

	public IdeasIndexer() {
		setDefaultSelectedFieldNames(Field.ASSET_TAG_NAMES, Field.COMPANY_ID, Field.CONTENT, Field.UID,
				Field.ENTRY_CLASS_PK, Field.GROUP_ID, Field.MODIFIED_DATE, Field.SCOPE_GROUP_ID, Field.TITLE,
				Field.DESCRIPTION);
		setFilterSearch(true);
		setPermissionAware(true);
	}

	@Override
	public String getClassName() {
		return CLASS_NAME;
	}

	@Override
	public boolean hasPermission(PermissionChecker permissionChecker, String entryClassName, long entryClassPK,
			String actionId) throws Exception {
		return true;
	}

	@Override
	public boolean isVisible(long classPK, int status) throws Exception {
		return true;
	}

	@Override
	public void postProcessContextBooleanFilter(BooleanFilter contextBooleanFilter, SearchContext searchContext)
			throws Exception {
		addStatus(contextBooleanFilter, searchContext);
	}

	@Override
	public void postProcessSearchQuery(BooleanQuery searchQuery, BooleanFilter fullQueryBooleanFilter,
			SearchContext searchContext) throws Exception {
		addSearchLocalizedTerm(searchQuery, searchContext, Field.TITLE, false);
	}

	@Override
	protected void doDelete(Ideas idea) throws Exception {
		deleteDocument(PortalUtil.getDefaultCompanyId(), idea.getIdeasId());
	}

	@Override
	protected Document doGetDocument(Ideas idea) throws Exception {
		final String CATEGORY = "category";
		Document document = getBaseModelDocument(CLASS_NAME, idea);
		document.addDate(Field.MODIFIED_DATE, idea.getModifiedDate());
		document.addKeyword(Field.TITLE, idea.getTitle());
		document.addKeyword(CATEGORY, idea.getCategory());
		document.addText(Field.TITLE, idea.getTitle());
		document.addText(Field.DESCRIPTION, idea.getDescription());
		return document;
	}

	@Override
	protected Summary doGetSummary(Document document, Locale locale, String snippet, PortletRequest portletRequest,
			PortletResponse portletResponse) throws Exception {
		Summary summary = createSummary(document);
		summary.setMaxContentLength(200);
		return summary;
	}

	@Override
	protected void doReindex(String className, long classPK) throws Exception {
		Ideas idea = _ideasLocalSerive.fetchIdeas(classPK);
		doReindex(idea);
	}

	@Override
	protected void doReindex(String[] ids) throws Exception {
		long companyId = GetterUtil.getLong(ids[0]);
		reindexIdeas(companyId);
	}

	@Override
	protected void doReindex(Ideas idea) throws Exception {

		Document document = getDocument(idea);
		IndexWriterHelperUtil.updateDocument(getSearchEngineId(), idea.getCompanyId(), document, isCommitImmediately());
	}

	protected void reindexIdeas(long companyId) throws PortalException {
		final IndexableActionableDynamicQuery indexableActionableDynamicQuery = _ideasLocalSerive
				.getIndexableActionableDynamicQuery();
		indexableActionableDynamicQuery.setCompanyId(companyId);
		indexableActionableDynamicQuery.setPerformActionMethod(

				new ActionableDynamicQuery.PerformActionMethod<Ideas>() {
					@Override
					public void performAction(Ideas idea) {
						try {
							Document document = getDocument(idea);
							indexableActionableDynamicQuery.addDocuments(document);
						} catch (PortalException pe) {
							if (_log.isWarnEnabled()) {
								_log.warn("Unable to index idea " + idea.getIdeasId(), pe);
							}
						}
					}
				});
		indexableActionableDynamicQuery.setSearchEngineId(getSearchEngineId());
		indexableActionableDynamicQuery.performActions();

	}

	private static final Log _log = LogFactoryUtil.getLog(IdeasIndexer.class);

	@Reference
	protected IndexWriterHelper indexWriterHelper;

	@Reference
	private IdeasLocalService _ideasLocalSerive;

}