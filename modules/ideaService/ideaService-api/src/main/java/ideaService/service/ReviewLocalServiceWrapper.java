/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ReviewLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ReviewLocalService
 * @generated
 */
@ProviderType
public class ReviewLocalServiceWrapper implements ReviewLocalService,
	ServiceWrapper<ReviewLocalService> {
	public ReviewLocalServiceWrapper(ReviewLocalService reviewLocalService) {
		_reviewLocalService = reviewLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _reviewLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _reviewLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _reviewLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _reviewLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _reviewLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _reviewLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Adds the review to the database. Also notifies the appropriate model listeners.
	*
	* @param review the review
	* @return the review that was added
	*/
	@Override
	public ideaService.model.Review addReview(ideaService.model.Review review) {
		return _reviewLocalService.addReview(review);
	}

	/**
	* Creates a new review with the primary key. Does not add the review to the database.
	*
	* @param reviewId the primary key for the new review
	* @return the new review
	*/
	@Override
	public ideaService.model.Review createReview(long reviewId) {
		return _reviewLocalService.createReview(reviewId);
	}

	@Override
	public ideaService.model.Review createReviewWithAutomatedDbId(
		boolean accepted, java.lang.String feedback, long ideasIdRef,
		long userId) {
		return _reviewLocalService.createReviewWithAutomatedDbId(accepted,
			feedback, ideasIdRef, userId);
	}

	/**
	* Deletes the review from the database. Also notifies the appropriate model listeners.
	*
	* @param review the review
	* @return the review that was removed
	*/
	@Override
	public ideaService.model.Review deleteReview(
		ideaService.model.Review review) {
		return _reviewLocalService.deleteReview(review);
	}

	/**
	* Deletes the review with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param reviewId the primary key of the review
	* @return the review that was removed
	* @throws PortalException if a review with the primary key could not be found
	*/
	@Override
	public ideaService.model.Review deleteReview(long reviewId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _reviewLocalService.deleteReview(reviewId);
	}

	@Override
	public ideaService.model.Review fetchReview(long reviewId) {
		return _reviewLocalService.fetchReview(reviewId);
	}

	/**
	* Returns the review matching the UUID and group.
	*
	* @param uuid the review's UUID
	* @param groupId the primary key of the group
	* @return the matching review, or <code>null</code> if a matching review could not be found
	*/
	@Override
	public ideaService.model.Review fetchReviewByUuidAndGroupId(
		java.lang.String uuid, long groupId) {
		return _reviewLocalService.fetchReviewByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns the review with the primary key.
	*
	* @param reviewId the primary key of the review
	* @return the review
	* @throws PortalException if a review with the primary key could not be found
	*/
	@Override
	public ideaService.model.Review getReview(long reviewId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _reviewLocalService.getReview(reviewId);
	}

	/**
	* Returns the review matching the UUID and group.
	*
	* @param uuid the review's UUID
	* @param groupId the primary key of the group
	* @return the matching review
	* @throws PortalException if a matching review could not be found
	*/
	@Override
	public ideaService.model.Review getReviewByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _reviewLocalService.getReviewByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Updates the review in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param review the review
	* @return the review that was updated
	*/
	@Override
	public ideaService.model.Review updateReview(
		ideaService.model.Review review) {
		return _reviewLocalService.updateReview(review);
	}

	/**
	* Returns the number of reviews.
	*
	* @return the number of reviews
	*/
	@Override
	public int getReviewsCount() {
		return _reviewLocalService.getReviewsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _reviewLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _reviewLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.ReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _reviewLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.ReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _reviewLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the reviews.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.ReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reviews
	* @param end the upper bound of the range of reviews (not inclusive)
	* @return the range of reviews
	*/
	@Override
	public java.util.List<ideaService.model.Review> getReviews(int start,
		int end) {
		return _reviewLocalService.getReviews(start, end);
	}

	/**
	* Returns all the reviews matching the UUID and company.
	*
	* @param uuid the UUID of the reviews
	* @param companyId the primary key of the company
	* @return the matching reviews, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<ideaService.model.Review> getReviewsByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return _reviewLocalService.getReviewsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns a range of reviews matching the UUID and company.
	*
	* @param uuid the UUID of the reviews
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of reviews
	* @param end the upper bound of the range of reviews (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching reviews, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<ideaService.model.Review> getReviewsByUuidAndCompanyId(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ideaService.model.Review> orderByComparator) {
		return _reviewLocalService.getReviewsByUuidAndCompanyId(uuid,
			companyId, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _reviewLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _reviewLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public void persistReviewAndPerformTypeChecks(
		ideaService.model.Review review) {
		_reviewLocalService.persistReviewAndPerformTypeChecks(review);
	}

	@Override
	public ReviewLocalService getWrappedService() {
		return _reviewLocalService;
	}

	@Override
	public void setWrappedService(ReviewLocalService reviewLocalService) {
		_reviewLocalService = reviewLocalService;
	}

	private ReviewLocalService _reviewLocalService;
}