/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import ideaService.exception.NoSuchCategoryException;

import ideaService.model.Category;

/**
 * The persistence interface for the category service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ideaService.service.persistence.impl.CategoryPersistenceImpl
 * @see CategoryUtil
 * @generated
 */
@ProviderType
public interface CategoryPersistence extends BasePersistence<Category> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CategoryUtil} to access the category persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the categories where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching categories
	*/
	public java.util.List<Category> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the categories where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @return the range of matching categories
	*/
	public java.util.List<Category> findByUuid(java.lang.String uuid,
		int start, int end);

	/**
	* Returns an ordered range of all the categories where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching categories
	*/
	public java.util.List<Category> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator);

	/**
	* Returns an ordered range of all the categories where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching categories
	*/
	public java.util.List<Category> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first category in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public Category findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator)
		throws NoSuchCategoryException;

	/**
	* Returns the first category in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching category, or <code>null</code> if a matching category could not be found
	*/
	public Category fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator);

	/**
	* Returns the last category in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public Category findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator)
		throws NoSuchCategoryException;

	/**
	* Returns the last category in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching category, or <code>null</code> if a matching category could not be found
	*/
	public Category fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator);

	/**
	* Returns the categories before and after the current category in the ordered set where uuid = &#63;.
	*
	* @param categoryId the primary key of the current category
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next category
	* @throws NoSuchCategoryException if a category with the primary key could not be found
	*/
	public Category[] findByUuid_PrevAndNext(long categoryId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator)
		throws NoSuchCategoryException;

	/**
	* Removes all the categories where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of categories where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching categories
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns the category where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchCategoryException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public Category findByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchCategoryException;

	/**
	* Returns the category where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching category, or <code>null</code> if a matching category could not be found
	*/
	public Category fetchByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns the category where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching category, or <code>null</code> if a matching category could not be found
	*/
	public Category fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the category where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the category that was removed
	*/
	public Category removeByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchCategoryException;

	/**
	* Returns the number of categories where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching categories
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns all the categories where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching categories
	*/
	public java.util.List<Category> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the categories where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @return the range of matching categories
	*/
	public java.util.List<Category> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the categories where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching categories
	*/
	public java.util.List<Category> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator);

	/**
	* Returns an ordered range of all the categories where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching categories
	*/
	public java.util.List<Category> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first category in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public Category findByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator)
		throws NoSuchCategoryException;

	/**
	* Returns the first category in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching category, or <code>null</code> if a matching category could not be found
	*/
	public Category fetchByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator);

	/**
	* Returns the last category in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public Category findByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator)
		throws NoSuchCategoryException;

	/**
	* Returns the last category in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching category, or <code>null</code> if a matching category could not be found
	*/
	public Category fetchByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator);

	/**
	* Returns the categories before and after the current category in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param categoryId the primary key of the current category
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next category
	* @throws NoSuchCategoryException if a category with the primary key could not be found
	*/
	public Category[] findByUuid_C_PrevAndNext(long categoryId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator)
		throws NoSuchCategoryException;

	/**
	* Removes all the categories where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of categories where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching categories
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the category where projectRef = &#63; or throws a {@link NoSuchCategoryException} if it could not be found.
	*
	* @param projectRef the project ref
	* @return the matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public Category findByProjectRef(long projectRef)
		throws NoSuchCategoryException;

	/**
	* Returns the category where projectRef = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param projectRef the project ref
	* @return the matching category, or <code>null</code> if a matching category could not be found
	*/
	public Category fetchByProjectRef(long projectRef);

	/**
	* Returns the category where projectRef = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param projectRef the project ref
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching category, or <code>null</code> if a matching category could not be found
	*/
	public Category fetchByProjectRef(long projectRef, boolean retrieveFromCache);

	/**
	* Removes the category where projectRef = &#63; from the database.
	*
	* @param projectRef the project ref
	* @return the category that was removed
	*/
	public Category removeByProjectRef(long projectRef)
		throws NoSuchCategoryException;

	/**
	* Returns the number of categories where projectRef = &#63;.
	*
	* @param projectRef the project ref
	* @return the number of matching categories
	*/
	public int countByProjectRef(long projectRef);

	/**
	* Returns the category where categoryTitle = &#63; or throws a {@link NoSuchCategoryException} if it could not be found.
	*
	* @param categoryTitle the category title
	* @return the matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public Category findByCategory(java.lang.String categoryTitle)
		throws NoSuchCategoryException;

	/**
	* Returns the category where categoryTitle = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param categoryTitle the category title
	* @return the matching category, or <code>null</code> if a matching category could not be found
	*/
	public Category fetchByCategory(java.lang.String categoryTitle);

	/**
	* Returns the category where categoryTitle = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param categoryTitle the category title
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching category, or <code>null</code> if a matching category could not be found
	*/
	public Category fetchByCategory(java.lang.String categoryTitle,
		boolean retrieveFromCache);

	/**
	* Removes the category where categoryTitle = &#63; from the database.
	*
	* @param categoryTitle the category title
	* @return the category that was removed
	*/
	public Category removeByCategory(java.lang.String categoryTitle)
		throws NoSuchCategoryException;

	/**
	* Returns the number of categories where categoryTitle = &#63;.
	*
	* @param categoryTitle the category title
	* @return the number of matching categories
	*/
	public int countByCategory(java.lang.String categoryTitle);

	/**
	* Returns all the categories where projectRef = &#63; and categoryTitle = &#63;.
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @return the matching categories
	*/
	public java.util.List<Category> findByProjectRefAndCategroy(
		long projectRef, java.lang.String categoryTitle);

	/**
	* Returns a range of all the categories where projectRef = &#63; and categoryTitle = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @return the range of matching categories
	*/
	public java.util.List<Category> findByProjectRefAndCategroy(
		long projectRef, java.lang.String categoryTitle, int start, int end);

	/**
	* Returns an ordered range of all the categories where projectRef = &#63; and categoryTitle = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching categories
	*/
	public java.util.List<Category> findByProjectRefAndCategroy(
		long projectRef, java.lang.String categoryTitle, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator);

	/**
	* Returns an ordered range of all the categories where projectRef = &#63; and categoryTitle = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching categories
	*/
	public java.util.List<Category> findByProjectRefAndCategroy(
		long projectRef, java.lang.String categoryTitle, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first category in the ordered set where projectRef = &#63; and categoryTitle = &#63;.
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public Category findByProjectRefAndCategroy_First(long projectRef,
		java.lang.String categoryTitle,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator)
		throws NoSuchCategoryException;

	/**
	* Returns the first category in the ordered set where projectRef = &#63; and categoryTitle = &#63;.
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching category, or <code>null</code> if a matching category could not be found
	*/
	public Category fetchByProjectRefAndCategroy_First(long projectRef,
		java.lang.String categoryTitle,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator);

	/**
	* Returns the last category in the ordered set where projectRef = &#63; and categoryTitle = &#63;.
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching category
	* @throws NoSuchCategoryException if a matching category could not be found
	*/
	public Category findByProjectRefAndCategroy_Last(long projectRef,
		java.lang.String categoryTitle,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator)
		throws NoSuchCategoryException;

	/**
	* Returns the last category in the ordered set where projectRef = &#63; and categoryTitle = &#63;.
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching category, or <code>null</code> if a matching category could not be found
	*/
	public Category fetchByProjectRefAndCategroy_Last(long projectRef,
		java.lang.String categoryTitle,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator);

	/**
	* Returns the categories before and after the current category in the ordered set where projectRef = &#63; and categoryTitle = &#63;.
	*
	* @param categoryId the primary key of the current category
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next category
	* @throws NoSuchCategoryException if a category with the primary key could not be found
	*/
	public Category[] findByProjectRefAndCategroy_PrevAndNext(long categoryId,
		long projectRef, java.lang.String categoryTitle,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator)
		throws NoSuchCategoryException;

	/**
	* Removes all the categories where projectRef = &#63; and categoryTitle = &#63; from the database.
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	*/
	public void removeByProjectRefAndCategroy(long projectRef,
		java.lang.String categoryTitle);

	/**
	* Returns the number of categories where projectRef = &#63; and categoryTitle = &#63;.
	*
	* @param projectRef the project ref
	* @param categoryTitle the category title
	* @return the number of matching categories
	*/
	public int countByProjectRefAndCategroy(long projectRef,
		java.lang.String categoryTitle);

	/**
	* Caches the category in the entity cache if it is enabled.
	*
	* @param category the category
	*/
	public void cacheResult(Category category);

	/**
	* Caches the categories in the entity cache if it is enabled.
	*
	* @param categories the categories
	*/
	public void cacheResult(java.util.List<Category> categories);

	/**
	* Creates a new category with the primary key. Does not add the category to the database.
	*
	* @param categoryId the primary key for the new category
	* @return the new category
	*/
	public Category create(long categoryId);

	/**
	* Removes the category with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param categoryId the primary key of the category
	* @return the category that was removed
	* @throws NoSuchCategoryException if a category with the primary key could not be found
	*/
	public Category remove(long categoryId) throws NoSuchCategoryException;

	public Category updateImpl(Category category);

	/**
	* Returns the category with the primary key or throws a {@link NoSuchCategoryException} if it could not be found.
	*
	* @param categoryId the primary key of the category
	* @return the category
	* @throws NoSuchCategoryException if a category with the primary key could not be found
	*/
	public Category findByPrimaryKey(long categoryId)
		throws NoSuchCategoryException;

	/**
	* Returns the category with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param categoryId the primary key of the category
	* @return the category, or <code>null</code> if a category with the primary key could not be found
	*/
	public Category fetchByPrimaryKey(long categoryId);

	@Override
	public java.util.Map<java.io.Serializable, Category> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the categories.
	*
	* @return the categories
	*/
	public java.util.List<Category> findAll();

	/**
	* Returns a range of all the categories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @return the range of categories
	*/
	public java.util.List<Category> findAll(int start, int end);

	/**
	* Returns an ordered range of all the categories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of categories
	*/
	public java.util.List<Category> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator);

	/**
	* Returns an ordered range of all the categories.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CategoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of categories
	* @param end the upper bound of the range of categories (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of categories
	*/
	public java.util.List<Category> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Category> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the categories from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of categories.
	*
	* @return the number of categories
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}