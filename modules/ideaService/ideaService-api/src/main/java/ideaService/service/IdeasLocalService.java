/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.exportimport.kernel.lar.PortletDataContext;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import ideaService.model.Ideas;

import ideasService.service.enums.ReviewStatus;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service interface for Ideas. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see IdeasLocalServiceUtil
 * @see ideaService.service.base.IdeasLocalServiceBaseImpl
 * @see ideaService.service.impl.IdeasLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface IdeasLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link IdeasLocalServiceUtil} to access the ideas local service. Add custom service methods to {@link ideaService.service.impl.IdeasLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public boolean addUserToRating(User u, long ideasId);

	public boolean removeUserFromRating(User u, long ideasId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	public DynamicQuery dynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	* Adds the ideas to the database. Also notifies the appropriate model listeners.
	*
	* @param ideas the ideas
	* @return the ideas that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Ideas addIdeas(Ideas ideas);

	/**
	* Creates a new ideas with the primary key. Does not add the ideas to the database.
	*
	* @param ideasId the primary key for the new ideas
	* @return the new ideas
	*/
	public Ideas createIdeas(long ideasId);

	/**
	* Creates a new Idea using the supplied parameters.
	*/
	public Ideas createIdeasWithAutomatedDbId(java.lang.String title,
		long userId, long groupId, long category,
		java.lang.String shortDescription, java.lang.String description,
		double latitude, double longitude, boolean published,
		boolean showOnMap, int rating, long projectIdRef,
		java.lang.String pageUrl, long layoutRef, long videoFileRef,
		java.lang.String videoUrl, ReviewStatus reviewStatus);

	/**
	* This method is used to create a intermediate Idea with less information in the first step of the input process.
	*
	* @param userId
	* @param groupId
	* @param title
	* @return
	*/
	public Ideas createIdeasWithAutomatedDbId(long userId, long projectRef,
		long groupId, long layoutRef, java.lang.String title,
		java.lang.String solution, java.lang.String description,
		java.lang.String importance, java.lang.String targetAudience,
		java.lang.String tags, java.lang.String goal, java.lang.String pageUrl,
		double latitude, double longitude, ReviewStatus reviewStatus,
		boolean isVisibleOnMap);

	/**
	* Deletes the ideas from the database. Also notifies the appropriate model listeners.
	*
	* @param ideas the ideas
	* @return the ideas that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public Ideas deleteIdeas(Ideas ideas);

	/**
	* Deletes the ideas with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas that was removed
	* @throws PortalException if a ideas with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public Ideas deleteIdeas(long ideasId) throws PortalException;

	@Indexable(type = IndexableType.DELETE)
	public Ideas deleteIdeasAndLayoutOnCascade(long id);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Ideas fetchIdeas(long ideasId);

	/**
	* Returns the ideas matching the UUID and group.
	*
	* @param uuid the ideas's UUID
	* @param groupId the primary key of the group
	* @return the matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Ideas fetchIdeasByUuidAndGroupId(java.lang.String uuid, long groupId);

	/**
	* Returns the ideas with the primary key.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas
	* @throws PortalException if a ideas with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Ideas getIdeas(long ideasId) throws PortalException;

	/**
	* Gets all Ideas that reference layoutRef
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Ideas getIdeasByLayoutIdRef(long layoutRef);

	/**
	* Returns the ideas matching the UUID and group.
	*
	* @param uuid the ideas's UUID
	* @param groupId the primary key of the group
	* @return the matching ideas
	* @throws PortalException if a matching ideas could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Ideas getIdeasByUuidAndGroupId(java.lang.String uuid, long groupId)
		throws PortalException;

	/**
	* Updates the ideas in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ideas the ideas
	* @return the ideas that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Ideas updateIdeas(Ideas ideas);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getIdeasRatingCount(long ideasId);

	/**
	* Returns the number of ideases.
	*
	* @return the number of ideases
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getIdeasesCount();

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getPictureUrlByIdeasRefAndPosition(long ideasRef,
		int position);

	public java.lang.String ratingListToString(List<java.lang.String> list);

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> getAllAccpetedIdeas();

	/**
	* @param projectId the project id
	* @return all Ideas with projectRef == projectId
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> getAllIdeasForProject(long projectId);

	/**
	* Finds all ideas with Category cat.
	*
	* @param cat the desired cat
	* @return a list with all ideas that have type equal to input param
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> getIdeasByCategory(long catId);

	/**
	* gets all ideas with published true
	*/
	@java.lang.Deprecated
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> getIdeasByIsPublished(boolean published);

	/**
	* gets all ideas that are visible on the map
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> getIdeasByIsVisibleOnMap(boolean visible);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> getIdeasByUserRoleProjectId(java.lang.String role,
		long userId, long projectId);

	/**
	* Returns a range of all the ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of ideases
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> getIdeases(int start, int end);

	/**
	* Returns all the ideases matching the UUID and company.
	*
	* @param uuid the UUID of the ideases
	* @param companyId the primary key of the company
	* @return the matching ideases, or an empty list if no matches were found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> getIdeasesByUuidAndCompanyId(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of ideases matching the UUID and company.
	*
	* @param uuid the UUID of the ideases
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching ideases, or an empty list if no matches were found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> getIdeasesByUuidAndCompanyId(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Ideas> orderByComparator);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<java.lang.String> getTagsForIdea(long ideasId);

	public List<java.lang.String> ratingStringToList(java.lang.String s);

	/**
	* Searches the DB for the given idea with a field that contains the query.
	* Only returns a result if the query is found in all given fields.
	* Only supports search within text fields.
	*
	* @param fieldNames
	* @param query
	* @param groupIds
	* @return a list of ideas with each containing the query.
	*/
	public List<Ideas> seachIdeasByFieldArray(java.lang.String[] fieldNames,
		java.lang.String query, long[] groupIds);

	/**
	* Searches the DB forthe given idea with a field that contains the query.
	* Only supports search within text fields.
	*
	* @param fieldName
	* @param query
	* @param groupIds
	* @return a list of ideas with each containing the query.
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> searchIdeasByField(java.lang.String fieldName,
		java.lang.String query, long[] groupIds);

	/**
	* Searches the DB for any field that contains the query.
	* Only supports search within text fields.
	*
	* @param fieldNames
	* @param query
	* @param groupIds
	* @return a list of ideas with each containing the query.
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> searchIdeasByFieldArrayQueryArray(
		java.lang.String[] fieldNames, java.lang.String[] queries,
		long[] groupIds);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long getPictureIdByIdeasRefAndPosition(long ideasRef, int position);

	/**
	* This is to be replaced or removed as soon as the elevator pitch functionality (Video recording in the browser) has been implemented.
	*
	* @param ideasId
	* @param pitch
	*/
	public void addElevatorPitchToExisitingIdea(long ideasId,
		java.lang.String pitch);

	public void addPictureToExistingIdea(long ideasId, long pictureId,
		java.lang.String pictureRef, int position);

	public void addVideoToExistingIdea(long ideasId, long videoId,
		java.lang.String videoRef, ThemeDisplay themedisplay);

	@Indexable(type = IndexableType.DELETE)
	public void deletePictureById(long pictureId);

	/**
	* persists the new Idea and performs some checks e.g. if the entry is a duplicate it won't be inserted.
	*
	* @param idea
	*/
	@Indexable(type = IndexableType.REINDEX)
	public void persistIdeasAndPerformTypeChecks(Ideas idea);

	public void setIdeaReviewStatus(long ideasId, ReviewStatus status);
}