/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import ideaService.model.Pictures;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the pictures service. This utility wraps {@link ideaService.service.persistence.impl.PicturesPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PicturesPersistence
 * @see ideaService.service.persistence.impl.PicturesPersistenceImpl
 * @generated
 */
@ProviderType
public class PicturesUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Pictures pictures) {
		getPersistence().clearCache(pictures);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Pictures> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Pictures> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Pictures> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Pictures> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Pictures update(Pictures pictures) {
		return getPersistence().update(pictures);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Pictures update(Pictures pictures,
		ServiceContext serviceContext) {
		return getPersistence().update(pictures, serviceContext);
	}

	/**
	* Returns all the pictureses where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @return the matching pictureses
	*/
	public static List<Pictures> findByIdeasRef(long IdeasRef) {
		return getPersistence().findByIdeasRef(IdeasRef);
	}

	/**
	* Returns a range of all the pictureses where IdeasRef = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param IdeasRef the ideas ref
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @return the range of matching pictureses
	*/
	public static List<Pictures> findByIdeasRef(long IdeasRef, int start,
		int end) {
		return getPersistence().findByIdeasRef(IdeasRef, start, end);
	}

	/**
	* Returns an ordered range of all the pictureses where IdeasRef = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param IdeasRef the ideas ref
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching pictureses
	*/
	public static List<Pictures> findByIdeasRef(long IdeasRef, int start,
		int end, OrderByComparator<Pictures> orderByComparator) {
		return getPersistence()
				   .findByIdeasRef(IdeasRef, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the pictureses where IdeasRef = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param IdeasRef the ideas ref
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching pictureses
	*/
	public static List<Pictures> findByIdeasRef(long IdeasRef, int start,
		int end, OrderByComparator<Pictures> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByIdeasRef(IdeasRef, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first pictures in the ordered set where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching pictures
	* @throws NoSuchPicturesException if a matching pictures could not be found
	*/
	public static Pictures findByIdeasRef_First(long IdeasRef,
		OrderByComparator<Pictures> orderByComparator)
		throws ideaService.exception.NoSuchPicturesException {
		return getPersistence().findByIdeasRef_First(IdeasRef, orderByComparator);
	}

	/**
	* Returns the first pictures in the ordered set where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching pictures, or <code>null</code> if a matching pictures could not be found
	*/
	public static Pictures fetchByIdeasRef_First(long IdeasRef,
		OrderByComparator<Pictures> orderByComparator) {
		return getPersistence()
				   .fetchByIdeasRef_First(IdeasRef, orderByComparator);
	}

	/**
	* Returns the last pictures in the ordered set where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching pictures
	* @throws NoSuchPicturesException if a matching pictures could not be found
	*/
	public static Pictures findByIdeasRef_Last(long IdeasRef,
		OrderByComparator<Pictures> orderByComparator)
		throws ideaService.exception.NoSuchPicturesException {
		return getPersistence().findByIdeasRef_Last(IdeasRef, orderByComparator);
	}

	/**
	* Returns the last pictures in the ordered set where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching pictures, or <code>null</code> if a matching pictures could not be found
	*/
	public static Pictures fetchByIdeasRef_Last(long IdeasRef,
		OrderByComparator<Pictures> orderByComparator) {
		return getPersistence().fetchByIdeasRef_Last(IdeasRef, orderByComparator);
	}

	/**
	* Returns the pictureses before and after the current pictures in the ordered set where IdeasRef = &#63;.
	*
	* @param PictureId the primary key of the current pictures
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next pictures
	* @throws NoSuchPicturesException if a pictures with the primary key could not be found
	*/
	public static Pictures[] findByIdeasRef_PrevAndNext(long PictureId,
		long IdeasRef, OrderByComparator<Pictures> orderByComparator)
		throws ideaService.exception.NoSuchPicturesException {
		return getPersistence()
				   .findByIdeasRef_PrevAndNext(PictureId, IdeasRef,
			orderByComparator);
	}

	/**
	* Removes all the pictureses where IdeasRef = &#63; from the database.
	*
	* @param IdeasRef the ideas ref
	*/
	public static void removeByIdeasRef(long IdeasRef) {
		getPersistence().removeByIdeasRef(IdeasRef);
	}

	/**
	* Returns the number of pictureses where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @return the number of matching pictureses
	*/
	public static int countByIdeasRef(long IdeasRef) {
		return getPersistence().countByIdeasRef(IdeasRef);
	}

	/**
	* Returns the pictures where IdeasRef = &#63; and Position = &#63; or throws a {@link NoSuchPicturesException} if it could not be found.
	*
	* @param IdeasRef the ideas ref
	* @param Position the position
	* @return the matching pictures
	* @throws NoSuchPicturesException if a matching pictures could not be found
	*/
	public static Pictures findByPositionIdeasRef(long IdeasRef, int Position)
		throws ideaService.exception.NoSuchPicturesException {
		return getPersistence().findByPositionIdeasRef(IdeasRef, Position);
	}

	/**
	* Returns the pictures where IdeasRef = &#63; and Position = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param IdeasRef the ideas ref
	* @param Position the position
	* @return the matching pictures, or <code>null</code> if a matching pictures could not be found
	*/
	public static Pictures fetchByPositionIdeasRef(long IdeasRef, int Position) {
		return getPersistence().fetchByPositionIdeasRef(IdeasRef, Position);
	}

	/**
	* Returns the pictures where IdeasRef = &#63; and Position = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param IdeasRef the ideas ref
	* @param Position the position
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching pictures, or <code>null</code> if a matching pictures could not be found
	*/
	public static Pictures fetchByPositionIdeasRef(long IdeasRef, int Position,
		boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByPositionIdeasRef(IdeasRef, Position,
			retrieveFromCache);
	}

	/**
	* Removes the pictures where IdeasRef = &#63; and Position = &#63; from the database.
	*
	* @param IdeasRef the ideas ref
	* @param Position the position
	* @return the pictures that was removed
	*/
	public static Pictures removeByPositionIdeasRef(long IdeasRef, int Position)
		throws ideaService.exception.NoSuchPicturesException {
		return getPersistence().removeByPositionIdeasRef(IdeasRef, Position);
	}

	/**
	* Returns the number of pictureses where IdeasRef = &#63; and Position = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param Position the position
	* @return the number of matching pictureses
	*/
	public static int countByPositionIdeasRef(long IdeasRef, int Position) {
		return getPersistence().countByPositionIdeasRef(IdeasRef, Position);
	}

	/**
	* Returns all the pictureses where PictureId = &#63;.
	*
	* @param PictureId the picture ID
	* @return the matching pictureses
	*/
	public static List<Pictures> findByPicturesId(long PictureId) {
		return getPersistence().findByPicturesId(PictureId);
	}

	/**
	* Returns a range of all the pictureses where PictureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param PictureId the picture ID
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @return the range of matching pictureses
	*/
	public static List<Pictures> findByPicturesId(long PictureId, int start,
		int end) {
		return getPersistence().findByPicturesId(PictureId, start, end);
	}

	/**
	* Returns an ordered range of all the pictureses where PictureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param PictureId the picture ID
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching pictureses
	*/
	public static List<Pictures> findByPicturesId(long PictureId, int start,
		int end, OrderByComparator<Pictures> orderByComparator) {
		return getPersistence()
				   .findByPicturesId(PictureId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the pictureses where PictureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param PictureId the picture ID
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching pictureses
	*/
	public static List<Pictures> findByPicturesId(long PictureId, int start,
		int end, OrderByComparator<Pictures> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByPicturesId(PictureId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first pictures in the ordered set where PictureId = &#63;.
	*
	* @param PictureId the picture ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching pictures
	* @throws NoSuchPicturesException if a matching pictures could not be found
	*/
	public static Pictures findByPicturesId_First(long PictureId,
		OrderByComparator<Pictures> orderByComparator)
		throws ideaService.exception.NoSuchPicturesException {
		return getPersistence()
				   .findByPicturesId_First(PictureId, orderByComparator);
	}

	/**
	* Returns the first pictures in the ordered set where PictureId = &#63;.
	*
	* @param PictureId the picture ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching pictures, or <code>null</code> if a matching pictures could not be found
	*/
	public static Pictures fetchByPicturesId_First(long PictureId,
		OrderByComparator<Pictures> orderByComparator) {
		return getPersistence()
				   .fetchByPicturesId_First(PictureId, orderByComparator);
	}

	/**
	* Returns the last pictures in the ordered set where PictureId = &#63;.
	*
	* @param PictureId the picture ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching pictures
	* @throws NoSuchPicturesException if a matching pictures could not be found
	*/
	public static Pictures findByPicturesId_Last(long PictureId,
		OrderByComparator<Pictures> orderByComparator)
		throws ideaService.exception.NoSuchPicturesException {
		return getPersistence()
				   .findByPicturesId_Last(PictureId, orderByComparator);
	}

	/**
	* Returns the last pictures in the ordered set where PictureId = &#63;.
	*
	* @param PictureId the picture ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching pictures, or <code>null</code> if a matching pictures could not be found
	*/
	public static Pictures fetchByPicturesId_Last(long PictureId,
		OrderByComparator<Pictures> orderByComparator) {
		return getPersistence()
				   .fetchByPicturesId_Last(PictureId, orderByComparator);
	}

	/**
	* Removes all the pictureses where PictureId = &#63; from the database.
	*
	* @param PictureId the picture ID
	*/
	public static void removeByPicturesId(long PictureId) {
		getPersistence().removeByPicturesId(PictureId);
	}

	/**
	* Returns the number of pictureses where PictureId = &#63;.
	*
	* @param PictureId the picture ID
	* @return the number of matching pictureses
	*/
	public static int countByPicturesId(long PictureId) {
		return getPersistence().countByPicturesId(PictureId);
	}

	/**
	* Caches the pictures in the entity cache if it is enabled.
	*
	* @param pictures the pictures
	*/
	public static void cacheResult(Pictures pictures) {
		getPersistence().cacheResult(pictures);
	}

	/**
	* Caches the pictureses in the entity cache if it is enabled.
	*
	* @param pictureses the pictureses
	*/
	public static void cacheResult(List<Pictures> pictureses) {
		getPersistence().cacheResult(pictureses);
	}

	/**
	* Creates a new pictures with the primary key. Does not add the pictures to the database.
	*
	* @param PictureId the primary key for the new pictures
	* @return the new pictures
	*/
	public static Pictures create(long PictureId) {
		return getPersistence().create(PictureId);
	}

	/**
	* Removes the pictures with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param PictureId the primary key of the pictures
	* @return the pictures that was removed
	* @throws NoSuchPicturesException if a pictures with the primary key could not be found
	*/
	public static Pictures remove(long PictureId)
		throws ideaService.exception.NoSuchPicturesException {
		return getPersistence().remove(PictureId);
	}

	public static Pictures updateImpl(Pictures pictures) {
		return getPersistence().updateImpl(pictures);
	}

	/**
	* Returns the pictures with the primary key or throws a {@link NoSuchPicturesException} if it could not be found.
	*
	* @param PictureId the primary key of the pictures
	* @return the pictures
	* @throws NoSuchPicturesException if a pictures with the primary key could not be found
	*/
	public static Pictures findByPrimaryKey(long PictureId)
		throws ideaService.exception.NoSuchPicturesException {
		return getPersistence().findByPrimaryKey(PictureId);
	}

	/**
	* Returns the pictures with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param PictureId the primary key of the pictures
	* @return the pictures, or <code>null</code> if a pictures with the primary key could not be found
	*/
	public static Pictures fetchByPrimaryKey(long PictureId) {
		return getPersistence().fetchByPrimaryKey(PictureId);
	}

	public static java.util.Map<java.io.Serializable, Pictures> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the pictureses.
	*
	* @return the pictureses
	*/
	public static List<Pictures> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the pictureses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @return the range of pictureses
	*/
	public static List<Pictures> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the pictureses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of pictureses
	*/
	public static List<Pictures> findAll(int start, int end,
		OrderByComparator<Pictures> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the pictureses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PicturesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of pictureses
	* @param end the upper bound of the range of pictureses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of pictureses
	*/
	public static List<Pictures> findAll(int start, int end,
		OrderByComparator<Pictures> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the pictureses from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of pictureses.
	*
	* @return the number of pictureses
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static PicturesPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<PicturesPersistence, PicturesPersistence> _serviceTracker =
		ServiceTrackerFactory.open(PicturesPersistence.class);
}