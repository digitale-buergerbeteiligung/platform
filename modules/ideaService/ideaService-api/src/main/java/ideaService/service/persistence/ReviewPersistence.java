/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import ideaService.exception.NoSuchReviewException;

import ideaService.model.Review;

/**
 * The persistence interface for the review service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ideaService.service.persistence.impl.ReviewPersistenceImpl
 * @see ReviewUtil
 * @generated
 */
@ProviderType
public interface ReviewPersistence extends BasePersistence<Review> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ReviewUtil} to access the review persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the reviews where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching reviews
	*/
	public java.util.List<Review> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the reviews where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of reviews
	* @param end the upper bound of the range of reviews (not inclusive)
	* @return the range of matching reviews
	*/
	public java.util.List<Review> findByUuid(java.lang.String uuid, int start,
		int end);

	/**
	* Returns an ordered range of all the reviews where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of reviews
	* @param end the upper bound of the range of reviews (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reviews
	*/
	public java.util.List<Review> findByUuid(java.lang.String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Review> orderByComparator);

	/**
	* Returns an ordered range of all the reviews where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of reviews
	* @param end the upper bound of the range of reviews (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching reviews
	*/
	public java.util.List<Review> findByUuid(java.lang.String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Review> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first review in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching review
	* @throws NoSuchReviewException if a matching review could not be found
	*/
	public Review findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Review> orderByComparator)
		throws NoSuchReviewException;

	/**
	* Returns the first review in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching review, or <code>null</code> if a matching review could not be found
	*/
	public Review fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Review> orderByComparator);

	/**
	* Returns the last review in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching review
	* @throws NoSuchReviewException if a matching review could not be found
	*/
	public Review findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Review> orderByComparator)
		throws NoSuchReviewException;

	/**
	* Returns the last review in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching review, or <code>null</code> if a matching review could not be found
	*/
	public Review fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Review> orderByComparator);

	/**
	* Returns the reviews before and after the current review in the ordered set where uuid = &#63;.
	*
	* @param reviewId the primary key of the current review
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next review
	* @throws NoSuchReviewException if a review with the primary key could not be found
	*/
	public Review[] findByUuid_PrevAndNext(long reviewId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Review> orderByComparator)
		throws NoSuchReviewException;

	/**
	* Removes all the reviews where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of reviews where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching reviews
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns the review where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchReviewException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching review
	* @throws NoSuchReviewException if a matching review could not be found
	*/
	public Review findByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchReviewException;

	/**
	* Returns the review where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching review, or <code>null</code> if a matching review could not be found
	*/
	public Review fetchByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns the review where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching review, or <code>null</code> if a matching review could not be found
	*/
	public Review fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the review where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the review that was removed
	*/
	public Review removeByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchReviewException;

	/**
	* Returns the number of reviews where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching reviews
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns all the reviews where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching reviews
	*/
	public java.util.List<Review> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the reviews where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of reviews
	* @param end the upper bound of the range of reviews (not inclusive)
	* @return the range of matching reviews
	*/
	public java.util.List<Review> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the reviews where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of reviews
	* @param end the upper bound of the range of reviews (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching reviews
	*/
	public java.util.List<Review> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Review> orderByComparator);

	/**
	* Returns an ordered range of all the reviews where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of reviews
	* @param end the upper bound of the range of reviews (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching reviews
	*/
	public java.util.List<Review> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Review> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first review in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching review
	* @throws NoSuchReviewException if a matching review could not be found
	*/
	public Review findByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Review> orderByComparator)
		throws NoSuchReviewException;

	/**
	* Returns the first review in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching review, or <code>null</code> if a matching review could not be found
	*/
	public Review fetchByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Review> orderByComparator);

	/**
	* Returns the last review in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching review
	* @throws NoSuchReviewException if a matching review could not be found
	*/
	public Review findByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Review> orderByComparator)
		throws NoSuchReviewException;

	/**
	* Returns the last review in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching review, or <code>null</code> if a matching review could not be found
	*/
	public Review fetchByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Review> orderByComparator);

	/**
	* Returns the reviews before and after the current review in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param reviewId the primary key of the current review
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next review
	* @throws NoSuchReviewException if a review with the primary key could not be found
	*/
	public Review[] findByUuid_C_PrevAndNext(long reviewId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Review> orderByComparator)
		throws NoSuchReviewException;

	/**
	* Removes all the reviews where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of reviews where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching reviews
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Caches the review in the entity cache if it is enabled.
	*
	* @param review the review
	*/
	public void cacheResult(Review review);

	/**
	* Caches the reviews in the entity cache if it is enabled.
	*
	* @param reviews the reviews
	*/
	public void cacheResult(java.util.List<Review> reviews);

	/**
	* Creates a new review with the primary key. Does not add the review to the database.
	*
	* @param reviewId the primary key for the new review
	* @return the new review
	*/
	public Review create(long reviewId);

	/**
	* Removes the review with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param reviewId the primary key of the review
	* @return the review that was removed
	* @throws NoSuchReviewException if a review with the primary key could not be found
	*/
	public Review remove(long reviewId) throws NoSuchReviewException;

	public Review updateImpl(Review review);

	/**
	* Returns the review with the primary key or throws a {@link NoSuchReviewException} if it could not be found.
	*
	* @param reviewId the primary key of the review
	* @return the review
	* @throws NoSuchReviewException if a review with the primary key could not be found
	*/
	public Review findByPrimaryKey(long reviewId) throws NoSuchReviewException;

	/**
	* Returns the review with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param reviewId the primary key of the review
	* @return the review, or <code>null</code> if a review with the primary key could not be found
	*/
	public Review fetchByPrimaryKey(long reviewId);

	@Override
	public java.util.Map<java.io.Serializable, Review> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the reviews.
	*
	* @return the reviews
	*/
	public java.util.List<Review> findAll();

	/**
	* Returns a range of all the reviews.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reviews
	* @param end the upper bound of the range of reviews (not inclusive)
	* @return the range of reviews
	*/
	public java.util.List<Review> findAll(int start, int end);

	/**
	* Returns an ordered range of all the reviews.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reviews
	* @param end the upper bound of the range of reviews (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of reviews
	*/
	public java.util.List<Review> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Review> orderByComparator);

	/**
	* Returns an ordered range of all the reviews.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ReviewModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of reviews
	* @param end the upper bound of the range of reviews (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of reviews
	*/
	public java.util.List<Review> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Review> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the reviews from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of reviews.
	*
	* @return the number of reviews
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}