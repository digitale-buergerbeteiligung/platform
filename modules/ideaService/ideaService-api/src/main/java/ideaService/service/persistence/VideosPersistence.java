/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import ideaService.exception.NoSuchVideosException;

import ideaService.model.Videos;

/**
 * The persistence interface for the videos service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ideaService.service.persistence.impl.VideosPersistenceImpl
 * @see VideosUtil
 * @generated
 */
@ProviderType
public interface VideosPersistence extends BasePersistence<Videos> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link VideosUtil} to access the videos persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the videoses where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @return the matching videoses
	*/
	public java.util.List<Videos> findByIdeasRef(long IdeasRef);

	/**
	* Returns a range of all the videoses where IdeasRef = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param IdeasRef the ideas ref
	* @param start the lower bound of the range of videoses
	* @param end the upper bound of the range of videoses (not inclusive)
	* @return the range of matching videoses
	*/
	public java.util.List<Videos> findByIdeasRef(long IdeasRef, int start,
		int end);

	/**
	* Returns an ordered range of all the videoses where IdeasRef = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param IdeasRef the ideas ref
	* @param start the lower bound of the range of videoses
	* @param end the upper bound of the range of videoses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching videoses
	*/
	public java.util.List<Videos> findByIdeasRef(long IdeasRef, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Videos> orderByComparator);

	/**
	* Returns an ordered range of all the videoses where IdeasRef = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param IdeasRef the ideas ref
	* @param start the lower bound of the range of videoses
	* @param end the upper bound of the range of videoses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching videoses
	*/
	public java.util.List<Videos> findByIdeasRef(long IdeasRef, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Videos> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first videos in the ordered set where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching videos
	* @throws NoSuchVideosException if a matching videos could not be found
	*/
	public Videos findByIdeasRef_First(long IdeasRef,
		com.liferay.portal.kernel.util.OrderByComparator<Videos> orderByComparator)
		throws NoSuchVideosException;

	/**
	* Returns the first videos in the ordered set where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching videos, or <code>null</code> if a matching videos could not be found
	*/
	public Videos fetchByIdeasRef_First(long IdeasRef,
		com.liferay.portal.kernel.util.OrderByComparator<Videos> orderByComparator);

	/**
	* Returns the last videos in the ordered set where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching videos
	* @throws NoSuchVideosException if a matching videos could not be found
	*/
	public Videos findByIdeasRef_Last(long IdeasRef,
		com.liferay.portal.kernel.util.OrderByComparator<Videos> orderByComparator)
		throws NoSuchVideosException;

	/**
	* Returns the last videos in the ordered set where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching videos, or <code>null</code> if a matching videos could not be found
	*/
	public Videos fetchByIdeasRef_Last(long IdeasRef,
		com.liferay.portal.kernel.util.OrderByComparator<Videos> orderByComparator);

	/**
	* Returns the videoses before and after the current videos in the ordered set where IdeasRef = &#63;.
	*
	* @param VideoId the primary key of the current videos
	* @param IdeasRef the ideas ref
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next videos
	* @throws NoSuchVideosException if a videos with the primary key could not be found
	*/
	public Videos[] findByIdeasRef_PrevAndNext(long VideoId, long IdeasRef,
		com.liferay.portal.kernel.util.OrderByComparator<Videos> orderByComparator)
		throws NoSuchVideosException;

	/**
	* Removes all the videoses where IdeasRef = &#63; from the database.
	*
	* @param IdeasRef the ideas ref
	*/
	public void removeByIdeasRef(long IdeasRef);

	/**
	* Returns the number of videoses where IdeasRef = &#63;.
	*
	* @param IdeasRef the ideas ref
	* @return the number of matching videoses
	*/
	public int countByIdeasRef(long IdeasRef);

	/**
	* Returns the videos where Extension = &#63; or throws a {@link NoSuchVideosException} if it could not be found.
	*
	* @param Extension the extension
	* @return the matching videos
	* @throws NoSuchVideosException if a matching videos could not be found
	*/
	public Videos findByExtension(java.lang.String Extension)
		throws NoSuchVideosException;

	/**
	* Returns the videos where Extension = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param Extension the extension
	* @return the matching videos, or <code>null</code> if a matching videos could not be found
	*/
	public Videos fetchByExtension(java.lang.String Extension);

	/**
	* Returns the videos where Extension = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param Extension the extension
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching videos, or <code>null</code> if a matching videos could not be found
	*/
	public Videos fetchByExtension(java.lang.String Extension,
		boolean retrieveFromCache);

	/**
	* Removes the videos where Extension = &#63; from the database.
	*
	* @param Extension the extension
	* @return the videos that was removed
	*/
	public Videos removeByExtension(java.lang.String Extension)
		throws NoSuchVideosException;

	/**
	* Returns the number of videoses where Extension = &#63;.
	*
	* @param Extension the extension
	* @return the number of matching videoses
	*/
	public int countByExtension(java.lang.String Extension);

	/**
	* Caches the videos in the entity cache if it is enabled.
	*
	* @param videos the videos
	*/
	public void cacheResult(Videos videos);

	/**
	* Caches the videoses in the entity cache if it is enabled.
	*
	* @param videoses the videoses
	*/
	public void cacheResult(java.util.List<Videos> videoses);

	/**
	* Creates a new videos with the primary key. Does not add the videos to the database.
	*
	* @param VideoId the primary key for the new videos
	* @return the new videos
	*/
	public Videos create(long VideoId);

	/**
	* Removes the videos with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param VideoId the primary key of the videos
	* @return the videos that was removed
	* @throws NoSuchVideosException if a videos with the primary key could not be found
	*/
	public Videos remove(long VideoId) throws NoSuchVideosException;

	public Videos updateImpl(Videos videos);

	/**
	* Returns the videos with the primary key or throws a {@link NoSuchVideosException} if it could not be found.
	*
	* @param VideoId the primary key of the videos
	* @return the videos
	* @throws NoSuchVideosException if a videos with the primary key could not be found
	*/
	public Videos findByPrimaryKey(long VideoId) throws NoSuchVideosException;

	/**
	* Returns the videos with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param VideoId the primary key of the videos
	* @return the videos, or <code>null</code> if a videos with the primary key could not be found
	*/
	public Videos fetchByPrimaryKey(long VideoId);

	@Override
	public java.util.Map<java.io.Serializable, Videos> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the videoses.
	*
	* @return the videoses
	*/
	public java.util.List<Videos> findAll();

	/**
	* Returns a range of all the videoses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of videoses
	* @param end the upper bound of the range of videoses (not inclusive)
	* @return the range of videoses
	*/
	public java.util.List<Videos> findAll(int start, int end);

	/**
	* Returns an ordered range of all the videoses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of videoses
	* @param end the upper bound of the range of videoses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of videoses
	*/
	public java.util.List<Videos> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Videos> orderByComparator);

	/**
	* Returns an ordered range of all the videoses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link VideosModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of videoses
	* @param end the upper bound of the range of videoses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of videoses
	*/
	public java.util.List<Videos> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Videos> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the videoses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of videoses.
	*
	* @return the number of videoses
	*/
	public int countAll();
}