/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link IdeasLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see IdeasLocalService
 * @generated
 */
@ProviderType
public class IdeasLocalServiceWrapper implements IdeasLocalService,
	ServiceWrapper<IdeasLocalService> {
	public IdeasLocalServiceWrapper(IdeasLocalService ideasLocalService) {
		_ideasLocalService = ideasLocalService;
	}

	@Override
	public boolean addUserToRating(com.liferay.portal.kernel.model.User u,
		long ideasId) {
		return _ideasLocalService.addUserToRating(u, ideasId);
	}

	@Override
	public boolean removeUserFromRating(
		com.liferay.portal.kernel.model.User u, long ideasId) {
		return _ideasLocalService.removeUserFromRating(u, ideasId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _ideasLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _ideasLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _ideasLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _ideasLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ideasLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ideasLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Adds the ideas to the database. Also notifies the appropriate model listeners.
	*
	* @param ideas the ideas
	* @return the ideas that was added
	*/
	@Override
	public ideaService.model.Ideas addIdeas(ideaService.model.Ideas ideas) {
		return _ideasLocalService.addIdeas(ideas);
	}

	/**
	* Creates a new ideas with the primary key. Does not add the ideas to the database.
	*
	* @param ideasId the primary key for the new ideas
	* @return the new ideas
	*/
	@Override
	public ideaService.model.Ideas createIdeas(long ideasId) {
		return _ideasLocalService.createIdeas(ideasId);
	}

	/**
	* Creates a new Idea using the supplied parameters.
	*/
	@Override
	public ideaService.model.Ideas createIdeasWithAutomatedDbId(
		java.lang.String title, long userId, long groupId, long category,
		java.lang.String shortDescription, java.lang.String description,
		double latitude, double longitude, boolean published,
		boolean showOnMap, int rating, long projectIdRef,
		java.lang.String pageUrl, long layoutRef, long videoFileRef,
		java.lang.String videoUrl,
		ideasService.service.enums.ReviewStatus reviewStatus) {
		return _ideasLocalService.createIdeasWithAutomatedDbId(title, userId,
			groupId, category, shortDescription, description, latitude,
			longitude, published, showOnMap, rating, projectIdRef, pageUrl,
			layoutRef, videoFileRef, videoUrl, reviewStatus);
	}

	/**
	* This method is used to create a intermediate Idea with less information in the first step of the input process.
	*
	* @param userId
	* @param groupId
	* @param title
	* @return
	*/
	@Override
	public ideaService.model.Ideas createIdeasWithAutomatedDbId(long userId,
		long projectRef, long groupId, long layoutRef, java.lang.String title,
		java.lang.String solution, java.lang.String description,
		java.lang.String importance, java.lang.String targetAudience,
		java.lang.String tags, java.lang.String goal, java.lang.String pageUrl,
		double latitude, double longitude,
		ideasService.service.enums.ReviewStatus reviewStatus,
		boolean isVisibleOnMap) {
		return _ideasLocalService.createIdeasWithAutomatedDbId(userId,
			projectRef, groupId, layoutRef, title, solution, description,
			importance, targetAudience, tags, goal, pageUrl, latitude,
			longitude, reviewStatus, isVisibleOnMap);
	}

	/**
	* Deletes the ideas from the database. Also notifies the appropriate model listeners.
	*
	* @param ideas the ideas
	* @return the ideas that was removed
	*/
	@Override
	public ideaService.model.Ideas deleteIdeas(ideaService.model.Ideas ideas) {
		return _ideasLocalService.deleteIdeas(ideas);
	}

	/**
	* Deletes the ideas with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas that was removed
	* @throws PortalException if a ideas with the primary key could not be found
	*/
	@Override
	public ideaService.model.Ideas deleteIdeas(long ideasId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ideasLocalService.deleteIdeas(ideasId);
	}

	@Override
	public ideaService.model.Ideas deleteIdeasAndLayoutOnCascade(long id) {
		return _ideasLocalService.deleteIdeasAndLayoutOnCascade(id);
	}

	@Override
	public ideaService.model.Ideas fetchIdeas(long ideasId) {
		return _ideasLocalService.fetchIdeas(ideasId);
	}

	/**
	* Returns the ideas matching the UUID and group.
	*
	* @param uuid the ideas's UUID
	* @param groupId the primary key of the group
	* @return the matching ideas, or <code>null</code> if a matching ideas could not be found
	*/
	@Override
	public ideaService.model.Ideas fetchIdeasByUuidAndGroupId(
		java.lang.String uuid, long groupId) {
		return _ideasLocalService.fetchIdeasByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns the ideas with the primary key.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas
	* @throws PortalException if a ideas with the primary key could not be found
	*/
	@Override
	public ideaService.model.Ideas getIdeas(long ideasId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ideasLocalService.getIdeas(ideasId);
	}

	/**
	* Gets all Ideas that reference layoutRef
	*/
	@Override
	public ideaService.model.Ideas getIdeasByLayoutIdRef(long layoutRef) {
		return _ideasLocalService.getIdeasByLayoutIdRef(layoutRef);
	}

	/**
	* Returns the ideas matching the UUID and group.
	*
	* @param uuid the ideas's UUID
	* @param groupId the primary key of the group
	* @return the matching ideas
	* @throws PortalException if a matching ideas could not be found
	*/
	@Override
	public ideaService.model.Ideas getIdeasByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ideasLocalService.getIdeasByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Updates the ideas in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ideas the ideas
	* @return the ideas that was updated
	*/
	@Override
	public ideaService.model.Ideas updateIdeas(ideaService.model.Ideas ideas) {
		return _ideasLocalService.updateIdeas(ideas);
	}

	@Override
	public int getIdeasRatingCount(long ideasId) {
		return _ideasLocalService.getIdeasRatingCount(ideasId);
	}

	/**
	* Returns the number of ideases.
	*
	* @return the number of ideases
	*/
	@Override
	public int getIdeasesCount() {
		return _ideasLocalService.getIdeasesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _ideasLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public java.lang.String getPictureUrlByIdeasRefAndPosition(long ideasRef,
		int position) {
		return _ideasLocalService.getPictureUrlByIdeasRefAndPosition(ideasRef,
			position);
	}

	@Override
	public java.lang.String ratingListToString(
		java.util.List<java.lang.String> list) {
		return _ideasLocalService.ratingListToString(list);
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _ideasLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _ideasLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _ideasLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	@Override
	public java.util.List<ideaService.model.Ideas> getAllAccpetedIdeas() {
		return _ideasLocalService.getAllAccpetedIdeas();
	}

	/**
	* @param projectId the project id
	* @return all Ideas with projectRef == projectId
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> getAllIdeasForProject(
		long projectId) {
		return _ideasLocalService.getAllIdeasForProject(projectId);
	}

	/**
	* Finds all ideas with Category cat.
	*
	* @param cat the desired cat
	* @return a list with all ideas that have type equal to input param
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> getIdeasByCategory(
		long catId) {
		return _ideasLocalService.getIdeasByCategory(catId);
	}

	/**
	* gets all ideas with published true
	*/
	@Deprecated
	@Override
	public java.util.List<ideaService.model.Ideas> getIdeasByIsPublished(
		boolean published) {
		return _ideasLocalService.getIdeasByIsPublished(published);
	}

	/**
	* gets all ideas that are visible on the map
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> getIdeasByIsVisibleOnMap(
		boolean visible) {
		return _ideasLocalService.getIdeasByIsVisibleOnMap(visible);
	}

	@Override
	public java.util.List<ideaService.model.Ideas> getIdeasByUserRoleProjectId(
		java.lang.String role, long userId, long projectId) {
		return _ideasLocalService.getIdeasByUserRoleProjectId(role, userId,
			projectId);
	}

	/**
	* Returns a range of all the ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of ideases
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> getIdeases(int start, int end) {
		return _ideasLocalService.getIdeases(start, end);
	}

	/**
	* Returns all the ideases matching the UUID and company.
	*
	* @param uuid the UUID of the ideases
	* @param companyId the primary key of the company
	* @return the matching ideases, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> getIdeasesByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return _ideasLocalService.getIdeasesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns a range of ideases matching the UUID and company.
	*
	* @param uuid the UUID of the ideases
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching ideases, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> getIdeasesByUuidAndCompanyId(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ideaService.model.Ideas> orderByComparator) {
		return _ideasLocalService.getIdeasesByUuidAndCompanyId(uuid, companyId,
			start, end, orderByComparator);
	}

	@Override
	public java.util.List<java.lang.String> getTagsForIdea(long ideasId) {
		return _ideasLocalService.getTagsForIdea(ideasId);
	}

	@Override
	public java.util.List<java.lang.String> ratingStringToList(
		java.lang.String s) {
		return _ideasLocalService.ratingStringToList(s);
	}

	/**
	* Searches the DB for the given idea with a field that contains the query.
	* Only returns a result if the query is found in all given fields.
	* Only supports search within text fields.
	*
	* @param fieldNames
	* @param query
	* @param groupIds
	* @return a list of ideas with each containing the query.
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> seachIdeasByFieldArray(
		java.lang.String[] fieldNames, java.lang.String query, long[] groupIds) {
		return _ideasLocalService.seachIdeasByFieldArray(fieldNames, query,
			groupIds);
	}

	/**
	* Searches the DB forthe given idea with a field that contains the query.
	* Only supports search within text fields.
	*
	* @param fieldName
	* @param query
	* @param groupIds
	* @return a list of ideas with each containing the query.
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> searchIdeasByField(
		java.lang.String fieldName, java.lang.String query, long[] groupIds) {
		return _ideasLocalService.searchIdeasByField(fieldName, query, groupIds);
	}

	/**
	* Searches the DB for any field that contains the query.
	* Only supports search within text fields.
	*
	* @param fieldNames
	* @param query
	* @param groupIds
	* @return a list of ideas with each containing the query.
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> searchIdeasByFieldArrayQueryArray(
		java.lang.String[] fieldNames, java.lang.String[] queries,
		long[] groupIds) {
		return _ideasLocalService.searchIdeasByFieldArrayQueryArray(fieldNames,
			queries, groupIds);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _ideasLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _ideasLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public long getPictureIdByIdeasRefAndPosition(long ideasRef, int position) {
		return _ideasLocalService.getPictureIdByIdeasRefAndPosition(ideasRef,
			position);
	}

	/**
	* This is to be replaced or removed as soon as the elevator pitch functionality (Video recording in the browser) has been implemented.
	*
	* @param ideasId
	* @param pitch
	*/
	@Override
	public void addElevatorPitchToExisitingIdea(long ideasId,
		java.lang.String pitch) {
		_ideasLocalService.addElevatorPitchToExisitingIdea(ideasId, pitch);
	}

	@Override
	public void addPictureToExistingIdea(long ideasId, long pictureId,
		java.lang.String pictureRef, int position) {
		_ideasLocalService.addPictureToExistingIdea(ideasId, pictureId,
			pictureRef, position);
	}

	@Override
	public void addVideoToExistingIdea(long ideasId, long videoId,
		java.lang.String videoRef,
		com.liferay.portal.kernel.theme.ThemeDisplay themedisplay) {
		_ideasLocalService.addVideoToExistingIdea(ideasId, videoId, videoRef,
			themedisplay);
	}

	@Override
	public void deletePictureById(long pictureId) {
		_ideasLocalService.deletePictureById(pictureId);
	}

	/**
	* persists the new Idea and performs some checks e.g. if the entry is a duplicate it won't be inserted.
	*
	* @param idea
	*/
	@Override
	public void persistIdeasAndPerformTypeChecks(ideaService.model.Ideas idea) {
		_ideasLocalService.persistIdeasAndPerformTypeChecks(idea);
	}

	@Override
	public void setIdeaReviewStatus(long ideasId,
		ideasService.service.enums.ReviewStatus status) {
		_ideasLocalService.setIdeaReviewStatus(ideasId, status);
	}

	@Override
	public IdeasLocalService getWrappedService() {
		return _ideasLocalService;
	}

	@Override
	public void setWrappedService(IdeasLocalService ideasLocalService) {
		_ideasLocalService = ideasLocalService;
	}

	private IdeasLocalService _ideasLocalService;
}