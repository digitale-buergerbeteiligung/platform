/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link IdeasService}.
 *
 * @author Brian Wing Shun Chan
 * @see IdeasService
 * @generated
 */
@ProviderType
public class IdeasServiceWrapper implements IdeasService,
	ServiceWrapper<IdeasService> {
	public IdeasServiceWrapper(IdeasService ideasService) {
		_ideasService = ideasService;
	}

	/**
	* @param pk the primaryKey
	* @param rating the rating to set
	* @return reponse json
	*/
	@Override
	public java.lang.String deleteIdea(java.lang.String Id) {
		return _ideasService.deleteIdea(Id);
	}

	/**
	* finds and returns all ideas.
	*/
	@Override
	public java.lang.String getIdeaById(long id) {
		return _ideasService.getIdeaById(id);
	}

	/**
	* Finds all ideas with Category category.
	* This is a remote function. Not to be used locally!
	*
	* @param cat the desired cat
	* @return a list with all ideas that have type equal to input param
	*/
	@Override
	public java.lang.String getIdeasByCategory(long categoryId) {
		return _ideasService.getIdeasByCategory(categoryId);
	}

	@Override
	public java.lang.String getIdeasByIsPublished(boolean published) {
		return _ideasService.getIdeasByIsPublished(published);
	}

	@Override
	public java.lang.String getIdeasByIsVisibleOnMap(boolean visible) {
		return _ideasService.getIdeasByIsVisibleOnMap(visible);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _ideasService.getOSGiServiceIdentifier();
	}

	@Override
	public java.lang.String seachIdeasByFieldArray(
		java.lang.String[] fieldNames, java.lang.String query, long[] groupIds) {
		return _ideasService.seachIdeasByFieldArray(fieldNames, query, groupIds);
	}

	/**
	* @param ratingRangeStart
	* @param ratingRangeEnd
	* @return all ideas with the given range.
	*/
	@Override
	public java.lang.String searchIdeasByField(java.lang.String fieldName,
		java.lang.String query, long[] groupIds) {
		return _ideasService.searchIdeasByField(fieldName, query, groupIds);
	}

	@Override
	public java.lang.String searchIdeasByFieldArrayQueryArray(
		java.lang.String[] fieldNames, java.lang.String[] queries,
		long[] groupIds) {
		return _ideasService.searchIdeasByFieldArrayQueryArray(fieldNames,
			queries, groupIds);
	}

	@Override
	public IdeasService getWrappedService() {
		return _ideasService;
	}

	@Override
	public void setWrappedService(IdeasService ideasService) {
		_ideasService = ideasService;
	}

	private IdeasService _ideasService;
}