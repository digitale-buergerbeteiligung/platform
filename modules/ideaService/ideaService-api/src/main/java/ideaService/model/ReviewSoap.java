/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link ideaService.service.http.ReviewServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see ideaService.service.http.ReviewServiceSoap
 * @generated
 */
@ProviderType
public class ReviewSoap implements Serializable {
	public static ReviewSoap toSoapModel(Review model) {
		ReviewSoap soapModel = new ReviewSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setReviewId(model.getReviewId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setAccepted(model.getAccepted());
		soapModel.setFeedback(model.getFeedback());
		soapModel.setIdeasIdRef(model.getIdeasIdRef());

		return soapModel;
	}

	public static ReviewSoap[] toSoapModels(Review[] models) {
		ReviewSoap[] soapModels = new ReviewSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ReviewSoap[][] toSoapModels(Review[][] models) {
		ReviewSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ReviewSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ReviewSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ReviewSoap[] toSoapModels(List<Review> models) {
		List<ReviewSoap> soapModels = new ArrayList<ReviewSoap>(models.size());

		for (Review model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ReviewSoap[soapModels.size()]);
	}

	public ReviewSoap() {
	}

	public long getPrimaryKey() {
		return _reviewId;
	}

	public void setPrimaryKey(long pk) {
		setReviewId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getReviewId() {
		return _reviewId;
	}

	public void setReviewId(long reviewId) {
		_reviewId = reviewId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public boolean getAccepted() {
		return _accepted;
	}

	public boolean isAccepted() {
		return _accepted;
	}

	public void setAccepted(boolean accepted) {
		_accepted = accepted;
	}

	public String getFeedback() {
		return _feedback;
	}

	public void setFeedback(String feedback) {
		_feedback = feedback;
	}

	public long getIdeasIdRef() {
		return _ideasIdRef;
	}

	public void setIdeasIdRef(long ideasIdRef) {
		_ideasIdRef = ideasIdRef;
	}

	private String _uuid;
	private long _reviewId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private boolean _accepted;
	private String _feedback;
	private long _ideasIdRef;
}