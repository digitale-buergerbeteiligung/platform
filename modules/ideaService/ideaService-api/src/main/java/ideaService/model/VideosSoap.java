/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link ideaService.service.http.VideosServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see ideaService.service.http.VideosServiceSoap
 * @generated
 */
@ProviderType
public class VideosSoap implements Serializable {
	public static VideosSoap toSoapModel(Videos model) {
		VideosSoap soapModel = new VideosSoap();

		soapModel.setVideoId(model.getVideoId());
		soapModel.setIdeasRef(model.getIdeasRef());
		soapModel.setFileRef(model.getFileRef());
		soapModel.setVideoUrl(model.getVideoUrl());
		soapModel.setExtension(model.getExtension());

		return soapModel;
	}

	public static VideosSoap[] toSoapModels(Videos[] models) {
		VideosSoap[] soapModels = new VideosSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static VideosSoap[][] toSoapModels(Videos[][] models) {
		VideosSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new VideosSoap[models.length][models[0].length];
		}
		else {
			soapModels = new VideosSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static VideosSoap[] toSoapModels(List<Videos> models) {
		List<VideosSoap> soapModels = new ArrayList<VideosSoap>(models.size());

		for (Videos model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new VideosSoap[soapModels.size()]);
	}

	public VideosSoap() {
	}

	public long getPrimaryKey() {
		return _VideoId;
	}

	public void setPrimaryKey(long pk) {
		setVideoId(pk);
	}

	public long getVideoId() {
		return _VideoId;
	}

	public void setVideoId(long VideoId) {
		_VideoId = VideoId;
	}

	public long getIdeasRef() {
		return _IdeasRef;
	}

	public void setIdeasRef(long IdeasRef) {
		_IdeasRef = IdeasRef;
	}

	public long getFileRef() {
		return _FileRef;
	}

	public void setFileRef(long FileRef) {
		_FileRef = FileRef;
	}

	public String getVideoUrl() {
		return _VideoUrl;
	}

	public void setVideoUrl(String VideoUrl) {
		_VideoUrl = VideoUrl;
	}

	public String getExtension() {
		return _Extension;
	}

	public void setExtension(String Extension) {
		_Extension = Extension;
	}

	private long _VideoId;
	private long _IdeasRef;
	private long _FileRef;
	private String _VideoUrl;
	private String _Extension;
}