/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Videos}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Videos
 * @generated
 */
@ProviderType
public class VideosWrapper implements Videos, ModelWrapper<Videos> {
	public VideosWrapper(Videos videos) {
		_videos = videos;
	}

	@Override
	public Class<?> getModelClass() {
		return Videos.class;
	}

	@Override
	public String getModelClassName() {
		return Videos.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("VideoId", getVideoId());
		attributes.put("IdeasRef", getIdeasRef());
		attributes.put("FileRef", getFileRef());
		attributes.put("VideoUrl", getVideoUrl());
		attributes.put("Extension", getExtension());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long VideoId = (Long)attributes.get("VideoId");

		if (VideoId != null) {
			setVideoId(VideoId);
		}

		Long IdeasRef = (Long)attributes.get("IdeasRef");

		if (IdeasRef != null) {
			setIdeasRef(IdeasRef);
		}

		Long FileRef = (Long)attributes.get("FileRef");

		if (FileRef != null) {
			setFileRef(FileRef);
		}

		String VideoUrl = (String)attributes.get("VideoUrl");

		if (VideoUrl != null) {
			setVideoUrl(VideoUrl);
		}

		String Extension = (String)attributes.get("Extension");

		if (Extension != null) {
			setExtension(Extension);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _videos.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _videos.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _videos.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _videos.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ideaService.model.Videos> toCacheModel() {
		return _videos.toCacheModel();
	}

	@Override
	public ideaService.model.Videos toEscapedModel() {
		return new VideosWrapper(_videos.toEscapedModel());
	}

	@Override
	public ideaService.model.Videos toUnescapedModel() {
		return new VideosWrapper(_videos.toUnescapedModel());
	}

	@Override
	public int compareTo(ideaService.model.Videos videos) {
		return _videos.compareTo(videos);
	}

	@Override
	public int hashCode() {
		return _videos.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _videos.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new VideosWrapper((Videos)_videos.clone());
	}

	/**
	* Returns the extension of this videos.
	*
	* @return the extension of this videos
	*/
	@Override
	public java.lang.String getExtension() {
		return _videos.getExtension();
	}

	/**
	* Returns the video url of this videos.
	*
	* @return the video url of this videos
	*/
	@Override
	public java.lang.String getVideoUrl() {
		return _videos.getVideoUrl();
	}

	@Override
	public java.lang.String toString() {
		return _videos.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _videos.toXmlString();
	}

	/**
	* Returns the file ref of this videos.
	*
	* @return the file ref of this videos
	*/
	@Override
	public long getFileRef() {
		return _videos.getFileRef();
	}

	/**
	* Returns the ideas ref of this videos.
	*
	* @return the ideas ref of this videos
	*/
	@Override
	public long getIdeasRef() {
		return _videos.getIdeasRef();
	}

	/**
	* Returns the primary key of this videos.
	*
	* @return the primary key of this videos
	*/
	@Override
	public long getPrimaryKey() {
		return _videos.getPrimaryKey();
	}

	/**
	* Returns the video ID of this videos.
	*
	* @return the video ID of this videos
	*/
	@Override
	public long getVideoId() {
		return _videos.getVideoId();
	}

	@Override
	public void persist() {
		_videos.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_videos.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_videos.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_videos.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_videos.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the extension of this videos.
	*
	* @param Extension the extension of this videos
	*/
	@Override
	public void setExtension(java.lang.String Extension) {
		_videos.setExtension(Extension);
	}

	/**
	* Sets the file ref of this videos.
	*
	* @param FileRef the file ref of this videos
	*/
	@Override
	public void setFileRef(long FileRef) {
		_videos.setFileRef(FileRef);
	}

	/**
	* Sets the ideas ref of this videos.
	*
	* @param IdeasRef the ideas ref of this videos
	*/
	@Override
	public void setIdeasRef(long IdeasRef) {
		_videos.setIdeasRef(IdeasRef);
	}

	@Override
	public void setNew(boolean n) {
		_videos.setNew(n);
	}

	/**
	* Sets the primary key of this videos.
	*
	* @param primaryKey the primary key of this videos
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_videos.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_videos.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the video ID of this videos.
	*
	* @param VideoId the video ID of this videos
	*/
	@Override
	public void setVideoId(long VideoId) {
		_videos.setVideoId(VideoId);
	}

	/**
	* Sets the video url of this videos.
	*
	* @param VideoUrl the video url of this videos
	*/
	@Override
	public void setVideoUrl(java.lang.String VideoUrl) {
		_videos.setVideoUrl(VideoUrl);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VideosWrapper)) {
			return false;
		}

		VideosWrapper videosWrapper = (VideosWrapper)obj;

		if (Objects.equals(_videos, videosWrapper._videos)) {
			return true;
		}

		return false;
	}

	@Override
	public Videos getWrappedModel() {
		return _videos;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _videos.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _videos.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_videos.resetOriginalValues();
	}

	private final Videos _videos;
}