/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

/**
 * The base model interface for the Videos service. Represents a row in the &quot;IDEA_Videos&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link ideaService.model.impl.VideosModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link ideaService.model.impl.VideosImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Videos
 * @see ideaService.model.impl.VideosImpl
 * @see ideaService.model.impl.VideosModelImpl
 * @generated
 */
@ProviderType
public interface VideosModel extends BaseModel<Videos> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a videos model instance should use the {@link Videos} interface instead.
	 */

	/**
	 * Returns the primary key of this videos.
	 *
	 * @return the primary key of this videos
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this videos.
	 *
	 * @param primaryKey the primary key of this videos
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the video ID of this videos.
	 *
	 * @return the video ID of this videos
	 */
	public long getVideoId();

	/**
	 * Sets the video ID of this videos.
	 *
	 * @param VideoId the video ID of this videos
	 */
	public void setVideoId(long VideoId);

	/**
	 * Returns the ideas ref of this videos.
	 *
	 * @return the ideas ref of this videos
	 */
	public long getIdeasRef();

	/**
	 * Sets the ideas ref of this videos.
	 *
	 * @param IdeasRef the ideas ref of this videos
	 */
	public void setIdeasRef(long IdeasRef);

	/**
	 * Returns the file ref of this videos.
	 *
	 * @return the file ref of this videos
	 */
	public long getFileRef();

	/**
	 * Sets the file ref of this videos.
	 *
	 * @param FileRef the file ref of this videos
	 */
	public void setFileRef(long FileRef);

	/**
	 * Returns the video url of this videos.
	 *
	 * @return the video url of this videos
	 */
	@AutoEscape
	public String getVideoUrl();

	/**
	 * Sets the video url of this videos.
	 *
	 * @param VideoUrl the video url of this videos
	 */
	public void setVideoUrl(String VideoUrl);

	/**
	 * Returns the extension of this videos.
	 *
	 * @return the extension of this videos
	 */
	@AutoEscape
	public String getExtension();

	/**
	 * Sets the extension of this videos.
	 *
	 * @param Extension the extension of this videos
	 */
	public void setExtension(String Extension);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(ideaService.model.Videos videos);

	@Override
	public int hashCode();

	@Override
	public CacheModel<ideaService.model.Videos> toCacheModel();

	@Override
	public ideaService.model.Videos toEscapedModel();

	@Override
	public ideaService.model.Videos toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}