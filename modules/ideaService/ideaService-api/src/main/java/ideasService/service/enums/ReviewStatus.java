package ideasService.service.enums;

public enum ReviewStatus {
SAVED("gespeichert"),SUBMITTED("eingereicht"),ACCEPTED("akzeptiert"),REJECTED("abgelehnt"),REALIZED("umgesetzt");

private final String reviewStatusDescription;

private ReviewStatus(String s){
	reviewStatusDescription = s;
}

public String getReviewStatusDescription(){
	return reviewStatusDescription;
}


}
