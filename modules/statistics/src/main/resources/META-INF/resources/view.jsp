<%@ include file="/init.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="aui" uri="http://liferay.com/tld/aui" %>
<portlet:defineObjects />
<style>
.links line {
  stroke: #999;
  stroke-opacity: 0.6;
}
.scatterLine {
  fill: none;
  stroke: steelblue;
  stroke-width: 2px;
}
</style>
<script>
    define._amd = define.amd;
    define.amd = false;
</script>
<script src="https://d3js.org/d3.v4.min.js"></script>
<script src="o/statistics.portlet/js/forcedGraphExample.js"></script>
<script src="o/statistics.portlet/js/barChartExample.js"></script>
<script src="o/statistics.portlet/js/pieChartExample.js"></script>
<script src="o/statistics.portlet/js/scatterPlotExample.js"></script>
<link href="o/statistics.portlet/css/chartStyles.css" rel="stylesheet"></link>
<script>
    define.amd = define._amd;
</script>

<div id="svgContainer" ></div>

<aui:button type="button"class = "btn btn-primary" id="newBtn1" value="Bar Chart" onClick="loadBarChart()"/>
<aui:button type="button"class = "btn btn-primary" id="newBtn2" value="Force Graph" onClick="addForceGraph()"/>
<aui:button type="button"class = "btn btn-primary" id="newBtn3" value="Pie Chart" onClick="addPieChart()"/>
<aui:button type="button"class = "btn btn-primary" id="newBtn4" value="Scatter Plot" onClick="addScatterPlot()"/>

<script type="text/javascript">
//default add force graph
addForceGraph();
</script>

