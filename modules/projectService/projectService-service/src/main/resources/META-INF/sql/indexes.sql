create index IX_4C2E3BF7 on PROJECT_Project (createDate);
create index IX_7F902F13 on PROJECT_Project (description[$COLUMN_LENGTH:300$]);
create index IX_E50534FB on PROJECT_Project (isPublished);
create index IX_B9960E20 on PROJECT_Project (layoutRef);
create index IX_44665B4D on PROJECT_Project (projectToken[$COLUMN_LENGTH:4000$]);
create index IX_544F762F on PROJECT_Project (title[$COLUMN_LENGTH:50$]);
create index IX_952EC9F3 on PROJECT_Project (userId);
create index IX_1ED7E06D on PROJECT_Project (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_2BC79E2F on PROJECT_Project (uuid_[$COLUMN_LENGTH:75$], groupId);