create table PROJECT_Project (
	uuid_ VARCHAR(75) null,
	projectId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	titleImgRef VARCHAR(500) null,
	projectToken STRING null,
	title VARCHAR(50) null,
	description VARCHAR(300) null,
	isPublished BOOLEAN,
	pageUrl VARCHAR(75) null,
	layoutRef LONG,
	titleFileRef LONG
);