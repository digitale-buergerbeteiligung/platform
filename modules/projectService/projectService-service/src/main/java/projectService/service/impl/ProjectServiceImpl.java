/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package projectService.service.impl;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;

import projectService.exception.NoSuchProjectException;
import projectService.model.Project;
import projectService.service.ProjectLocalServiceUtil;
import projectService.service.base.ProjectServiceBaseImpl;
import projectService.service.persistence.ProjectUtil;

/**
 * The implementation of the project remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link projectService.service.ProjectService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ProjectServiceBaseImpl
 * @see projectService.service.ProjectServiceUtil
 */
public class ProjectServiceImpl extends ProjectServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link projectService.service.ProjectServiceUtil} to access the project remote service.
	 */

	@JSONWebService(method = "GET")
	public String getProjectById(long projectId) throws NoSuchProjectException{
		return JSONFactoryUtil.looseSerialize(ProjectUtil.findByPrimaryKey(projectId));
	}

	@JSONWebService(method = "GET")
	public String getAllProjects(){
		return JSONFactoryUtil.looseSerialize(ProjectLocalServiceUtil.getAllProjects());
	}
	//TODO validate
	@JSONWebService(method = "POST")
	public String insertNewProject(long groupId, long companyId, long userId, String title, String description,
			boolean isPublished, String titleImgRef, String url, long layoutRef, long titleFileRef){
		Project p = ProjectLocalServiceUtil.createProjectWithAutomatedDbId(groupId, companyId, userId, title, description,
				isPublished, titleImgRef, url, layoutRef, titleFileRef);
		if(p != null){
			p.persist();
			return JSONFactoryUtil.looseSerialize("success");
		}
		else{
			return JSONFactoryUtil.looseSerialize("project not created");
		}
	}
}