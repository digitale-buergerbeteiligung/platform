/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package projectService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import projectService.exception.NoSuchProjectException;

import projectService.model.Project;

import java.util.Date;

/**
 * The persistence interface for the project service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see projectService.service.persistence.impl.ProjectPersistenceImpl
 * @see ProjectUtil
 * @generated
 */
@ProviderType
public interface ProjectPersistence extends BasePersistence<Project> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProjectUtil} to access the project persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the projects where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching projects
	*/
	public java.util.List<Project> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the projects where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @return the range of matching projects
	*/
	public java.util.List<Project> findByUuid(java.lang.String uuid, int start,
		int end);

	/**
	* Returns an ordered range of all the projects where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching projects
	*/
	public java.util.List<Project> findByUuid(java.lang.String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Project> orderByComparator);

	/**
	* Returns an ordered range of all the projects where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching projects
	*/
	public java.util.List<Project> findByUuid(java.lang.String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Project> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public Project findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Project> orderByComparator)
		throws NoSuchProjectException;

	/**
	* Returns the first project in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Project> orderByComparator);

	/**
	* Returns the last project in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public Project findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Project> orderByComparator)
		throws NoSuchProjectException;

	/**
	* Returns the last project in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Project> orderByComparator);

	/**
	* Returns the projects before and after the current project in the ordered set where uuid = &#63;.
	*
	* @param projectId the primary key of the current project
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project
	* @throws NoSuchProjectException if a project with the primary key could not be found
	*/
	public Project[] findByUuid_PrevAndNext(long projectId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Project> orderByComparator)
		throws NoSuchProjectException;

	/**
	* Removes all the projects where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of projects where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching projects
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns the project where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public Project findByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchProjectException;

	/**
	* Returns the project where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns the project where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the project where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the project that was removed
	*/
	public Project removeByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchProjectException;

	/**
	* Returns the number of projects where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching projects
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns all the projects where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching projects
	*/
	public java.util.List<Project> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the projects where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @return the range of matching projects
	*/
	public java.util.List<Project> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the projects where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching projects
	*/
	public java.util.List<Project> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Project> orderByComparator);

	/**
	* Returns an ordered range of all the projects where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching projects
	*/
	public java.util.List<Project> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Project> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public Project findByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Project> orderByComparator)
		throws NoSuchProjectException;

	/**
	* Returns the first project in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Project> orderByComparator);

	/**
	* Returns the last project in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public Project findByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Project> orderByComparator)
		throws NoSuchProjectException;

	/**
	* Returns the last project in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Project> orderByComparator);

	/**
	* Returns the projects before and after the current project in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param projectId the primary key of the current project
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project
	* @throws NoSuchProjectException if a project with the primary key could not be found
	*/
	public Project[] findByUuid_C_PrevAndNext(long projectId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Project> orderByComparator)
		throws NoSuchProjectException;

	/**
	* Removes all the projects where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of projects where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching projects
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the project where createDate = &#63; or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param createDate the create date
	* @return the matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public Project findByCreateDate(Date createDate)
		throws NoSuchProjectException;

	/**
	* Returns the project where createDate = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param createDate the create date
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByCreateDate(Date createDate);

	/**
	* Returns the project where createDate = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param createDate the create date
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByCreateDate(Date createDate, boolean retrieveFromCache);

	/**
	* Removes the project where createDate = &#63; from the database.
	*
	* @param createDate the create date
	* @return the project that was removed
	*/
	public Project removeByCreateDate(Date createDate)
		throws NoSuchProjectException;

	/**
	* Returns the number of projects where createDate = &#63;.
	*
	* @param createDate the create date
	* @return the number of matching projects
	*/
	public int countByCreateDate(Date createDate);

	/**
	* Returns the project where title = &#63; or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param title the title
	* @return the matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public Project findByTitle(java.lang.String title)
		throws NoSuchProjectException;

	/**
	* Returns the project where title = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param title the title
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByTitle(java.lang.String title);

	/**
	* Returns the project where title = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param title the title
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByTitle(java.lang.String title,
		boolean retrieveFromCache);

	/**
	* Removes the project where title = &#63; from the database.
	*
	* @param title the title
	* @return the project that was removed
	*/
	public Project removeByTitle(java.lang.String title)
		throws NoSuchProjectException;

	/**
	* Returns the number of projects where title = &#63;.
	*
	* @param title the title
	* @return the number of matching projects
	*/
	public int countByTitle(java.lang.String title);

	/**
	* Returns the project where description = &#63; or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param description the description
	* @return the matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public Project findByDescription(java.lang.String description)
		throws NoSuchProjectException;

	/**
	* Returns the project where description = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param description the description
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByDescription(java.lang.String description);

	/**
	* Returns the project where description = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param description the description
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByDescription(java.lang.String description,
		boolean retrieveFromCache);

	/**
	* Removes the project where description = &#63; from the database.
	*
	* @param description the description
	* @return the project that was removed
	*/
	public Project removeByDescription(java.lang.String description)
		throws NoSuchProjectException;

	/**
	* Returns the number of projects where description = &#63;.
	*
	* @param description the description
	* @return the number of matching projects
	*/
	public int countByDescription(java.lang.String description);

	/**
	* Returns the project where isPublished = &#63; or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param isPublished the is published
	* @return the matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public Project findByIsPublished(boolean isPublished)
		throws NoSuchProjectException;

	/**
	* Returns the project where isPublished = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param isPublished the is published
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByIsPublished(boolean isPublished);

	/**
	* Returns the project where isPublished = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param isPublished the is published
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByIsPublished(boolean isPublished,
		boolean retrieveFromCache);

	/**
	* Removes the project where isPublished = &#63; from the database.
	*
	* @param isPublished the is published
	* @return the project that was removed
	*/
	public Project removeByIsPublished(boolean isPublished)
		throws NoSuchProjectException;

	/**
	* Returns the number of projects where isPublished = &#63;.
	*
	* @param isPublished the is published
	* @return the number of matching projects
	*/
	public int countByIsPublished(boolean isPublished);

	/**
	* Returns the project where userId = &#63; or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param userId the user ID
	* @return the matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public Project findByUserId(long userId) throws NoSuchProjectException;

	/**
	* Returns the project where userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param userId the user ID
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByUserId(long userId);

	/**
	* Returns the project where userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param userId the user ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByUserId(long userId, boolean retrieveFromCache);

	/**
	* Removes the project where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @return the project that was removed
	*/
	public Project removeByUserId(long userId) throws NoSuchProjectException;

	/**
	* Returns the number of projects where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching projects
	*/
	public int countByUserId(long userId);

	/**
	* Returns the project where projectToken = &#63; or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param projectToken the project token
	* @return the matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public Project findByProjectToken(java.lang.String projectToken)
		throws NoSuchProjectException;

	/**
	* Returns the project where projectToken = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param projectToken the project token
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByProjectToken(java.lang.String projectToken);

	/**
	* Returns the project where projectToken = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param projectToken the project token
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByProjectToken(java.lang.String projectToken,
		boolean retrieveFromCache);

	/**
	* Removes the project where projectToken = &#63; from the database.
	*
	* @param projectToken the project token
	* @return the project that was removed
	*/
	public Project removeByProjectToken(java.lang.String projectToken)
		throws NoSuchProjectException;

	/**
	* Returns the number of projects where projectToken = &#63;.
	*
	* @param projectToken the project token
	* @return the number of matching projects
	*/
	public int countByProjectToken(java.lang.String projectToken);

	/**
	* Returns the project where layoutRef = &#63; or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param layoutRef the layout ref
	* @return the matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public Project findByLayoutRef(long layoutRef)
		throws NoSuchProjectException;

	/**
	* Returns the project where layoutRef = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param layoutRef the layout ref
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByLayoutRef(long layoutRef);

	/**
	* Returns the project where layoutRef = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param layoutRef the layout ref
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public Project fetchByLayoutRef(long layoutRef, boolean retrieveFromCache);

	/**
	* Removes the project where layoutRef = &#63; from the database.
	*
	* @param layoutRef the layout ref
	* @return the project that was removed
	*/
	public Project removeByLayoutRef(long layoutRef)
		throws NoSuchProjectException;

	/**
	* Returns the number of projects where layoutRef = &#63;.
	*
	* @param layoutRef the layout ref
	* @return the number of matching projects
	*/
	public int countByLayoutRef(long layoutRef);

	/**
	* Caches the project in the entity cache if it is enabled.
	*
	* @param project the project
	*/
	public void cacheResult(Project project);

	/**
	* Caches the projects in the entity cache if it is enabled.
	*
	* @param projects the projects
	*/
	public void cacheResult(java.util.List<Project> projects);

	/**
	* Creates a new project with the primary key. Does not add the project to the database.
	*
	* @param projectId the primary key for the new project
	* @return the new project
	*/
	public Project create(long projectId);

	/**
	* Removes the project with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectId the primary key of the project
	* @return the project that was removed
	* @throws NoSuchProjectException if a project with the primary key could not be found
	*/
	public Project remove(long projectId) throws NoSuchProjectException;

	public Project updateImpl(Project project);

	/**
	* Returns the project with the primary key or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param projectId the primary key of the project
	* @return the project
	* @throws NoSuchProjectException if a project with the primary key could not be found
	*/
	public Project findByPrimaryKey(long projectId)
		throws NoSuchProjectException;

	/**
	* Returns the project with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param projectId the primary key of the project
	* @return the project, or <code>null</code> if a project with the primary key could not be found
	*/
	public Project fetchByPrimaryKey(long projectId);

	@Override
	public java.util.Map<java.io.Serializable, Project> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the projects.
	*
	* @return the projects
	*/
	public java.util.List<Project> findAll();

	/**
	* Returns a range of all the projects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @return the range of projects
	*/
	public java.util.List<Project> findAll(int start, int end);

	/**
	* Returns an ordered range of all the projects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of projects
	*/
	public java.util.List<Project> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Project> orderByComparator);

	/**
	* Returns an ordered range of all the projects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of projects
	*/
	public java.util.List<Project> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Project> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the projects from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of projects.
	*
	* @return the number of projects
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}