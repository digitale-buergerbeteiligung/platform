/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package projectService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import projectService.model.Project;

import java.util.Date;
import java.util.List;

/**
 * The persistence utility for the project service. This utility wraps {@link projectService.service.persistence.impl.ProjectPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ProjectPersistence
 * @see projectService.service.persistence.impl.ProjectPersistenceImpl
 * @generated
 */
@ProviderType
public class ProjectUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Project project) {
		getPersistence().clearCache(project);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Project> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Project> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Project> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Project> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Project update(Project project) {
		return getPersistence().update(project);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Project update(Project project, ServiceContext serviceContext) {
		return getPersistence().update(project, serviceContext);
	}

	/**
	* Returns all the projects where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching projects
	*/
	public static List<Project> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the projects where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @return the range of matching projects
	*/
	public static List<Project> findByUuid(java.lang.String uuid, int start,
		int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the projects where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching projects
	*/
	public static List<Project> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Project> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the projects where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching projects
	*/
	public static List<Project> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Project> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first project in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public static Project findByUuid_First(java.lang.String uuid,
		OrderByComparator<Project> orderByComparator)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first project in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<Project> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last project in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public static Project findByUuid_Last(java.lang.String uuid,
		OrderByComparator<Project> orderByComparator)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last project in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<Project> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the projects before and after the current project in the ordered set where uuid = &#63;.
	*
	* @param projectId the primary key of the current project
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project
	* @throws NoSuchProjectException if a project with the primary key could not be found
	*/
	public static Project[] findByUuid_PrevAndNext(long projectId,
		java.lang.String uuid, OrderByComparator<Project> orderByComparator)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence()
				   .findByUuid_PrevAndNext(projectId, uuid, orderByComparator);
	}

	/**
	* Removes all the projects where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of projects where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching projects
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the project where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public static Project findByUUID_G(java.lang.String uuid, long groupId)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the project where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the project where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the project where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the project that was removed
	*/
	public static Project removeByUUID_G(java.lang.String uuid, long groupId)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of projects where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching projects
	*/
	public static int countByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the projects where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching projects
	*/
	public static List<Project> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the projects where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @return the range of matching projects
	*/
	public static List<Project> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the projects where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching projects
	*/
	public static List<Project> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Project> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the projects where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching projects
	*/
	public static List<Project> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Project> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first project in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public static Project findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Project> orderByComparator)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first project in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Project> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last project in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public static Project findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Project> orderByComparator)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last project in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Project> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the projects before and after the current project in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param projectId the primary key of the current project
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project
	* @throws NoSuchProjectException if a project with the primary key could not be found
	*/
	public static Project[] findByUuid_C_PrevAndNext(long projectId,
		java.lang.String uuid, long companyId,
		OrderByComparator<Project> orderByComparator)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(projectId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the projects where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of projects where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching projects
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns the project where createDate = &#63; or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param createDate the create date
	* @return the matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public static Project findByCreateDate(Date createDate)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().findByCreateDate(createDate);
	}

	/**
	* Returns the project where createDate = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param createDate the create date
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByCreateDate(Date createDate) {
		return getPersistence().fetchByCreateDate(createDate);
	}

	/**
	* Returns the project where createDate = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param createDate the create date
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByCreateDate(Date createDate,
		boolean retrieveFromCache) {
		return getPersistence().fetchByCreateDate(createDate, retrieveFromCache);
	}

	/**
	* Removes the project where createDate = &#63; from the database.
	*
	* @param createDate the create date
	* @return the project that was removed
	*/
	public static Project removeByCreateDate(Date createDate)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().removeByCreateDate(createDate);
	}

	/**
	* Returns the number of projects where createDate = &#63;.
	*
	* @param createDate the create date
	* @return the number of matching projects
	*/
	public static int countByCreateDate(Date createDate) {
		return getPersistence().countByCreateDate(createDate);
	}

	/**
	* Returns the project where title = &#63; or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param title the title
	* @return the matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public static Project findByTitle(java.lang.String title)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().findByTitle(title);
	}

	/**
	* Returns the project where title = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param title the title
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByTitle(java.lang.String title) {
		return getPersistence().fetchByTitle(title);
	}

	/**
	* Returns the project where title = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param title the title
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByTitle(java.lang.String title,
		boolean retrieveFromCache) {
		return getPersistence().fetchByTitle(title, retrieveFromCache);
	}

	/**
	* Removes the project where title = &#63; from the database.
	*
	* @param title the title
	* @return the project that was removed
	*/
	public static Project removeByTitle(java.lang.String title)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().removeByTitle(title);
	}

	/**
	* Returns the number of projects where title = &#63;.
	*
	* @param title the title
	* @return the number of matching projects
	*/
	public static int countByTitle(java.lang.String title) {
		return getPersistence().countByTitle(title);
	}

	/**
	* Returns the project where description = &#63; or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param description the description
	* @return the matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public static Project findByDescription(java.lang.String description)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().findByDescription(description);
	}

	/**
	* Returns the project where description = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param description the description
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByDescription(java.lang.String description) {
		return getPersistence().fetchByDescription(description);
	}

	/**
	* Returns the project where description = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param description the description
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByDescription(java.lang.String description,
		boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByDescription(description, retrieveFromCache);
	}

	/**
	* Removes the project where description = &#63; from the database.
	*
	* @param description the description
	* @return the project that was removed
	*/
	public static Project removeByDescription(java.lang.String description)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().removeByDescription(description);
	}

	/**
	* Returns the number of projects where description = &#63;.
	*
	* @param description the description
	* @return the number of matching projects
	*/
	public static int countByDescription(java.lang.String description) {
		return getPersistence().countByDescription(description);
	}

	/**
	* Returns the project where isPublished = &#63; or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param isPublished the is published
	* @return the matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public static Project findByIsPublished(boolean isPublished)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().findByIsPublished(isPublished);
	}

	/**
	* Returns the project where isPublished = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param isPublished the is published
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByIsPublished(boolean isPublished) {
		return getPersistence().fetchByIsPublished(isPublished);
	}

	/**
	* Returns the project where isPublished = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param isPublished the is published
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByIsPublished(boolean isPublished,
		boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByIsPublished(isPublished, retrieveFromCache);
	}

	/**
	* Removes the project where isPublished = &#63; from the database.
	*
	* @param isPublished the is published
	* @return the project that was removed
	*/
	public static Project removeByIsPublished(boolean isPublished)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().removeByIsPublished(isPublished);
	}

	/**
	* Returns the number of projects where isPublished = &#63;.
	*
	* @param isPublished the is published
	* @return the number of matching projects
	*/
	public static int countByIsPublished(boolean isPublished) {
		return getPersistence().countByIsPublished(isPublished);
	}

	/**
	* Returns the project where userId = &#63; or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param userId the user ID
	* @return the matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public static Project findByUserId(long userId)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().findByUserId(userId);
	}

	/**
	* Returns the project where userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param userId the user ID
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByUserId(long userId) {
		return getPersistence().fetchByUserId(userId);
	}

	/**
	* Returns the project where userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param userId the user ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByUserId(long userId, boolean retrieveFromCache) {
		return getPersistence().fetchByUserId(userId, retrieveFromCache);
	}

	/**
	* Removes the project where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @return the project that was removed
	*/
	public static Project removeByUserId(long userId)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().removeByUserId(userId);
	}

	/**
	* Returns the number of projects where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching projects
	*/
	public static int countByUserId(long userId) {
		return getPersistence().countByUserId(userId);
	}

	/**
	* Returns the project where projectToken = &#63; or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param projectToken the project token
	* @return the matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public static Project findByProjectToken(java.lang.String projectToken)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().findByProjectToken(projectToken);
	}

	/**
	* Returns the project where projectToken = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param projectToken the project token
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByProjectToken(java.lang.String projectToken) {
		return getPersistence().fetchByProjectToken(projectToken);
	}

	/**
	* Returns the project where projectToken = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param projectToken the project token
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByProjectToken(java.lang.String projectToken,
		boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByProjectToken(projectToken, retrieveFromCache);
	}

	/**
	* Removes the project where projectToken = &#63; from the database.
	*
	* @param projectToken the project token
	* @return the project that was removed
	*/
	public static Project removeByProjectToken(java.lang.String projectToken)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().removeByProjectToken(projectToken);
	}

	/**
	* Returns the number of projects where projectToken = &#63;.
	*
	* @param projectToken the project token
	* @return the number of matching projects
	*/
	public static int countByProjectToken(java.lang.String projectToken) {
		return getPersistence().countByProjectToken(projectToken);
	}

	/**
	* Returns the project where layoutRef = &#63; or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param layoutRef the layout ref
	* @return the matching project
	* @throws NoSuchProjectException if a matching project could not be found
	*/
	public static Project findByLayoutRef(long layoutRef)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().findByLayoutRef(layoutRef);
	}

	/**
	* Returns the project where layoutRef = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param layoutRef the layout ref
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByLayoutRef(long layoutRef) {
		return getPersistence().fetchByLayoutRef(layoutRef);
	}

	/**
	* Returns the project where layoutRef = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param layoutRef the layout ref
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project, or <code>null</code> if a matching project could not be found
	*/
	public static Project fetchByLayoutRef(long layoutRef,
		boolean retrieveFromCache) {
		return getPersistence().fetchByLayoutRef(layoutRef, retrieveFromCache);
	}

	/**
	* Removes the project where layoutRef = &#63; from the database.
	*
	* @param layoutRef the layout ref
	* @return the project that was removed
	*/
	public static Project removeByLayoutRef(long layoutRef)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().removeByLayoutRef(layoutRef);
	}

	/**
	* Returns the number of projects where layoutRef = &#63;.
	*
	* @param layoutRef the layout ref
	* @return the number of matching projects
	*/
	public static int countByLayoutRef(long layoutRef) {
		return getPersistence().countByLayoutRef(layoutRef);
	}

	/**
	* Caches the project in the entity cache if it is enabled.
	*
	* @param project the project
	*/
	public static void cacheResult(Project project) {
		getPersistence().cacheResult(project);
	}

	/**
	* Caches the projects in the entity cache if it is enabled.
	*
	* @param projects the projects
	*/
	public static void cacheResult(List<Project> projects) {
		getPersistence().cacheResult(projects);
	}

	/**
	* Creates a new project with the primary key. Does not add the project to the database.
	*
	* @param projectId the primary key for the new project
	* @return the new project
	*/
	public static Project create(long projectId) {
		return getPersistence().create(projectId);
	}

	/**
	* Removes the project with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectId the primary key of the project
	* @return the project that was removed
	* @throws NoSuchProjectException if a project with the primary key could not be found
	*/
	public static Project remove(long projectId)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().remove(projectId);
	}

	public static Project updateImpl(Project project) {
		return getPersistence().updateImpl(project);
	}

	/**
	* Returns the project with the primary key or throws a {@link NoSuchProjectException} if it could not be found.
	*
	* @param projectId the primary key of the project
	* @return the project
	* @throws NoSuchProjectException if a project with the primary key could not be found
	*/
	public static Project findByPrimaryKey(long projectId)
		throws projectService.exception.NoSuchProjectException {
		return getPersistence().findByPrimaryKey(projectId);
	}

	/**
	* Returns the project with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param projectId the primary key of the project
	* @return the project, or <code>null</code> if a project with the primary key could not be found
	*/
	public static Project fetchByPrimaryKey(long projectId) {
		return getPersistence().fetchByPrimaryKey(projectId);
	}

	public static java.util.Map<java.io.Serializable, Project> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the projects.
	*
	* @return the projects
	*/
	public static List<Project> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the projects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @return the range of projects
	*/
	public static List<Project> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the projects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of projects
	*/
	public static List<Project> findAll(int start, int end,
		OrderByComparator<Project> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the projects.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of projects
	* @param end the upper bound of the range of projects (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of projects
	*/
	public static List<Project> findAll(int start, int end,
		OrderByComparator<Project> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the projects from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of projects.
	*
	* @return the number of projects
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static ProjectPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ProjectPersistence, ProjectPersistence> _serviceTracker =
		ServiceTrackerFactory.open(ProjectPersistence.class);
}