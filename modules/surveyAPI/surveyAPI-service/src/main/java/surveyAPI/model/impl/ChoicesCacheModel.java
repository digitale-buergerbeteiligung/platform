/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import surveyAPI.model.Choices;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Choices in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Choices
 * @generated
 */
@ProviderType
public class ChoicesCacheModel implements CacheModel<Choices>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ChoicesCacheModel)) {
			return false;
		}

		ChoicesCacheModel choicesCacheModel = (ChoicesCacheModel)obj;

		if (ChoiceID == choicesCacheModel.ChoiceID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, ChoiceID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{ChoiceID=");
		sb.append(ChoiceID);
		sb.append(", ChoiceText=");
		sb.append(ChoiceText);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Choices toEntityModel() {
		ChoicesImpl choicesImpl = new ChoicesImpl();

		choicesImpl.setChoiceID(ChoiceID);

		if (ChoiceText == null) {
			choicesImpl.setChoiceText(StringPool.BLANK);
		}
		else {
			choicesImpl.setChoiceText(ChoiceText);
		}

		choicesImpl.resetOriginalValues();

		return choicesImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ChoiceID = objectInput.readLong();
		ChoiceText = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ChoiceID);

		if (ChoiceText == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ChoiceText);
		}
	}

	public long ChoiceID;
	public String ChoiceText;
}