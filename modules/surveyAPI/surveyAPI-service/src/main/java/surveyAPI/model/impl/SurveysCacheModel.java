/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import surveyAPI.model.Surveys;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Surveys in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Surveys
 * @generated
 */
@ProviderType
public class SurveysCacheModel implements CacheModel<Surveys>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SurveysCacheModel)) {
			return false;
		}

		SurveysCacheModel surveysCacheModel = (SurveysCacheModel)obj;

		if (SurveyID == surveysCacheModel.SurveyID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, SurveyID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{SurveyID=");
		sb.append(SurveyID);
		sb.append(", SurveyName=");
		sb.append(SurveyName);
		sb.append(", DateOfCreation=");
		sb.append(DateOfCreation);
		sb.append(", QuestionIds=");
		sb.append(QuestionIds);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Surveys toEntityModel() {
		SurveysImpl surveysImpl = new SurveysImpl();

		surveysImpl.setSurveyID(SurveyID);

		if (SurveyName == null) {
			surveysImpl.setSurveyName(StringPool.BLANK);
		}
		else {
			surveysImpl.setSurveyName(SurveyName);
		}

		if (DateOfCreation == Long.MIN_VALUE) {
			surveysImpl.setDateOfCreation(null);
		}
		else {
			surveysImpl.setDateOfCreation(new Date(DateOfCreation));
		}

		if (QuestionIds == null) {
			surveysImpl.setQuestionIds(StringPool.BLANK);
		}
		else {
			surveysImpl.setQuestionIds(QuestionIds);
		}

		surveysImpl.resetOriginalValues();

		return surveysImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		SurveyID = objectInput.readLong();
		SurveyName = objectInput.readUTF();
		DateOfCreation = objectInput.readLong();
		QuestionIds = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(SurveyID);

		if (SurveyName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(SurveyName);
		}

		objectOutput.writeLong(DateOfCreation);

		if (QuestionIds == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(QuestionIds);
		}
	}

	public long SurveyID;
	public String SurveyName;
	public long DateOfCreation;
	public String QuestionIds;
}