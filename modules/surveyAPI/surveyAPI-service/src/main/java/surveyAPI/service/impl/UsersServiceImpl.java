/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.impl;

import org.json.JSONException;
import org.json.JSONObject;

import com.liferay.portal.kernel.jsonwebservice.JSONWebService;

import aQute.bnd.annotation.ProviderType;
import surveyAPI.service.UsersLocalServiceUtil;
import surveyAPI.service.base.UsersServiceBaseImpl;

/**
 * The implementation of the users remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link surveyAPI.service.UsersService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UsersServiceBaseImpl
 * @see surveyAPI.service.UsersServiceUtil
 */
@ProviderType
public class UsersServiceImpl extends UsersServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link surveyAPI.service.UsersServiceUtil} to access the users remote service.
	 */
	/**
	 * 
	 * @param jsonInput 
	 * {
	 * 		"SurveyID":"",
	 * 		ResChoices: [
	 * 			{"QuestionId":"","ChoiceId":""},
	 * 			{"QuestionId":"","ChoiceId":""}
	 * 		],
	 * 		ResTexts: {
	 * 			"QuestionId":"text", //Here QuestionId is of type long, this is 1:"hello"
	 * 			"QuestionId":"text"
 * 			},
	 * 		"UserEmail" : emailAddress
	 * }
	 * @return
	 * @throws JSONException 
	 */
	@JSONWebService(method = "POST")
	public JSONObject addUserResponse(String jsonInput) throws JSONException{
		return UsersLocalServiceUtil.addUserResponse(jsonInput);
	}
}