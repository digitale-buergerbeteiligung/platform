/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import surveyAPI.exception.NoSuchUsersException;

import surveyAPI.model.Users;

import surveyAPI.model.impl.UsersImpl;
import surveyAPI.model.impl.UsersModelImpl;

import surveyAPI.service.persistence.UsersPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the users service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UsersPersistence
 * @see surveyAPI.service.persistence.UsersUtil
 * @generated
 */
@ProviderType
public class UsersPersistenceImpl extends BasePersistenceImpl<Users>
	implements UsersPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UsersUtil} to access the users persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UsersImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UsersModelImpl.ENTITY_CACHE_ENABLED,
			UsersModelImpl.FINDER_CACHE_ENABLED, UsersImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UsersModelImpl.ENTITY_CACHE_ENABLED,
			UsersModelImpl.FINDER_CACHE_ENABLED, UsersImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UsersModelImpl.ENTITY_CACHE_ENABLED,
			UsersModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UNAME = new FinderPath(UsersModelImpl.ENTITY_CACHE_ENABLED,
			UsersModelImpl.FINDER_CACHE_ENABLED, UsersImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUName",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UNAME = new FinderPath(UsersModelImpl.ENTITY_CACHE_ENABLED,
			UsersModelImpl.FINDER_CACHE_ENABLED, UsersImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUName",
			new String[] { String.class.getName() },
			UsersModelImpl.USERNAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UNAME = new FinderPath(UsersModelImpl.ENTITY_CACHE_ENABLED,
			UsersModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUName",
			new String[] { String.class.getName() });

	/**
	 * Returns all the userses where UserName = &#63;.
	 *
	 * @param UserName the user name
	 * @return the matching userses
	 */
	@Override
	public List<Users> findByUName(String UserName) {
		return findByUName(UserName, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the userses where UserName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param UserName the user name
	 * @param start the lower bound of the range of userses
	 * @param end the upper bound of the range of userses (not inclusive)
	 * @return the range of matching userses
	 */
	@Override
	public List<Users> findByUName(String UserName, int start, int end) {
		return findByUName(UserName, start, end, null);
	}

	/**
	 * Returns an ordered range of all the userses where UserName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param UserName the user name
	 * @param start the lower bound of the range of userses
	 * @param end the upper bound of the range of userses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching userses
	 */
	@Override
	public List<Users> findByUName(String UserName, int start, int end,
		OrderByComparator<Users> orderByComparator) {
		return findByUName(UserName, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the userses where UserName = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param UserName the user name
	 * @param start the lower bound of the range of userses
	 * @param end the upper bound of the range of userses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching userses
	 */
	@Override
	public List<Users> findByUName(String UserName, int start, int end,
		OrderByComparator<Users> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UNAME;
			finderArgs = new Object[] { UserName };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UNAME;
			finderArgs = new Object[] { UserName, start, end, orderByComparator };
		}

		List<Users> list = null;

		if (retrieveFromCache) {
			list = (List<Users>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Users users : list) {
					if (!Objects.equals(UserName, users.getUserName())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERS_WHERE);

			boolean bindUserName = false;

			if (UserName == null) {
				query.append(_FINDER_COLUMN_UNAME_USERNAME_1);
			}
			else if (UserName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UNAME_USERNAME_3);
			}
			else {
				bindUserName = true;

				query.append(_FINDER_COLUMN_UNAME_USERNAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UsersModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUserName) {
					qPos.add(UserName);
				}

				if (!pagination) {
					list = (List<Users>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Users>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first users in the ordered set where UserName = &#63;.
	 *
	 * @param UserName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users
	 * @throws NoSuchUsersException if a matching users could not be found
	 */
	@Override
	public Users findByUName_First(String UserName,
		OrderByComparator<Users> orderByComparator) throws NoSuchUsersException {
		Users users = fetchByUName_First(UserName, orderByComparator);

		if (users != null) {
			return users;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("UserName=");
		msg.append(UserName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUsersException(msg.toString());
	}

	/**
	 * Returns the first users in the ordered set where UserName = &#63;.
	 *
	 * @param UserName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching users, or <code>null</code> if a matching users could not be found
	 */
	@Override
	public Users fetchByUName_First(String UserName,
		OrderByComparator<Users> orderByComparator) {
		List<Users> list = findByUName(UserName, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last users in the ordered set where UserName = &#63;.
	 *
	 * @param UserName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users
	 * @throws NoSuchUsersException if a matching users could not be found
	 */
	@Override
	public Users findByUName_Last(String UserName,
		OrderByComparator<Users> orderByComparator) throws NoSuchUsersException {
		Users users = fetchByUName_Last(UserName, orderByComparator);

		if (users != null) {
			return users;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("UserName=");
		msg.append(UserName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchUsersException(msg.toString());
	}

	/**
	 * Returns the last users in the ordered set where UserName = &#63;.
	 *
	 * @param UserName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching users, or <code>null</code> if a matching users could not be found
	 */
	@Override
	public Users fetchByUName_Last(String UserName,
		OrderByComparator<Users> orderByComparator) {
		int count = countByUName(UserName);

		if (count == 0) {
			return null;
		}

		List<Users> list = findByUName(UserName, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the userses before and after the current users in the ordered set where UserName = &#63;.
	 *
	 * @param UserID the primary key of the current users
	 * @param UserName the user name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next users
	 * @throws NoSuchUsersException if a users with the primary key could not be found
	 */
	@Override
	public Users[] findByUName_PrevAndNext(long UserID, String UserName,
		OrderByComparator<Users> orderByComparator) throws NoSuchUsersException {
		Users users = findByPrimaryKey(UserID);

		Session session = null;

		try {
			session = openSession();

			Users[] array = new UsersImpl[3];

			array[0] = getByUName_PrevAndNext(session, users, UserName,
					orderByComparator, true);

			array[1] = users;

			array[2] = getByUName_PrevAndNext(session, users, UserName,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Users getByUName_PrevAndNext(Session session, Users users,
		String UserName, OrderByComparator<Users> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERS_WHERE);

		boolean bindUserName = false;

		if (UserName == null) {
			query.append(_FINDER_COLUMN_UNAME_USERNAME_1);
		}
		else if (UserName.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UNAME_USERNAME_3);
		}
		else {
			bindUserName = true;

			query.append(_FINDER_COLUMN_UNAME_USERNAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UsersModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUserName) {
			qPos.add(UserName);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(users);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Users> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the userses where UserName = &#63; from the database.
	 *
	 * @param UserName the user name
	 */
	@Override
	public void removeByUName(String UserName) {
		for (Users users : findByUName(UserName, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(users);
		}
	}

	/**
	 * Returns the number of userses where UserName = &#63;.
	 *
	 * @param UserName the user name
	 * @return the number of matching userses
	 */
	@Override
	public int countByUName(String UserName) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UNAME;

		Object[] finderArgs = new Object[] { UserName };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERS_WHERE);

			boolean bindUserName = false;

			if (UserName == null) {
				query.append(_FINDER_COLUMN_UNAME_USERNAME_1);
			}
			else if (UserName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UNAME_USERNAME_3);
			}
			else {
				bindUserName = true;

				query.append(_FINDER_COLUMN_UNAME_USERNAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUserName) {
					qPos.add(UserName);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UNAME_USERNAME_1 = "users.UserName IS NULL";
	private static final String _FINDER_COLUMN_UNAME_USERNAME_2 = "users.UserName = ?";
	private static final String _FINDER_COLUMN_UNAME_USERNAME_3 = "(users.UserName IS NULL OR users.UserName = '')";

	public UsersPersistenceImpl() {
		setModelClass(Users.class);
	}

	/**
	 * Caches the users in the entity cache if it is enabled.
	 *
	 * @param users the users
	 */
	@Override
	public void cacheResult(Users users) {
		entityCache.putResult(UsersModelImpl.ENTITY_CACHE_ENABLED,
			UsersImpl.class, users.getPrimaryKey(), users);

		users.resetOriginalValues();
	}

	/**
	 * Caches the userses in the entity cache if it is enabled.
	 *
	 * @param userses the userses
	 */
	@Override
	public void cacheResult(List<Users> userses) {
		for (Users users : userses) {
			if (entityCache.getResult(UsersModelImpl.ENTITY_CACHE_ENABLED,
						UsersImpl.class, users.getPrimaryKey()) == null) {
				cacheResult(users);
			}
			else {
				users.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all userses.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(UsersImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the users.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Users users) {
		entityCache.removeResult(UsersModelImpl.ENTITY_CACHE_ENABLED,
			UsersImpl.class, users.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Users> userses) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Users users : userses) {
			entityCache.removeResult(UsersModelImpl.ENTITY_CACHE_ENABLED,
				UsersImpl.class, users.getPrimaryKey());
		}
	}

	/**
	 * Creates a new users with the primary key. Does not add the users to the database.
	 *
	 * @param UserID the primary key for the new users
	 * @return the new users
	 */
	@Override
	public Users create(long UserID) {
		Users users = new UsersImpl();

		users.setNew(true);
		users.setPrimaryKey(UserID);

		return users;
	}

	/**
	 * Removes the users with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param UserID the primary key of the users
	 * @return the users that was removed
	 * @throws NoSuchUsersException if a users with the primary key could not be found
	 */
	@Override
	public Users remove(long UserID) throws NoSuchUsersException {
		return remove((Serializable)UserID);
	}

	/**
	 * Removes the users with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the users
	 * @return the users that was removed
	 * @throws NoSuchUsersException if a users with the primary key could not be found
	 */
	@Override
	public Users remove(Serializable primaryKey) throws NoSuchUsersException {
		Session session = null;

		try {
			session = openSession();

			Users users = (Users)session.get(UsersImpl.class, primaryKey);

			if (users == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUsersException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(users);
		}
		catch (NoSuchUsersException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Users removeImpl(Users users) {
		users = toUnwrappedModel(users);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(users)) {
				users = (Users)session.get(UsersImpl.class,
						users.getPrimaryKeyObj());
			}

			if (users != null) {
				session.delete(users);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (users != null) {
			clearCache(users);
		}

		return users;
	}

	@Override
	public Users updateImpl(Users users) {
		users = toUnwrappedModel(users);

		boolean isNew = users.isNew();

		UsersModelImpl usersModelImpl = (UsersModelImpl)users;

		Session session = null;

		try {
			session = openSession();

			if (users.isNew()) {
				session.save(users);

				users.setNew(false);
			}
			else {
				users = (Users)session.merge(users);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!UsersModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { usersModelImpl.getUserName() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UNAME, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UNAME,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((usersModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UNAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						usersModelImpl.getOriginalUserName()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UNAME, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UNAME,
					args);

				args = new Object[] { usersModelImpl.getUserName() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UNAME, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UNAME,
					args);
			}
		}

		entityCache.putResult(UsersModelImpl.ENTITY_CACHE_ENABLED,
			UsersImpl.class, users.getPrimaryKey(), users, false);

		users.resetOriginalValues();

		return users;
	}

	protected Users toUnwrappedModel(Users users) {
		if (users instanceof UsersImpl) {
			return users;
		}

		UsersImpl usersImpl = new UsersImpl();

		usersImpl.setNew(users.isNew());
		usersImpl.setPrimaryKey(users.getPrimaryKey());

		usersImpl.setUserID(users.getUserID());
		usersImpl.setUserName(users.getUserName());
		usersImpl.setEmail(users.getEmail());

		return usersImpl;
	}

	/**
	 * Returns the users with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the users
	 * @return the users
	 * @throws NoSuchUsersException if a users with the primary key could not be found
	 */
	@Override
	public Users findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUsersException {
		Users users = fetchByPrimaryKey(primaryKey);

		if (users == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUsersException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return users;
	}

	/**
	 * Returns the users with the primary key or throws a {@link NoSuchUsersException} if it could not be found.
	 *
	 * @param UserID the primary key of the users
	 * @return the users
	 * @throws NoSuchUsersException if a users with the primary key could not be found
	 */
	@Override
	public Users findByPrimaryKey(long UserID) throws NoSuchUsersException {
		return findByPrimaryKey((Serializable)UserID);
	}

	/**
	 * Returns the users with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the users
	 * @return the users, or <code>null</code> if a users with the primary key could not be found
	 */
	@Override
	public Users fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(UsersModelImpl.ENTITY_CACHE_ENABLED,
				UsersImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Users users = (Users)serializable;

		if (users == null) {
			Session session = null;

			try {
				session = openSession();

				users = (Users)session.get(UsersImpl.class, primaryKey);

				if (users != null) {
					cacheResult(users);
				}
				else {
					entityCache.putResult(UsersModelImpl.ENTITY_CACHE_ENABLED,
						UsersImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(UsersModelImpl.ENTITY_CACHE_ENABLED,
					UsersImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return users;
	}

	/**
	 * Returns the users with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param UserID the primary key of the users
	 * @return the users, or <code>null</code> if a users with the primary key could not be found
	 */
	@Override
	public Users fetchByPrimaryKey(long UserID) {
		return fetchByPrimaryKey((Serializable)UserID);
	}

	@Override
	public Map<Serializable, Users> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Users> map = new HashMap<Serializable, Users>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Users users = fetchByPrimaryKey(primaryKey);

			if (users != null) {
				map.put(primaryKey, users);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(UsersModelImpl.ENTITY_CACHE_ENABLED,
					UsersImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Users)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_USERS_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Users users : (List<Users>)q.list()) {
				map.put(users.getPrimaryKeyObj(), users);

				cacheResult(users);

				uncachedPrimaryKeys.remove(users.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(UsersModelImpl.ENTITY_CACHE_ENABLED,
					UsersImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the userses.
	 *
	 * @return the userses
	 */
	@Override
	public List<Users> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the userses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of userses
	 * @param end the upper bound of the range of userses (not inclusive)
	 * @return the range of userses
	 */
	@Override
	public List<Users> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the userses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of userses
	 * @param end the upper bound of the range of userses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of userses
	 */
	@Override
	public List<Users> findAll(int start, int end,
		OrderByComparator<Users> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the userses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of userses
	 * @param end the upper bound of the range of userses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of userses
	 */
	@Override
	public List<Users> findAll(int start, int end,
		OrderByComparator<Users> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Users> list = null;

		if (retrieveFromCache) {
			list = (List<Users>)finderCache.getResult(finderPath, finderArgs,
					this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_USERS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_USERS;

				if (pagination) {
					sql = sql.concat(UsersModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Users>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Users>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the userses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Users users : findAll()) {
			remove(users);
		}
	}

	/**
	 * Returns the number of userses.
	 *
	 * @return the number of userses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_USERS);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return UsersModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the users persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(UsersImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_USERS = "SELECT users FROM Users users";
	private static final String _SQL_SELECT_USERS_WHERE_PKS_IN = "SELECT users FROM Users users WHERE UserID IN (";
	private static final String _SQL_SELECT_USERS_WHERE = "SELECT users FROM Users users WHERE ";
	private static final String _SQL_COUNT_USERS = "SELECT COUNT(users) FROM Users users";
	private static final String _SQL_COUNT_USERS_WHERE = "SELECT COUNT(users) FROM Users users WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "users.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Users exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Users exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(UsersPersistenceImpl.class);
}