/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.http;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import surveyAPI.service.SurveysServiceUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link SurveysServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link surveyAPI.model.SurveysSoap}.
 * If the method in the service utility returns a
 * {@link surveyAPI.model.Surveys}, that is translated to a
 * {@link surveyAPI.model.SurveysSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SurveysServiceHttp
 * @see surveyAPI.model.SurveysSoap
 * @see SurveysServiceUtil
 * @generated
 */
@ProviderType
public class SurveysServiceSoap {
	/**
	* JSON Web service to fetch all the question ids associated with a given survey id
	*/
	public static org.json.JSONObject getAllQuestionIds()
		throws RemoteException {
		try {
			org.json.JSONObject returnValue = SurveysServiceUtil.getAllQuestionIds();

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static org.json.JSONObject getAllQuestionIds(long surveyID)
		throws RemoteException {
		try {
			org.json.JSONObject returnValue = SurveysServiceUtil.getAllQuestionIds(surveyID);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	/**
	* JSON Web service to get full survey
	*
	* @return JSONObject {Questions:JSONArray, Choices:JSONArray, QuestionIds:JSONArray}
	*/
	public static org.json.JSONObject getSurvey(long surveyId)
		throws RemoteException {
		try {
			org.json.JSONObject returnValue = SurveysServiceUtil.getSurvey(surveyId);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	/**
	* JSON Web Service to add a new survey
	*/
	public static org.json.JSONObject addNewSurvey() throws RemoteException {
		try {
			org.json.JSONObject returnValue = SurveysServiceUtil.addNewSurvey();

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	/**
	* This just adds a survey and returns the survey ID, user can later add questions
	*
	* @param name
	* @return
	* @throws JSONException
	*/
	public static org.json.JSONObject addNewSurvey(java.lang.String name)
		throws RemoteException {
		try {
			org.json.JSONObject returnValue = SurveysServiceUtil.addNewSurvey(name);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	/**
	* This adds a Survey with all the questions and choices
	*
	* @param name
	* @param jsonInput - Json array of json objects
	[
	{"QName":"", "QText":"", "Required":"", "Choices":["choice 1", "choice 2", "choice 3"], "Type":"Single/multiple/boolean"},
	{"QName":"", "QText":"", "Required":"", "Choices":["choice 1", "choice 2", "choice 3"], "Type":"Single/multiple/boolean"}
	]
	* @return
	* @throws JSONException
	*/
	public static org.json.JSONObject addNewSurvey(java.lang.String name,
		java.lang.String jsonInput) throws RemoteException {
		try {
			org.json.JSONObject returnValue = SurveysServiceUtil.addNewSurvey(name,
					jsonInput);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	/**
	* JSON Web service to delete an existing survey based on survey ID
	*/
	public static org.json.JSONObject deleteSurvey() throws RemoteException {
		try {
			org.json.JSONObject returnValue = SurveysServiceUtil.deleteSurvey();

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static org.json.JSONObject deleteSurvey(long surveyID)
		throws RemoteException {
		try {
			org.json.JSONObject returnValue = SurveysServiceUtil.deleteSurvey(surveyID);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(SurveysServiceSoap.class);
}