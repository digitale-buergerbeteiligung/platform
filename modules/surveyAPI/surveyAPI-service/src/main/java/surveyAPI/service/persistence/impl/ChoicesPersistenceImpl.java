/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import surveyAPI.exception.NoSuchChoicesException;

import surveyAPI.model.Choices;

import surveyAPI.model.impl.ChoicesImpl;
import surveyAPI.model.impl.ChoicesModelImpl;

import surveyAPI.service.persistence.ChoicesPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the choices service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ChoicesPersistence
 * @see surveyAPI.service.persistence.ChoicesUtil
 * @generated
 */
@ProviderType
public class ChoicesPersistenceImpl extends BasePersistenceImpl<Choices>
	implements ChoicesPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ChoicesUtil} to access the choices persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ChoicesImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ChoicesModelImpl.ENTITY_CACHE_ENABLED,
			ChoicesModelImpl.FINDER_CACHE_ENABLED, ChoicesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ChoicesModelImpl.ENTITY_CACHE_ENABLED,
			ChoicesModelImpl.FINDER_CACHE_ENABLED, ChoicesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ChoicesModelImpl.ENTITY_CACHE_ENABLED,
			ChoicesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CTEXT = new FinderPath(ChoicesModelImpl.ENTITY_CACHE_ENABLED,
			ChoicesModelImpl.FINDER_CACHE_ENABLED, ChoicesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCTEXT",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CTEXT = new FinderPath(ChoicesModelImpl.ENTITY_CACHE_ENABLED,
			ChoicesModelImpl.FINDER_CACHE_ENABLED, ChoicesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCTEXT",
			new String[] { String.class.getName() },
			ChoicesModelImpl.CHOICETEXT_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CTEXT = new FinderPath(ChoicesModelImpl.ENTITY_CACHE_ENABLED,
			ChoicesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCTEXT",
			new String[] { String.class.getName() });

	/**
	 * Returns all the choiceses where ChoiceText = &#63;.
	 *
	 * @param ChoiceText the choice text
	 * @return the matching choiceses
	 */
	@Override
	public List<Choices> findByCTEXT(String ChoiceText) {
		return findByCTEXT(ChoiceText, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the choiceses where ChoiceText = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ChoiceText the choice text
	 * @param start the lower bound of the range of choiceses
	 * @param end the upper bound of the range of choiceses (not inclusive)
	 * @return the range of matching choiceses
	 */
	@Override
	public List<Choices> findByCTEXT(String ChoiceText, int start, int end) {
		return findByCTEXT(ChoiceText, start, end, null);
	}

	/**
	 * Returns an ordered range of all the choiceses where ChoiceText = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ChoiceText the choice text
	 * @param start the lower bound of the range of choiceses
	 * @param end the upper bound of the range of choiceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching choiceses
	 */
	@Override
	public List<Choices> findByCTEXT(String ChoiceText, int start, int end,
		OrderByComparator<Choices> orderByComparator) {
		return findByCTEXT(ChoiceText, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the choiceses where ChoiceText = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ChoiceText the choice text
	 * @param start the lower bound of the range of choiceses
	 * @param end the upper bound of the range of choiceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching choiceses
	 */
	@Override
	public List<Choices> findByCTEXT(String ChoiceText, int start, int end,
		OrderByComparator<Choices> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CTEXT;
			finderArgs = new Object[] { ChoiceText };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CTEXT;
			finderArgs = new Object[] { ChoiceText, start, end, orderByComparator };
		}

		List<Choices> list = null;

		if (retrieveFromCache) {
			list = (List<Choices>)finderCache.getResult(finderPath, finderArgs,
					this);

			if ((list != null) && !list.isEmpty()) {
				for (Choices choices : list) {
					if (!Objects.equals(ChoiceText, choices.getChoiceText())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CHOICES_WHERE);

			boolean bindChoiceText = false;

			if (ChoiceText == null) {
				query.append(_FINDER_COLUMN_CTEXT_CHOICETEXT_1);
			}
			else if (ChoiceText.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CTEXT_CHOICETEXT_3);
			}
			else {
				bindChoiceText = true;

				query.append(_FINDER_COLUMN_CTEXT_CHOICETEXT_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ChoicesModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindChoiceText) {
					qPos.add(ChoiceText);
				}

				if (!pagination) {
					list = (List<Choices>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Choices>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first choices in the ordered set where ChoiceText = &#63;.
	 *
	 * @param ChoiceText the choice text
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching choices
	 * @throws NoSuchChoicesException if a matching choices could not be found
	 */
	@Override
	public Choices findByCTEXT_First(String ChoiceText,
		OrderByComparator<Choices> orderByComparator)
		throws NoSuchChoicesException {
		Choices choices = fetchByCTEXT_First(ChoiceText, orderByComparator);

		if (choices != null) {
			return choices;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ChoiceText=");
		msg.append(ChoiceText);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchChoicesException(msg.toString());
	}

	/**
	 * Returns the first choices in the ordered set where ChoiceText = &#63;.
	 *
	 * @param ChoiceText the choice text
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching choices, or <code>null</code> if a matching choices could not be found
	 */
	@Override
	public Choices fetchByCTEXT_First(String ChoiceText,
		OrderByComparator<Choices> orderByComparator) {
		List<Choices> list = findByCTEXT(ChoiceText, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last choices in the ordered set where ChoiceText = &#63;.
	 *
	 * @param ChoiceText the choice text
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching choices
	 * @throws NoSuchChoicesException if a matching choices could not be found
	 */
	@Override
	public Choices findByCTEXT_Last(String ChoiceText,
		OrderByComparator<Choices> orderByComparator)
		throws NoSuchChoicesException {
		Choices choices = fetchByCTEXT_Last(ChoiceText, orderByComparator);

		if (choices != null) {
			return choices;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ChoiceText=");
		msg.append(ChoiceText);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchChoicesException(msg.toString());
	}

	/**
	 * Returns the last choices in the ordered set where ChoiceText = &#63;.
	 *
	 * @param ChoiceText the choice text
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching choices, or <code>null</code> if a matching choices could not be found
	 */
	@Override
	public Choices fetchByCTEXT_Last(String ChoiceText,
		OrderByComparator<Choices> orderByComparator) {
		int count = countByCTEXT(ChoiceText);

		if (count == 0) {
			return null;
		}

		List<Choices> list = findByCTEXT(ChoiceText, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the choiceses before and after the current choices in the ordered set where ChoiceText = &#63;.
	 *
	 * @param ChoiceID the primary key of the current choices
	 * @param ChoiceText the choice text
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next choices
	 * @throws NoSuchChoicesException if a choices with the primary key could not be found
	 */
	@Override
	public Choices[] findByCTEXT_PrevAndNext(long ChoiceID, String ChoiceText,
		OrderByComparator<Choices> orderByComparator)
		throws NoSuchChoicesException {
		Choices choices = findByPrimaryKey(ChoiceID);

		Session session = null;

		try {
			session = openSession();

			Choices[] array = new ChoicesImpl[3];

			array[0] = getByCTEXT_PrevAndNext(session, choices, ChoiceText,
					orderByComparator, true);

			array[1] = choices;

			array[2] = getByCTEXT_PrevAndNext(session, choices, ChoiceText,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Choices getByCTEXT_PrevAndNext(Session session, Choices choices,
		String ChoiceText, OrderByComparator<Choices> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CHOICES_WHERE);

		boolean bindChoiceText = false;

		if (ChoiceText == null) {
			query.append(_FINDER_COLUMN_CTEXT_CHOICETEXT_1);
		}
		else if (ChoiceText.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CTEXT_CHOICETEXT_3);
		}
		else {
			bindChoiceText = true;

			query.append(_FINDER_COLUMN_CTEXT_CHOICETEXT_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ChoicesModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindChoiceText) {
			qPos.add(ChoiceText);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(choices);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Choices> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the choiceses where ChoiceText = &#63; from the database.
	 *
	 * @param ChoiceText the choice text
	 */
	@Override
	public void removeByCTEXT(String ChoiceText) {
		for (Choices choices : findByCTEXT(ChoiceText, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(choices);
		}
	}

	/**
	 * Returns the number of choiceses where ChoiceText = &#63;.
	 *
	 * @param ChoiceText the choice text
	 * @return the number of matching choiceses
	 */
	@Override
	public int countByCTEXT(String ChoiceText) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CTEXT;

		Object[] finderArgs = new Object[] { ChoiceText };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CHOICES_WHERE);

			boolean bindChoiceText = false;

			if (ChoiceText == null) {
				query.append(_FINDER_COLUMN_CTEXT_CHOICETEXT_1);
			}
			else if (ChoiceText.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CTEXT_CHOICETEXT_3);
			}
			else {
				bindChoiceText = true;

				query.append(_FINDER_COLUMN_CTEXT_CHOICETEXT_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindChoiceText) {
					qPos.add(ChoiceText);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CTEXT_CHOICETEXT_1 = "choices.ChoiceText IS NULL";
	private static final String _FINDER_COLUMN_CTEXT_CHOICETEXT_2 = "choices.ChoiceText = ?";
	private static final String _FINDER_COLUMN_CTEXT_CHOICETEXT_3 = "(choices.ChoiceText IS NULL OR choices.ChoiceText = '')";

	public ChoicesPersistenceImpl() {
		setModelClass(Choices.class);
	}

	/**
	 * Caches the choices in the entity cache if it is enabled.
	 *
	 * @param choices the choices
	 */
	@Override
	public void cacheResult(Choices choices) {
		entityCache.putResult(ChoicesModelImpl.ENTITY_CACHE_ENABLED,
			ChoicesImpl.class, choices.getPrimaryKey(), choices);

		choices.resetOriginalValues();
	}

	/**
	 * Caches the choiceses in the entity cache if it is enabled.
	 *
	 * @param choiceses the choiceses
	 */
	@Override
	public void cacheResult(List<Choices> choiceses) {
		for (Choices choices : choiceses) {
			if (entityCache.getResult(ChoicesModelImpl.ENTITY_CACHE_ENABLED,
						ChoicesImpl.class, choices.getPrimaryKey()) == null) {
				cacheResult(choices);
			}
			else {
				choices.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all choiceses.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ChoicesImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the choices.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Choices choices) {
		entityCache.removeResult(ChoicesModelImpl.ENTITY_CACHE_ENABLED,
			ChoicesImpl.class, choices.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Choices> choiceses) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Choices choices : choiceses) {
			entityCache.removeResult(ChoicesModelImpl.ENTITY_CACHE_ENABLED,
				ChoicesImpl.class, choices.getPrimaryKey());
		}
	}

	/**
	 * Creates a new choices with the primary key. Does not add the choices to the database.
	 *
	 * @param ChoiceID the primary key for the new choices
	 * @return the new choices
	 */
	@Override
	public Choices create(long ChoiceID) {
		Choices choices = new ChoicesImpl();

		choices.setNew(true);
		choices.setPrimaryKey(ChoiceID);

		return choices;
	}

	/**
	 * Removes the choices with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ChoiceID the primary key of the choices
	 * @return the choices that was removed
	 * @throws NoSuchChoicesException if a choices with the primary key could not be found
	 */
	@Override
	public Choices remove(long ChoiceID) throws NoSuchChoicesException {
		return remove((Serializable)ChoiceID);
	}

	/**
	 * Removes the choices with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the choices
	 * @return the choices that was removed
	 * @throws NoSuchChoicesException if a choices with the primary key could not be found
	 */
	@Override
	public Choices remove(Serializable primaryKey)
		throws NoSuchChoicesException {
		Session session = null;

		try {
			session = openSession();

			Choices choices = (Choices)session.get(ChoicesImpl.class, primaryKey);

			if (choices == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchChoicesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(choices);
		}
		catch (NoSuchChoicesException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Choices removeImpl(Choices choices) {
		choices = toUnwrappedModel(choices);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(choices)) {
				choices = (Choices)session.get(ChoicesImpl.class,
						choices.getPrimaryKeyObj());
			}

			if (choices != null) {
				session.delete(choices);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (choices != null) {
			clearCache(choices);
		}

		return choices;
	}

	@Override
	public Choices updateImpl(Choices choices) {
		choices = toUnwrappedModel(choices);

		boolean isNew = choices.isNew();

		ChoicesModelImpl choicesModelImpl = (ChoicesModelImpl)choices;

		Session session = null;

		try {
			session = openSession();

			if (choices.isNew()) {
				session.save(choices);

				choices.setNew(false);
			}
			else {
				choices = (Choices)session.merge(choices);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!ChoicesModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { choicesModelImpl.getChoiceText() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_CTEXT, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CTEXT,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((choicesModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CTEXT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						choicesModelImpl.getOriginalChoiceText()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_CTEXT, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CTEXT,
					args);

				args = new Object[] { choicesModelImpl.getChoiceText() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_CTEXT, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CTEXT,
					args);
			}
		}

		entityCache.putResult(ChoicesModelImpl.ENTITY_CACHE_ENABLED,
			ChoicesImpl.class, choices.getPrimaryKey(), choices, false);

		choices.resetOriginalValues();

		return choices;
	}

	protected Choices toUnwrappedModel(Choices choices) {
		if (choices instanceof ChoicesImpl) {
			return choices;
		}

		ChoicesImpl choicesImpl = new ChoicesImpl();

		choicesImpl.setNew(choices.isNew());
		choicesImpl.setPrimaryKey(choices.getPrimaryKey());

		choicesImpl.setChoiceID(choices.getChoiceID());
		choicesImpl.setChoiceText(choices.getChoiceText());

		return choicesImpl;
	}

	/**
	 * Returns the choices with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the choices
	 * @return the choices
	 * @throws NoSuchChoicesException if a choices with the primary key could not be found
	 */
	@Override
	public Choices findByPrimaryKey(Serializable primaryKey)
		throws NoSuchChoicesException {
		Choices choices = fetchByPrimaryKey(primaryKey);

		if (choices == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchChoicesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return choices;
	}

	/**
	 * Returns the choices with the primary key or throws a {@link NoSuchChoicesException} if it could not be found.
	 *
	 * @param ChoiceID the primary key of the choices
	 * @return the choices
	 * @throws NoSuchChoicesException if a choices with the primary key could not be found
	 */
	@Override
	public Choices findByPrimaryKey(long ChoiceID)
		throws NoSuchChoicesException {
		return findByPrimaryKey((Serializable)ChoiceID);
	}

	/**
	 * Returns the choices with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the choices
	 * @return the choices, or <code>null</code> if a choices with the primary key could not be found
	 */
	@Override
	public Choices fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ChoicesModelImpl.ENTITY_CACHE_ENABLED,
				ChoicesImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Choices choices = (Choices)serializable;

		if (choices == null) {
			Session session = null;

			try {
				session = openSession();

				choices = (Choices)session.get(ChoicesImpl.class, primaryKey);

				if (choices != null) {
					cacheResult(choices);
				}
				else {
					entityCache.putResult(ChoicesModelImpl.ENTITY_CACHE_ENABLED,
						ChoicesImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ChoicesModelImpl.ENTITY_CACHE_ENABLED,
					ChoicesImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return choices;
	}

	/**
	 * Returns the choices with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ChoiceID the primary key of the choices
	 * @return the choices, or <code>null</code> if a choices with the primary key could not be found
	 */
	@Override
	public Choices fetchByPrimaryKey(long ChoiceID) {
		return fetchByPrimaryKey((Serializable)ChoiceID);
	}

	@Override
	public Map<Serializable, Choices> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Choices> map = new HashMap<Serializable, Choices>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Choices choices = fetchByPrimaryKey(primaryKey);

			if (choices != null) {
				map.put(primaryKey, choices);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ChoicesModelImpl.ENTITY_CACHE_ENABLED,
					ChoicesImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Choices)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_CHOICES_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Choices choices : (List<Choices>)q.list()) {
				map.put(choices.getPrimaryKeyObj(), choices);

				cacheResult(choices);

				uncachedPrimaryKeys.remove(choices.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ChoicesModelImpl.ENTITY_CACHE_ENABLED,
					ChoicesImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the choiceses.
	 *
	 * @return the choiceses
	 */
	@Override
	public List<Choices> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the choiceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of choiceses
	 * @param end the upper bound of the range of choiceses (not inclusive)
	 * @return the range of choiceses
	 */
	@Override
	public List<Choices> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the choiceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of choiceses
	 * @param end the upper bound of the range of choiceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of choiceses
	 */
	@Override
	public List<Choices> findAll(int start, int end,
		OrderByComparator<Choices> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the choiceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of choiceses
	 * @param end the upper bound of the range of choiceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of choiceses
	 */
	@Override
	public List<Choices> findAll(int start, int end,
		OrderByComparator<Choices> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Choices> list = null;

		if (retrieveFromCache) {
			list = (List<Choices>)finderCache.getResult(finderPath, finderArgs,
					this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_CHOICES);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CHOICES;

				if (pagination) {
					sql = sql.concat(ChoicesModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Choices>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Choices>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the choiceses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Choices choices : findAll()) {
			remove(choices);
		}
	}

	/**
	 * Returns the number of choiceses.
	 *
	 * @return the number of choiceses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CHOICES);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ChoicesModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the choices persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ChoicesImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_CHOICES = "SELECT choices FROM Choices choices";
	private static final String _SQL_SELECT_CHOICES_WHERE_PKS_IN = "SELECT choices FROM Choices choices WHERE ChoiceID IN (";
	private static final String _SQL_SELECT_CHOICES_WHERE = "SELECT choices FROM Choices choices WHERE ";
	private static final String _SQL_COUNT_CHOICES = "SELECT COUNT(choices) FROM Choices choices";
	private static final String _SQL_COUNT_CHOICES_WHERE = "SELECT COUNT(choices) FROM Choices choices WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "choices.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Choices exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Choices exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ChoicesPersistenceImpl.class);
}