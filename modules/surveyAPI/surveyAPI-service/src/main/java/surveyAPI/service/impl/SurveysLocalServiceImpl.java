/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.liferay.portal.kernel.exception.PortalException;

import aQute.bnd.annotation.ProviderType;
import surveyAPI.model.QuestionOptions;
import surveyAPI.model.Surveys;
import surveyAPI.model.impl.SurveysImpl;
import surveyAPI.service.ChoicesLocalServiceUtil;
import surveyAPI.service.QuestionOptionsLocalServiceUtil;
import surveyAPI.service.QuestionsLocalServiceUtil;
import surveyAPI.service.SurveysLocalServiceUtil;
import surveyAPI.service.base.SurveysLocalServiceBaseImpl;

/**
 * The implementation of the surveys local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link surveyAPI.service.SurveysLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SurveysLocalServiceBaseImpl
 * @see surveyAPI.service.SurveysLocalServiceUtil
 */
@ProviderType
public class SurveysLocalServiceImpl extends SurveysLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link surveyAPI.service.SurveysLocalServiceUtil} to access the surveys local service.
	 */
	public JSONObject getSurvey(long surveyId) throws JSONException{
		try{
			JSONObject surveyJSON = new JSONObject();//{k:v,k:[ , , ],k:[[],[],[]]}			
			Surveys surveyObj = SurveysLocalServiceUtil.getSurveys(surveyId);
			surveyJSON.put("Survey", surveyObj.getSurveyName());
			
			JSONArray questionArray = new JSONArray();//[ , , ]
			JSONArray choicesArray = new JSONArray();//[ [qnChoiceArray], [qnChoiceArray], [qnChoiceArray] ]
			
			String[] qIds = surveyObj.getQuestionIds().split(",");
			JSONArray questionIdArray = new JSONArray(Arrays.asList(qIds));
			
			for(String qId: qIds){
				questionArray.put(QuestionsLocalServiceUtil.getQuestions(Long.parseLong(qId)).getQuestionText());
				Iterator<QuestionOptions> iter = QuestionOptionsLocalServiceUtil.findByQID(Long.parseLong(qId)).iterator();
				JSONArray qnChoiceArray = new JSONArray();
				while(iter.hasNext()){
					long choiceId = (iter.next().getChoiceID());
					qnChoiceArray.put(ChoicesLocalServiceUtil.getChoices(choiceId));
				}
				choicesArray.put(qnChoiceArray);
			}
			
			surveyJSON.put("Questions", questionArray);
			surveyJSON.put("Choices", choicesArray);
			surveyJSON.put("QuestionIds", questionIdArray);
			
			return surveyJSON;
		}catch(PortalException ex){
			ex.printStackTrace();
			return new JSONObject().put("error", "Found no survey matching the ID");
		}
	}
	
	public JSONObject addNewSurvey(String name, String jsonInput)throws JSONException{
		JSONObject returnJSON = new JSONObject();
		if(!name.isEmpty()){
			Surveys newSurvey = new SurveysImpl();
			newSurvey.setQuestionIds("");
			newSurvey.setSurveyName(name);
			newSurvey.setDateOfCreation(new Date(System.currentTimeMillis()));
			
			SurveysLocalServiceUtil.addSurveys(newSurvey);
			long surveyId = newSurvey.getPrimaryKey();
			
			if(!jsonInput.isEmpty()){
				try{
					JSONArray surveyQuestionArray = new JSONArray(jsonInput);
					
					for(int i=0;i<surveyQuestionArray.length();i++){
						JSONObject questionJson = surveyQuestionArray.getJSONObject(i);
						JSONArray choiceArray = questionJson.getJSONArray("Choices");
						QuestionsLocalServiceUtil.quesNChoiceUtil(surveyId, questionJson.getString("QName"),questionJson.getString("QText"), questionJson.getBoolean("Required"), choiceArray, questionJson.getString("Type"));
					}
				}catch(JSONException ex){
					ex.printStackTrace();
					return returnJSON.put("error message", "Invalid input");
				}
			}
			
			return returnJSON.put("surveyID", newSurvey.getPrimaryKey());
		}else{
			return returnJSON.put("error message", "Empty input");
		}
	}
	
	public JSONObject addNewSurvey(String name) throws JSONException{
		if(!name.isEmpty()){
			Surveys newSurvey = new SurveysImpl();
			newSurvey.setQuestionIds("");
			newSurvey.setSurveyName(name);
			newSurvey.setDateOfCreation(new Date(System.currentTimeMillis()));
			
			SurveysLocalServiceUtil.addSurveys(newSurvey);
			return (new JSONObject()).put("surveyID", newSurvey.getPrimaryKey());
		}else{
			return (new JSONObject()).put("error message", "Empty input");
		}
	}
	
	public JSONObject deleteSurvey(long surveyID) throws JSONException{
		try{
			String qIds = SurveysLocalServiceUtil.getSurveys(surveyID).getQuestionIds();
			SurveysLocalServiceUtil.deleteSurveys(surveyID);
			if(qIds!=null && !qIds.isEmpty()){
				String[] qIdArray = qIds.split(",");
				for(String qId:qIdArray){
					// This try checks for the case when question id is not present in the Questions table
					try{
						QuestionsLocalServiceUtil.deleteQuestions(Long.parseLong(qId));
					}catch(PortalException e){
						e.printStackTrace();
					}
				}
			}
			return (new JSONObject()).put("message", "success");
		}catch (PortalException e) {
			return (new JSONObject()).put("error message", "Found no survey matching the ID");
		}
	}
	
	public JSONObject getAllQuestionIds(long surveyID) throws JSONException{
		try{
			Surveys surveyObj = SurveysLocalServiceUtil.getSurveys(surveyID);
			
			JSONArray jsArray = new JSONArray(Arrays.asList(surveyObj.getQuestionIds().split(",")));
			return (new JSONObject()).put("QuestionIds", jsArray);
		}catch(PortalException ex){
			return (new JSONObject()).put("error message", "Found no survey matching the ID");
		}
	}
}