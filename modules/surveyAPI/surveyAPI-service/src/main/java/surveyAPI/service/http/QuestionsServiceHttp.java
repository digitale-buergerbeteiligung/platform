/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.http;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.HttpPrincipal;
import com.liferay.portal.kernel.service.http.TunnelUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;

import surveyAPI.service.QuestionsServiceUtil;

/**
 * Provides the HTTP utility for the
 * {@link QuestionsServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it requires an additional
 * {@link HttpPrincipal} parameter.
 *
 * <p>
 * The benefits of using the HTTP utility is that it is fast and allows for
 * tunneling without the cost of serializing to text. The drawback is that it
 * only works with Java.
 * </p>
 *
 * <p>
 * Set the property <b>tunnel.servlet.hosts.allowed</b> in portal.properties to
 * configure security.
 * </p>
 *
 * <p>
 * The HTTP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuestionsServiceSoap
 * @see HttpPrincipal
 * @see QuestionsServiceUtil
 * @generated
 */
@ProviderType
public class QuestionsServiceHttp {
	public static org.json.JSONObject deleteQuestion(
		HttpPrincipal httpPrincipal, long surveyID, long qID)
		throws org.json.JSONException {
		try {
			MethodKey methodKey = new MethodKey(QuestionsServiceUtil.class,
					"deleteQuestion", _deleteQuestionParameterTypes0);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					surveyID, qID);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				if (e instanceof org.json.JSONException) {
					throw (org.json.JSONException)e;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (org.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static org.json.JSONObject editQuestion(
		HttpPrincipal httpPrincipal, long qID, java.lang.String qText)
		throws org.json.JSONException {
		try {
			MethodKey methodKey = new MethodKey(QuestionsServiceUtil.class,
					"editQuestion", _editQuestionParameterTypes1);

			MethodHandler methodHandler = new MethodHandler(methodKey, qID,
					qText);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				if (e instanceof org.json.JSONException) {
					throw (org.json.JSONException)e;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (org.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static org.json.JSONObject addNewQuestion(
		HttpPrincipal httpPrincipal, long surveyID, java.lang.String QName,
		java.lang.String QText, java.lang.String Type, boolean required)
		throws org.json.JSONException {
		try {
			MethodKey methodKey = new MethodKey(QuestionsServiceUtil.class,
					"addNewQuestion", _addNewQuestionParameterTypes2);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					surveyID, QName, QText, Type, required);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				if (e instanceof org.json.JSONException) {
					throw (org.json.JSONException)e;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (org.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static org.json.JSONObject addQuestionNChoices(
		HttpPrincipal httpPrincipal, java.lang.String quesNChoices)
		throws org.json.JSONException {
		try {
			MethodKey methodKey = new MethodKey(QuestionsServiceUtil.class,
					"addQuestionNChoices", _addQuestionNChoicesParameterTypes3);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					quesNChoices);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				if (e instanceof org.json.JSONException) {
					throw (org.json.JSONException)e;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (org.json.JSONObject)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(QuestionsServiceHttp.class);
	private static final Class<?>[] _deleteQuestionParameterTypes0 = new Class[] {
			long.class, long.class
		};
	private static final Class<?>[] _editQuestionParameterTypes1 = new Class[] {
			long.class, java.lang.String.class
		};
	private static final Class<?>[] _addNewQuestionParameterTypes2 = new Class[] {
			long.class, java.lang.String.class, java.lang.String.class,
			java.lang.String.class, boolean.class
		};
	private static final Class<?>[] _addQuestionNChoicesParameterTypes3 = new Class[] {
			java.lang.String.class
		};
}