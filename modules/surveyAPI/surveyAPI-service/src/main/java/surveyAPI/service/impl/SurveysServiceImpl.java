/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.impl;

import org.json.JSONException;
import org.json.JSONObject;

import com.liferay.portal.kernel.jsonwebservice.JSONWebService;

import aQute.bnd.annotation.ProviderType;
import surveyAPI.service.SurveysLocalServiceUtil;
import surveyAPI.service.base.SurveysServiceBaseImpl;

/**
 * The implementation of the surveys remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link surveyAPI.service.SurveysService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Saurabh Narayan Singh
 * @see SurveysServiceBaseImpl
 * @see surveyAPI.service.SurveysServiceUtil
 */
@ProviderType
public class SurveysServiceImpl extends SurveysServiceBaseImpl{
	
	/**
	 * JSON Web service to fetch all the question ids associated with a given survey id
	 */
	@JSONWebService(method = "GET")
	public JSONObject getAllQuestionIds() throws JSONException{
		return (new JSONObject()).put("error message", "Invalid input - Missing parameter");
	}
	
	@JSONWebService(method = "GET")
	public JSONObject getAllQuestionIds(long surveyID) throws JSONException{
		return SurveysLocalServiceUtil.getAllQuestionIds(surveyID);
	}
	
	/**
	 * JSON Web service to get full survey
	 * @return JSONObject {Questions:JSONArray, Choices:JSONArray, QuestionIds:JSONArray}
	 */
	@JSONWebService(method = "GET")
	public JSONObject getSurvey(long surveyId) throws JSONException{
		return SurveysLocalServiceUtil.getSurvey(surveyId);
	}
	
	/**
	 * JSON Web Service to add a new survey
	 */
	@JSONWebService(method = "POST")
	public JSONObject addNewSurvey() throws JSONException{
		return (new JSONObject()).put("error message", "Invalid input - Missing parameter");
	}
	
	/**
	 * This just adds a survey and returns the survey ID, user can later add questions
	 * @param name
	 * @return
	 * @throws JSONException
	 */
	@JSONWebService(method = "POST")
	public JSONObject addNewSurvey(String name) throws JSONException{
		return SurveysLocalServiceUtil.addNewSurvey(name);
	}
	
	/**
	 * This adds a Survey with all the questions and choices
	 * @param name
	 * @param jsonInput - Json array of json objects
	 * [
	 * 		{"QName":"", "QText":"", "Required":"", "Choices":["choice 1", "choice 2", "choice 3"], "Type":"Single/multiple/boolean"},
	 * 		{"QName":"", "QText":"", "Required":"", "Choices":["choice 1", "choice 2", "choice 3"], "Type":"Single/multiple/boolean"}
	 * ]
	 * @return
	 * @throws JSONException
	 */
	@JSONWebService(method = "POST")
	public JSONObject addNewSurvey(String name, String jsonInput)throws JSONException{
		return SurveysLocalServiceUtil.addNewSurvey(name, jsonInput);
	}
	
	/**
	 * JSON Web service to delete an existing survey based on survey ID
	 */
	@JSONWebService(method = "DELETE")
	public JSONObject deleteSurvey() throws JSONException{
		return (new JSONObject()).put("error message", "Invalid input - Missing parameter");
	}
	
	@JSONWebService(method = "DELETE")
	public JSONObject deleteSurvey(long surveyID) throws JSONException{
		return SurveysLocalServiceUtil.deleteSurvey(surveyID);
	}
}