/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import surveyAPI.exception.NoSuchQuestionOptionsException;

import surveyAPI.model.QuestionOptions;

import surveyAPI.model.impl.QuestionOptionsImpl;
import surveyAPI.model.impl.QuestionOptionsModelImpl;

import surveyAPI.service.persistence.QuestionOptionsPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the question options service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see QuestionOptionsPersistence
 * @see surveyAPI.service.persistence.QuestionOptionsUtil
 * @generated
 */
@ProviderType
public class QuestionOptionsPersistenceImpl extends BasePersistenceImpl<QuestionOptions>
	implements QuestionOptionsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link QuestionOptionsUtil} to access the question options persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = QuestionOptionsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionOptionsModelImpl.FINDER_CACHE_ENABLED,
			QuestionOptionsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionOptionsModelImpl.FINDER_CACHE_ENABLED,
			QuestionOptionsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionOptionsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QID = new FinderPath(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionOptionsModelImpl.FINDER_CACHE_ENABLED,
			QuestionOptionsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByQID",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QID = new FinderPath(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionOptionsModelImpl.FINDER_CACHE_ENABLED,
			QuestionOptionsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByQID",
			new String[] { Long.class.getName() },
			QuestionOptionsModelImpl.QUESTIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_QID = new FinderPath(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionOptionsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByQID",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the question optionses where QuestionID = &#63;.
	 *
	 * @param QuestionID the question ID
	 * @return the matching question optionses
	 */
	@Override
	public List<QuestionOptions> findByQID(long QuestionID) {
		return findByQID(QuestionID, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the question optionses where QuestionID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param QuestionID the question ID
	 * @param start the lower bound of the range of question optionses
	 * @param end the upper bound of the range of question optionses (not inclusive)
	 * @return the range of matching question optionses
	 */
	@Override
	public List<QuestionOptions> findByQID(long QuestionID, int start, int end) {
		return findByQID(QuestionID, start, end, null);
	}

	/**
	 * Returns an ordered range of all the question optionses where QuestionID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param QuestionID the question ID
	 * @param start the lower bound of the range of question optionses
	 * @param end the upper bound of the range of question optionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching question optionses
	 */
	@Override
	public List<QuestionOptions> findByQID(long QuestionID, int start, int end,
		OrderByComparator<QuestionOptions> orderByComparator) {
		return findByQID(QuestionID, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the question optionses where QuestionID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param QuestionID the question ID
	 * @param start the lower bound of the range of question optionses
	 * @param end the upper bound of the range of question optionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching question optionses
	 */
	@Override
	public List<QuestionOptions> findByQID(long QuestionID, int start, int end,
		OrderByComparator<QuestionOptions> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QID;
			finderArgs = new Object[] { QuestionID };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QID;
			finderArgs = new Object[] { QuestionID, start, end, orderByComparator };
		}

		List<QuestionOptions> list = null;

		if (retrieveFromCache) {
			list = (List<QuestionOptions>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (QuestionOptions questionOptions : list) {
					if ((QuestionID != questionOptions.getQuestionID())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_QUESTIONOPTIONS_WHERE);

			query.append(_FINDER_COLUMN_QID_QUESTIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(QuestionOptionsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(QuestionID);

				if (!pagination) {
					list = (List<QuestionOptions>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<QuestionOptions>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first question options in the ordered set where QuestionID = &#63;.
	 *
	 * @param QuestionID the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching question options
	 * @throws NoSuchQuestionOptionsException if a matching question options could not be found
	 */
	@Override
	public QuestionOptions findByQID_First(long QuestionID,
		OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException {
		QuestionOptions questionOptions = fetchByQID_First(QuestionID,
				orderByComparator);

		if (questionOptions != null) {
			return questionOptions;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("QuestionID=");
		msg.append(QuestionID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuestionOptionsException(msg.toString());
	}

	/**
	 * Returns the first question options in the ordered set where QuestionID = &#63;.
	 *
	 * @param QuestionID the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching question options, or <code>null</code> if a matching question options could not be found
	 */
	@Override
	public QuestionOptions fetchByQID_First(long QuestionID,
		OrderByComparator<QuestionOptions> orderByComparator) {
		List<QuestionOptions> list = findByQID(QuestionID, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last question options in the ordered set where QuestionID = &#63;.
	 *
	 * @param QuestionID the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching question options
	 * @throws NoSuchQuestionOptionsException if a matching question options could not be found
	 */
	@Override
	public QuestionOptions findByQID_Last(long QuestionID,
		OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException {
		QuestionOptions questionOptions = fetchByQID_Last(QuestionID,
				orderByComparator);

		if (questionOptions != null) {
			return questionOptions;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("QuestionID=");
		msg.append(QuestionID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuestionOptionsException(msg.toString());
	}

	/**
	 * Returns the last question options in the ordered set where QuestionID = &#63;.
	 *
	 * @param QuestionID the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching question options, or <code>null</code> if a matching question options could not be found
	 */
	@Override
	public QuestionOptions fetchByQID_Last(long QuestionID,
		OrderByComparator<QuestionOptions> orderByComparator) {
		int count = countByQID(QuestionID);

		if (count == 0) {
			return null;
		}

		List<QuestionOptions> list = findByQID(QuestionID, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the question optionses before and after the current question options in the ordered set where QuestionID = &#63;.
	 *
	 * @param OptionID the primary key of the current question options
	 * @param QuestionID the question ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next question options
	 * @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	 */
	@Override
	public QuestionOptions[] findByQID_PrevAndNext(long OptionID,
		long QuestionID, OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException {
		QuestionOptions questionOptions = findByPrimaryKey(OptionID);

		Session session = null;

		try {
			session = openSession();

			QuestionOptions[] array = new QuestionOptionsImpl[3];

			array[0] = getByQID_PrevAndNext(session, questionOptions,
					QuestionID, orderByComparator, true);

			array[1] = questionOptions;

			array[2] = getByQID_PrevAndNext(session, questionOptions,
					QuestionID, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected QuestionOptions getByQID_PrevAndNext(Session session,
		QuestionOptions questionOptions, long QuestionID,
		OrderByComparator<QuestionOptions> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_QUESTIONOPTIONS_WHERE);

		query.append(_FINDER_COLUMN_QID_QUESTIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(QuestionOptionsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(QuestionID);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(questionOptions);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<QuestionOptions> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the question optionses where QuestionID = &#63; from the database.
	 *
	 * @param QuestionID the question ID
	 */
	@Override
	public void removeByQID(long QuestionID) {
		for (QuestionOptions questionOptions : findByQID(QuestionID,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(questionOptions);
		}
	}

	/**
	 * Returns the number of question optionses where QuestionID = &#63;.
	 *
	 * @param QuestionID the question ID
	 * @return the number of matching question optionses
	 */
	@Override
	public int countByQID(long QuestionID) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_QID;

		Object[] finderArgs = new Object[] { QuestionID };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_QUESTIONOPTIONS_WHERE);

			query.append(_FINDER_COLUMN_QID_QUESTIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(QuestionID);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_QID_QUESTIONID_2 = "questionOptions.QuestionID = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CHID = new FinderPath(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionOptionsModelImpl.FINDER_CACHE_ENABLED,
			QuestionOptionsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCHID",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHID = new FinderPath(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionOptionsModelImpl.FINDER_CACHE_ENABLED,
			QuestionOptionsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCHID",
			new String[] { Long.class.getName() },
			QuestionOptionsModelImpl.CHOICEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CHID = new FinderPath(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionOptionsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCHID",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the question optionses where ChoiceID = &#63;.
	 *
	 * @param ChoiceID the choice ID
	 * @return the matching question optionses
	 */
	@Override
	public List<QuestionOptions> findByCHID(long ChoiceID) {
		return findByCHID(ChoiceID, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the question optionses where ChoiceID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ChoiceID the choice ID
	 * @param start the lower bound of the range of question optionses
	 * @param end the upper bound of the range of question optionses (not inclusive)
	 * @return the range of matching question optionses
	 */
	@Override
	public List<QuestionOptions> findByCHID(long ChoiceID, int start, int end) {
		return findByCHID(ChoiceID, start, end, null);
	}

	/**
	 * Returns an ordered range of all the question optionses where ChoiceID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ChoiceID the choice ID
	 * @param start the lower bound of the range of question optionses
	 * @param end the upper bound of the range of question optionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching question optionses
	 */
	@Override
	public List<QuestionOptions> findByCHID(long ChoiceID, int start, int end,
		OrderByComparator<QuestionOptions> orderByComparator) {
		return findByCHID(ChoiceID, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the question optionses where ChoiceID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ChoiceID the choice ID
	 * @param start the lower bound of the range of question optionses
	 * @param end the upper bound of the range of question optionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching question optionses
	 */
	@Override
	public List<QuestionOptions> findByCHID(long ChoiceID, int start, int end,
		OrderByComparator<QuestionOptions> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHID;
			finderArgs = new Object[] { ChoiceID };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CHID;
			finderArgs = new Object[] { ChoiceID, start, end, orderByComparator };
		}

		List<QuestionOptions> list = null;

		if (retrieveFromCache) {
			list = (List<QuestionOptions>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (QuestionOptions questionOptions : list) {
					if ((ChoiceID != questionOptions.getChoiceID())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_QUESTIONOPTIONS_WHERE);

			query.append(_FINDER_COLUMN_CHID_CHOICEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(QuestionOptionsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ChoiceID);

				if (!pagination) {
					list = (List<QuestionOptions>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<QuestionOptions>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first question options in the ordered set where ChoiceID = &#63;.
	 *
	 * @param ChoiceID the choice ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching question options
	 * @throws NoSuchQuestionOptionsException if a matching question options could not be found
	 */
	@Override
	public QuestionOptions findByCHID_First(long ChoiceID,
		OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException {
		QuestionOptions questionOptions = fetchByCHID_First(ChoiceID,
				orderByComparator);

		if (questionOptions != null) {
			return questionOptions;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ChoiceID=");
		msg.append(ChoiceID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuestionOptionsException(msg.toString());
	}

	/**
	 * Returns the first question options in the ordered set where ChoiceID = &#63;.
	 *
	 * @param ChoiceID the choice ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching question options, or <code>null</code> if a matching question options could not be found
	 */
	@Override
	public QuestionOptions fetchByCHID_First(long ChoiceID,
		OrderByComparator<QuestionOptions> orderByComparator) {
		List<QuestionOptions> list = findByCHID(ChoiceID, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last question options in the ordered set where ChoiceID = &#63;.
	 *
	 * @param ChoiceID the choice ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching question options
	 * @throws NoSuchQuestionOptionsException if a matching question options could not be found
	 */
	@Override
	public QuestionOptions findByCHID_Last(long ChoiceID,
		OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException {
		QuestionOptions questionOptions = fetchByCHID_Last(ChoiceID,
				orderByComparator);

		if (questionOptions != null) {
			return questionOptions;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ChoiceID=");
		msg.append(ChoiceID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuestionOptionsException(msg.toString());
	}

	/**
	 * Returns the last question options in the ordered set where ChoiceID = &#63;.
	 *
	 * @param ChoiceID the choice ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching question options, or <code>null</code> if a matching question options could not be found
	 */
	@Override
	public QuestionOptions fetchByCHID_Last(long ChoiceID,
		OrderByComparator<QuestionOptions> orderByComparator) {
		int count = countByCHID(ChoiceID);

		if (count == 0) {
			return null;
		}

		List<QuestionOptions> list = findByCHID(ChoiceID, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the question optionses before and after the current question options in the ordered set where ChoiceID = &#63;.
	 *
	 * @param OptionID the primary key of the current question options
	 * @param ChoiceID the choice ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next question options
	 * @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	 */
	@Override
	public QuestionOptions[] findByCHID_PrevAndNext(long OptionID,
		long ChoiceID, OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException {
		QuestionOptions questionOptions = findByPrimaryKey(OptionID);

		Session session = null;

		try {
			session = openSession();

			QuestionOptions[] array = new QuestionOptionsImpl[3];

			array[0] = getByCHID_PrevAndNext(session, questionOptions,
					ChoiceID, orderByComparator, true);

			array[1] = questionOptions;

			array[2] = getByCHID_PrevAndNext(session, questionOptions,
					ChoiceID, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected QuestionOptions getByCHID_PrevAndNext(Session session,
		QuestionOptions questionOptions, long ChoiceID,
		OrderByComparator<QuestionOptions> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_QUESTIONOPTIONS_WHERE);

		query.append(_FINDER_COLUMN_CHID_CHOICEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(QuestionOptionsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ChoiceID);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(questionOptions);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<QuestionOptions> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the question optionses where ChoiceID = &#63; from the database.
	 *
	 * @param ChoiceID the choice ID
	 */
	@Override
	public void removeByCHID(long ChoiceID) {
		for (QuestionOptions questionOptions : findByCHID(ChoiceID,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(questionOptions);
		}
	}

	/**
	 * Returns the number of question optionses where ChoiceID = &#63;.
	 *
	 * @param ChoiceID the choice ID
	 * @return the number of matching question optionses
	 */
	@Override
	public int countByCHID(long ChoiceID) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CHID;

		Object[] finderArgs = new Object[] { ChoiceID };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_QUESTIONOPTIONS_WHERE);

			query.append(_FINDER_COLUMN_CHID_CHOICEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ChoiceID);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CHID_CHOICEID_2 = "questionOptions.ChoiceID = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_QUESIDNCHOICEID =
		new FinderPath(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionOptionsModelImpl.FINDER_CACHE_ENABLED,
			QuestionOptionsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByQuesIdNChoiceId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESIDNCHOICEID =
		new FinderPath(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionOptionsModelImpl.FINDER_CACHE_ENABLED,
			QuestionOptionsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByQuesIdNChoiceId",
			new String[] { Long.class.getName(), Long.class.getName() },
			QuestionOptionsModelImpl.QUESTIONID_COLUMN_BITMASK |
			QuestionOptionsModelImpl.CHOICEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_QUESIDNCHOICEID = new FinderPath(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionOptionsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByQuesIdNChoiceId",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns all the question optionses where QuestionID = &#63; and ChoiceID = &#63;.
	 *
	 * @param QuestionID the question ID
	 * @param ChoiceID the choice ID
	 * @return the matching question optionses
	 */
	@Override
	public List<QuestionOptions> findByQuesIdNChoiceId(long QuestionID,
		long ChoiceID) {
		return findByQuesIdNChoiceId(QuestionID, ChoiceID, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the question optionses where QuestionID = &#63; and ChoiceID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param QuestionID the question ID
	 * @param ChoiceID the choice ID
	 * @param start the lower bound of the range of question optionses
	 * @param end the upper bound of the range of question optionses (not inclusive)
	 * @return the range of matching question optionses
	 */
	@Override
	public List<QuestionOptions> findByQuesIdNChoiceId(long QuestionID,
		long ChoiceID, int start, int end) {
		return findByQuesIdNChoiceId(QuestionID, ChoiceID, start, end, null);
	}

	/**
	 * Returns an ordered range of all the question optionses where QuestionID = &#63; and ChoiceID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param QuestionID the question ID
	 * @param ChoiceID the choice ID
	 * @param start the lower bound of the range of question optionses
	 * @param end the upper bound of the range of question optionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching question optionses
	 */
	@Override
	public List<QuestionOptions> findByQuesIdNChoiceId(long QuestionID,
		long ChoiceID, int start, int end,
		OrderByComparator<QuestionOptions> orderByComparator) {
		return findByQuesIdNChoiceId(QuestionID, ChoiceID, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the question optionses where QuestionID = &#63; and ChoiceID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param QuestionID the question ID
	 * @param ChoiceID the choice ID
	 * @param start the lower bound of the range of question optionses
	 * @param end the upper bound of the range of question optionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching question optionses
	 */
	@Override
	public List<QuestionOptions> findByQuesIdNChoiceId(long QuestionID,
		long ChoiceID, int start, int end,
		OrderByComparator<QuestionOptions> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESIDNCHOICEID;
			finderArgs = new Object[] { QuestionID, ChoiceID };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_QUESIDNCHOICEID;
			finderArgs = new Object[] {
					QuestionID, ChoiceID,
					
					start, end, orderByComparator
				};
		}

		List<QuestionOptions> list = null;

		if (retrieveFromCache) {
			list = (List<QuestionOptions>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (QuestionOptions questionOptions : list) {
					if ((QuestionID != questionOptions.getQuestionID()) ||
							(ChoiceID != questionOptions.getChoiceID())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_QUESTIONOPTIONS_WHERE);

			query.append(_FINDER_COLUMN_QUESIDNCHOICEID_QUESTIONID_2);

			query.append(_FINDER_COLUMN_QUESIDNCHOICEID_CHOICEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(QuestionOptionsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(QuestionID);

				qPos.add(ChoiceID);

				if (!pagination) {
					list = (List<QuestionOptions>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<QuestionOptions>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first question options in the ordered set where QuestionID = &#63; and ChoiceID = &#63;.
	 *
	 * @param QuestionID the question ID
	 * @param ChoiceID the choice ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching question options
	 * @throws NoSuchQuestionOptionsException if a matching question options could not be found
	 */
	@Override
	public QuestionOptions findByQuesIdNChoiceId_First(long QuestionID,
		long ChoiceID, OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException {
		QuestionOptions questionOptions = fetchByQuesIdNChoiceId_First(QuestionID,
				ChoiceID, orderByComparator);

		if (questionOptions != null) {
			return questionOptions;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("QuestionID=");
		msg.append(QuestionID);

		msg.append(", ChoiceID=");
		msg.append(ChoiceID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuestionOptionsException(msg.toString());
	}

	/**
	 * Returns the first question options in the ordered set where QuestionID = &#63; and ChoiceID = &#63;.
	 *
	 * @param QuestionID the question ID
	 * @param ChoiceID the choice ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching question options, or <code>null</code> if a matching question options could not be found
	 */
	@Override
	public QuestionOptions fetchByQuesIdNChoiceId_First(long QuestionID,
		long ChoiceID, OrderByComparator<QuestionOptions> orderByComparator) {
		List<QuestionOptions> list = findByQuesIdNChoiceId(QuestionID,
				ChoiceID, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last question options in the ordered set where QuestionID = &#63; and ChoiceID = &#63;.
	 *
	 * @param QuestionID the question ID
	 * @param ChoiceID the choice ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching question options
	 * @throws NoSuchQuestionOptionsException if a matching question options could not be found
	 */
	@Override
	public QuestionOptions findByQuesIdNChoiceId_Last(long QuestionID,
		long ChoiceID, OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException {
		QuestionOptions questionOptions = fetchByQuesIdNChoiceId_Last(QuestionID,
				ChoiceID, orderByComparator);

		if (questionOptions != null) {
			return questionOptions;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("QuestionID=");
		msg.append(QuestionID);

		msg.append(", ChoiceID=");
		msg.append(ChoiceID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchQuestionOptionsException(msg.toString());
	}

	/**
	 * Returns the last question options in the ordered set where QuestionID = &#63; and ChoiceID = &#63;.
	 *
	 * @param QuestionID the question ID
	 * @param ChoiceID the choice ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching question options, or <code>null</code> if a matching question options could not be found
	 */
	@Override
	public QuestionOptions fetchByQuesIdNChoiceId_Last(long QuestionID,
		long ChoiceID, OrderByComparator<QuestionOptions> orderByComparator) {
		int count = countByQuesIdNChoiceId(QuestionID, ChoiceID);

		if (count == 0) {
			return null;
		}

		List<QuestionOptions> list = findByQuesIdNChoiceId(QuestionID,
				ChoiceID, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the question optionses before and after the current question options in the ordered set where QuestionID = &#63; and ChoiceID = &#63;.
	 *
	 * @param OptionID the primary key of the current question options
	 * @param QuestionID the question ID
	 * @param ChoiceID the choice ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next question options
	 * @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	 */
	@Override
	public QuestionOptions[] findByQuesIdNChoiceId_PrevAndNext(long OptionID,
		long QuestionID, long ChoiceID,
		OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException {
		QuestionOptions questionOptions = findByPrimaryKey(OptionID);

		Session session = null;

		try {
			session = openSession();

			QuestionOptions[] array = new QuestionOptionsImpl[3];

			array[0] = getByQuesIdNChoiceId_PrevAndNext(session,
					questionOptions, QuestionID, ChoiceID, orderByComparator,
					true);

			array[1] = questionOptions;

			array[2] = getByQuesIdNChoiceId_PrevAndNext(session,
					questionOptions, QuestionID, ChoiceID, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected QuestionOptions getByQuesIdNChoiceId_PrevAndNext(
		Session session, QuestionOptions questionOptions, long QuestionID,
		long ChoiceID, OrderByComparator<QuestionOptions> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_QUESTIONOPTIONS_WHERE);

		query.append(_FINDER_COLUMN_QUESIDNCHOICEID_QUESTIONID_2);

		query.append(_FINDER_COLUMN_QUESIDNCHOICEID_CHOICEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(QuestionOptionsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(QuestionID);

		qPos.add(ChoiceID);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(questionOptions);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<QuestionOptions> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the question optionses where QuestionID = &#63; and ChoiceID = &#63; from the database.
	 *
	 * @param QuestionID the question ID
	 * @param ChoiceID the choice ID
	 */
	@Override
	public void removeByQuesIdNChoiceId(long QuestionID, long ChoiceID) {
		for (QuestionOptions questionOptions : findByQuesIdNChoiceId(
				QuestionID, ChoiceID, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(questionOptions);
		}
	}

	/**
	 * Returns the number of question optionses where QuestionID = &#63; and ChoiceID = &#63;.
	 *
	 * @param QuestionID the question ID
	 * @param ChoiceID the choice ID
	 * @return the number of matching question optionses
	 */
	@Override
	public int countByQuesIdNChoiceId(long QuestionID, long ChoiceID) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_QUESIDNCHOICEID;

		Object[] finderArgs = new Object[] { QuestionID, ChoiceID };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_QUESTIONOPTIONS_WHERE);

			query.append(_FINDER_COLUMN_QUESIDNCHOICEID_QUESTIONID_2);

			query.append(_FINDER_COLUMN_QUESIDNCHOICEID_CHOICEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(QuestionID);

				qPos.add(ChoiceID);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_QUESIDNCHOICEID_QUESTIONID_2 = "questionOptions.QuestionID = ? AND ";
	private static final String _FINDER_COLUMN_QUESIDNCHOICEID_CHOICEID_2 = "questionOptions.ChoiceID = ?";

	public QuestionOptionsPersistenceImpl() {
		setModelClass(QuestionOptions.class);
	}

	/**
	 * Caches the question options in the entity cache if it is enabled.
	 *
	 * @param questionOptions the question options
	 */
	@Override
	public void cacheResult(QuestionOptions questionOptions) {
		entityCache.putResult(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionOptionsImpl.class, questionOptions.getPrimaryKey(),
			questionOptions);

		questionOptions.resetOriginalValues();
	}

	/**
	 * Caches the question optionses in the entity cache if it is enabled.
	 *
	 * @param questionOptionses the question optionses
	 */
	@Override
	public void cacheResult(List<QuestionOptions> questionOptionses) {
		for (QuestionOptions questionOptions : questionOptionses) {
			if (entityCache.getResult(
						QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
						QuestionOptionsImpl.class,
						questionOptions.getPrimaryKey()) == null) {
				cacheResult(questionOptions);
			}
			else {
				questionOptions.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all question optionses.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(QuestionOptionsImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the question options.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(QuestionOptions questionOptions) {
		entityCache.removeResult(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionOptionsImpl.class, questionOptions.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<QuestionOptions> questionOptionses) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (QuestionOptions questionOptions : questionOptionses) {
			entityCache.removeResult(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
				QuestionOptionsImpl.class, questionOptions.getPrimaryKey());
		}
	}

	/**
	 * Creates a new question options with the primary key. Does not add the question options to the database.
	 *
	 * @param OptionID the primary key for the new question options
	 * @return the new question options
	 */
	@Override
	public QuestionOptions create(long OptionID) {
		QuestionOptions questionOptions = new QuestionOptionsImpl();

		questionOptions.setNew(true);
		questionOptions.setPrimaryKey(OptionID);

		return questionOptions;
	}

	/**
	 * Removes the question options with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param OptionID the primary key of the question options
	 * @return the question options that was removed
	 * @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	 */
	@Override
	public QuestionOptions remove(long OptionID)
		throws NoSuchQuestionOptionsException {
		return remove((Serializable)OptionID);
	}

	/**
	 * Removes the question options with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the question options
	 * @return the question options that was removed
	 * @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	 */
	@Override
	public QuestionOptions remove(Serializable primaryKey)
		throws NoSuchQuestionOptionsException {
		Session session = null;

		try {
			session = openSession();

			QuestionOptions questionOptions = (QuestionOptions)session.get(QuestionOptionsImpl.class,
					primaryKey);

			if (questionOptions == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchQuestionOptionsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(questionOptions);
		}
		catch (NoSuchQuestionOptionsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected QuestionOptions removeImpl(QuestionOptions questionOptions) {
		questionOptions = toUnwrappedModel(questionOptions);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(questionOptions)) {
				questionOptions = (QuestionOptions)session.get(QuestionOptionsImpl.class,
						questionOptions.getPrimaryKeyObj());
			}

			if (questionOptions != null) {
				session.delete(questionOptions);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (questionOptions != null) {
			clearCache(questionOptions);
		}

		return questionOptions;
	}

	@Override
	public QuestionOptions updateImpl(QuestionOptions questionOptions) {
		questionOptions = toUnwrappedModel(questionOptions);

		boolean isNew = questionOptions.isNew();

		QuestionOptionsModelImpl questionOptionsModelImpl = (QuestionOptionsModelImpl)questionOptions;

		Session session = null;

		try {
			session = openSession();

			if (questionOptions.isNew()) {
				session.save(questionOptions);

				questionOptions.setNew(false);
			}
			else {
				questionOptions = (QuestionOptions)session.merge(questionOptions);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!QuestionOptionsModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] {
					questionOptionsModelImpl.getQuestionID()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_QID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QID,
				args);

			args = new Object[] { questionOptionsModelImpl.getChoiceID() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_CHID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHID,
				args);

			args = new Object[] {
					questionOptionsModelImpl.getQuestionID(),
					questionOptionsModelImpl.getChoiceID()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_QUESIDNCHOICEID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESIDNCHOICEID,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((questionOptionsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						questionOptionsModelImpl.getOriginalQuestionID()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_QID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QID,
					args);

				args = new Object[] { questionOptionsModelImpl.getQuestionID() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_QID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QID,
					args);
			}

			if ((questionOptionsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						questionOptionsModelImpl.getOriginalChoiceID()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_CHID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHID,
					args);

				args = new Object[] { questionOptionsModelImpl.getChoiceID() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_CHID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHID,
					args);
			}

			if ((questionOptionsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESIDNCHOICEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						questionOptionsModelImpl.getOriginalQuestionID(),
						questionOptionsModelImpl.getOriginalChoiceID()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_QUESIDNCHOICEID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESIDNCHOICEID,
					args);

				args = new Object[] {
						questionOptionsModelImpl.getQuestionID(),
						questionOptionsModelImpl.getChoiceID()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_QUESIDNCHOICEID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_QUESIDNCHOICEID,
					args);
			}
		}

		entityCache.putResult(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
			QuestionOptionsImpl.class, questionOptions.getPrimaryKey(),
			questionOptions, false);

		questionOptions.resetOriginalValues();

		return questionOptions;
	}

	protected QuestionOptions toUnwrappedModel(QuestionOptions questionOptions) {
		if (questionOptions instanceof QuestionOptionsImpl) {
			return questionOptions;
		}

		QuestionOptionsImpl questionOptionsImpl = new QuestionOptionsImpl();

		questionOptionsImpl.setNew(questionOptions.isNew());
		questionOptionsImpl.setPrimaryKey(questionOptions.getPrimaryKey());

		questionOptionsImpl.setOptionID(questionOptions.getOptionID());
		questionOptionsImpl.setQuestionID(questionOptions.getQuestionID());
		questionOptionsImpl.setChoiceID(questionOptions.getChoiceID());

		return questionOptionsImpl;
	}

	/**
	 * Returns the question options with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the question options
	 * @return the question options
	 * @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	 */
	@Override
	public QuestionOptions findByPrimaryKey(Serializable primaryKey)
		throws NoSuchQuestionOptionsException {
		QuestionOptions questionOptions = fetchByPrimaryKey(primaryKey);

		if (questionOptions == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchQuestionOptionsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return questionOptions;
	}

	/**
	 * Returns the question options with the primary key or throws a {@link NoSuchQuestionOptionsException} if it could not be found.
	 *
	 * @param OptionID the primary key of the question options
	 * @return the question options
	 * @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	 */
	@Override
	public QuestionOptions findByPrimaryKey(long OptionID)
		throws NoSuchQuestionOptionsException {
		return findByPrimaryKey((Serializable)OptionID);
	}

	/**
	 * Returns the question options with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the question options
	 * @return the question options, or <code>null</code> if a question options with the primary key could not be found
	 */
	@Override
	public QuestionOptions fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
				QuestionOptionsImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		QuestionOptions questionOptions = (QuestionOptions)serializable;

		if (questionOptions == null) {
			Session session = null;

			try {
				session = openSession();

				questionOptions = (QuestionOptions)session.get(QuestionOptionsImpl.class,
						primaryKey);

				if (questionOptions != null) {
					cacheResult(questionOptions);
				}
				else {
					entityCache.putResult(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
						QuestionOptionsImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
					QuestionOptionsImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return questionOptions;
	}

	/**
	 * Returns the question options with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param OptionID the primary key of the question options
	 * @return the question options, or <code>null</code> if a question options with the primary key could not be found
	 */
	@Override
	public QuestionOptions fetchByPrimaryKey(long OptionID) {
		return fetchByPrimaryKey((Serializable)OptionID);
	}

	@Override
	public Map<Serializable, QuestionOptions> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, QuestionOptions> map = new HashMap<Serializable, QuestionOptions>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			QuestionOptions questionOptions = fetchByPrimaryKey(primaryKey);

			if (questionOptions != null) {
				map.put(primaryKey, questionOptions);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
					QuestionOptionsImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (QuestionOptions)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_QUESTIONOPTIONS_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (QuestionOptions questionOptions : (List<QuestionOptions>)q.list()) {
				map.put(questionOptions.getPrimaryKeyObj(), questionOptions);

				cacheResult(questionOptions);

				uncachedPrimaryKeys.remove(questionOptions.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(QuestionOptionsModelImpl.ENTITY_CACHE_ENABLED,
					QuestionOptionsImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the question optionses.
	 *
	 * @return the question optionses
	 */
	@Override
	public List<QuestionOptions> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the question optionses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of question optionses
	 * @param end the upper bound of the range of question optionses (not inclusive)
	 * @return the range of question optionses
	 */
	@Override
	public List<QuestionOptions> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the question optionses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of question optionses
	 * @param end the upper bound of the range of question optionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of question optionses
	 */
	@Override
	public List<QuestionOptions> findAll(int start, int end,
		OrderByComparator<QuestionOptions> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the question optionses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of question optionses
	 * @param end the upper bound of the range of question optionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of question optionses
	 */
	@Override
	public List<QuestionOptions> findAll(int start, int end,
		OrderByComparator<QuestionOptions> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<QuestionOptions> list = null;

		if (retrieveFromCache) {
			list = (List<QuestionOptions>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_QUESTIONOPTIONS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_QUESTIONOPTIONS;

				if (pagination) {
					sql = sql.concat(QuestionOptionsModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<QuestionOptions>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<QuestionOptions>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the question optionses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (QuestionOptions questionOptions : findAll()) {
			remove(questionOptions);
		}
	}

	/**
	 * Returns the number of question optionses.
	 *
	 * @return the number of question optionses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_QUESTIONOPTIONS);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return QuestionOptionsModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the question options persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(QuestionOptionsImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_QUESTIONOPTIONS = "SELECT questionOptions FROM QuestionOptions questionOptions";
	private static final String _SQL_SELECT_QUESTIONOPTIONS_WHERE_PKS_IN = "SELECT questionOptions FROM QuestionOptions questionOptions WHERE OptionID IN (";
	private static final String _SQL_SELECT_QUESTIONOPTIONS_WHERE = "SELECT questionOptions FROM QuestionOptions questionOptions WHERE ";
	private static final String _SQL_COUNT_QUESTIONOPTIONS = "SELECT COUNT(questionOptions) FROM QuestionOptions questionOptions";
	private static final String _SQL_COUNT_QUESTIONOPTIONS_WHERE = "SELECT COUNT(questionOptions) FROM QuestionOptions questionOptions WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "questionOptions.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No QuestionOptions exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No QuestionOptions exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(QuestionOptionsPersistenceImpl.class);
}