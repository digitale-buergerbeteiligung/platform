/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import surveyAPI.model.Choices;

import java.util.List;

/**
 * The persistence utility for the choices service. This utility wraps {@link surveyAPI.service.persistence.impl.ChoicesPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ChoicesPersistence
 * @see surveyAPI.service.persistence.impl.ChoicesPersistenceImpl
 * @generated
 */
@ProviderType
public class ChoicesUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Choices choices) {
		getPersistence().clearCache(choices);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Choices> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Choices> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Choices> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Choices> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Choices update(Choices choices) {
		return getPersistence().update(choices);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Choices update(Choices choices, ServiceContext serviceContext) {
		return getPersistence().update(choices, serviceContext);
	}

	/**
	* Returns all the choiceses where ChoiceText = &#63;.
	*
	* @param ChoiceText the choice text
	* @return the matching choiceses
	*/
	public static List<Choices> findByCTEXT(java.lang.String ChoiceText) {
		return getPersistence().findByCTEXT(ChoiceText);
	}

	/**
	* Returns a range of all the choiceses where ChoiceText = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ChoiceText the choice text
	* @param start the lower bound of the range of choiceses
	* @param end the upper bound of the range of choiceses (not inclusive)
	* @return the range of matching choiceses
	*/
	public static List<Choices> findByCTEXT(java.lang.String ChoiceText,
		int start, int end) {
		return getPersistence().findByCTEXT(ChoiceText, start, end);
	}

	/**
	* Returns an ordered range of all the choiceses where ChoiceText = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ChoiceText the choice text
	* @param start the lower bound of the range of choiceses
	* @param end the upper bound of the range of choiceses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching choiceses
	*/
	public static List<Choices> findByCTEXT(java.lang.String ChoiceText,
		int start, int end, OrderByComparator<Choices> orderByComparator) {
		return getPersistence()
				   .findByCTEXT(ChoiceText, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the choiceses where ChoiceText = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ChoiceText the choice text
	* @param start the lower bound of the range of choiceses
	* @param end the upper bound of the range of choiceses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching choiceses
	*/
	public static List<Choices> findByCTEXT(java.lang.String ChoiceText,
		int start, int end, OrderByComparator<Choices> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByCTEXT(ChoiceText, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first choices in the ordered set where ChoiceText = &#63;.
	*
	* @param ChoiceText the choice text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching choices
	* @throws NoSuchChoicesException if a matching choices could not be found
	*/
	public static Choices findByCTEXT_First(java.lang.String ChoiceText,
		OrderByComparator<Choices> orderByComparator)
		throws surveyAPI.exception.NoSuchChoicesException {
		return getPersistence().findByCTEXT_First(ChoiceText, orderByComparator);
	}

	/**
	* Returns the first choices in the ordered set where ChoiceText = &#63;.
	*
	* @param ChoiceText the choice text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching choices, or <code>null</code> if a matching choices could not be found
	*/
	public static Choices fetchByCTEXT_First(java.lang.String ChoiceText,
		OrderByComparator<Choices> orderByComparator) {
		return getPersistence().fetchByCTEXT_First(ChoiceText, orderByComparator);
	}

	/**
	* Returns the last choices in the ordered set where ChoiceText = &#63;.
	*
	* @param ChoiceText the choice text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching choices
	* @throws NoSuchChoicesException if a matching choices could not be found
	*/
	public static Choices findByCTEXT_Last(java.lang.String ChoiceText,
		OrderByComparator<Choices> orderByComparator)
		throws surveyAPI.exception.NoSuchChoicesException {
		return getPersistence().findByCTEXT_Last(ChoiceText, orderByComparator);
	}

	/**
	* Returns the last choices in the ordered set where ChoiceText = &#63;.
	*
	* @param ChoiceText the choice text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching choices, or <code>null</code> if a matching choices could not be found
	*/
	public static Choices fetchByCTEXT_Last(java.lang.String ChoiceText,
		OrderByComparator<Choices> orderByComparator) {
		return getPersistence().fetchByCTEXT_Last(ChoiceText, orderByComparator);
	}

	/**
	* Returns the choiceses before and after the current choices in the ordered set where ChoiceText = &#63;.
	*
	* @param ChoiceID the primary key of the current choices
	* @param ChoiceText the choice text
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next choices
	* @throws NoSuchChoicesException if a choices with the primary key could not be found
	*/
	public static Choices[] findByCTEXT_PrevAndNext(long ChoiceID,
		java.lang.String ChoiceText,
		OrderByComparator<Choices> orderByComparator)
		throws surveyAPI.exception.NoSuchChoicesException {
		return getPersistence()
				   .findByCTEXT_PrevAndNext(ChoiceID, ChoiceText,
			orderByComparator);
	}

	/**
	* Removes all the choiceses where ChoiceText = &#63; from the database.
	*
	* @param ChoiceText the choice text
	*/
	public static void removeByCTEXT(java.lang.String ChoiceText) {
		getPersistence().removeByCTEXT(ChoiceText);
	}

	/**
	* Returns the number of choiceses where ChoiceText = &#63;.
	*
	* @param ChoiceText the choice text
	* @return the number of matching choiceses
	*/
	public static int countByCTEXT(java.lang.String ChoiceText) {
		return getPersistence().countByCTEXT(ChoiceText);
	}

	/**
	* Caches the choices in the entity cache if it is enabled.
	*
	* @param choices the choices
	*/
	public static void cacheResult(Choices choices) {
		getPersistence().cacheResult(choices);
	}

	/**
	* Caches the choiceses in the entity cache if it is enabled.
	*
	* @param choiceses the choiceses
	*/
	public static void cacheResult(List<Choices> choiceses) {
		getPersistence().cacheResult(choiceses);
	}

	/**
	* Creates a new choices with the primary key. Does not add the choices to the database.
	*
	* @param ChoiceID the primary key for the new choices
	* @return the new choices
	*/
	public static Choices create(long ChoiceID) {
		return getPersistence().create(ChoiceID);
	}

	/**
	* Removes the choices with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ChoiceID the primary key of the choices
	* @return the choices that was removed
	* @throws NoSuchChoicesException if a choices with the primary key could not be found
	*/
	public static Choices remove(long ChoiceID)
		throws surveyAPI.exception.NoSuchChoicesException {
		return getPersistence().remove(ChoiceID);
	}

	public static Choices updateImpl(Choices choices) {
		return getPersistence().updateImpl(choices);
	}

	/**
	* Returns the choices with the primary key or throws a {@link NoSuchChoicesException} if it could not be found.
	*
	* @param ChoiceID the primary key of the choices
	* @return the choices
	* @throws NoSuchChoicesException if a choices with the primary key could not be found
	*/
	public static Choices findByPrimaryKey(long ChoiceID)
		throws surveyAPI.exception.NoSuchChoicesException {
		return getPersistence().findByPrimaryKey(ChoiceID);
	}

	/**
	* Returns the choices with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ChoiceID the primary key of the choices
	* @return the choices, or <code>null</code> if a choices with the primary key could not be found
	*/
	public static Choices fetchByPrimaryKey(long ChoiceID) {
		return getPersistence().fetchByPrimaryKey(ChoiceID);
	}

	public static java.util.Map<java.io.Serializable, Choices> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the choiceses.
	*
	* @return the choiceses
	*/
	public static List<Choices> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the choiceses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of choiceses
	* @param end the upper bound of the range of choiceses (not inclusive)
	* @return the range of choiceses
	*/
	public static List<Choices> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the choiceses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of choiceses
	* @param end the upper bound of the range of choiceses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of choiceses
	*/
	public static List<Choices> findAll(int start, int end,
		OrderByComparator<Choices> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the choiceses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ChoicesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of choiceses
	* @param end the upper bound of the range of choiceses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of choiceses
	*/
	public static List<Choices> findAll(int start, int end,
		OrderByComparator<Choices> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the choiceses from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of choiceses.
	*
	* @return the number of choiceses
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ChoicesPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ChoicesPersistence, ChoicesPersistence> _serviceTracker =
		ServiceTrackerFactory.open(ChoicesPersistence.class);
}