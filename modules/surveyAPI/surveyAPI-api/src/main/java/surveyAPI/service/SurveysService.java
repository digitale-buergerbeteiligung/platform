/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.security.access.control.AccessControlled;
import com.liferay.portal.kernel.service.BaseService;
import com.liferay.portal.kernel.spring.osgi.OSGiBeanProperties;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Provides the remote service interface for Surveys. Methods of this
 * service are expected to have security checks based on the propagated JAAS
 * credentials because this service can be accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see SurveysServiceUtil
 * @see surveyAPI.service.base.SurveysServiceBaseImpl
 * @see surveyAPI.service.impl.SurveysServiceImpl
 * @generated
 */
@AccessControlled
@JSONWebService
@OSGiBeanProperties(property =  {
	"json.web.service.context.name=survey", "json.web.service.context.path=Surveys"}, service = SurveysService.class)
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface SurveysService extends BaseService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SurveysServiceUtil} to access the surveys remote service. Add custom service methods to {@link surveyAPI.service.impl.SurveysServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	/**
	* JSON Web Service to add a new survey
	*/
	@JSONWebService(method = "POST")
	public JSONObject addNewSurvey() throws JSONException;

	/**
	* This just adds a survey and returns the survey ID, user can later add questions
	*
	* @param name
	* @return
	* @throws JSONException
	*/
	@JSONWebService(method = "POST")
	public JSONObject addNewSurvey(java.lang.String name)
		throws JSONException;

	/**
	* This adds a Survey with all the questions and choices
	*
	* @param name
	* @param jsonInput - Json array of json objects
	[
	{"QName":"", "QText":"", "Required":"", "Choices":["choice 1", "choice 2", "choice 3"], "Type":"Single/multiple/boolean"},
	{"QName":"", "QText":"", "Required":"", "Choices":["choice 1", "choice 2", "choice 3"], "Type":"Single/multiple/boolean"}
	]
	* @return
	* @throws JSONException
	*/
	@JSONWebService(method = "POST")
	public JSONObject addNewSurvey(java.lang.String name,
		java.lang.String jsonInput) throws JSONException;

	/**
	* JSON Web service to delete an existing survey based on survey ID
	*/
	@JSONWebService(method = "DELETE")
	public JSONObject deleteSurvey() throws JSONException;

	@JSONWebService(method = "DELETE")
	public JSONObject deleteSurvey(long surveyID) throws JSONException;

	/**
	* JSON Web service to fetch all the question ids associated with a given survey id
	*/
	@JSONWebService(method = "GET")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONObject getAllQuestionIds() throws JSONException;

	@JSONWebService(method = "GET")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONObject getAllQuestionIds(long surveyID)
		throws JSONException;

	/**
	* JSON Web service to get full survey
	*
	* @return JSONObject {Questions:JSONArray, Choices:JSONArray, QuestionIds:JSONArray}
	*/
	@JSONWebService(method = "GET")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public JSONObject getSurvey(long surveyId) throws JSONException;
}