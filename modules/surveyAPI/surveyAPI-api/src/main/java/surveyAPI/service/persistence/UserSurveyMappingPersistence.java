/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import surveyAPI.exception.NoSuchUserSurveyMappingException;

import surveyAPI.model.UserSurveyMapping;

/**
 * The persistence interface for the user survey mapping service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see surveyAPI.service.persistence.impl.UserSurveyMappingPersistenceImpl
 * @see UserSurveyMappingUtil
 * @generated
 */
@ProviderType
public interface UserSurveyMappingPersistence extends BasePersistence<UserSurveyMapping> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UserSurveyMappingUtil} to access the user survey mapping persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the user survey mapping in the entity cache if it is enabled.
	*
	* @param userSurveyMapping the user survey mapping
	*/
	public void cacheResult(UserSurveyMapping userSurveyMapping);

	/**
	* Caches the user survey mappings in the entity cache if it is enabled.
	*
	* @param userSurveyMappings the user survey mappings
	*/
	public void cacheResult(
		java.util.List<UserSurveyMapping> userSurveyMappings);

	/**
	* Creates a new user survey mapping with the primary key. Does not add the user survey mapping to the database.
	*
	* @param userSurveyMappingPK the primary key for the new user survey mapping
	* @return the new user survey mapping
	*/
	public UserSurveyMapping create(
		surveyAPI.service.persistence.UserSurveyMappingPK userSurveyMappingPK);

	/**
	* Removes the user survey mapping with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userSurveyMappingPK the primary key of the user survey mapping
	* @return the user survey mapping that was removed
	* @throws NoSuchUserSurveyMappingException if a user survey mapping with the primary key could not be found
	*/
	public UserSurveyMapping remove(
		surveyAPI.service.persistence.UserSurveyMappingPK userSurveyMappingPK)
		throws NoSuchUserSurveyMappingException;

	public UserSurveyMapping updateImpl(UserSurveyMapping userSurveyMapping);

	/**
	* Returns the user survey mapping with the primary key or throws a {@link NoSuchUserSurveyMappingException} if it could not be found.
	*
	* @param userSurveyMappingPK the primary key of the user survey mapping
	* @return the user survey mapping
	* @throws NoSuchUserSurveyMappingException if a user survey mapping with the primary key could not be found
	*/
	public UserSurveyMapping findByPrimaryKey(
		surveyAPI.service.persistence.UserSurveyMappingPK userSurveyMappingPK)
		throws NoSuchUserSurveyMappingException;

	/**
	* Returns the user survey mapping with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userSurveyMappingPK the primary key of the user survey mapping
	* @return the user survey mapping, or <code>null</code> if a user survey mapping with the primary key could not be found
	*/
	public UserSurveyMapping fetchByPrimaryKey(
		surveyAPI.service.persistence.UserSurveyMappingPK userSurveyMappingPK);

	@Override
	public java.util.Map<java.io.Serializable, UserSurveyMapping> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the user survey mappings.
	*
	* @return the user survey mappings
	*/
	public java.util.List<UserSurveyMapping> findAll();

	/**
	* Returns a range of all the user survey mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserSurveyMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user survey mappings
	* @param end the upper bound of the range of user survey mappings (not inclusive)
	* @return the range of user survey mappings
	*/
	public java.util.List<UserSurveyMapping> findAll(int start, int end);

	/**
	* Returns an ordered range of all the user survey mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserSurveyMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user survey mappings
	* @param end the upper bound of the range of user survey mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user survey mappings
	*/
	public java.util.List<UserSurveyMapping> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserSurveyMapping> orderByComparator);

	/**
	* Returns an ordered range of all the user survey mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserSurveyMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user survey mappings
	* @param end the upper bound of the range of user survey mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of user survey mappings
	*/
	public java.util.List<UserSurveyMapping> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserSurveyMapping> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the user survey mappings from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of user survey mappings.
	*
	* @return the number of user survey mappings
	*/
	public int countAll();
}