/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InputTypeLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see InputTypeLocalService
 * @generated
 */
@ProviderType
public class InputTypeLocalServiceWrapper implements InputTypeLocalService,
	ServiceWrapper<InputTypeLocalService> {
	public InputTypeLocalServiceWrapper(
		InputTypeLocalService inputTypeLocalService) {
		_inputTypeLocalService = inputTypeLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _inputTypeLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _inputTypeLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _inputTypeLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _inputTypeLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _inputTypeLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of input types.
	*
	* @return the number of input types
	*/
	@Override
	public int getInputTypesCount() {
		return _inputTypeLocalService.getInputTypesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _inputTypeLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _inputTypeLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.InputTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _inputTypeLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.InputTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _inputTypeLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the input types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.InputTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of input types
	* @param end the upper bound of the range of input types (not inclusive)
	* @return the range of input types
	*/
	@Override
	public java.util.List<surveyAPI.model.InputType> getInputTypes(int start,
		int end) {
		return _inputTypeLocalService.getInputTypes(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _inputTypeLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _inputTypeLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Adds the input type to the database. Also notifies the appropriate model listeners.
	*
	* @param inputType the input type
	* @return the input type that was added
	*/
	@Override
	public surveyAPI.model.InputType addInputType(
		surveyAPI.model.InputType inputType) {
		return _inputTypeLocalService.addInputType(inputType);
	}

	/**
	* Creates a new input type with the primary key. Does not add the input type to the database.
	*
	* @param TypeID the primary key for the new input type
	* @return the new input type
	*/
	@Override
	public surveyAPI.model.InputType createInputType(int TypeID) {
		return _inputTypeLocalService.createInputType(TypeID);
	}

	/**
	* Deletes the input type with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param TypeID the primary key of the input type
	* @return the input type that was removed
	* @throws PortalException if a input type with the primary key could not be found
	*/
	@Override
	public surveyAPI.model.InputType deleteInputType(int TypeID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _inputTypeLocalService.deleteInputType(TypeID);
	}

	/**
	* Deletes the input type from the database. Also notifies the appropriate model listeners.
	*
	* @param inputType the input type
	* @return the input type that was removed
	*/
	@Override
	public surveyAPI.model.InputType deleteInputType(
		surveyAPI.model.InputType inputType) {
		return _inputTypeLocalService.deleteInputType(inputType);
	}

	@Override
	public surveyAPI.model.InputType fetchInputType(int TypeID) {
		return _inputTypeLocalService.fetchInputType(TypeID);
	}

	/**
	* Returns the input type with the primary key.
	*
	* @param TypeID the primary key of the input type
	* @return the input type
	* @throws PortalException if a input type with the primary key could not be found
	*/
	@Override
	public surveyAPI.model.InputType getInputType(int TypeID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _inputTypeLocalService.getInputType(TypeID);
	}

	/**
	* Updates the input type in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param inputType the input type
	* @return the input type that was updated
	*/
	@Override
	public surveyAPI.model.InputType updateInputType(
		surveyAPI.model.InputType inputType) {
		return _inputTypeLocalService.updateInputType(inputType);
	}

	@Override
	public InputTypeLocalService getWrappedService() {
		return _inputTypeLocalService;
	}

	@Override
	public void setWrappedService(InputTypeLocalService inputTypeLocalService) {
		_inputTypeLocalService = inputTypeLocalService;
	}

	private InputTypeLocalService _inputTypeLocalService;
}