/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import surveyAPI.model.UserSurveyMapping;

import java.util.List;

/**
 * The persistence utility for the user survey mapping service. This utility wraps {@link surveyAPI.service.persistence.impl.UserSurveyMappingPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserSurveyMappingPersistence
 * @see surveyAPI.service.persistence.impl.UserSurveyMappingPersistenceImpl
 * @generated
 */
@ProviderType
public class UserSurveyMappingUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(UserSurveyMapping userSurveyMapping) {
		getPersistence().clearCache(userSurveyMapping);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<UserSurveyMapping> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<UserSurveyMapping> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<UserSurveyMapping> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<UserSurveyMapping> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static UserSurveyMapping update(UserSurveyMapping userSurveyMapping) {
		return getPersistence().update(userSurveyMapping);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static UserSurveyMapping update(
		UserSurveyMapping userSurveyMapping, ServiceContext serviceContext) {
		return getPersistence().update(userSurveyMapping, serviceContext);
	}

	/**
	* Caches the user survey mapping in the entity cache if it is enabled.
	*
	* @param userSurveyMapping the user survey mapping
	*/
	public static void cacheResult(UserSurveyMapping userSurveyMapping) {
		getPersistence().cacheResult(userSurveyMapping);
	}

	/**
	* Caches the user survey mappings in the entity cache if it is enabled.
	*
	* @param userSurveyMappings the user survey mappings
	*/
	public static void cacheResult(List<UserSurveyMapping> userSurveyMappings) {
		getPersistence().cacheResult(userSurveyMappings);
	}

	/**
	* Creates a new user survey mapping with the primary key. Does not add the user survey mapping to the database.
	*
	* @param userSurveyMappingPK the primary key for the new user survey mapping
	* @return the new user survey mapping
	*/
	public static UserSurveyMapping create(
		surveyAPI.service.persistence.UserSurveyMappingPK userSurveyMappingPK) {
		return getPersistence().create(userSurveyMappingPK);
	}

	/**
	* Removes the user survey mapping with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userSurveyMappingPK the primary key of the user survey mapping
	* @return the user survey mapping that was removed
	* @throws NoSuchUserSurveyMappingException if a user survey mapping with the primary key could not be found
	*/
	public static UserSurveyMapping remove(
		surveyAPI.service.persistence.UserSurveyMappingPK userSurveyMappingPK)
		throws surveyAPI.exception.NoSuchUserSurveyMappingException {
		return getPersistence().remove(userSurveyMappingPK);
	}

	public static UserSurveyMapping updateImpl(
		UserSurveyMapping userSurveyMapping) {
		return getPersistence().updateImpl(userSurveyMapping);
	}

	/**
	* Returns the user survey mapping with the primary key or throws a {@link NoSuchUserSurveyMappingException} if it could not be found.
	*
	* @param userSurveyMappingPK the primary key of the user survey mapping
	* @return the user survey mapping
	* @throws NoSuchUserSurveyMappingException if a user survey mapping with the primary key could not be found
	*/
	public static UserSurveyMapping findByPrimaryKey(
		surveyAPI.service.persistence.UserSurveyMappingPK userSurveyMappingPK)
		throws surveyAPI.exception.NoSuchUserSurveyMappingException {
		return getPersistence().findByPrimaryKey(userSurveyMappingPK);
	}

	/**
	* Returns the user survey mapping with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userSurveyMappingPK the primary key of the user survey mapping
	* @return the user survey mapping, or <code>null</code> if a user survey mapping with the primary key could not be found
	*/
	public static UserSurveyMapping fetchByPrimaryKey(
		surveyAPI.service.persistence.UserSurveyMappingPK userSurveyMappingPK) {
		return getPersistence().fetchByPrimaryKey(userSurveyMappingPK);
	}

	public static java.util.Map<java.io.Serializable, UserSurveyMapping> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the user survey mappings.
	*
	* @return the user survey mappings
	*/
	public static List<UserSurveyMapping> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the user survey mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserSurveyMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user survey mappings
	* @param end the upper bound of the range of user survey mappings (not inclusive)
	* @return the range of user survey mappings
	*/
	public static List<UserSurveyMapping> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the user survey mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserSurveyMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user survey mappings
	* @param end the upper bound of the range of user survey mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user survey mappings
	*/
	public static List<UserSurveyMapping> findAll(int start, int end,
		OrderByComparator<UserSurveyMapping> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the user survey mappings.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserSurveyMappingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user survey mappings
	* @param end the upper bound of the range of user survey mappings (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of user survey mappings
	*/
	public static List<UserSurveyMapping> findAll(int start, int end,
		OrderByComparator<UserSurveyMapping> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the user survey mappings from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of user survey mappings.
	*
	* @return the number of user survey mappings
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static UserSurveyMappingPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<UserSurveyMappingPersistence, UserSurveyMappingPersistence> _serviceTracker =
		ServiceTrackerFactory.open(UserSurveyMappingPersistence.class);
}