/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import surveyAPI.model.Users;

import java.util.List;

/**
 * The persistence utility for the users service. This utility wraps {@link surveyAPI.service.persistence.impl.UsersPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UsersPersistence
 * @see surveyAPI.service.persistence.impl.UsersPersistenceImpl
 * @generated
 */
@ProviderType
public class UsersUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Users users) {
		getPersistence().clearCache(users);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Users> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Users> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Users> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator<Users> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Users update(Users users) {
		return getPersistence().update(users);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Users update(Users users, ServiceContext serviceContext) {
		return getPersistence().update(users, serviceContext);
	}

	/**
	* Returns all the userses where UserName = &#63;.
	*
	* @param UserName the user name
	* @return the matching userses
	*/
	public static List<Users> findByUName(java.lang.String UserName) {
		return getPersistence().findByUName(UserName);
	}

	/**
	* Returns a range of all the userses where UserName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param UserName the user name
	* @param start the lower bound of the range of userses
	* @param end the upper bound of the range of userses (not inclusive)
	* @return the range of matching userses
	*/
	public static List<Users> findByUName(java.lang.String UserName, int start,
		int end) {
		return getPersistence().findByUName(UserName, start, end);
	}

	/**
	* Returns an ordered range of all the userses where UserName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param UserName the user name
	* @param start the lower bound of the range of userses
	* @param end the upper bound of the range of userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching userses
	*/
	public static List<Users> findByUName(java.lang.String UserName, int start,
		int end, OrderByComparator<Users> orderByComparator) {
		return getPersistence()
				   .findByUName(UserName, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the userses where UserName = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param UserName the user name
	* @param start the lower bound of the range of userses
	* @param end the upper bound of the range of userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching userses
	*/
	public static List<Users> findByUName(java.lang.String UserName, int start,
		int end, OrderByComparator<Users> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUName(UserName, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first users in the ordered set where UserName = &#63;.
	*
	* @param UserName the user name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching users
	* @throws NoSuchUsersException if a matching users could not be found
	*/
	public static Users findByUName_First(java.lang.String UserName,
		OrderByComparator<Users> orderByComparator)
		throws surveyAPI.exception.NoSuchUsersException {
		return getPersistence().findByUName_First(UserName, orderByComparator);
	}

	/**
	* Returns the first users in the ordered set where UserName = &#63;.
	*
	* @param UserName the user name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching users, or <code>null</code> if a matching users could not be found
	*/
	public static Users fetchByUName_First(java.lang.String UserName,
		OrderByComparator<Users> orderByComparator) {
		return getPersistence().fetchByUName_First(UserName, orderByComparator);
	}

	/**
	* Returns the last users in the ordered set where UserName = &#63;.
	*
	* @param UserName the user name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching users
	* @throws NoSuchUsersException if a matching users could not be found
	*/
	public static Users findByUName_Last(java.lang.String UserName,
		OrderByComparator<Users> orderByComparator)
		throws surveyAPI.exception.NoSuchUsersException {
		return getPersistence().findByUName_Last(UserName, orderByComparator);
	}

	/**
	* Returns the last users in the ordered set where UserName = &#63;.
	*
	* @param UserName the user name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching users, or <code>null</code> if a matching users could not be found
	*/
	public static Users fetchByUName_Last(java.lang.String UserName,
		OrderByComparator<Users> orderByComparator) {
		return getPersistence().fetchByUName_Last(UserName, orderByComparator);
	}

	/**
	* Returns the userses before and after the current users in the ordered set where UserName = &#63;.
	*
	* @param UserID the primary key of the current users
	* @param UserName the user name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next users
	* @throws NoSuchUsersException if a users with the primary key could not be found
	*/
	public static Users[] findByUName_PrevAndNext(long UserID,
		java.lang.String UserName, OrderByComparator<Users> orderByComparator)
		throws surveyAPI.exception.NoSuchUsersException {
		return getPersistence()
				   .findByUName_PrevAndNext(UserID, UserName, orderByComparator);
	}

	/**
	* Removes all the userses where UserName = &#63; from the database.
	*
	* @param UserName the user name
	*/
	public static void removeByUName(java.lang.String UserName) {
		getPersistence().removeByUName(UserName);
	}

	/**
	* Returns the number of userses where UserName = &#63;.
	*
	* @param UserName the user name
	* @return the number of matching userses
	*/
	public static int countByUName(java.lang.String UserName) {
		return getPersistence().countByUName(UserName);
	}

	/**
	* Caches the users in the entity cache if it is enabled.
	*
	* @param users the users
	*/
	public static void cacheResult(Users users) {
		getPersistence().cacheResult(users);
	}

	/**
	* Caches the userses in the entity cache if it is enabled.
	*
	* @param userses the userses
	*/
	public static void cacheResult(List<Users> userses) {
		getPersistence().cacheResult(userses);
	}

	/**
	* Creates a new users with the primary key. Does not add the users to the database.
	*
	* @param UserID the primary key for the new users
	* @return the new users
	*/
	public static Users create(long UserID) {
		return getPersistence().create(UserID);
	}

	/**
	* Removes the users with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param UserID the primary key of the users
	* @return the users that was removed
	* @throws NoSuchUsersException if a users with the primary key could not be found
	*/
	public static Users remove(long UserID)
		throws surveyAPI.exception.NoSuchUsersException {
		return getPersistence().remove(UserID);
	}

	public static Users updateImpl(Users users) {
		return getPersistence().updateImpl(users);
	}

	/**
	* Returns the users with the primary key or throws a {@link NoSuchUsersException} if it could not be found.
	*
	* @param UserID the primary key of the users
	* @return the users
	* @throws NoSuchUsersException if a users with the primary key could not be found
	*/
	public static Users findByPrimaryKey(long UserID)
		throws surveyAPI.exception.NoSuchUsersException {
		return getPersistence().findByPrimaryKey(UserID);
	}

	/**
	* Returns the users with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param UserID the primary key of the users
	* @return the users, or <code>null</code> if a users with the primary key could not be found
	*/
	public static Users fetchByPrimaryKey(long UserID) {
		return getPersistence().fetchByPrimaryKey(UserID);
	}

	public static java.util.Map<java.io.Serializable, Users> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the userses.
	*
	* @return the userses
	*/
	public static List<Users> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the userses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of userses
	* @param end the upper bound of the range of userses (not inclusive)
	* @return the range of userses
	*/
	public static List<Users> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the userses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of userses
	* @param end the upper bound of the range of userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of userses
	*/
	public static List<Users> findAll(int start, int end,
		OrderByComparator<Users> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the userses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UsersModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of userses
	* @param end the upper bound of the range of userses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of userses
	*/
	public static List<Users> findAll(int start, int end,
		OrderByComparator<Users> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the userses from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of userses.
	*
	* @return the number of userses
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static UsersPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<UsersPersistence, UsersPersistence> _serviceTracker =
		ServiceTrackerFactory.open(UsersPersistence.class);
}