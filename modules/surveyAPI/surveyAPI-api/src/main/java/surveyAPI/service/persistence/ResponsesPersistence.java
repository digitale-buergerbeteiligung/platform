/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import surveyAPI.exception.NoSuchResponsesException;

import surveyAPI.model.Responses;

/**
 * The persistence interface for the responses service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see surveyAPI.service.persistence.impl.ResponsesPersistenceImpl
 * @see ResponsesUtil
 * @generated
 */
@ProviderType
public interface ResponsesPersistence extends BasePersistence<Responses> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ResponsesUtil} to access the responses persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the responseses where UserID = &#63;.
	*
	* @param UserID the user ID
	* @return the matching responseses
	*/
	public java.util.List<Responses> findByUID(long UserID);

	/**
	* Returns a range of all the responseses where UserID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param UserID the user ID
	* @param start the lower bound of the range of responseses
	* @param end the upper bound of the range of responseses (not inclusive)
	* @return the range of matching responseses
	*/
	public java.util.List<Responses> findByUID(long UserID, int start, int end);

	/**
	* Returns an ordered range of all the responseses where UserID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param UserID the user ID
	* @param start the lower bound of the range of responseses
	* @param end the upper bound of the range of responseses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching responseses
	*/
	public java.util.List<Responses> findByUID(long UserID, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Responses> orderByComparator);

	/**
	* Returns an ordered range of all the responseses where UserID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param UserID the user ID
	* @param start the lower bound of the range of responseses
	* @param end the upper bound of the range of responseses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching responseses
	*/
	public java.util.List<Responses> findByUID(long UserID, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Responses> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first responses in the ordered set where UserID = &#63;.
	*
	* @param UserID the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching responses
	* @throws NoSuchResponsesException if a matching responses could not be found
	*/
	public Responses findByUID_First(long UserID,
		com.liferay.portal.kernel.util.OrderByComparator<Responses> orderByComparator)
		throws NoSuchResponsesException;

	/**
	* Returns the first responses in the ordered set where UserID = &#63;.
	*
	* @param UserID the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching responses, or <code>null</code> if a matching responses could not be found
	*/
	public Responses fetchByUID_First(long UserID,
		com.liferay.portal.kernel.util.OrderByComparator<Responses> orderByComparator);

	/**
	* Returns the last responses in the ordered set where UserID = &#63;.
	*
	* @param UserID the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching responses
	* @throws NoSuchResponsesException if a matching responses could not be found
	*/
	public Responses findByUID_Last(long UserID,
		com.liferay.portal.kernel.util.OrderByComparator<Responses> orderByComparator)
		throws NoSuchResponsesException;

	/**
	* Returns the last responses in the ordered set where UserID = &#63;.
	*
	* @param UserID the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching responses, or <code>null</code> if a matching responses could not be found
	*/
	public Responses fetchByUID_Last(long UserID,
		com.liferay.portal.kernel.util.OrderByComparator<Responses> orderByComparator);

	/**
	* Returns the responseses before and after the current responses in the ordered set where UserID = &#63;.
	*
	* @param ResponseID the primary key of the current responses
	* @param UserID the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next responses
	* @throws NoSuchResponsesException if a responses with the primary key could not be found
	*/
	public Responses[] findByUID_PrevAndNext(long ResponseID, long UserID,
		com.liferay.portal.kernel.util.OrderByComparator<Responses> orderByComparator)
		throws NoSuchResponsesException;

	/**
	* Removes all the responseses where UserID = &#63; from the database.
	*
	* @param UserID the user ID
	*/
	public void removeByUID(long UserID);

	/**
	* Returns the number of responseses where UserID = &#63;.
	*
	* @param UserID the user ID
	* @return the number of matching responseses
	*/
	public int countByUID(long UserID);

	/**
	* Caches the responses in the entity cache if it is enabled.
	*
	* @param responses the responses
	*/
	public void cacheResult(Responses responses);

	/**
	* Caches the responseses in the entity cache if it is enabled.
	*
	* @param responseses the responseses
	*/
	public void cacheResult(java.util.List<Responses> responseses);

	/**
	* Creates a new responses with the primary key. Does not add the responses to the database.
	*
	* @param ResponseID the primary key for the new responses
	* @return the new responses
	*/
	public Responses create(long ResponseID);

	/**
	* Removes the responses with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ResponseID the primary key of the responses
	* @return the responses that was removed
	* @throws NoSuchResponsesException if a responses with the primary key could not be found
	*/
	public Responses remove(long ResponseID) throws NoSuchResponsesException;

	public Responses updateImpl(Responses responses);

	/**
	* Returns the responses with the primary key or throws a {@link NoSuchResponsesException} if it could not be found.
	*
	* @param ResponseID the primary key of the responses
	* @return the responses
	* @throws NoSuchResponsesException if a responses with the primary key could not be found
	*/
	public Responses findByPrimaryKey(long ResponseID)
		throws NoSuchResponsesException;

	/**
	* Returns the responses with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ResponseID the primary key of the responses
	* @return the responses, or <code>null</code> if a responses with the primary key could not be found
	*/
	public Responses fetchByPrimaryKey(long ResponseID);

	@Override
	public java.util.Map<java.io.Serializable, Responses> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the responseses.
	*
	* @return the responseses
	*/
	public java.util.List<Responses> findAll();

	/**
	* Returns a range of all the responseses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of responseses
	* @param end the upper bound of the range of responseses (not inclusive)
	* @return the range of responseses
	*/
	public java.util.List<Responses> findAll(int start, int end);

	/**
	* Returns an ordered range of all the responseses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of responseses
	* @param end the upper bound of the range of responseses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of responseses
	*/
	public java.util.List<Responses> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Responses> orderByComparator);

	/**
	* Returns an ordered range of all the responseses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ResponsesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of responseses
	* @param end the upper bound of the range of responseses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of responseses
	*/
	public java.util.List<Responses> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Responses> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the responseses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of responseses.
	*
	* @return the number of responseses
	*/
	public int countAll();
}