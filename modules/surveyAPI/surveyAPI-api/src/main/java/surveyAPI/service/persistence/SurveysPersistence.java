/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import surveyAPI.exception.NoSuchSurveysException;

import surveyAPI.model.Surveys;

/**
 * The persistence interface for the surveys service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see surveyAPI.service.persistence.impl.SurveysPersistenceImpl
 * @see SurveysUtil
 * @generated
 */
@ProviderType
public interface SurveysPersistence extends BasePersistence<Surveys> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SurveysUtil} to access the surveys persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the surveys in the entity cache if it is enabled.
	*
	* @param surveys the surveys
	*/
	public void cacheResult(Surveys surveys);

	/**
	* Caches the surveyses in the entity cache if it is enabled.
	*
	* @param surveyses the surveyses
	*/
	public void cacheResult(java.util.List<Surveys> surveyses);

	/**
	* Creates a new surveys with the primary key. Does not add the surveys to the database.
	*
	* @param SurveyID the primary key for the new surveys
	* @return the new surveys
	*/
	public Surveys create(long SurveyID);

	/**
	* Removes the surveys with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param SurveyID the primary key of the surveys
	* @return the surveys that was removed
	* @throws NoSuchSurveysException if a surveys with the primary key could not be found
	*/
	public Surveys remove(long SurveyID) throws NoSuchSurveysException;

	public Surveys updateImpl(Surveys surveys);

	/**
	* Returns the surveys with the primary key or throws a {@link NoSuchSurveysException} if it could not be found.
	*
	* @param SurveyID the primary key of the surveys
	* @return the surveys
	* @throws NoSuchSurveysException if a surveys with the primary key could not be found
	*/
	public Surveys findByPrimaryKey(long SurveyID)
		throws NoSuchSurveysException;

	/**
	* Returns the surveys with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param SurveyID the primary key of the surveys
	* @return the surveys, or <code>null</code> if a surveys with the primary key could not be found
	*/
	public Surveys fetchByPrimaryKey(long SurveyID);

	@Override
	public java.util.Map<java.io.Serializable, Surveys> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the surveyses.
	*
	* @return the surveyses
	*/
	public java.util.List<Surveys> findAll();

	/**
	* Returns a range of all the surveyses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SurveysModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of surveyses
	* @param end the upper bound of the range of surveyses (not inclusive)
	* @return the range of surveyses
	*/
	public java.util.List<Surveys> findAll(int start, int end);

	/**
	* Returns an ordered range of all the surveyses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SurveysModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of surveyses
	* @param end the upper bound of the range of surveyses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of surveyses
	*/
	public java.util.List<Surveys> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Surveys> orderByComparator);

	/**
	* Returns an ordered range of all the surveyses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SurveysModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of surveyses
	* @param end the upper bound of the range of surveyses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of surveyses
	*/
	public java.util.List<Surveys> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Surveys> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the surveyses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of surveyses.
	*
	* @return the number of surveyses
	*/
	public int countAll();
}