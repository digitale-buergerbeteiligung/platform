/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import surveyAPI.exception.NoSuchInputTypeException;

import surveyAPI.model.InputType;

/**
 * The persistence interface for the input type service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see surveyAPI.service.persistence.impl.InputTypePersistenceImpl
 * @see InputTypeUtil
 * @generated
 */
@ProviderType
public interface InputTypePersistence extends BasePersistence<InputType> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link InputTypeUtil} to access the input type persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the input type in the entity cache if it is enabled.
	*
	* @param inputType the input type
	*/
	public void cacheResult(InputType inputType);

	/**
	* Caches the input types in the entity cache if it is enabled.
	*
	* @param inputTypes the input types
	*/
	public void cacheResult(java.util.List<InputType> inputTypes);

	/**
	* Creates a new input type with the primary key. Does not add the input type to the database.
	*
	* @param TypeID the primary key for the new input type
	* @return the new input type
	*/
	public InputType create(int TypeID);

	/**
	* Removes the input type with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param TypeID the primary key of the input type
	* @return the input type that was removed
	* @throws NoSuchInputTypeException if a input type with the primary key could not be found
	*/
	public InputType remove(int TypeID) throws NoSuchInputTypeException;

	public InputType updateImpl(InputType inputType);

	/**
	* Returns the input type with the primary key or throws a {@link NoSuchInputTypeException} if it could not be found.
	*
	* @param TypeID the primary key of the input type
	* @return the input type
	* @throws NoSuchInputTypeException if a input type with the primary key could not be found
	*/
	public InputType findByPrimaryKey(int TypeID)
		throws NoSuchInputTypeException;

	/**
	* Returns the input type with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param TypeID the primary key of the input type
	* @return the input type, or <code>null</code> if a input type with the primary key could not be found
	*/
	public InputType fetchByPrimaryKey(int TypeID);

	@Override
	public java.util.Map<java.io.Serializable, InputType> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the input types.
	*
	* @return the input types
	*/
	public java.util.List<InputType> findAll();

	/**
	* Returns a range of all the input types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link InputTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of input types
	* @param end the upper bound of the range of input types (not inclusive)
	* @return the range of input types
	*/
	public java.util.List<InputType> findAll(int start, int end);

	/**
	* Returns an ordered range of all the input types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link InputTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of input types
	* @param end the upper bound of the range of input types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of input types
	*/
	public java.util.List<InputType> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<InputType> orderByComparator);

	/**
	* Returns an ordered range of all the input types.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link InputTypeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of input types
	* @param end the upper bound of the range of input types (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of input types
	*/
	public java.util.List<InputType> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<InputType> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the input types from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of input types.
	*
	* @return the number of input types
	*/
	public int countAll();
}