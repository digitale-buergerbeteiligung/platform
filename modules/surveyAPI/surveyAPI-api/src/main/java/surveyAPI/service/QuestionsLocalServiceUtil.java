/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Questions. This utility wraps
 * {@link surveyAPI.service.impl.QuestionsLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see QuestionsLocalService
 * @see surveyAPI.service.base.QuestionsLocalServiceBaseImpl
 * @see surveyAPI.service.impl.QuestionsLocalServiceImpl
 * @generated
 */
@ProviderType
public class QuestionsLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link surveyAPI.service.impl.QuestionsLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of questionses.
	*
	* @return the number of questionses
	*/
	public static int getQuestionsesCount() {
		return getService().getQuestionsesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.QuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.QuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns a range of all the questionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.QuestionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of questionses
	* @param end the upper bound of the range of questionses (not inclusive)
	* @return the range of questionses
	*/
	public static java.util.List<surveyAPI.model.Questions> getQuestionses(
		int start, int end) {
		return getService().getQuestionses(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static org.json.JSONObject addQuestionNChoices(
		java.lang.String quesNChoices) throws org.json.JSONException {
		return getService().addQuestionNChoices(quesNChoices);
	}

	public static org.json.JSONObject deleteQuestion(long surveyID, long qID)
		throws org.json.JSONException {
		return getService().deleteQuestion(surveyID, qID);
	}

	public static org.json.JSONObject editQuestion(long qID,
		java.lang.String qText) throws org.json.JSONException {
		return getService().editQuestion(qID, qText);
	}

	public static org.json.JSONObject quesNChoiceUtil(long surveyID,
		java.lang.String QName, java.lang.String QText, boolean required,
		org.json.JSONArray choiceArr, java.lang.String type)
		throws org.json.JSONException {
		return getService()
				   .quesNChoiceUtil(surveyID, QName, QText, required,
			choiceArr, type);
	}

	/**
	* Adds the questions to the database. Also notifies the appropriate model listeners.
	*
	* @param questions the questions
	* @return the questions that was added
	*/
	public static surveyAPI.model.Questions addQuestions(
		surveyAPI.model.Questions questions) {
		return getService().addQuestions(questions);
	}

	/**
	* Creates a new questions with the primary key. Does not add the questions to the database.
	*
	* @param QuestionID the primary key for the new questions
	* @return the new questions
	*/
	public static surveyAPI.model.Questions createQuestions(long QuestionID) {
		return getService().createQuestions(QuestionID);
	}

	/**
	* Deletes the questions with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param QuestionID the primary key of the questions
	* @return the questions that was removed
	* @throws PortalException if a questions with the primary key could not be found
	*/
	public static surveyAPI.model.Questions deleteQuestions(long QuestionID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteQuestions(QuestionID);
	}

	/**
	* Deletes the questions from the database. Also notifies the appropriate model listeners.
	*
	* @param questions the questions
	* @return the questions that was removed
	*/
	public static surveyAPI.model.Questions deleteQuestions(
		surveyAPI.model.Questions questions) {
		return getService().deleteQuestions(questions);
	}

	public static surveyAPI.model.Questions fetchQuestions(long QuestionID) {
		return getService().fetchQuestions(QuestionID);
	}

	/**
	* Returns the questions with the primary key.
	*
	* @param QuestionID the primary key of the questions
	* @return the questions
	* @throws PortalException if a questions with the primary key could not be found
	*/
	public static surveyAPI.model.Questions getQuestions(long QuestionID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getQuestions(QuestionID);
	}

	/**
	* Updates the questions in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param questions the questions
	* @return the questions that was updated
	*/
	public static surveyAPI.model.Questions updateQuestions(
		surveyAPI.model.Questions questions) {
		return getService().updateQuestions(questions);
	}

	public static QuestionsLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<QuestionsLocalService, QuestionsLocalService> _serviceTracker =
		ServiceTrackerFactory.open(QuestionsLocalService.class);
}