/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ResponsesService}.
 *
 * @author Brian Wing Shun Chan
 * @see ResponsesService
 * @generated
 */
@ProviderType
public class ResponsesServiceWrapper implements ResponsesService,
	ServiceWrapper<ResponsesService> {
	public ResponsesServiceWrapper(ResponsesService responsesService) {
		_responsesService = responsesService;
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _responsesService.getOSGiServiceIdentifier();
	}

	@Override
	public ResponsesService getWrappedService() {
		return _responsesService;
	}

	@Override
	public void setWrappedService(ResponsesService responsesService) {
		_responsesService = responsesService;
	}

	private ResponsesService _responsesService;
}