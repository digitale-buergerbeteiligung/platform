/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link UsersService}.
 *
 * @author Brian Wing Shun Chan
 * @see UsersService
 * @generated
 */
@ProviderType
public class UsersServiceWrapper implements UsersService,
	ServiceWrapper<UsersService> {
	public UsersServiceWrapper(UsersService usersService) {
		_usersService = usersService;
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _usersService.getOSGiServiceIdentifier();
	}

	/**
	* @param jsonInput
	{
	"SurveyID":"",
	ResChoices: [
	{"QuestionId":"","ChoiceId":""},
	{"QuestionId":"","ChoiceId":""}
	],
	ResTexts: {
	"QuestionId":"text", //Here QuestionId is of type long, this is 1:"hello"
	"QuestionId":"text"
	},
	"UserEmail" : emailAddress
	}
	* @return
	* @throws JSONException
	*/
	@Override
	public org.json.JSONObject addUserResponse(java.lang.String jsonInput)
		throws org.json.JSONException {
		return _usersService.addUserResponse(jsonInput);
	}

	@Override
	public UsersService getWrappedService() {
		return _usersService;
	}

	@Override
	public void setWrappedService(UsersService usersService) {
		_usersService = usersService;
	}

	private UsersService _usersService;
}