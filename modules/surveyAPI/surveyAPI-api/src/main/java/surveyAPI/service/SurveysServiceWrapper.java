/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SurveysService}.
 *
 * @author Brian Wing Shun Chan
 * @see SurveysService
 * @generated
 */
@ProviderType
public class SurveysServiceWrapper implements SurveysService,
	ServiceWrapper<SurveysService> {
	public SurveysServiceWrapper(SurveysService surveysService) {
		_surveysService = surveysService;
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _surveysService.getOSGiServiceIdentifier();
	}

	/**
	* JSON Web Service to add a new survey
	*/
	@Override
	public org.json.JSONObject addNewSurvey() throws org.json.JSONException {
		return _surveysService.addNewSurvey();
	}

	/**
	* This just adds a survey and returns the survey ID, user can later add questions
	*
	* @param name
	* @return
	* @throws JSONException
	*/
	@Override
	public org.json.JSONObject addNewSurvey(java.lang.String name)
		throws org.json.JSONException {
		return _surveysService.addNewSurvey(name);
	}

	/**
	* This adds a Survey with all the questions and choices
	*
	* @param name
	* @param jsonInput - Json array of json objects
	[
	{"QName":"", "QText":"", "Required":"", "Choices":["choice 1", "choice 2", "choice 3"], "Type":"Single/multiple/boolean"},
	{"QName":"", "QText":"", "Required":"", "Choices":["choice 1", "choice 2", "choice 3"], "Type":"Single/multiple/boolean"}
	]
	* @return
	* @throws JSONException
	*/
	@Override
	public org.json.JSONObject addNewSurvey(java.lang.String name,
		java.lang.String jsonInput) throws org.json.JSONException {
		return _surveysService.addNewSurvey(name, jsonInput);
	}

	/**
	* JSON Web service to delete an existing survey based on survey ID
	*/
	@Override
	public org.json.JSONObject deleteSurvey() throws org.json.JSONException {
		return _surveysService.deleteSurvey();
	}

	@Override
	public org.json.JSONObject deleteSurvey(long surveyID)
		throws org.json.JSONException {
		return _surveysService.deleteSurvey(surveyID);
	}

	/**
	* JSON Web service to fetch all the question ids associated with a given survey id
	*/
	@Override
	public org.json.JSONObject getAllQuestionIds()
		throws org.json.JSONException {
		return _surveysService.getAllQuestionIds();
	}

	@Override
	public org.json.JSONObject getAllQuestionIds(long surveyID)
		throws org.json.JSONException {
		return _surveysService.getAllQuestionIds(surveyID);
	}

	/**
	* JSON Web service to get full survey
	*
	* @return JSONObject {Questions:JSONArray, Choices:JSONArray, QuestionIds:JSONArray}
	*/
	@Override
	public org.json.JSONObject getSurvey(long surveyId)
		throws org.json.JSONException {
		return _surveysService.getSurvey(surveyId);
	}

	@Override
	public SurveysService getWrappedService() {
		return _surveysService;
	}

	@Override
	public void setWrappedService(SurveysService surveysService) {
		_surveysService = surveysService;
	}

	private SurveysService _surveysService;
}