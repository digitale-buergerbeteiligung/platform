/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SurveysLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see SurveysLocalService
 * @generated
 */
@ProviderType
public class SurveysLocalServiceWrapper implements SurveysLocalService,
	ServiceWrapper<SurveysLocalService> {
	public SurveysLocalServiceWrapper(SurveysLocalService surveysLocalService) {
		_surveysLocalService = surveysLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _surveysLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _surveysLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _surveysLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _surveysLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _surveysLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of surveyses.
	*
	* @return the number of surveyses
	*/
	@Override
	public int getSurveysesCount() {
		return _surveysLocalService.getSurveysesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _surveysLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _surveysLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.SurveysModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _surveysLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.SurveysModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _surveysLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the surveyses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link surveyAPI.model.impl.SurveysModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of surveyses
	* @param end the upper bound of the range of surveyses (not inclusive)
	* @return the range of surveyses
	*/
	@Override
	public java.util.List<surveyAPI.model.Surveys> getSurveyses(int start,
		int end) {
		return _surveysLocalService.getSurveyses(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _surveysLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _surveysLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public org.json.JSONObject addNewSurvey(java.lang.String name)
		throws org.json.JSONException {
		return _surveysLocalService.addNewSurvey(name);
	}

	@Override
	public org.json.JSONObject addNewSurvey(java.lang.String name,
		java.lang.String jsonInput) throws org.json.JSONException {
		return _surveysLocalService.addNewSurvey(name, jsonInput);
	}

	@Override
	public org.json.JSONObject deleteSurvey(long surveyID)
		throws org.json.JSONException {
		return _surveysLocalService.deleteSurvey(surveyID);
	}

	@Override
	public org.json.JSONObject getAllQuestionIds(long surveyID)
		throws org.json.JSONException {
		return _surveysLocalService.getAllQuestionIds(surveyID);
	}

	@Override
	public org.json.JSONObject getSurvey(long surveyId)
		throws org.json.JSONException {
		return _surveysLocalService.getSurvey(surveyId);
	}

	/**
	* Adds the surveys to the database. Also notifies the appropriate model listeners.
	*
	* @param surveys the surveys
	* @return the surveys that was added
	*/
	@Override
	public surveyAPI.model.Surveys addSurveys(surveyAPI.model.Surveys surveys) {
		return _surveysLocalService.addSurveys(surveys);
	}

	/**
	* Creates a new surveys with the primary key. Does not add the surveys to the database.
	*
	* @param SurveyID the primary key for the new surveys
	* @return the new surveys
	*/
	@Override
	public surveyAPI.model.Surveys createSurveys(long SurveyID) {
		return _surveysLocalService.createSurveys(SurveyID);
	}

	/**
	* Deletes the surveys with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param SurveyID the primary key of the surveys
	* @return the surveys that was removed
	* @throws PortalException if a surveys with the primary key could not be found
	*/
	@Override
	public surveyAPI.model.Surveys deleteSurveys(long SurveyID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _surveysLocalService.deleteSurveys(SurveyID);
	}

	/**
	* Deletes the surveys from the database. Also notifies the appropriate model listeners.
	*
	* @param surveys the surveys
	* @return the surveys that was removed
	*/
	@Override
	public surveyAPI.model.Surveys deleteSurveys(
		surveyAPI.model.Surveys surveys) {
		return _surveysLocalService.deleteSurveys(surveys);
	}

	@Override
	public surveyAPI.model.Surveys fetchSurveys(long SurveyID) {
		return _surveysLocalService.fetchSurveys(SurveyID);
	}

	/**
	* Returns the surveys with the primary key.
	*
	* @param SurveyID the primary key of the surveys
	* @return the surveys
	* @throws PortalException if a surveys with the primary key could not be found
	*/
	@Override
	public surveyAPI.model.Surveys getSurveys(long SurveyID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _surveysLocalService.getSurveys(SurveyID);
	}

	/**
	* Updates the surveys in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param surveys the surveys
	* @return the surveys that was updated
	*/
	@Override
	public surveyAPI.model.Surveys updateSurveys(
		surveyAPI.model.Surveys surveys) {
		return _surveysLocalService.updateSurveys(surveys);
	}

	@Override
	public SurveysLocalService getWrappedService() {
		return _surveysLocalService;
	}

	@Override
	public void setWrappedService(SurveysLocalService surveysLocalService) {
		_surveysLocalService = surveysLocalService;
	}

	private SurveysLocalService _surveysLocalService;
}