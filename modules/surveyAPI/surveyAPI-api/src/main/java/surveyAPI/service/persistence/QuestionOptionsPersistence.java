/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import surveyAPI.exception.NoSuchQuestionOptionsException;

import surveyAPI.model.QuestionOptions;

/**
 * The persistence interface for the question options service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see surveyAPI.service.persistence.impl.QuestionOptionsPersistenceImpl
 * @see QuestionOptionsUtil
 * @generated
 */
@ProviderType
public interface QuestionOptionsPersistence extends BasePersistence<QuestionOptions> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link QuestionOptionsUtil} to access the question options persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the question optionses where QuestionID = &#63;.
	*
	* @param QuestionID the question ID
	* @return the matching question optionses
	*/
	public java.util.List<QuestionOptions> findByQID(long QuestionID);

	/**
	* Returns a range of all the question optionses where QuestionID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param QuestionID the question ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @return the range of matching question optionses
	*/
	public java.util.List<QuestionOptions> findByQID(long QuestionID,
		int start, int end);

	/**
	* Returns an ordered range of all the question optionses where QuestionID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param QuestionID the question ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching question optionses
	*/
	public java.util.List<QuestionOptions> findByQID(long QuestionID,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator);

	/**
	* Returns an ordered range of all the question optionses where QuestionID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param QuestionID the question ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching question optionses
	*/
	public java.util.List<QuestionOptions> findByQID(long QuestionID,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first question options in the ordered set where QuestionID = &#63;.
	*
	* @param QuestionID the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching question options
	* @throws NoSuchQuestionOptionsException if a matching question options could not be found
	*/
	public QuestionOptions findByQID_First(long QuestionID,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException;

	/**
	* Returns the first question options in the ordered set where QuestionID = &#63;.
	*
	* @param QuestionID the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching question options, or <code>null</code> if a matching question options could not be found
	*/
	public QuestionOptions fetchByQID_First(long QuestionID,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator);

	/**
	* Returns the last question options in the ordered set where QuestionID = &#63;.
	*
	* @param QuestionID the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching question options
	* @throws NoSuchQuestionOptionsException if a matching question options could not be found
	*/
	public QuestionOptions findByQID_Last(long QuestionID,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException;

	/**
	* Returns the last question options in the ordered set where QuestionID = &#63;.
	*
	* @param QuestionID the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching question options, or <code>null</code> if a matching question options could not be found
	*/
	public QuestionOptions fetchByQID_Last(long QuestionID,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator);

	/**
	* Returns the question optionses before and after the current question options in the ordered set where QuestionID = &#63;.
	*
	* @param OptionID the primary key of the current question options
	* @param QuestionID the question ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next question options
	* @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	*/
	public QuestionOptions[] findByQID_PrevAndNext(long OptionID,
		long QuestionID,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException;

	/**
	* Removes all the question optionses where QuestionID = &#63; from the database.
	*
	* @param QuestionID the question ID
	*/
	public void removeByQID(long QuestionID);

	/**
	* Returns the number of question optionses where QuestionID = &#63;.
	*
	* @param QuestionID the question ID
	* @return the number of matching question optionses
	*/
	public int countByQID(long QuestionID);

	/**
	* Returns all the question optionses where ChoiceID = &#63;.
	*
	* @param ChoiceID the choice ID
	* @return the matching question optionses
	*/
	public java.util.List<QuestionOptions> findByCHID(long ChoiceID);

	/**
	* Returns a range of all the question optionses where ChoiceID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ChoiceID the choice ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @return the range of matching question optionses
	*/
	public java.util.List<QuestionOptions> findByCHID(long ChoiceID, int start,
		int end);

	/**
	* Returns an ordered range of all the question optionses where ChoiceID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ChoiceID the choice ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching question optionses
	*/
	public java.util.List<QuestionOptions> findByCHID(long ChoiceID, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator);

	/**
	* Returns an ordered range of all the question optionses where ChoiceID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ChoiceID the choice ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching question optionses
	*/
	public java.util.List<QuestionOptions> findByCHID(long ChoiceID, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first question options in the ordered set where ChoiceID = &#63;.
	*
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching question options
	* @throws NoSuchQuestionOptionsException if a matching question options could not be found
	*/
	public QuestionOptions findByCHID_First(long ChoiceID,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException;

	/**
	* Returns the first question options in the ordered set where ChoiceID = &#63;.
	*
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching question options, or <code>null</code> if a matching question options could not be found
	*/
	public QuestionOptions fetchByCHID_First(long ChoiceID,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator);

	/**
	* Returns the last question options in the ordered set where ChoiceID = &#63;.
	*
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching question options
	* @throws NoSuchQuestionOptionsException if a matching question options could not be found
	*/
	public QuestionOptions findByCHID_Last(long ChoiceID,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException;

	/**
	* Returns the last question options in the ordered set where ChoiceID = &#63;.
	*
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching question options, or <code>null</code> if a matching question options could not be found
	*/
	public QuestionOptions fetchByCHID_Last(long ChoiceID,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator);

	/**
	* Returns the question optionses before and after the current question options in the ordered set where ChoiceID = &#63;.
	*
	* @param OptionID the primary key of the current question options
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next question options
	* @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	*/
	public QuestionOptions[] findByCHID_PrevAndNext(long OptionID,
		long ChoiceID,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException;

	/**
	* Removes all the question optionses where ChoiceID = &#63; from the database.
	*
	* @param ChoiceID the choice ID
	*/
	public void removeByCHID(long ChoiceID);

	/**
	* Returns the number of question optionses where ChoiceID = &#63;.
	*
	* @param ChoiceID the choice ID
	* @return the number of matching question optionses
	*/
	public int countByCHID(long ChoiceID);

	/**
	* Returns all the question optionses where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @return the matching question optionses
	*/
	public java.util.List<QuestionOptions> findByQuesIdNChoiceId(
		long QuestionID, long ChoiceID);

	/**
	* Returns a range of all the question optionses where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @return the range of matching question optionses
	*/
	public java.util.List<QuestionOptions> findByQuesIdNChoiceId(
		long QuestionID, long ChoiceID, int start, int end);

	/**
	* Returns an ordered range of all the question optionses where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching question optionses
	*/
	public java.util.List<QuestionOptions> findByQuesIdNChoiceId(
		long QuestionID, long ChoiceID, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator);

	/**
	* Returns an ordered range of all the question optionses where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching question optionses
	*/
	public java.util.List<QuestionOptions> findByQuesIdNChoiceId(
		long QuestionID, long ChoiceID, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first question options in the ordered set where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching question options
	* @throws NoSuchQuestionOptionsException if a matching question options could not be found
	*/
	public QuestionOptions findByQuesIdNChoiceId_First(long QuestionID,
		long ChoiceID,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException;

	/**
	* Returns the first question options in the ordered set where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching question options, or <code>null</code> if a matching question options could not be found
	*/
	public QuestionOptions fetchByQuesIdNChoiceId_First(long QuestionID,
		long ChoiceID,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator);

	/**
	* Returns the last question options in the ordered set where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching question options
	* @throws NoSuchQuestionOptionsException if a matching question options could not be found
	*/
	public QuestionOptions findByQuesIdNChoiceId_Last(long QuestionID,
		long ChoiceID,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException;

	/**
	* Returns the last question options in the ordered set where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching question options, or <code>null</code> if a matching question options could not be found
	*/
	public QuestionOptions fetchByQuesIdNChoiceId_Last(long QuestionID,
		long ChoiceID,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator);

	/**
	* Returns the question optionses before and after the current question options in the ordered set where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* @param OptionID the primary key of the current question options
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next question options
	* @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	*/
	public QuestionOptions[] findByQuesIdNChoiceId_PrevAndNext(long OptionID,
		long QuestionID, long ChoiceID,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator)
		throws NoSuchQuestionOptionsException;

	/**
	* Removes all the question optionses where QuestionID = &#63; and ChoiceID = &#63; from the database.
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	*/
	public void removeByQuesIdNChoiceId(long QuestionID, long ChoiceID);

	/**
	* Returns the number of question optionses where QuestionID = &#63; and ChoiceID = &#63;.
	*
	* @param QuestionID the question ID
	* @param ChoiceID the choice ID
	* @return the number of matching question optionses
	*/
	public int countByQuesIdNChoiceId(long QuestionID, long ChoiceID);

	/**
	* Caches the question options in the entity cache if it is enabled.
	*
	* @param questionOptions the question options
	*/
	public void cacheResult(QuestionOptions questionOptions);

	/**
	* Caches the question optionses in the entity cache if it is enabled.
	*
	* @param questionOptionses the question optionses
	*/
	public void cacheResult(java.util.List<QuestionOptions> questionOptionses);

	/**
	* Creates a new question options with the primary key. Does not add the question options to the database.
	*
	* @param OptionID the primary key for the new question options
	* @return the new question options
	*/
	public QuestionOptions create(long OptionID);

	/**
	* Removes the question options with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param OptionID the primary key of the question options
	* @return the question options that was removed
	* @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	*/
	public QuestionOptions remove(long OptionID)
		throws NoSuchQuestionOptionsException;

	public QuestionOptions updateImpl(QuestionOptions questionOptions);

	/**
	* Returns the question options with the primary key or throws a {@link NoSuchQuestionOptionsException} if it could not be found.
	*
	* @param OptionID the primary key of the question options
	* @return the question options
	* @throws NoSuchQuestionOptionsException if a question options with the primary key could not be found
	*/
	public QuestionOptions findByPrimaryKey(long OptionID)
		throws NoSuchQuestionOptionsException;

	/**
	* Returns the question options with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param OptionID the primary key of the question options
	* @return the question options, or <code>null</code> if a question options with the primary key could not be found
	*/
	public QuestionOptions fetchByPrimaryKey(long OptionID);

	@Override
	public java.util.Map<java.io.Serializable, QuestionOptions> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the question optionses.
	*
	* @return the question optionses
	*/
	public java.util.List<QuestionOptions> findAll();

	/**
	* Returns a range of all the question optionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @return the range of question optionses
	*/
	public java.util.List<QuestionOptions> findAll(int start, int end);

	/**
	* Returns an ordered range of all the question optionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of question optionses
	*/
	public java.util.List<QuestionOptions> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator);

	/**
	* Returns an ordered range of all the question optionses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link QuestionOptionsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of question optionses
	* @param end the upper bound of the range of question optionses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of question optionses
	*/
	public java.util.List<QuestionOptions> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<QuestionOptions> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the question optionses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of question optionses.
	*
	* @return the number of question optionses
	*/
	public int countAll();
}