/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link UserSurveyMapping}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserSurveyMapping
 * @generated
 */
@ProviderType
public class UserSurveyMappingWrapper implements UserSurveyMapping,
	ModelWrapper<UserSurveyMapping> {
	public UserSurveyMappingWrapper(UserSurveyMapping userSurveyMapping) {
		_userSurveyMapping = userSurveyMapping;
	}

	@Override
	public Class<?> getModelClass() {
		return UserSurveyMapping.class;
	}

	@Override
	public String getModelClassName() {
		return UserSurveyMapping.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("UserID", getUserID());
		attributes.put("SurveyID", getSurveyID());
		attributes.put("DateOfSurvey", getDateOfSurvey());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long UserID = (Long)attributes.get("UserID");

		if (UserID != null) {
			setUserID(UserID);
		}

		Long SurveyID = (Long)attributes.get("SurveyID");

		if (SurveyID != null) {
			setSurveyID(SurveyID);
		}

		Date DateOfSurvey = (Date)attributes.get("DateOfSurvey");

		if (DateOfSurvey != null) {
			setDateOfSurvey(DateOfSurvey);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _userSurveyMapping.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _userSurveyMapping.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _userSurveyMapping.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _userSurveyMapping.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<surveyAPI.model.UserSurveyMapping> toCacheModel() {
		return _userSurveyMapping.toCacheModel();
	}

	@Override
	public int compareTo(surveyAPI.model.UserSurveyMapping userSurveyMapping) {
		return _userSurveyMapping.compareTo(userSurveyMapping);
	}

	@Override
	public int hashCode() {
		return _userSurveyMapping.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _userSurveyMapping.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new UserSurveyMappingWrapper((UserSurveyMapping)_userSurveyMapping.clone());
	}

	@Override
	public java.lang.String toString() {
		return _userSurveyMapping.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _userSurveyMapping.toXmlString();
	}

	/**
	* Returns the date of survey of this user survey mapping.
	*
	* @return the date of survey of this user survey mapping
	*/
	@Override
	public Date getDateOfSurvey() {
		return _userSurveyMapping.getDateOfSurvey();
	}

	/**
	* Returns the survey ID of this user survey mapping.
	*
	* @return the survey ID of this user survey mapping
	*/
	@Override
	public long getSurveyID() {
		return _userSurveyMapping.getSurveyID();
	}

	/**
	* Returns the user ID of this user survey mapping.
	*
	* @return the user ID of this user survey mapping
	*/
	@Override
	public long getUserID() {
		return _userSurveyMapping.getUserID();
	}

	@Override
	public surveyAPI.model.UserSurveyMapping toEscapedModel() {
		return new UserSurveyMappingWrapper(_userSurveyMapping.toEscapedModel());
	}

	@Override
	public surveyAPI.model.UserSurveyMapping toUnescapedModel() {
		return new UserSurveyMappingWrapper(_userSurveyMapping.toUnescapedModel());
	}

	/**
	* Returns the primary key of this user survey mapping.
	*
	* @return the primary key of this user survey mapping
	*/
	@Override
	public surveyAPI.service.persistence.UserSurveyMappingPK getPrimaryKey() {
		return _userSurveyMapping.getPrimaryKey();
	}

	@Override
	public void persist() {
		_userSurveyMapping.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_userSurveyMapping.setCachedModel(cachedModel);
	}

	/**
	* Sets the date of survey of this user survey mapping.
	*
	* @param DateOfSurvey the date of survey of this user survey mapping
	*/
	@Override
	public void setDateOfSurvey(Date DateOfSurvey) {
		_userSurveyMapping.setDateOfSurvey(DateOfSurvey);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_userSurveyMapping.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_userSurveyMapping.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_userSurveyMapping.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_userSurveyMapping.setNew(n);
	}

	/**
	* Sets the primary key of this user survey mapping.
	*
	* @param primaryKey the primary key of this user survey mapping
	*/
	@Override
	public void setPrimaryKey(
		surveyAPI.service.persistence.UserSurveyMappingPK primaryKey) {
		_userSurveyMapping.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_userSurveyMapping.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the survey ID of this user survey mapping.
	*
	* @param SurveyID the survey ID of this user survey mapping
	*/
	@Override
	public void setSurveyID(long SurveyID) {
		_userSurveyMapping.setSurveyID(SurveyID);
	}

	/**
	* Sets the user ID of this user survey mapping.
	*
	* @param UserID the user ID of this user survey mapping
	*/
	@Override
	public void setUserID(long UserID) {
		_userSurveyMapping.setUserID(UserID);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserSurveyMappingWrapper)) {
			return false;
		}

		UserSurveyMappingWrapper userSurveyMappingWrapper = (UserSurveyMappingWrapper)obj;

		if (Objects.equals(_userSurveyMapping,
					userSurveyMappingWrapper._userSurveyMapping)) {
			return true;
		}

		return false;
	}

	@Override
	public UserSurveyMapping getWrappedModel() {
		return _userSurveyMapping;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _userSurveyMapping.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _userSurveyMapping.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_userSurveyMapping.resetOriginalValues();
	}

	private final UserSurveyMapping _userSurveyMapping;
}