/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the Questions service. Represents a row in the &quot;SURVEY_Questions&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see QuestionsModel
 * @see surveyAPI.model.impl.QuestionsImpl
 * @see surveyAPI.model.impl.QuestionsModelImpl
 * @generated
 */
@ImplementationClassName("surveyAPI.model.impl.QuestionsImpl")
@ProviderType
public interface Questions extends QuestionsModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link surveyAPI.model.impl.QuestionsImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<Questions, Long> QUESTION_ID_ACCESSOR = new Accessor<Questions, Long>() {
			@Override
			public Long get(Questions questions) {
				return questions.getQuestionID();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<Questions> getTypeClass() {
				return Questions.class;
			}
		};
}