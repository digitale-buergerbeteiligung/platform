/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link surveyAPI.service.http.UsersServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see surveyAPI.service.http.UsersServiceSoap
 * @generated
 */
@ProviderType
public class UsersSoap implements Serializable {
	public static UsersSoap toSoapModel(Users model) {
		UsersSoap soapModel = new UsersSoap();

		soapModel.setUserID(model.getUserID());
		soapModel.setUserName(model.getUserName());
		soapModel.setEmail(model.getEmail());

		return soapModel;
	}

	public static UsersSoap[] toSoapModels(Users[] models) {
		UsersSoap[] soapModels = new UsersSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UsersSoap[][] toSoapModels(Users[][] models) {
		UsersSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UsersSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UsersSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UsersSoap[] toSoapModels(List<Users> models) {
		List<UsersSoap> soapModels = new ArrayList<UsersSoap>(models.size());

		for (Users model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UsersSoap[soapModels.size()]);
	}

	public UsersSoap() {
	}

	public long getPrimaryKey() {
		return _UserID;
	}

	public void setPrimaryKey(long pk) {
		setUserID(pk);
	}

	public long getUserID() {
		return _UserID;
	}

	public void setUserID(long UserID) {
		_UserID = UserID;
	}

	public String getUserName() {
		return _UserName;
	}

	public void setUserName(String UserName) {
		_UserName = UserName;
	}

	public String getEmail() {
		return _Email;
	}

	public void setEmail(String Email) {
		_Email = Email;
	}

	private long _UserID;
	private String _UserName;
	private String _Email;
}