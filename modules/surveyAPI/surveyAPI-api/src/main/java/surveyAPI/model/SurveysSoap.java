/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link surveyAPI.service.http.SurveysServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see surveyAPI.service.http.SurveysServiceSoap
 * @generated
 */
@ProviderType
public class SurveysSoap implements Serializable {
	public static SurveysSoap toSoapModel(Surveys model) {
		SurveysSoap soapModel = new SurveysSoap();

		soapModel.setSurveyID(model.getSurveyID());
		soapModel.setSurveyName(model.getSurveyName());
		soapModel.setDateOfCreation(model.getDateOfCreation());
		soapModel.setQuestionIds(model.getQuestionIds());

		return soapModel;
	}

	public static SurveysSoap[] toSoapModels(Surveys[] models) {
		SurveysSoap[] soapModels = new SurveysSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static SurveysSoap[][] toSoapModels(Surveys[][] models) {
		SurveysSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new SurveysSoap[models.length][models[0].length];
		}
		else {
			soapModels = new SurveysSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static SurveysSoap[] toSoapModels(List<Surveys> models) {
		List<SurveysSoap> soapModels = new ArrayList<SurveysSoap>(models.size());

		for (Surveys model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new SurveysSoap[soapModels.size()]);
	}

	public SurveysSoap() {
	}

	public long getPrimaryKey() {
		return _SurveyID;
	}

	public void setPrimaryKey(long pk) {
		setSurveyID(pk);
	}

	public long getSurveyID() {
		return _SurveyID;
	}

	public void setSurveyID(long SurveyID) {
		_SurveyID = SurveyID;
	}

	public String getSurveyName() {
		return _SurveyName;
	}

	public void setSurveyName(String SurveyName) {
		_SurveyName = SurveyName;
	}

	public Date getDateOfCreation() {
		return _DateOfCreation;
	}

	public void setDateOfCreation(Date DateOfCreation) {
		_DateOfCreation = DateOfCreation;
	}

	public String getQuestionIds() {
		return _QuestionIds;
	}

	public void setQuestionIds(String QuestionIds) {
		_QuestionIds = QuestionIds;
	}

	private long _SurveyID;
	private String _SurveyName;
	private Date _DateOfCreation;
	private String _QuestionIds;
}