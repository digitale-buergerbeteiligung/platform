/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link surveyAPI.service.http.ChoicesServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see surveyAPI.service.http.ChoicesServiceSoap
 * @generated
 */
@ProviderType
public class ChoicesSoap implements Serializable {
	public static ChoicesSoap toSoapModel(Choices model) {
		ChoicesSoap soapModel = new ChoicesSoap();

		soapModel.setChoiceID(model.getChoiceID());
		soapModel.setChoiceText(model.getChoiceText());

		return soapModel;
	}

	public static ChoicesSoap[] toSoapModels(Choices[] models) {
		ChoicesSoap[] soapModels = new ChoicesSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ChoicesSoap[][] toSoapModels(Choices[][] models) {
		ChoicesSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ChoicesSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ChoicesSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ChoicesSoap[] toSoapModels(List<Choices> models) {
		List<ChoicesSoap> soapModels = new ArrayList<ChoicesSoap>(models.size());

		for (Choices model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ChoicesSoap[soapModels.size()]);
	}

	public ChoicesSoap() {
	}

	public long getPrimaryKey() {
		return _ChoiceID;
	}

	public void setPrimaryKey(long pk) {
		setChoiceID(pk);
	}

	public long getChoiceID() {
		return _ChoiceID;
	}

	public void setChoiceID(long ChoiceID) {
		_ChoiceID = ChoiceID;
	}

	public String getChoiceText() {
		return _ChoiceText;
	}

	public void setChoiceText(String ChoiceText) {
		_ChoiceText = ChoiceText;
	}

	private long _ChoiceID;
	private String _ChoiceText;
}