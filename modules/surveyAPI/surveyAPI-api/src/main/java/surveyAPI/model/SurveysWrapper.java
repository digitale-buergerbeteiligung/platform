/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Surveys}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Surveys
 * @generated
 */
@ProviderType
public class SurveysWrapper implements Surveys, ModelWrapper<Surveys> {
	public SurveysWrapper(Surveys surveys) {
		_surveys = surveys;
	}

	@Override
	public Class<?> getModelClass() {
		return Surveys.class;
	}

	@Override
	public String getModelClassName() {
		return Surveys.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("SurveyID", getSurveyID());
		attributes.put("SurveyName", getSurveyName());
		attributes.put("DateOfCreation", getDateOfCreation());
		attributes.put("QuestionIds", getQuestionIds());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long SurveyID = (Long)attributes.get("SurveyID");

		if (SurveyID != null) {
			setSurveyID(SurveyID);
		}

		String SurveyName = (String)attributes.get("SurveyName");

		if (SurveyName != null) {
			setSurveyName(SurveyName);
		}

		Date DateOfCreation = (Date)attributes.get("DateOfCreation");

		if (DateOfCreation != null) {
			setDateOfCreation(DateOfCreation);
		}

		String QuestionIds = (String)attributes.get("QuestionIds");

		if (QuestionIds != null) {
			setQuestionIds(QuestionIds);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _surveys.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _surveys.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _surveys.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _surveys.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<surveyAPI.model.Surveys> toCacheModel() {
		return _surveys.toCacheModel();
	}

	@Override
	public int compareTo(surveyAPI.model.Surveys surveys) {
		return _surveys.compareTo(surveys);
	}

	@Override
	public int hashCode() {
		return _surveys.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _surveys.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new SurveysWrapper((Surveys)_surveys.clone());
	}

	/**
	* Returns the question IDs of this surveys.
	*
	* @return the question IDs of this surveys
	*/
	@Override
	public java.lang.String getQuestionIds() {
		return _surveys.getQuestionIds();
	}

	/**
	* Returns the survey name of this surveys.
	*
	* @return the survey name of this surveys
	*/
	@Override
	public java.lang.String getSurveyName() {
		return _surveys.getSurveyName();
	}

	@Override
	public java.lang.String toString() {
		return _surveys.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _surveys.toXmlString();
	}

	/**
	* Returns the date of creation of this surveys.
	*
	* @return the date of creation of this surveys
	*/
	@Override
	public Date getDateOfCreation() {
		return _surveys.getDateOfCreation();
	}

	/**
	* Returns the primary key of this surveys.
	*
	* @return the primary key of this surveys
	*/
	@Override
	public long getPrimaryKey() {
		return _surveys.getPrimaryKey();
	}

	/**
	* Returns the survey ID of this surveys.
	*
	* @return the survey ID of this surveys
	*/
	@Override
	public long getSurveyID() {
		return _surveys.getSurveyID();
	}

	@Override
	public surveyAPI.model.Surveys toEscapedModel() {
		return new SurveysWrapper(_surveys.toEscapedModel());
	}

	@Override
	public surveyAPI.model.Surveys toUnescapedModel() {
		return new SurveysWrapper(_surveys.toUnescapedModel());
	}

	@Override
	public void persist() {
		_surveys.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_surveys.setCachedModel(cachedModel);
	}

	/**
	* Sets the date of creation of this surveys.
	*
	* @param DateOfCreation the date of creation of this surveys
	*/
	@Override
	public void setDateOfCreation(Date DateOfCreation) {
		_surveys.setDateOfCreation(DateOfCreation);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_surveys.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_surveys.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_surveys.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_surveys.setNew(n);
	}

	/**
	* Sets the primary key of this surveys.
	*
	* @param primaryKey the primary key of this surveys
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_surveys.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_surveys.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the question IDs of this surveys.
	*
	* @param QuestionIds the question IDs of this surveys
	*/
	@Override
	public void setQuestionIds(java.lang.String QuestionIds) {
		_surveys.setQuestionIds(QuestionIds);
	}

	/**
	* Sets the survey ID of this surveys.
	*
	* @param SurveyID the survey ID of this surveys
	*/
	@Override
	public void setSurveyID(long SurveyID) {
		_surveys.setSurveyID(SurveyID);
	}

	/**
	* Sets the survey name of this surveys.
	*
	* @param SurveyName the survey name of this surveys
	*/
	@Override
	public void setSurveyName(java.lang.String SurveyName) {
		_surveys.setSurveyName(SurveyName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SurveysWrapper)) {
			return false;
		}

		SurveysWrapper surveysWrapper = (SurveysWrapper)obj;

		if (Objects.equals(_surveys, surveysWrapper._surveys)) {
			return true;
		}

		return false;
	}

	@Override
	public Surveys getWrappedModel() {
		return _surveys;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _surveys.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _surveys.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_surveys.resetOriginalValues();
	}

	private final Surveys _surveys;
}