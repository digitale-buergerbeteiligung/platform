/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link surveyAPI.service.http.ResponsesServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see surveyAPI.service.http.ResponsesServiceSoap
 * @generated
 */
@ProviderType
public class ResponsesSoap implements Serializable {
	public static ResponsesSoap toSoapModel(Responses model) {
		ResponsesSoap soapModel = new ResponsesSoap();

		soapModel.setResponseID(model.getResponseID());
		soapModel.setUserID(model.getUserID());
		soapModel.setOptionID(model.getOptionID());
		soapModel.setResponseText(model.getResponseText());

		return soapModel;
	}

	public static ResponsesSoap[] toSoapModels(Responses[] models) {
		ResponsesSoap[] soapModels = new ResponsesSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ResponsesSoap[][] toSoapModels(Responses[][] models) {
		ResponsesSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ResponsesSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ResponsesSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ResponsesSoap[] toSoapModels(List<Responses> models) {
		List<ResponsesSoap> soapModels = new ArrayList<ResponsesSoap>(models.size());

		for (Responses model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ResponsesSoap[soapModels.size()]);
	}

	public ResponsesSoap() {
	}

	public long getPrimaryKey() {
		return _ResponseID;
	}

	public void setPrimaryKey(long pk) {
		setResponseID(pk);
	}

	public long getResponseID() {
		return _ResponseID;
	}

	public void setResponseID(long ResponseID) {
		_ResponseID = ResponseID;
	}

	public long getUserID() {
		return _UserID;
	}

	public void setUserID(long UserID) {
		_UserID = UserID;
	}

	public long getOptionID() {
		return _OptionID;
	}

	public void setOptionID(long OptionID) {
		_OptionID = OptionID;
	}

	public String getResponseText() {
		return _ResponseText;
	}

	public void setResponseText(String ResponseText) {
		_ResponseText = ResponseText;
	}

	private long _ResponseID;
	private long _UserID;
	private long _OptionID;
	private String _ResponseText;
}