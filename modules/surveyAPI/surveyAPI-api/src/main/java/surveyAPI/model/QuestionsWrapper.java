/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Questions}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Questions
 * @generated
 */
@ProviderType
public class QuestionsWrapper implements Questions, ModelWrapper<Questions> {
	public QuestionsWrapper(Questions questions) {
		_questions = questions;
	}

	@Override
	public Class<?> getModelClass() {
		return Questions.class;
	}

	@Override
	public String getModelClassName() {
		return Questions.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("QuestionID", getQuestionID());
		attributes.put("ResponseType", getResponseType());
		attributes.put("QuestionName", getQuestionName());
		attributes.put("QuestionText", getQuestionText());
		attributes.put("DateOfCreation", getDateOfCreation());
		attributes.put("Required", getRequired());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long QuestionID = (Long)attributes.get("QuestionID");

		if (QuestionID != null) {
			setQuestionID(QuestionID);
		}

		String ResponseType = (String)attributes.get("ResponseType");

		if (ResponseType != null) {
			setResponseType(ResponseType);
		}

		String QuestionName = (String)attributes.get("QuestionName");

		if (QuestionName != null) {
			setQuestionName(QuestionName);
		}

		String QuestionText = (String)attributes.get("QuestionText");

		if (QuestionText != null) {
			setQuestionText(QuestionText);
		}

		Date DateOfCreation = (Date)attributes.get("DateOfCreation");

		if (DateOfCreation != null) {
			setDateOfCreation(DateOfCreation);
		}

		Boolean Required = (Boolean)attributes.get("Required");

		if (Required != null) {
			setRequired(Required);
		}
	}

	/**
	* Returns the required of this questions.
	*
	* @return the required of this questions
	*/
	@Override
	public boolean getRequired() {
		return _questions.getRequired();
	}

	@Override
	public boolean isCachedModel() {
		return _questions.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _questions.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _questions.isNew();
	}

	/**
	* Returns <code>true</code> if this questions is required.
	*
	* @return <code>true</code> if this questions is required; <code>false</code> otherwise
	*/
	@Override
	public boolean isRequired() {
		return _questions.isRequired();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _questions.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<surveyAPI.model.Questions> toCacheModel() {
		return _questions.toCacheModel();
	}

	@Override
	public int compareTo(surveyAPI.model.Questions questions) {
		return _questions.compareTo(questions);
	}

	@Override
	public int hashCode() {
		return _questions.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _questions.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new QuestionsWrapper((Questions)_questions.clone());
	}

	/**
	* Returns the question name of this questions.
	*
	* @return the question name of this questions
	*/
	@Override
	public java.lang.String getQuestionName() {
		return _questions.getQuestionName();
	}

	/**
	* Returns the question text of this questions.
	*
	* @return the question text of this questions
	*/
	@Override
	public java.lang.String getQuestionText() {
		return _questions.getQuestionText();
	}

	/**
	* Returns the response type of this questions.
	*
	* @return the response type of this questions
	*/
	@Override
	public java.lang.String getResponseType() {
		return _questions.getResponseType();
	}

	@Override
	public java.lang.String toString() {
		return _questions.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _questions.toXmlString();
	}

	/**
	* Returns the date of creation of this questions.
	*
	* @return the date of creation of this questions
	*/
	@Override
	public Date getDateOfCreation() {
		return _questions.getDateOfCreation();
	}

	/**
	* Returns the primary key of this questions.
	*
	* @return the primary key of this questions
	*/
	@Override
	public long getPrimaryKey() {
		return _questions.getPrimaryKey();
	}

	/**
	* Returns the question ID of this questions.
	*
	* @return the question ID of this questions
	*/
	@Override
	public long getQuestionID() {
		return _questions.getQuestionID();
	}

	@Override
	public surveyAPI.model.Questions toEscapedModel() {
		return new QuestionsWrapper(_questions.toEscapedModel());
	}

	@Override
	public surveyAPI.model.Questions toUnescapedModel() {
		return new QuestionsWrapper(_questions.toUnescapedModel());
	}

	@Override
	public void persist() {
		_questions.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_questions.setCachedModel(cachedModel);
	}

	/**
	* Sets the date of creation of this questions.
	*
	* @param DateOfCreation the date of creation of this questions
	*/
	@Override
	public void setDateOfCreation(Date DateOfCreation) {
		_questions.setDateOfCreation(DateOfCreation);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_questions.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_questions.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_questions.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_questions.setNew(n);
	}

	/**
	* Sets the primary key of this questions.
	*
	* @param primaryKey the primary key of this questions
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_questions.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_questions.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the question ID of this questions.
	*
	* @param QuestionID the question ID of this questions
	*/
	@Override
	public void setQuestionID(long QuestionID) {
		_questions.setQuestionID(QuestionID);
	}

	/**
	* Sets the question name of this questions.
	*
	* @param QuestionName the question name of this questions
	*/
	@Override
	public void setQuestionName(java.lang.String QuestionName) {
		_questions.setQuestionName(QuestionName);
	}

	/**
	* Sets the question text of this questions.
	*
	* @param QuestionText the question text of this questions
	*/
	@Override
	public void setQuestionText(java.lang.String QuestionText) {
		_questions.setQuestionText(QuestionText);
	}

	/**
	* Sets whether this questions is required.
	*
	* @param Required the required of this questions
	*/
	@Override
	public void setRequired(boolean Required) {
		_questions.setRequired(Required);
	}

	/**
	* Sets the response type of this questions.
	*
	* @param ResponseType the response type of this questions
	*/
	@Override
	public void setResponseType(java.lang.String ResponseType) {
		_questions.setResponseType(ResponseType);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof QuestionsWrapper)) {
			return false;
		}

		QuestionsWrapper questionsWrapper = (QuestionsWrapper)obj;

		if (Objects.equals(_questions, questionsWrapper._questions)) {
			return true;
		}

		return false;
	}

	@Override
	public Questions getWrappedModel() {
		return _questions;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _questions.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _questions.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_questions.resetOriginalValues();
	}

	private final Questions _questions;
}