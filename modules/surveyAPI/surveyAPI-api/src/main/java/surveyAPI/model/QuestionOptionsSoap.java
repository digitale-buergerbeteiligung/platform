/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link surveyAPI.service.http.QuestionOptionsServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see surveyAPI.service.http.QuestionOptionsServiceSoap
 * @generated
 */
@ProviderType
public class QuestionOptionsSoap implements Serializable {
	public static QuestionOptionsSoap toSoapModel(QuestionOptions model) {
		QuestionOptionsSoap soapModel = new QuestionOptionsSoap();

		soapModel.setOptionID(model.getOptionID());
		soapModel.setQuestionID(model.getQuestionID());
		soapModel.setChoiceID(model.getChoiceID());

		return soapModel;
	}

	public static QuestionOptionsSoap[] toSoapModels(QuestionOptions[] models) {
		QuestionOptionsSoap[] soapModels = new QuestionOptionsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static QuestionOptionsSoap[][] toSoapModels(
		QuestionOptions[][] models) {
		QuestionOptionsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new QuestionOptionsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new QuestionOptionsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static QuestionOptionsSoap[] toSoapModels(
		List<QuestionOptions> models) {
		List<QuestionOptionsSoap> soapModels = new ArrayList<QuestionOptionsSoap>(models.size());

		for (QuestionOptions model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new QuestionOptionsSoap[soapModels.size()]);
	}

	public QuestionOptionsSoap() {
	}

	public long getPrimaryKey() {
		return _OptionID;
	}

	public void setPrimaryKey(long pk) {
		setOptionID(pk);
	}

	public long getOptionID() {
		return _OptionID;
	}

	public void setOptionID(long OptionID) {
		_OptionID = OptionID;
	}

	public long getQuestionID() {
		return _QuestionID;
	}

	public void setQuestionID(long QuestionID) {
		_QuestionID = QuestionID;
	}

	public long getChoiceID() {
		return _ChoiceID;
	}

	public void setChoiceID(long ChoiceID) {
		_ChoiceID = ChoiceID;
	}

	private long _OptionID;
	private long _QuestionID;
	private long _ChoiceID;
}