/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package surveyAPI.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Users}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Users
 * @generated
 */
@ProviderType
public class UsersWrapper implements Users, ModelWrapper<Users> {
	public UsersWrapper(Users users) {
		_users = users;
	}

	@Override
	public Class<?> getModelClass() {
		return Users.class;
	}

	@Override
	public String getModelClassName() {
		return Users.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("UserID", getUserID());
		attributes.put("UserName", getUserName());
		attributes.put("Email", getEmail());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long UserID = (Long)attributes.get("UserID");

		if (UserID != null) {
			setUserID(UserID);
		}

		String UserName = (String)attributes.get("UserName");

		if (UserName != null) {
			setUserName(UserName);
		}

		String Email = (String)attributes.get("Email");

		if (Email != null) {
			setEmail(Email);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _users.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _users.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _users.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _users.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<surveyAPI.model.Users> toCacheModel() {
		return _users.toCacheModel();
	}

	@Override
	public int compareTo(surveyAPI.model.Users users) {
		return _users.compareTo(users);
	}

	@Override
	public int hashCode() {
		return _users.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _users.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new UsersWrapper((Users)_users.clone());
	}

	/**
	* Returns the email of this users.
	*
	* @return the email of this users
	*/
	@Override
	public java.lang.String getEmail() {
		return _users.getEmail();
	}

	/**
	* Returns the user name of this users.
	*
	* @return the user name of this users
	*/
	@Override
	public java.lang.String getUserName() {
		return _users.getUserName();
	}

	@Override
	public java.lang.String toString() {
		return _users.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _users.toXmlString();
	}

	/**
	* Returns the primary key of this users.
	*
	* @return the primary key of this users
	*/
	@Override
	public long getPrimaryKey() {
		return _users.getPrimaryKey();
	}

	/**
	* Returns the user ID of this users.
	*
	* @return the user ID of this users
	*/
	@Override
	public long getUserID() {
		return _users.getUserID();
	}

	@Override
	public surveyAPI.model.Users toEscapedModel() {
		return new UsersWrapper(_users.toEscapedModel());
	}

	@Override
	public surveyAPI.model.Users toUnescapedModel() {
		return new UsersWrapper(_users.toUnescapedModel());
	}

	@Override
	public void persist() {
		_users.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_users.setCachedModel(cachedModel);
	}

	/**
	* Sets the email of this users.
	*
	* @param Email the email of this users
	*/
	@Override
	public void setEmail(java.lang.String Email) {
		_users.setEmail(Email);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_users.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_users.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_users.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_users.setNew(n);
	}

	/**
	* Sets the primary key of this users.
	*
	* @param primaryKey the primary key of this users
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_users.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_users.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the user ID of this users.
	*
	* @param UserID the user ID of this users
	*/
	@Override
	public void setUserID(long UserID) {
		_users.setUserID(UserID);
	}

	/**
	* Sets the user name of this users.
	*
	* @param UserName the user name of this users
	*/
	@Override
	public void setUserName(java.lang.String UserName) {
		_users.setUserName(UserName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UsersWrapper)) {
			return false;
		}

		UsersWrapper usersWrapper = (UsersWrapper)obj;

		if (Objects.equals(_users, usersWrapper._users)) {
			return true;
		}

		return false;
	}

	@Override
	public Users getWrappedModel() {
		return _users;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _users.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _users.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_users.resetOriginalValues();
	}

	private final Users _users;
}