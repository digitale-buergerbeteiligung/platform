create index IX_4BDFBC47 on ANALYSIS_Analysis (algorithmApplied[$COLUMN_LENGTH:75$]);
create index IX_3EEB8755 on ANALYSIS_Analysis (input[$COLUMN_LENGTH:75$]);
create index IX_4B90BCB5 on ANALYSIS_Analysis (timeExecuted);

create index IX_A38C1C23 on ANALYSIS_TagData (Tag[$COLUMN_LENGTH:75$]);