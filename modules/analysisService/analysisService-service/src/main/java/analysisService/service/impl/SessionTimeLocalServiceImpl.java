/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import analysisService.model.ClickCount;
import analysisService.model.SessionTime;
import analysisService.model.impl.SessionTimeImpl;
import analysisService.service.ClickCountLocalServiceUtil;
import analysisService.service.SessionTimeLocalServiceUtil;
import analysisService.service.base.SessionTimeLocalServiceBaseImpl;
import analysisService.service.enums.ButtonPositions;
import analysisService.service.persistence.ClickCountPK;

/**
 * The implementation of the session time local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link analysisService.service.SessionTimeLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SessionTimeLocalServiceBaseImpl
 * @see analysisService.service.SessionTimeLocalServiceUtil
 */
public class SessionTimeLocalServiceImpl extends SessionTimeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link analysisService.service.SessionTimeLocalServiceUtil} to access the session time local service.
	 */
	
	public long addNewSession(Date time, String groupID){
		SessionTime newSessionTime = new SessionTimeImpl();
		newSessionTime.setStartTime(time);
		newSessionTime.setGroupID(groupID);
		
		SessionTimeLocalServiceUtil.addSessionTime(newSessionTime);
		
		return newSessionTime.getPrimaryKey();
	}
	
	/**
	 * groupID : String : ID of the portal. Used to track the session length.
	 * buttonID : String : Used to keep track of clicks on the button based on ID
	 */
	public void trackClick(String groupID, String buttonID){
		ClickCount trackClickObject;
		if(ClickCountLocalServiceUtil.fetchClickCount(new ClickCountPK(groupID, buttonID))==null){
			trackClickObject = ClickCountLocalServiceUtil.createClickCount(new ClickCountPK(groupID, buttonID));
			trackClickObject.setCount(1);
		}else{
			trackClickObject = ClickCountLocalServiceUtil.fetchClickCount(new ClickCountPK(groupID, buttonID));
			trackClickObject.setCount(trackClickObject.getCount()+1);
		}

		ClickCountLocalServiceUtil.updateClickCount(trackClickObject);
	}
	
	/**
	 * groupID : String : ID of the portal. Used to track the session length
	 * ButtonPositions : ENUM : FIRST - LAST - MIDDLE
	 * sessionID : If it's the first click, send default sessionID - 0
	 * returns the sessionID after logging the click
	 */
	public long trackClickSession(String groupID, ButtonPositions position, long sessionID){
		//Create date object with format : yyyy/MM/dd HH:mm:ss.S
		DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.S");
		DateFormat df = DateFormat.getDateInstance();
		Date timeNow = new Date();
		try{
			timeNow = df.parse(sdf.format(new Date()));
		}catch(Exception ex){
		}

		//create an object for session tracking
		SessionTime testSession ;
		
		//check for default value, meaning session has not been stored in service builder yet
		if("First".equals(position.getButtonPositionDescription())&&sessionID==0){
			return(SessionTimeLocalServiceUtil.addNewSession(timeNow, groupID));
		}
		if("Last".equals(position.getButtonPositionDescription())&&sessionID!=0)
		{
			testSession = SessionTimeLocalServiceUtil.fetchSessionTime(sessionID);
			if(testSession!=null){
				testSession.setEndTime(timeNow);
			}
			SessionTimeLocalServiceUtil.updateSessionTime(testSession);
		}
		return sessionID;
	}
}