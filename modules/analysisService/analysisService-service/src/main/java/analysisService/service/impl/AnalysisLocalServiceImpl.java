/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.tensorflow.Tensor;

import analysisService.data.machinelearning.ImageLabelerTensorFlow;
import analysisService.service.base.AnalysisLocalServiceBaseImpl;
/**
 * The implementation of the analysis local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link analysisService.service.AnalysisLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AnalysisLocalServiceBaseImpl
 * @see analysisService.service.AnalysisLocalServiceUtil
 */
public class AnalysisLocalServiceImpl extends AnalysisLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link analysisService.service.AnalysisLocalServiceUtil} to access the analysis local service.
	 */

	public int predictImage(File file, byte [] model){
		Path path = Paths.get(file.getAbsolutePath());
		try {
			byte[] data = Files.readAllBytes(path);
			ImageLabelerTensorFlow i = new ImageLabelerTensorFlow();
			Tensor<Float> ten = i.constructAndExecuteGraphToNormalizeImage(data);
			float[] result = i.executeInceptionGraph(model, ten);
			return i.maxIndex(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}

}