/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import analysisService.exception.NoSuchTagDataException;

import analysisService.model.TagData;

import analysisService.model.impl.TagDataImpl;
import analysisService.model.impl.TagDataModelImpl;

import analysisService.service.persistence.TagDataPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the tag data service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TagDataPersistence
 * @see analysisService.service.persistence.TagDataUtil
 * @generated
 */
@ProviderType
public class TagDataPersistenceImpl extends BasePersistenceImpl<TagData>
	implements TagDataPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link TagDataUtil} to access the tag data persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = TagDataImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TagDataModelImpl.ENTITY_CACHE_ENABLED,
			TagDataModelImpl.FINDER_CACHE_ENABLED, TagDataImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TagDataModelImpl.ENTITY_CACHE_ENABLED,
			TagDataModelImpl.FINDER_CACHE_ENABLED, TagDataImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TagDataModelImpl.ENTITY_CACHE_ENABLED,
			TagDataModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_TAGNAME = new FinderPath(TagDataModelImpl.ENTITY_CACHE_ENABLED,
			TagDataModelImpl.FINDER_CACHE_ENABLED, TagDataImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByTagName",
			new String[] { String.class.getName() },
			TagDataModelImpl.TAG_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TAGNAME = new FinderPath(TagDataModelImpl.ENTITY_CACHE_ENABLED,
			TagDataModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTagName",
			new String[] { String.class.getName() });

	/**
	 * Returns the tag data where Tag = &#63; or throws a {@link NoSuchTagDataException} if it could not be found.
	 *
	 * @param Tag the tag
	 * @return the matching tag data
	 * @throws NoSuchTagDataException if a matching tag data could not be found
	 */
	@Override
	public TagData findByTagName(String Tag) throws NoSuchTagDataException {
		TagData tagData = fetchByTagName(Tag);

		if (tagData == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("Tag=");
			msg.append(Tag);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchTagDataException(msg.toString());
		}

		return tagData;
	}

	/**
	 * Returns the tag data where Tag = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param Tag the tag
	 * @return the matching tag data, or <code>null</code> if a matching tag data could not be found
	 */
	@Override
	public TagData fetchByTagName(String Tag) {
		return fetchByTagName(Tag, true);
	}

	/**
	 * Returns the tag data where Tag = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param Tag the tag
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching tag data, or <code>null</code> if a matching tag data could not be found
	 */
	@Override
	public TagData fetchByTagName(String Tag, boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { Tag };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_TAGNAME,
					finderArgs, this);
		}

		if (result instanceof TagData) {
			TagData tagData = (TagData)result;

			if (!Objects.equals(Tag, tagData.getTag())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_TAGDATA_WHERE);

			boolean bindTag = false;

			if (Tag == null) {
				query.append(_FINDER_COLUMN_TAGNAME_TAG_1);
			}
			else if (Tag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TAGNAME_TAG_3);
			}
			else {
				bindTag = true;

				query.append(_FINDER_COLUMN_TAGNAME_TAG_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTag) {
					qPos.add(Tag);
				}

				List<TagData> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_TAGNAME,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"TagDataPersistenceImpl.fetchByTagName(String, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					TagData tagData = list.get(0);

					result = tagData;

					cacheResult(tagData);

					if ((tagData.getTag() == null) ||
							!tagData.getTag().equals(Tag)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_TAGNAME,
							finderArgs, tagData);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_TAGNAME,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (TagData)result;
		}
	}

	/**
	 * Removes the tag data where Tag = &#63; from the database.
	 *
	 * @param Tag the tag
	 * @return the tag data that was removed
	 */
	@Override
	public TagData removeByTagName(String Tag) throws NoSuchTagDataException {
		TagData tagData = findByTagName(Tag);

		return remove(tagData);
	}

	/**
	 * Returns the number of tag datas where Tag = &#63;.
	 *
	 * @param Tag the tag
	 * @return the number of matching tag datas
	 */
	@Override
	public int countByTagName(String Tag) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_TAGNAME;

		Object[] finderArgs = new Object[] { Tag };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TAGDATA_WHERE);

			boolean bindTag = false;

			if (Tag == null) {
				query.append(_FINDER_COLUMN_TAGNAME_TAG_1);
			}
			else if (Tag.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TAGNAME_TAG_3);
			}
			else {
				bindTag = true;

				query.append(_FINDER_COLUMN_TAGNAME_TAG_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindTag) {
					qPos.add(Tag);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_TAGNAME_TAG_1 = "tagData.Tag IS NULL";
	private static final String _FINDER_COLUMN_TAGNAME_TAG_2 = "tagData.Tag = ?";
	private static final String _FINDER_COLUMN_TAGNAME_TAG_3 = "(tagData.Tag IS NULL OR tagData.Tag = '')";

	public TagDataPersistenceImpl() {
		setModelClass(TagData.class);
	}

	/**
	 * Caches the tag data in the entity cache if it is enabled.
	 *
	 * @param tagData the tag data
	 */
	@Override
	public void cacheResult(TagData tagData) {
		entityCache.putResult(TagDataModelImpl.ENTITY_CACHE_ENABLED,
			TagDataImpl.class, tagData.getPrimaryKey(), tagData);

		finderCache.putResult(FINDER_PATH_FETCH_BY_TAGNAME,
			new Object[] { tagData.getTag() }, tagData);

		tagData.resetOriginalValues();
	}

	/**
	 * Caches the tag datas in the entity cache if it is enabled.
	 *
	 * @param tagDatas the tag datas
	 */
	@Override
	public void cacheResult(List<TagData> tagDatas) {
		for (TagData tagData : tagDatas) {
			if (entityCache.getResult(TagDataModelImpl.ENTITY_CACHE_ENABLED,
						TagDataImpl.class, tagData.getPrimaryKey()) == null) {
				cacheResult(tagData);
			}
			else {
				tagData.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all tag datas.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(TagDataImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the tag data.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(TagData tagData) {
		entityCache.removeResult(TagDataModelImpl.ENTITY_CACHE_ENABLED,
			TagDataImpl.class, tagData.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((TagDataModelImpl)tagData, true);
	}

	@Override
	public void clearCache(List<TagData> tagDatas) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (TagData tagData : tagDatas) {
			entityCache.removeResult(TagDataModelImpl.ENTITY_CACHE_ENABLED,
				TagDataImpl.class, tagData.getPrimaryKey());

			clearUniqueFindersCache((TagDataModelImpl)tagData, true);
		}
	}

	protected void cacheUniqueFindersCache(TagDataModelImpl tagDataModelImpl) {
		Object[] args = new Object[] { tagDataModelImpl.getTag() };

		finderCache.putResult(FINDER_PATH_COUNT_BY_TAGNAME, args,
			Long.valueOf(1), false);
		finderCache.putResult(FINDER_PATH_FETCH_BY_TAGNAME, args,
			tagDataModelImpl, false);
	}

	protected void clearUniqueFindersCache(TagDataModelImpl tagDataModelImpl,
		boolean clearCurrent) {
		if (clearCurrent) {
			Object[] args = new Object[] { tagDataModelImpl.getTag() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_TAGNAME, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_TAGNAME, args);
		}

		if ((tagDataModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_TAGNAME.getColumnBitmask()) != 0) {
			Object[] args = new Object[] { tagDataModelImpl.getOriginalTag() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_TAGNAME, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_TAGNAME, args);
		}
	}

	/**
	 * Creates a new tag data with the primary key. Does not add the tag data to the database.
	 *
	 * @param TagID the primary key for the new tag data
	 * @return the new tag data
	 */
	@Override
	public TagData create(long TagID) {
		TagData tagData = new TagDataImpl();

		tagData.setNew(true);
		tagData.setPrimaryKey(TagID);

		return tagData;
	}

	/**
	 * Removes the tag data with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param TagID the primary key of the tag data
	 * @return the tag data that was removed
	 * @throws NoSuchTagDataException if a tag data with the primary key could not be found
	 */
	@Override
	public TagData remove(long TagID) throws NoSuchTagDataException {
		return remove((Serializable)TagID);
	}

	/**
	 * Removes the tag data with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the tag data
	 * @return the tag data that was removed
	 * @throws NoSuchTagDataException if a tag data with the primary key could not be found
	 */
	@Override
	public TagData remove(Serializable primaryKey)
		throws NoSuchTagDataException {
		Session session = null;

		try {
			session = openSession();

			TagData tagData = (TagData)session.get(TagDataImpl.class, primaryKey);

			if (tagData == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTagDataException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(tagData);
		}
		catch (NoSuchTagDataException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected TagData removeImpl(TagData tagData) {
		tagData = toUnwrappedModel(tagData);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(tagData)) {
				tagData = (TagData)session.get(TagDataImpl.class,
						tagData.getPrimaryKeyObj());
			}

			if (tagData != null) {
				session.delete(tagData);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (tagData != null) {
			clearCache(tagData);
		}

		return tagData;
	}

	@Override
	public TagData updateImpl(TagData tagData) {
		tagData = toUnwrappedModel(tagData);

		boolean isNew = tagData.isNew();

		TagDataModelImpl tagDataModelImpl = (TagDataModelImpl)tagData;

		Session session = null;

		try {
			session = openSession();

			if (tagData.isNew()) {
				session.save(tagData);

				tagData.setNew(false);
			}
			else {
				tagData = (TagData)session.merge(tagData);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!TagDataModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(TagDataModelImpl.ENTITY_CACHE_ENABLED,
			TagDataImpl.class, tagData.getPrimaryKey(), tagData, false);

		clearUniqueFindersCache(tagDataModelImpl, false);
		cacheUniqueFindersCache(tagDataModelImpl);

		tagData.resetOriginalValues();

		return tagData;
	}

	protected TagData toUnwrappedModel(TagData tagData) {
		if (tagData instanceof TagDataImpl) {
			return tagData;
		}

		TagDataImpl tagDataImpl = new TagDataImpl();

		tagDataImpl.setNew(tagData.isNew());
		tagDataImpl.setPrimaryKey(tagData.getPrimaryKey());

		tagDataImpl.setTagID(tagData.getTagID());
		tagDataImpl.setTag(tagData.getTag());

		return tagDataImpl;
	}

	/**
	 * Returns the tag data with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the tag data
	 * @return the tag data
	 * @throws NoSuchTagDataException if a tag data with the primary key could not be found
	 */
	@Override
	public TagData findByPrimaryKey(Serializable primaryKey)
		throws NoSuchTagDataException {
		TagData tagData = fetchByPrimaryKey(primaryKey);

		if (tagData == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchTagDataException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return tagData;
	}

	/**
	 * Returns the tag data with the primary key or throws a {@link NoSuchTagDataException} if it could not be found.
	 *
	 * @param TagID the primary key of the tag data
	 * @return the tag data
	 * @throws NoSuchTagDataException if a tag data with the primary key could not be found
	 */
	@Override
	public TagData findByPrimaryKey(long TagID) throws NoSuchTagDataException {
		return findByPrimaryKey((Serializable)TagID);
	}

	/**
	 * Returns the tag data with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the tag data
	 * @return the tag data, or <code>null</code> if a tag data with the primary key could not be found
	 */
	@Override
	public TagData fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(TagDataModelImpl.ENTITY_CACHE_ENABLED,
				TagDataImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		TagData tagData = (TagData)serializable;

		if (tagData == null) {
			Session session = null;

			try {
				session = openSession();

				tagData = (TagData)session.get(TagDataImpl.class, primaryKey);

				if (tagData != null) {
					cacheResult(tagData);
				}
				else {
					entityCache.putResult(TagDataModelImpl.ENTITY_CACHE_ENABLED,
						TagDataImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(TagDataModelImpl.ENTITY_CACHE_ENABLED,
					TagDataImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return tagData;
	}

	/**
	 * Returns the tag data with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param TagID the primary key of the tag data
	 * @return the tag data, or <code>null</code> if a tag data with the primary key could not be found
	 */
	@Override
	public TagData fetchByPrimaryKey(long TagID) {
		return fetchByPrimaryKey((Serializable)TagID);
	}

	@Override
	public Map<Serializable, TagData> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, TagData> map = new HashMap<Serializable, TagData>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			TagData tagData = fetchByPrimaryKey(primaryKey);

			if (tagData != null) {
				map.put(primaryKey, tagData);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(TagDataModelImpl.ENTITY_CACHE_ENABLED,
					TagDataImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (TagData)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_TAGDATA_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (TagData tagData : (List<TagData>)q.list()) {
				map.put(tagData.getPrimaryKeyObj(), tagData);

				cacheResult(tagData);

				uncachedPrimaryKeys.remove(tagData.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(TagDataModelImpl.ENTITY_CACHE_ENABLED,
					TagDataImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the tag datas.
	 *
	 * @return the tag datas
	 */
	@Override
	public List<TagData> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the tag datas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TagDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tag datas
	 * @param end the upper bound of the range of tag datas (not inclusive)
	 * @return the range of tag datas
	 */
	@Override
	public List<TagData> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the tag datas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TagDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tag datas
	 * @param end the upper bound of the range of tag datas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of tag datas
	 */
	@Override
	public List<TagData> findAll(int start, int end,
		OrderByComparator<TagData> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the tag datas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TagDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of tag datas
	 * @param end the upper bound of the range of tag datas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of tag datas
	 */
	@Override
	public List<TagData> findAll(int start, int end,
		OrderByComparator<TagData> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<TagData> list = null;

		if (retrieveFromCache) {
			list = (List<TagData>)finderCache.getResult(finderPath, finderArgs,
					this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_TAGDATA);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TAGDATA;

				if (pagination) {
					sql = sql.concat(TagDataModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<TagData>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<TagData>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the tag datas from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (TagData tagData : findAll()) {
			remove(tagData);
		}
	}

	/**
	 * Returns the number of tag datas.
	 *
	 * @return the number of tag datas
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TAGDATA);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return TagDataModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the tag data persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(TagDataImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_TAGDATA = "SELECT tagData FROM TagData tagData";
	private static final String _SQL_SELECT_TAGDATA_WHERE_PKS_IN = "SELECT tagData FROM TagData tagData WHERE TagID IN (";
	private static final String _SQL_SELECT_TAGDATA_WHERE = "SELECT tagData FROM TagData tagData WHERE ";
	private static final String _SQL_COUNT_TAGDATA = "SELECT COUNT(tagData) FROM TagData tagData";
	private static final String _SQL_COUNT_TAGDATA_WHERE = "SELECT COUNT(tagData) FROM TagData tagData WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "tagData.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No TagData exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No TagData exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(TagDataPersistenceImpl.class);
}