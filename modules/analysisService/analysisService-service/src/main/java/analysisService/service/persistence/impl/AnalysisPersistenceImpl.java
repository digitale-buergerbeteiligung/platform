/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import analysisService.exception.NoSuchAnalysisException;

import analysisService.model.Analysis;

import analysisService.model.impl.AnalysisImpl;
import analysisService.model.impl.AnalysisModelImpl;

import analysisService.service.persistence.AnalysisPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the analysis service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AnalysisPersistence
 * @see analysisService.service.persistence.AnalysisUtil
 * @generated
 */
@ProviderType
public class AnalysisPersistenceImpl extends BasePersistenceImpl<Analysis>
	implements AnalysisPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AnalysisUtil} to access the analysis persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AnalysisImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
			AnalysisModelImpl.FINDER_CACHE_ENABLED, AnalysisImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
			AnalysisModelImpl.FINDER_CACHE_ENABLED, AnalysisImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
			AnalysisModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_INPUT = new FinderPath(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
			AnalysisModelImpl.FINDER_CACHE_ENABLED, AnalysisImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByinput",
			new String[] { String.class.getName() },
			AnalysisModelImpl.INPUT_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_INPUT = new FinderPath(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
			AnalysisModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByinput",
			new String[] { String.class.getName() });

	/**
	 * Returns the analysis where input = &#63; or throws a {@link NoSuchAnalysisException} if it could not be found.
	 *
	 * @param input the input
	 * @return the matching analysis
	 * @throws NoSuchAnalysisException if a matching analysis could not be found
	 */
	@Override
	public Analysis findByinput(String input) throws NoSuchAnalysisException {
		Analysis analysis = fetchByinput(input);

		if (analysis == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("input=");
			msg.append(input);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchAnalysisException(msg.toString());
		}

		return analysis;
	}

	/**
	 * Returns the analysis where input = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param input the input
	 * @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	 */
	@Override
	public Analysis fetchByinput(String input) {
		return fetchByinput(input, true);
	}

	/**
	 * Returns the analysis where input = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param input the input
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	 */
	@Override
	public Analysis fetchByinput(String input, boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { input };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_INPUT,
					finderArgs, this);
		}

		if (result instanceof Analysis) {
			Analysis analysis = (Analysis)result;

			if (!Objects.equals(input, analysis.getInput())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_ANALYSIS_WHERE);

			boolean bindInput = false;

			if (input == null) {
				query.append(_FINDER_COLUMN_INPUT_INPUT_1);
			}
			else if (input.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_INPUT_INPUT_3);
			}
			else {
				bindInput = true;

				query.append(_FINDER_COLUMN_INPUT_INPUT_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindInput) {
					qPos.add(input);
				}

				List<Analysis> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_INPUT,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"AnalysisPersistenceImpl.fetchByinput(String, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					Analysis analysis = list.get(0);

					result = analysis;

					cacheResult(analysis);

					if ((analysis.getInput() == null) ||
							!analysis.getInput().equals(input)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_INPUT,
							finderArgs, analysis);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_INPUT, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Analysis)result;
		}
	}

	/**
	 * Removes the analysis where input = &#63; from the database.
	 *
	 * @param input the input
	 * @return the analysis that was removed
	 */
	@Override
	public Analysis removeByinput(String input) throws NoSuchAnalysisException {
		Analysis analysis = findByinput(input);

		return remove(analysis);
	}

	/**
	 * Returns the number of analysises where input = &#63;.
	 *
	 * @param input the input
	 * @return the number of matching analysises
	 */
	@Override
	public int countByinput(String input) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_INPUT;

		Object[] finderArgs = new Object[] { input };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ANALYSIS_WHERE);

			boolean bindInput = false;

			if (input == null) {
				query.append(_FINDER_COLUMN_INPUT_INPUT_1);
			}
			else if (input.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_INPUT_INPUT_3);
			}
			else {
				bindInput = true;

				query.append(_FINDER_COLUMN_INPUT_INPUT_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindInput) {
					qPos.add(input);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_INPUT_INPUT_1 = "analysis.input IS NULL";
	private static final String _FINDER_COLUMN_INPUT_INPUT_2 = "analysis.input = ?";
	private static final String _FINDER_COLUMN_INPUT_INPUT_3 = "(analysis.input IS NULL OR analysis.input = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_ALGORITHMAPPLIED = new FinderPath(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
			AnalysisModelImpl.FINDER_CACHE_ENABLED, AnalysisImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByalgorithmApplied",
			new String[] { String.class.getName() },
			AnalysisModelImpl.ALGORITHMAPPLIED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ALGORITHMAPPLIED = new FinderPath(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
			AnalysisModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByalgorithmApplied", new String[] { String.class.getName() });

	/**
	 * Returns the analysis where algorithmApplied = &#63; or throws a {@link NoSuchAnalysisException} if it could not be found.
	 *
	 * @param algorithmApplied the algorithm applied
	 * @return the matching analysis
	 * @throws NoSuchAnalysisException if a matching analysis could not be found
	 */
	@Override
	public Analysis findByalgorithmApplied(String algorithmApplied)
		throws NoSuchAnalysisException {
		Analysis analysis = fetchByalgorithmApplied(algorithmApplied);

		if (analysis == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("algorithmApplied=");
			msg.append(algorithmApplied);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchAnalysisException(msg.toString());
		}

		return analysis;
	}

	/**
	 * Returns the analysis where algorithmApplied = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param algorithmApplied the algorithm applied
	 * @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	 */
	@Override
	public Analysis fetchByalgorithmApplied(String algorithmApplied) {
		return fetchByalgorithmApplied(algorithmApplied, true);
	}

	/**
	 * Returns the analysis where algorithmApplied = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param algorithmApplied the algorithm applied
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	 */
	@Override
	public Analysis fetchByalgorithmApplied(String algorithmApplied,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { algorithmApplied };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_ALGORITHMAPPLIED,
					finderArgs, this);
		}

		if (result instanceof Analysis) {
			Analysis analysis = (Analysis)result;

			if (!Objects.equals(algorithmApplied, analysis.getAlgorithmApplied())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_ANALYSIS_WHERE);

			boolean bindAlgorithmApplied = false;

			if (algorithmApplied == null) {
				query.append(_FINDER_COLUMN_ALGORITHMAPPLIED_ALGORITHMAPPLIED_1);
			}
			else if (algorithmApplied.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ALGORITHMAPPLIED_ALGORITHMAPPLIED_3);
			}
			else {
				bindAlgorithmApplied = true;

				query.append(_FINDER_COLUMN_ALGORITHMAPPLIED_ALGORITHMAPPLIED_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindAlgorithmApplied) {
					qPos.add(algorithmApplied);
				}

				List<Analysis> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_ALGORITHMAPPLIED,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"AnalysisPersistenceImpl.fetchByalgorithmApplied(String, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					Analysis analysis = list.get(0);

					result = analysis;

					cacheResult(analysis);

					if ((analysis.getAlgorithmApplied() == null) ||
							!analysis.getAlgorithmApplied()
										 .equals(algorithmApplied)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_ALGORITHMAPPLIED,
							finderArgs, analysis);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_ALGORITHMAPPLIED,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Analysis)result;
		}
	}

	/**
	 * Removes the analysis where algorithmApplied = &#63; from the database.
	 *
	 * @param algorithmApplied the algorithm applied
	 * @return the analysis that was removed
	 */
	@Override
	public Analysis removeByalgorithmApplied(String algorithmApplied)
		throws NoSuchAnalysisException {
		Analysis analysis = findByalgorithmApplied(algorithmApplied);

		return remove(analysis);
	}

	/**
	 * Returns the number of analysises where algorithmApplied = &#63;.
	 *
	 * @param algorithmApplied the algorithm applied
	 * @return the number of matching analysises
	 */
	@Override
	public int countByalgorithmApplied(String algorithmApplied) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ALGORITHMAPPLIED;

		Object[] finderArgs = new Object[] { algorithmApplied };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ANALYSIS_WHERE);

			boolean bindAlgorithmApplied = false;

			if (algorithmApplied == null) {
				query.append(_FINDER_COLUMN_ALGORITHMAPPLIED_ALGORITHMAPPLIED_1);
			}
			else if (algorithmApplied.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ALGORITHMAPPLIED_ALGORITHMAPPLIED_3);
			}
			else {
				bindAlgorithmApplied = true;

				query.append(_FINDER_COLUMN_ALGORITHMAPPLIED_ALGORITHMAPPLIED_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindAlgorithmApplied) {
					qPos.add(algorithmApplied);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ALGORITHMAPPLIED_ALGORITHMAPPLIED_1 =
		"analysis.algorithmApplied IS NULL";
	private static final String _FINDER_COLUMN_ALGORITHMAPPLIED_ALGORITHMAPPLIED_2 =
		"analysis.algorithmApplied = ?";
	private static final String _FINDER_COLUMN_ALGORITHMAPPLIED_ALGORITHMAPPLIED_3 =
		"(analysis.algorithmApplied IS NULL OR analysis.algorithmApplied = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_TIMEEXECUTED = new FinderPath(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
			AnalysisModelImpl.FINDER_CACHE_ENABLED, AnalysisImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchBytimeExecuted",
			new String[] { Long.class.getName() },
			AnalysisModelImpl.TIMEEXECUTED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TIMEEXECUTED = new FinderPath(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
			AnalysisModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBytimeExecuted",
			new String[] { Long.class.getName() });

	/**
	 * Returns the analysis where timeExecuted = &#63; or throws a {@link NoSuchAnalysisException} if it could not be found.
	 *
	 * @param timeExecuted the time executed
	 * @return the matching analysis
	 * @throws NoSuchAnalysisException if a matching analysis could not be found
	 */
	@Override
	public Analysis findBytimeExecuted(long timeExecuted)
		throws NoSuchAnalysisException {
		Analysis analysis = fetchBytimeExecuted(timeExecuted);

		if (analysis == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("timeExecuted=");
			msg.append(timeExecuted);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchAnalysisException(msg.toString());
		}

		return analysis;
	}

	/**
	 * Returns the analysis where timeExecuted = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param timeExecuted the time executed
	 * @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	 */
	@Override
	public Analysis fetchBytimeExecuted(long timeExecuted) {
		return fetchBytimeExecuted(timeExecuted, true);
	}

	/**
	 * Returns the analysis where timeExecuted = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param timeExecuted the time executed
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	 */
	@Override
	public Analysis fetchBytimeExecuted(long timeExecuted,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { timeExecuted };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_TIMEEXECUTED,
					finderArgs, this);
		}

		if (result instanceof Analysis) {
			Analysis analysis = (Analysis)result;

			if ((timeExecuted != analysis.getTimeExecuted())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_ANALYSIS_WHERE);

			query.append(_FINDER_COLUMN_TIMEEXECUTED_TIMEEXECUTED_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(timeExecuted);

				List<Analysis> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_TIMEEXECUTED,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"AnalysisPersistenceImpl.fetchBytimeExecuted(long, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					Analysis analysis = list.get(0);

					result = analysis;

					cacheResult(analysis);

					if ((analysis.getTimeExecuted() != timeExecuted)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_TIMEEXECUTED,
							finderArgs, analysis);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_TIMEEXECUTED,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Analysis)result;
		}
	}

	/**
	 * Removes the analysis where timeExecuted = &#63; from the database.
	 *
	 * @param timeExecuted the time executed
	 * @return the analysis that was removed
	 */
	@Override
	public Analysis removeBytimeExecuted(long timeExecuted)
		throws NoSuchAnalysisException {
		Analysis analysis = findBytimeExecuted(timeExecuted);

		return remove(analysis);
	}

	/**
	 * Returns the number of analysises where timeExecuted = &#63;.
	 *
	 * @param timeExecuted the time executed
	 * @return the number of matching analysises
	 */
	@Override
	public int countBytimeExecuted(long timeExecuted) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_TIMEEXECUTED;

		Object[] finderArgs = new Object[] { timeExecuted };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ANALYSIS_WHERE);

			query.append(_FINDER_COLUMN_TIMEEXECUTED_TIMEEXECUTED_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(timeExecuted);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_TIMEEXECUTED_TIMEEXECUTED_2 = "analysis.timeExecuted = ?";

	public AnalysisPersistenceImpl() {
		setModelClass(Analysis.class);
	}

	/**
	 * Caches the analysis in the entity cache if it is enabled.
	 *
	 * @param analysis the analysis
	 */
	@Override
	public void cacheResult(Analysis analysis) {
		entityCache.putResult(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
			AnalysisImpl.class, analysis.getPrimaryKey(), analysis);

		finderCache.putResult(FINDER_PATH_FETCH_BY_INPUT,
			new Object[] { analysis.getInput() }, analysis);

		finderCache.putResult(FINDER_PATH_FETCH_BY_ALGORITHMAPPLIED,
			new Object[] { analysis.getAlgorithmApplied() }, analysis);

		finderCache.putResult(FINDER_PATH_FETCH_BY_TIMEEXECUTED,
			new Object[] { analysis.getTimeExecuted() }, analysis);

		analysis.resetOriginalValues();
	}

	/**
	 * Caches the analysises in the entity cache if it is enabled.
	 *
	 * @param analysises the analysises
	 */
	@Override
	public void cacheResult(List<Analysis> analysises) {
		for (Analysis analysis : analysises) {
			if (entityCache.getResult(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
						AnalysisImpl.class, analysis.getPrimaryKey()) == null) {
				cacheResult(analysis);
			}
			else {
				analysis.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all analysises.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(AnalysisImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the analysis.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Analysis analysis) {
		entityCache.removeResult(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
			AnalysisImpl.class, analysis.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((AnalysisModelImpl)analysis, true);
	}

	@Override
	public void clearCache(List<Analysis> analysises) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Analysis analysis : analysises) {
			entityCache.removeResult(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
				AnalysisImpl.class, analysis.getPrimaryKey());

			clearUniqueFindersCache((AnalysisModelImpl)analysis, true);
		}
	}

	protected void cacheUniqueFindersCache(AnalysisModelImpl analysisModelImpl) {
		Object[] args = new Object[] { analysisModelImpl.getInput() };

		finderCache.putResult(FINDER_PATH_COUNT_BY_INPUT, args,
			Long.valueOf(1), false);
		finderCache.putResult(FINDER_PATH_FETCH_BY_INPUT, args,
			analysisModelImpl, false);

		args = new Object[] { analysisModelImpl.getAlgorithmApplied() };

		finderCache.putResult(FINDER_PATH_COUNT_BY_ALGORITHMAPPLIED, args,
			Long.valueOf(1), false);
		finderCache.putResult(FINDER_PATH_FETCH_BY_ALGORITHMAPPLIED, args,
			analysisModelImpl, false);

		args = new Object[] { analysisModelImpl.getTimeExecuted() };

		finderCache.putResult(FINDER_PATH_COUNT_BY_TIMEEXECUTED, args,
			Long.valueOf(1), false);
		finderCache.putResult(FINDER_PATH_FETCH_BY_TIMEEXECUTED, args,
			analysisModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		AnalysisModelImpl analysisModelImpl, boolean clearCurrent) {
		if (clearCurrent) {
			Object[] args = new Object[] { analysisModelImpl.getInput() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_INPUT, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_INPUT, args);
		}

		if ((analysisModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_INPUT.getColumnBitmask()) != 0) {
			Object[] args = new Object[] { analysisModelImpl.getOriginalInput() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_INPUT, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_INPUT, args);
		}

		if (clearCurrent) {
			Object[] args = new Object[] { analysisModelImpl.getAlgorithmApplied() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_ALGORITHMAPPLIED, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_ALGORITHMAPPLIED, args);
		}

		if ((analysisModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_ALGORITHMAPPLIED.getColumnBitmask()) != 0) {
			Object[] args = new Object[] {
					analysisModelImpl.getOriginalAlgorithmApplied()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_ALGORITHMAPPLIED, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_ALGORITHMAPPLIED, args);
		}

		if (clearCurrent) {
			Object[] args = new Object[] { analysisModelImpl.getTimeExecuted() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_TIMEEXECUTED, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_TIMEEXECUTED, args);
		}

		if ((analysisModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_TIMEEXECUTED.getColumnBitmask()) != 0) {
			Object[] args = new Object[] {
					analysisModelImpl.getOriginalTimeExecuted()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_TIMEEXECUTED, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_TIMEEXECUTED, args);
		}
	}

	/**
	 * Creates a new analysis with the primary key. Does not add the analysis to the database.
	 *
	 * @param dataId the primary key for the new analysis
	 * @return the new analysis
	 */
	@Override
	public Analysis create(long dataId) {
		Analysis analysis = new AnalysisImpl();

		analysis.setNew(true);
		analysis.setPrimaryKey(dataId);

		analysis.setCompanyId(companyProvider.getCompanyId());

		return analysis;
	}

	/**
	 * Removes the analysis with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param dataId the primary key of the analysis
	 * @return the analysis that was removed
	 * @throws NoSuchAnalysisException if a analysis with the primary key could not be found
	 */
	@Override
	public Analysis remove(long dataId) throws NoSuchAnalysisException {
		return remove((Serializable)dataId);
	}

	/**
	 * Removes the analysis with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the analysis
	 * @return the analysis that was removed
	 * @throws NoSuchAnalysisException if a analysis with the primary key could not be found
	 */
	@Override
	public Analysis remove(Serializable primaryKey)
		throws NoSuchAnalysisException {
		Session session = null;

		try {
			session = openSession();

			Analysis analysis = (Analysis)session.get(AnalysisImpl.class,
					primaryKey);

			if (analysis == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAnalysisException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(analysis);
		}
		catch (NoSuchAnalysisException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Analysis removeImpl(Analysis analysis) {
		analysis = toUnwrappedModel(analysis);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(analysis)) {
				analysis = (Analysis)session.get(AnalysisImpl.class,
						analysis.getPrimaryKeyObj());
			}

			if (analysis != null) {
				session.delete(analysis);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (analysis != null) {
			clearCache(analysis);
		}

		return analysis;
	}

	@Override
	public Analysis updateImpl(Analysis analysis) {
		analysis = toUnwrappedModel(analysis);

		boolean isNew = analysis.isNew();

		AnalysisModelImpl analysisModelImpl = (AnalysisModelImpl)analysis;

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (analysis.getCreateDate() == null)) {
			if (serviceContext == null) {
				analysis.setCreateDate(now);
			}
			else {
				analysis.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!analysisModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				analysis.setModifiedDate(now);
			}
			else {
				analysis.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (analysis.isNew()) {
				session.save(analysis);

				analysis.setNew(false);
			}
			else {
				analysis = (Analysis)session.merge(analysis);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!AnalysisModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
			AnalysisImpl.class, analysis.getPrimaryKey(), analysis, false);

		clearUniqueFindersCache(analysisModelImpl, false);
		cacheUniqueFindersCache(analysisModelImpl);

		analysis.resetOriginalValues();

		return analysis;
	}

	protected Analysis toUnwrappedModel(Analysis analysis) {
		if (analysis instanceof AnalysisImpl) {
			return analysis;
		}

		AnalysisImpl analysisImpl = new AnalysisImpl();

		analysisImpl.setNew(analysis.isNew());
		analysisImpl.setPrimaryKey(analysis.getPrimaryKey());

		analysisImpl.setDataId(analysis.getDataId());
		analysisImpl.setGroupId(analysis.getGroupId());
		analysisImpl.setCompanyId(analysis.getCompanyId());
		analysisImpl.setUserId(analysis.getUserId());
		analysisImpl.setUserName(analysis.getUserName());
		analysisImpl.setCreateDate(analysis.getCreateDate());
		analysisImpl.setModifiedDate(analysis.getModifiedDate());
		analysisImpl.setInput(analysis.getInput());
		analysisImpl.setResult(analysis.getResult());
		analysisImpl.setAlgorithmApplied(analysis.getAlgorithmApplied());
		analysisImpl.setTimeExecuted(analysis.getTimeExecuted());

		return analysisImpl;
	}

	/**
	 * Returns the analysis with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the analysis
	 * @return the analysis
	 * @throws NoSuchAnalysisException if a analysis with the primary key could not be found
	 */
	@Override
	public Analysis findByPrimaryKey(Serializable primaryKey)
		throws NoSuchAnalysisException {
		Analysis analysis = fetchByPrimaryKey(primaryKey);

		if (analysis == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchAnalysisException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return analysis;
	}

	/**
	 * Returns the analysis with the primary key or throws a {@link NoSuchAnalysisException} if it could not be found.
	 *
	 * @param dataId the primary key of the analysis
	 * @return the analysis
	 * @throws NoSuchAnalysisException if a analysis with the primary key could not be found
	 */
	@Override
	public Analysis findByPrimaryKey(long dataId)
		throws NoSuchAnalysisException {
		return findByPrimaryKey((Serializable)dataId);
	}

	/**
	 * Returns the analysis with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the analysis
	 * @return the analysis, or <code>null</code> if a analysis with the primary key could not be found
	 */
	@Override
	public Analysis fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
				AnalysisImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Analysis analysis = (Analysis)serializable;

		if (analysis == null) {
			Session session = null;

			try {
				session = openSession();

				analysis = (Analysis)session.get(AnalysisImpl.class, primaryKey);

				if (analysis != null) {
					cacheResult(analysis);
				}
				else {
					entityCache.putResult(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
						AnalysisImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
					AnalysisImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return analysis;
	}

	/**
	 * Returns the analysis with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param dataId the primary key of the analysis
	 * @return the analysis, or <code>null</code> if a analysis with the primary key could not be found
	 */
	@Override
	public Analysis fetchByPrimaryKey(long dataId) {
		return fetchByPrimaryKey((Serializable)dataId);
	}

	@Override
	public Map<Serializable, Analysis> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Analysis> map = new HashMap<Serializable, Analysis>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Analysis analysis = fetchByPrimaryKey(primaryKey);

			if (analysis != null) {
				map.put(primaryKey, analysis);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
					AnalysisImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Analysis)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_ANALYSIS_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Analysis analysis : (List<Analysis>)q.list()) {
				map.put(analysis.getPrimaryKeyObj(), analysis);

				cacheResult(analysis);

				uncachedPrimaryKeys.remove(analysis.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(AnalysisModelImpl.ENTITY_CACHE_ENABLED,
					AnalysisImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the analysises.
	 *
	 * @return the analysises
	 */
	@Override
	public List<Analysis> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the analysises.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AnalysisModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of analysises
	 * @param end the upper bound of the range of analysises (not inclusive)
	 * @return the range of analysises
	 */
	@Override
	public List<Analysis> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the analysises.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AnalysisModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of analysises
	 * @param end the upper bound of the range of analysises (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of analysises
	 */
	@Override
	public List<Analysis> findAll(int start, int end,
		OrderByComparator<Analysis> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the analysises.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AnalysisModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of analysises
	 * @param end the upper bound of the range of analysises (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of analysises
	 */
	@Override
	public List<Analysis> findAll(int start, int end,
		OrderByComparator<Analysis> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Analysis> list = null;

		if (retrieveFromCache) {
			list = (List<Analysis>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_ANALYSIS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ANALYSIS;

				if (pagination) {
					sql = sql.concat(AnalysisModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Analysis>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Analysis>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the analysises from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Analysis analysis : findAll()) {
			remove(analysis);
		}
	}

	/**
	 * Returns the number of analysises.
	 *
	 * @return the number of analysises
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ANALYSIS);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return AnalysisModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the analysis persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(AnalysisImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_ANALYSIS = "SELECT analysis FROM Analysis analysis";
	private static final String _SQL_SELECT_ANALYSIS_WHERE_PKS_IN = "SELECT analysis FROM Analysis analysis WHERE dataId IN (";
	private static final String _SQL_SELECT_ANALYSIS_WHERE = "SELECT analysis FROM Analysis analysis WHERE ";
	private static final String _SQL_COUNT_ANALYSIS = "SELECT COUNT(analysis) FROM Analysis analysis";
	private static final String _SQL_COUNT_ANALYSIS_WHERE = "SELECT COUNT(analysis) FROM Analysis analysis WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "analysis.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Analysis exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Analysis exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(AnalysisPersistenceImpl.class);
}