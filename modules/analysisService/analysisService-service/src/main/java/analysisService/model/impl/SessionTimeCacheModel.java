/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.model.impl;

import aQute.bnd.annotation.ProviderType;

import analysisService.model.SessionTime;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing SessionTime in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see SessionTime
 * @generated
 */
@ProviderType
public class SessionTimeCacheModel implements CacheModel<SessionTime>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SessionTimeCacheModel)) {
			return false;
		}

		SessionTimeCacheModel sessionTimeCacheModel = (SessionTimeCacheModel)obj;

		if (SessionID == sessionTimeCacheModel.SessionID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, SessionID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{SessionID=");
		sb.append(SessionID);
		sb.append(", GroupID=");
		sb.append(GroupID);
		sb.append(", StartTime=");
		sb.append(StartTime);
		sb.append(", EndTime=");
		sb.append(EndTime);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public SessionTime toEntityModel() {
		SessionTimeImpl sessionTimeImpl = new SessionTimeImpl();

		sessionTimeImpl.setSessionID(SessionID);

		if (GroupID == null) {
			sessionTimeImpl.setGroupID(StringPool.BLANK);
		}
		else {
			sessionTimeImpl.setGroupID(GroupID);
		}

		if (StartTime == Long.MIN_VALUE) {
			sessionTimeImpl.setStartTime(null);
		}
		else {
			sessionTimeImpl.setStartTime(new Date(StartTime));
		}

		if (EndTime == Long.MIN_VALUE) {
			sessionTimeImpl.setEndTime(null);
		}
		else {
			sessionTimeImpl.setEndTime(new Date(EndTime));
		}

		sessionTimeImpl.resetOriginalValues();

		return sessionTimeImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		SessionID = objectInput.readLong();
		GroupID = objectInput.readUTF();
		StartTime = objectInput.readLong();
		EndTime = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(SessionID);

		if (GroupID == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(GroupID);
		}

		objectOutput.writeLong(StartTime);
		objectOutput.writeLong(EndTime);
	}

	public long SessionID;
	public String GroupID;
	public long StartTime;
	public long EndTime;
}