/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the SessionTime service. Represents a row in the &quot;ANALYSIS_SessionTime&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link analysisService.model.impl.SessionTimeModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link analysisService.model.impl.SessionTimeImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SessionTime
 * @see analysisService.model.impl.SessionTimeImpl
 * @see analysisService.model.impl.SessionTimeModelImpl
 * @generated
 */
@ProviderType
public interface SessionTimeModel extends BaseModel<SessionTime> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a session time model instance should use the {@link SessionTime} interface instead.
	 */

	/**
	 * Returns the primary key of this session time.
	 *
	 * @return the primary key of this session time
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this session time.
	 *
	 * @param primaryKey the primary key of this session time
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the session ID of this session time.
	 *
	 * @return the session ID of this session time
	 */
	public long getSessionID();

	/**
	 * Sets the session ID of this session time.
	 *
	 * @param SessionID the session ID of this session time
	 */
	public void setSessionID(long SessionID);

	/**
	 * Returns the group ID of this session time.
	 *
	 * @return the group ID of this session time
	 */
	@AutoEscape
	public String getGroupID();

	/**
	 * Sets the group ID of this session time.
	 *
	 * @param GroupID the group ID of this session time
	 */
	public void setGroupID(String GroupID);

	/**
	 * Returns the start time of this session time.
	 *
	 * @return the start time of this session time
	 */
	public Date getStartTime();

	/**
	 * Sets the start time of this session time.
	 *
	 * @param StartTime the start time of this session time
	 */
	public void setStartTime(Date StartTime);

	/**
	 * Returns the end time of this session time.
	 *
	 * @return the end time of this session time
	 */
	public Date getEndTime();

	/**
	 * Sets the end time of this session time.
	 *
	 * @param EndTime the end time of this session time
	 */
	public void setEndTime(Date EndTime);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(analysisService.model.SessionTime sessionTime);

	@Override
	public int hashCode();

	@Override
	public CacheModel<analysisService.model.SessionTime> toCacheModel();

	@Override
	public analysisService.model.SessionTime toEscapedModel();

	@Override
	public analysisService.model.SessionTime toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}