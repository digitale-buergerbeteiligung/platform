/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SessionTimeLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see SessionTimeLocalService
 * @generated
 */
@ProviderType
public class SessionTimeLocalServiceWrapper implements SessionTimeLocalService,
	ServiceWrapper<SessionTimeLocalService> {
	public SessionTimeLocalServiceWrapper(
		SessionTimeLocalService sessionTimeLocalService) {
		_sessionTimeLocalService = sessionTimeLocalService;
	}

	/**
	* Adds the session time to the database. Also notifies the appropriate model listeners.
	*
	* @param sessionTime the session time
	* @return the session time that was added
	*/
	@Override
	public analysisService.model.SessionTime addSessionTime(
		analysisService.model.SessionTime sessionTime) {
		return _sessionTimeLocalService.addSessionTime(sessionTime);
	}

	/**
	* Creates a new session time with the primary key. Does not add the session time to the database.
	*
	* @param SessionID the primary key for the new session time
	* @return the new session time
	*/
	@Override
	public analysisService.model.SessionTime createSessionTime(long SessionID) {
		return _sessionTimeLocalService.createSessionTime(SessionID);
	}

	/**
	* Deletes the session time from the database. Also notifies the appropriate model listeners.
	*
	* @param sessionTime the session time
	* @return the session time that was removed
	*/
	@Override
	public analysisService.model.SessionTime deleteSessionTime(
		analysisService.model.SessionTime sessionTime) {
		return _sessionTimeLocalService.deleteSessionTime(sessionTime);
	}

	/**
	* Deletes the session time with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param SessionID the primary key of the session time
	* @return the session time that was removed
	* @throws PortalException if a session time with the primary key could not be found
	*/
	@Override
	public analysisService.model.SessionTime deleteSessionTime(long SessionID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _sessionTimeLocalService.deleteSessionTime(SessionID);
	}

	@Override
	public analysisService.model.SessionTime fetchSessionTime(long SessionID) {
		return _sessionTimeLocalService.fetchSessionTime(SessionID);
	}

	/**
	* Returns the session time with the primary key.
	*
	* @param SessionID the primary key of the session time
	* @return the session time
	* @throws PortalException if a session time with the primary key could not be found
	*/
	@Override
	public analysisService.model.SessionTime getSessionTime(long SessionID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _sessionTimeLocalService.getSessionTime(SessionID);
	}

	/**
	* Updates the session time in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param sessionTime the session time
	* @return the session time that was updated
	*/
	@Override
	public analysisService.model.SessionTime updateSessionTime(
		analysisService.model.SessionTime sessionTime) {
		return _sessionTimeLocalService.updateSessionTime(sessionTime);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _sessionTimeLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _sessionTimeLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _sessionTimeLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _sessionTimeLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _sessionTimeLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of session times.
	*
	* @return the number of session times
	*/
	@Override
	public int getSessionTimesCount() {
		return _sessionTimeLocalService.getSessionTimesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _sessionTimeLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _sessionTimeLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link analysisService.model.impl.SessionTimeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _sessionTimeLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link analysisService.model.impl.SessionTimeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _sessionTimeLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the session times.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link analysisService.model.impl.SessionTimeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of session times
	* @param end the upper bound of the range of session times (not inclusive)
	* @return the range of session times
	*/
	@Override
	public java.util.List<analysisService.model.SessionTime> getSessionTimes(
		int start, int end) {
		return _sessionTimeLocalService.getSessionTimes(start, end);
	}

	@Override
	public long addNewSession(java.util.Date time, java.lang.String groupID) {
		return _sessionTimeLocalService.addNewSession(time, groupID);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _sessionTimeLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _sessionTimeLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	/**
	* groupID : String : ID of the portal. Used to track the session length
	* ButtonPositions : ENUM : FIRST - LAST - MIDDLE
	* sessionID : If it's the first click, send default sessionID - 0
	* returns the sessionID after logging the click
	*/
	@Override
	public long trackClickSession(java.lang.String groupID,
		analysisService.service.enums.ButtonPositions position, long sessionID) {
		return _sessionTimeLocalService.trackClickSession(groupID, position,
			sessionID);
	}

	/**
	* groupID : String : ID of the portal. Used to track the session length.
	* buttonID : String : Used to keep track of clicks on the button based on ID
	*/
	@Override
	public void trackClick(java.lang.String groupID, java.lang.String buttonID) {
		_sessionTimeLocalService.trackClick(groupID, buttonID);
	}

	@Override
	public SessionTimeLocalService getWrappedService() {
		return _sessionTimeLocalService;
	}

	@Override
	public void setWrappedService(
		SessionTimeLocalService sessionTimeLocalService) {
		_sessionTimeLocalService = sessionTimeLocalService;
	}

	private SessionTimeLocalService _sessionTimeLocalService;
}