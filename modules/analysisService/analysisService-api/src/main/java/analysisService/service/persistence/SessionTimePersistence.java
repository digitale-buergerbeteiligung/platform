/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import analysisService.exception.NoSuchSessionTimeException;

import analysisService.model.SessionTime;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the session time service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see analysisService.service.persistence.impl.SessionTimePersistenceImpl
 * @see SessionTimeUtil
 * @generated
 */
@ProviderType
public interface SessionTimePersistence extends BasePersistence<SessionTime> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SessionTimeUtil} to access the session time persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the session time in the entity cache if it is enabled.
	*
	* @param sessionTime the session time
	*/
	public void cacheResult(SessionTime sessionTime);

	/**
	* Caches the session times in the entity cache if it is enabled.
	*
	* @param sessionTimes the session times
	*/
	public void cacheResult(java.util.List<SessionTime> sessionTimes);

	/**
	* Creates a new session time with the primary key. Does not add the session time to the database.
	*
	* @param SessionID the primary key for the new session time
	* @return the new session time
	*/
	public SessionTime create(long SessionID);

	/**
	* Removes the session time with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param SessionID the primary key of the session time
	* @return the session time that was removed
	* @throws NoSuchSessionTimeException if a session time with the primary key could not be found
	*/
	public SessionTime remove(long SessionID) throws NoSuchSessionTimeException;

	public SessionTime updateImpl(SessionTime sessionTime);

	/**
	* Returns the session time with the primary key or throws a {@link NoSuchSessionTimeException} if it could not be found.
	*
	* @param SessionID the primary key of the session time
	* @return the session time
	* @throws NoSuchSessionTimeException if a session time with the primary key could not be found
	*/
	public SessionTime findByPrimaryKey(long SessionID)
		throws NoSuchSessionTimeException;

	/**
	* Returns the session time with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param SessionID the primary key of the session time
	* @return the session time, or <code>null</code> if a session time with the primary key could not be found
	*/
	public SessionTime fetchByPrimaryKey(long SessionID);

	@Override
	public java.util.Map<java.io.Serializable, SessionTime> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the session times.
	*
	* @return the session times
	*/
	public java.util.List<SessionTime> findAll();

	/**
	* Returns a range of all the session times.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SessionTimeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of session times
	* @param end the upper bound of the range of session times (not inclusive)
	* @return the range of session times
	*/
	public java.util.List<SessionTime> findAll(int start, int end);

	/**
	* Returns an ordered range of all the session times.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SessionTimeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of session times
	* @param end the upper bound of the range of session times (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of session times
	*/
	public java.util.List<SessionTime> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SessionTime> orderByComparator);

	/**
	* Returns an ordered range of all the session times.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SessionTimeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of session times
	* @param end the upper bound of the range of session times (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of session times
	*/
	public java.util.List<SessionTime> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SessionTime> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the session times from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of session times.
	*
	* @return the number of session times
	*/
	public int countAll();
}