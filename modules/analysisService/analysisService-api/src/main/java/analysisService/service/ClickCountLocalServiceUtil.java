/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for ClickCount. This utility wraps
 * {@link analysisService.service.impl.ClickCountLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ClickCountLocalService
 * @see analysisService.service.base.ClickCountLocalServiceBaseImpl
 * @see analysisService.service.impl.ClickCountLocalServiceImpl
 * @generated
 */
@ProviderType
public class ClickCountLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link analysisService.service.impl.ClickCountLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the click count to the database. Also notifies the appropriate model listeners.
	*
	* @param clickCount the click count
	* @return the click count that was added
	*/
	public static analysisService.model.ClickCount addClickCount(
		analysisService.model.ClickCount clickCount) {
		return getService().addClickCount(clickCount);
	}

	/**
	* Creates a new click count with the primary key. Does not add the click count to the database.
	*
	* @param clickCountPK the primary key for the new click count
	* @return the new click count
	*/
	public static analysisService.model.ClickCount createClickCount(
		analysisService.service.persistence.ClickCountPK clickCountPK) {
		return getService().createClickCount(clickCountPK);
	}

	/**
	* Deletes the click count from the database. Also notifies the appropriate model listeners.
	*
	* @param clickCount the click count
	* @return the click count that was removed
	*/
	public static analysisService.model.ClickCount deleteClickCount(
		analysisService.model.ClickCount clickCount) {
		return getService().deleteClickCount(clickCount);
	}

	/**
	* Deletes the click count with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param clickCountPK the primary key of the click count
	* @return the click count that was removed
	* @throws PortalException if a click count with the primary key could not be found
	*/
	public static analysisService.model.ClickCount deleteClickCount(
		analysisService.service.persistence.ClickCountPK clickCountPK)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteClickCount(clickCountPK);
	}

	public static analysisService.model.ClickCount fetchClickCount(
		analysisService.service.persistence.ClickCountPK clickCountPK) {
		return getService().fetchClickCount(clickCountPK);
	}

	/**
	* Returns the click count with the primary key.
	*
	* @param clickCountPK the primary key of the click count
	* @return the click count
	* @throws PortalException if a click count with the primary key could not be found
	*/
	public static analysisService.model.ClickCount getClickCount(
		analysisService.service.persistence.ClickCountPK clickCountPK)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getClickCount(clickCountPK);
	}

	/**
	* Updates the click count in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param clickCount the click count
	* @return the click count that was updated
	*/
	public static analysisService.model.ClickCount updateClickCount(
		analysisService.model.ClickCount clickCount) {
		return getService().updateClickCount(clickCount);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of click counts.
	*
	* @return the number of click counts
	*/
	public static int getClickCountsCount() {
		return getService().getClickCountsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link analysisService.model.impl.ClickCountModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link analysisService.model.impl.ClickCountModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns a range of all the click counts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link analysisService.model.impl.ClickCountModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of click counts
	* @param end the upper bound of the range of click counts (not inclusive)
	* @return the range of click counts
	*/
	public static java.util.List<analysisService.model.ClickCount> getClickCounts(
		int start, int end) {
		return getService().getClickCounts(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static ClickCountLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ClickCountLocalService, ClickCountLocalService> _serviceTracker =
		ServiceTrackerFactory.open(ClickCountLocalService.class);
}