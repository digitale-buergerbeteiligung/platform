/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package analysisService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import analysisService.exception.NoSuchAnalysisException;

import analysisService.model.Analysis;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the analysis service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see analysisService.service.persistence.impl.AnalysisPersistenceImpl
 * @see AnalysisUtil
 * @generated
 */
@ProviderType
public interface AnalysisPersistence extends BasePersistence<Analysis> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AnalysisUtil} to access the analysis persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the analysis where input = &#63; or throws a {@link NoSuchAnalysisException} if it could not be found.
	*
	* @param input the input
	* @return the matching analysis
	* @throws NoSuchAnalysisException if a matching analysis could not be found
	*/
	public Analysis findByinput(java.lang.String input)
		throws NoSuchAnalysisException;

	/**
	* Returns the analysis where input = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param input the input
	* @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	*/
	public Analysis fetchByinput(java.lang.String input);

	/**
	* Returns the analysis where input = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param input the input
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	*/
	public Analysis fetchByinput(java.lang.String input,
		boolean retrieveFromCache);

	/**
	* Removes the analysis where input = &#63; from the database.
	*
	* @param input the input
	* @return the analysis that was removed
	*/
	public Analysis removeByinput(java.lang.String input)
		throws NoSuchAnalysisException;

	/**
	* Returns the number of analysises where input = &#63;.
	*
	* @param input the input
	* @return the number of matching analysises
	*/
	public int countByinput(java.lang.String input);

	/**
	* Returns the analysis where algorithmApplied = &#63; or throws a {@link NoSuchAnalysisException} if it could not be found.
	*
	* @param algorithmApplied the algorithm applied
	* @return the matching analysis
	* @throws NoSuchAnalysisException if a matching analysis could not be found
	*/
	public Analysis findByalgorithmApplied(java.lang.String algorithmApplied)
		throws NoSuchAnalysisException;

	/**
	* Returns the analysis where algorithmApplied = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param algorithmApplied the algorithm applied
	* @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	*/
	public Analysis fetchByalgorithmApplied(java.lang.String algorithmApplied);

	/**
	* Returns the analysis where algorithmApplied = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param algorithmApplied the algorithm applied
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	*/
	public Analysis fetchByalgorithmApplied(java.lang.String algorithmApplied,
		boolean retrieveFromCache);

	/**
	* Removes the analysis where algorithmApplied = &#63; from the database.
	*
	* @param algorithmApplied the algorithm applied
	* @return the analysis that was removed
	*/
	public Analysis removeByalgorithmApplied(java.lang.String algorithmApplied)
		throws NoSuchAnalysisException;

	/**
	* Returns the number of analysises where algorithmApplied = &#63;.
	*
	* @param algorithmApplied the algorithm applied
	* @return the number of matching analysises
	*/
	public int countByalgorithmApplied(java.lang.String algorithmApplied);

	/**
	* Returns the analysis where timeExecuted = &#63; or throws a {@link NoSuchAnalysisException} if it could not be found.
	*
	* @param timeExecuted the time executed
	* @return the matching analysis
	* @throws NoSuchAnalysisException if a matching analysis could not be found
	*/
	public Analysis findBytimeExecuted(long timeExecuted)
		throws NoSuchAnalysisException;

	/**
	* Returns the analysis where timeExecuted = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param timeExecuted the time executed
	* @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	*/
	public Analysis fetchBytimeExecuted(long timeExecuted);

	/**
	* Returns the analysis where timeExecuted = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param timeExecuted the time executed
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching analysis, or <code>null</code> if a matching analysis could not be found
	*/
	public Analysis fetchBytimeExecuted(long timeExecuted,
		boolean retrieveFromCache);

	/**
	* Removes the analysis where timeExecuted = &#63; from the database.
	*
	* @param timeExecuted the time executed
	* @return the analysis that was removed
	*/
	public Analysis removeBytimeExecuted(long timeExecuted)
		throws NoSuchAnalysisException;

	/**
	* Returns the number of analysises where timeExecuted = &#63;.
	*
	* @param timeExecuted the time executed
	* @return the number of matching analysises
	*/
	public int countBytimeExecuted(long timeExecuted);

	/**
	* Caches the analysis in the entity cache if it is enabled.
	*
	* @param analysis the analysis
	*/
	public void cacheResult(Analysis analysis);

	/**
	* Caches the analysises in the entity cache if it is enabled.
	*
	* @param analysises the analysises
	*/
	public void cacheResult(java.util.List<Analysis> analysises);

	/**
	* Creates a new analysis with the primary key. Does not add the analysis to the database.
	*
	* @param dataId the primary key for the new analysis
	* @return the new analysis
	*/
	public Analysis create(long dataId);

	/**
	* Removes the analysis with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param dataId the primary key of the analysis
	* @return the analysis that was removed
	* @throws NoSuchAnalysisException if a analysis with the primary key could not be found
	*/
	public Analysis remove(long dataId) throws NoSuchAnalysisException;

	public Analysis updateImpl(Analysis analysis);

	/**
	* Returns the analysis with the primary key or throws a {@link NoSuchAnalysisException} if it could not be found.
	*
	* @param dataId the primary key of the analysis
	* @return the analysis
	* @throws NoSuchAnalysisException if a analysis with the primary key could not be found
	*/
	public Analysis findByPrimaryKey(long dataId)
		throws NoSuchAnalysisException;

	/**
	* Returns the analysis with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param dataId the primary key of the analysis
	* @return the analysis, or <code>null</code> if a analysis with the primary key could not be found
	*/
	public Analysis fetchByPrimaryKey(long dataId);

	@Override
	public java.util.Map<java.io.Serializable, Analysis> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the analysises.
	*
	* @return the analysises
	*/
	public java.util.List<Analysis> findAll();

	/**
	* Returns a range of all the analysises.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AnalysisModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of analysises
	* @param end the upper bound of the range of analysises (not inclusive)
	* @return the range of analysises
	*/
	public java.util.List<Analysis> findAll(int start, int end);

	/**
	* Returns an ordered range of all the analysises.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AnalysisModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of analysises
	* @param end the upper bound of the range of analysises (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of analysises
	*/
	public java.util.List<Analysis> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Analysis> orderByComparator);

	/**
	* Returns an ordered range of all the analysises.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AnalysisModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of analysises
	* @param end the upper bound of the range of analysises (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of analysises
	*/
	public java.util.List<Analysis> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Analysis> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the analysises from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of analysises.
	*
	* @return the number of analysises
	*/
	public int countAll();
}