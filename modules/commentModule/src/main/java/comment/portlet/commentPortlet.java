package comment.portlet;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

import comment.constants.commentPortletKeys;
import ideaService.model.Ideas;
import ideaService.service.IdeasLocalServiceUtil;

/**
 * @author singh
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.EEN",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Kommentare",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + commentPortletKeys.comment,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user"
	},
	service = Portlet.class
)
public class commentPortlet extends MVCPortlet {
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try{
			ThemeDisplay themeDisplay = ((ThemeDisplay)(renderRequest.getAttribute(WebKeys.THEME_DISPLAY)));

			long layoutRef = (themeDisplay.getLayout().getPrimaryKey());

			Ideas idea = IdeasLocalServiceUtil.getIdeasByLayoutIdRef(layoutRef);

			//This is the code we need for fetching all the messages - if ever we need to put it in the UI
			/*List<MBMessage> msgs = MBMessageLocalServiceUtil.getMessages(Ideas.class.getName(), idea.getIdeasId(), 0);
			for(int i = 0; i < msgs.size(); i++){
				if(msgs.get(i).getParentMessageId()!=0){
					System.out.println(msgs.get(i).getSubject());
				}
			}*/

			renderRequest.setAttribute("classPK", idea.getPrimaryKey());
			renderRequest.setAttribute("userId", themeDisplay.getUserId());
			renderRequest.setAttribute("currentURL", themeDisplay.getURLCurrent());
			renderRequest.setAttribute("className", Ideas.class.getName());

		}catch(Exception ex){
			ex.printStackTrace();
		}
		super.doView(renderRequest, renderResponse);
	}

	@SuppressWarnings("deprecation")
	public void commentTestSubmit(ActionRequest request, ActionResponse response)throws PortletException, SystemException {
		try{
			invokeTaglibDiscussion(request, response);
		}catch (Exception e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}